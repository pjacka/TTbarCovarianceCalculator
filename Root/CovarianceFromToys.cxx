#include "TTbarCovarianceCalculator/CovarianceFromToys.h"
ClassImp(CovarianceFromToys)

CovarianceFromToys::CovarianceFromToys(const int nrows):m_nrows(nrows){
  m_counter=0;
  m_cov= std::make_shared<TMatrixT<double> >(nrows,nrows);
  m_mean.resize(nrows,0);
}
void CovarianceFromToys::addToy(TH1* h){
  m_counter++;
  double content;
  for(int i=0;i<m_nrows;i++){
    content=h->GetBinContent(i+1);
    m_mean[i]+=content;
    for(int j=0;j<m_nrows;j++)(*m_cov)[i][j]+=(content*h->GetBinContent(j+1));
  }
}
void CovarianceFromToys::finalize(){
  for(int i=0;i<m_nrows;i++)m_mean[i]/=m_counter;
  for(int i=0;i<m_nrows;i++)for(int j=0;j<m_nrows;j++)(*m_cov)[i][j] = (*m_cov)[i][j]/m_counter - m_mean[i]*m_mean[j] ;
}

void CovarianceFromToys::printMeanAndRelUnc() const {
  for(int i=0;i<m_nrows;i++) {
    std::cout << "bin " << i+1 << " : " << m_mean[i] << "*(1 +/- " << sqrt((*m_cov)[i][i])/m_mean[i] << ")" << std::endl;
  }
}
