#include "TTbarCovarianceCalculator/Pseudoexperiment.h"
#include "TTbarHelperFunctions/functions.h"
#include "TTbarHelperFunctions/histManipFunctions.h"
ClassImp(Pseudoexperiment)

Pseudoexperiment::Pseudoexperiment(): m_gap("                  "){
}

Pseudoexperiment::Pseudoexperiment(PseudoexperimentsCalculator* psedudoexperimentCalculator): m_gap("                  "){
	cout << "Pseudoexperiment: Creating instance from PseudoexperimentsCalculator." << endl;
	m_variable_name= psedudoexperimentCalculator->m_variable;
	cout << m_gap << "Variable name: " << m_variable_name << endl;
	
	createEmptyHistos(psedudoexperimentCalculator);
	m_nbins_reco=m_dataOnlyStat->GetNbinsX();
	m_nbins_truth=m_migrationOnlyStat->GetYaxis()->GetNbins();
	cout << m_gap << "Number of bins: " << m_nbins_reco << endl;
	
	m_lumi=psedudoexperimentCalculator->m_lumi;
	
}

void Pseudoexperiment::createEmptyHistos(PseudoexperimentsCalculator* psedudoexperimentCalculator){
  m_dataOnlyStat = (TH1D*)psedudoexperimentCalculator->m_hist_template->Clone();
  m_dataOnlyMCStat = (TH1D*)psedudoexperimentCalculator->m_hist_template->Clone();
  m_dataOnlySys = (TH1D*)psedudoexperimentCalculator->m_hist_template->Clone();
  m_dataStatAndSys = (TH1D*)psedudoexperimentCalculator->m_hist_template->Clone();
  m_dataStatMCStatAndSys = (TH1D*)psedudoexperimentCalculator->m_hist_template->Clone();
  m_signalRecoOnlyStat = (TH1D*)psedudoexperimentCalculator->m_hist_template->Clone();
  m_signalRecoOnlySys = (TH1D*)psedudoexperimentCalculator->m_hist_template->Clone();
  m_signalRecoStatAndSys = (TH1D*)psedudoexperimentCalculator->m_hist_template->Clone();
  m_bkgMCOnlyStat = (TH1D*)psedudoexperimentCalculator->m_hist_template->Clone();
  m_bkgMCOnlySys = (TH1D*)psedudoexperimentCalculator->m_hist_template->Clone();
  m_bkgMCStatAndSys = (TH1D*)psedudoexperimentCalculator->m_hist_template->Clone();
  m_bkgMultijetOnlyStat = (TH1D*)psedudoexperimentCalculator->m_hist_template->Clone();
  m_bkgMultijetOnlySys = (TH1D*)psedudoexperimentCalculator->m_hist_template->Clone();
  m_bkgMultijetStatAndSys = (TH1D*)psedudoexperimentCalculator->m_hist_template->Clone();
  m_bkgAllOnlyStat = (TH1D*)psedudoexperimentCalculator->m_hist_template->Clone();
  m_bkgAllOnlySys = (TH1D*)psedudoexperimentCalculator->m_hist_template->Clone();
  m_bkgAllStatAndSys = (TH1D*)psedudoexperimentCalculator->m_hist_template->Clone();
  m_pseudodataMCStat = (TH1D*)psedudoexperimentCalculator->m_hist_template->Clone();
  m_pseudodataDataStat = (TH1D*)psedudoexperimentCalculator->m_hist_template->Clone();
  m_pseudodataSys = (TH1D*)psedudoexperimentCalculator->m_hist_template->Clone();
  m_pseudodataMCStatAndSys = (TH1D*)psedudoexperimentCalculator->m_hist_template->Clone();
  m_pseudodataDataStatAndSys = (TH1D*)psedudoexperimentCalculator->m_hist_template->Clone();
  m_pseudodataDataStatMCStatAndSys = (TH1D*)psedudoexperimentCalculator->m_hist_template->Clone();
  
  
  m_ABCDInputsMCOnlyStat.resize(15);
  m_ABCDInputsMCOnlySys.resize(15);
  m_ABCDInputsMCStatAndSys.resize(15);
  m_ABCDInputsDataOnlyStat.resize(15);
  m_ABCDInputsDataOnlySys.resize(15);
  m_ABCDInputsDataStatAndSys.resize(15);
  for(int i=0;i<15;i++){
    m_ABCDInputsMCOnlyStat[i] = (TH1D*)psedudoexperimentCalculator->m_hist_template->Clone();
    m_ABCDInputsMCOnlySys[i] = (TH1D*)psedudoexperimentCalculator->m_hist_template->Clone();
    m_ABCDInputsMCStatAndSys[i] = (TH1D*)psedudoexperimentCalculator->m_hist_template->Clone();
    m_ABCDInputsDataOnlyStat[i] = (TH1D*)psedudoexperimentCalculator->m_hist_template->Clone();
    m_ABCDInputsDataOnlySys[i] = (TH1D*)psedudoexperimentCalculator->m_hist_template->Clone();
    m_ABCDInputsDataStatAndSys[i] = (TH1D*)psedudoexperimentCalculator->m_hist_template->Clone();
  }
  
  m_migrationOnlyStat = (TH2D*)psedudoexperimentCalculator->m_sysHistos->m_nominalHistos->migration->Clone();
  m_migrationOnlySys = (TH2D*)psedudoexperimentCalculator->m_sysHistos->m_nominalHistos->migration->Clone();
  m_migrationStatAndSys = (TH2D*)psedudoexperimentCalculator->m_sysHistos->m_nominalHistos->migration->Clone();
  m_signalTruthOnlyStat = (TH1D*)psedudoexperimentCalculator->m_hist_template_truth->Clone();
  m_signalTruthOnlySys = (TH1D*)psedudoexperimentCalculator->m_hist_template_truth->Clone();
  m_signalTruthStatAndSys = (TH1D*)psedudoexperimentCalculator->m_hist_template_truth->Clone();
  m_effOfRecoLevelCutsNominatorOnlyStat = (TH1D*)psedudoexperimentCalculator->m_hist_template_truth->Clone();
  m_effOfRecoLevelCutsNominatorOnlySys = (TH1D*)psedudoexperimentCalculator->m_hist_template_truth->Clone();
  m_effOfRecoLevelCutsNominatorStatAndSys = (TH1D*)psedudoexperimentCalculator->m_hist_template_truth->Clone();
  m_effOfTruthLevelCutsNominatorOnlyStat = (TH1D*)psedudoexperimentCalculator->m_hist_template->Clone();
  m_effOfTruthLevelCutsNominatorOnlySys = (TH1D*)psedudoexperimentCalculator->m_hist_template->Clone();
  m_effOfTruthLevelCutsNominatorStatAndSys = (TH1D*)psedudoexperimentCalculator->m_hist_template->Clone();
  
  m_signalModelingShifts.resize(psedudoexperimentCalculator->m_nbins_truth);
  m_signalModelingShiftsAlternative.resize(psedudoexperimentCalculator->m_nbins_truth);
  
  m_histLumi = new TH1D("","",1,0,1);
  
  
  
}


void Pseudoexperiment::prepareDataStat(TH1D* data){
	double x;
	for(int ibin=1;ibin<=m_nbins_reco;ibin++){
		x=data->GetBinContent(ibin);
		if(x<100.)m_dataOnlyStat->SetBinContent(ibin,gRandom->Poisson(x));
		else m_dataOnlyStat->SetBinContent(ibin,gRandom->Gaus(x,sqrt(x)));
	}
}
void Pseudoexperiment::prepareSignalMCStat(TH2D* migrationNominal, TH1D* effOfRecoLevelCutsNominatorNominal ,TH1D* signalRecoNominal, TH1D* effOfTruthLevelCutsNominatorNominal,TH1D* signalTruthNominal){
	int i,j;
	double x;
	double mu,sigma;
	for(i=1;i<=m_nbins_reco;i++)for(j=1;j<=m_nbins_truth;j++){
		mu=migrationNominal->GetBinContent(i,j);
		//cout << m_gap << "mu " << mu << endl;
		if(mu > 0){
			sigma=migrationNominal->GetBinError(i,j);
			//do x=gRandom->Gaus(mu,sigma);  while(x<0);
			x=functions::getRandomContent(mu,sigma);
			//cout << m_gap << "i/nbinsx: " << i <<"/" << m_nbins_reco << " j/nbinsy: " << j <<"/" << m_nbins_truth << "smeared " << x << endl;
			m_migrationOnlyStat->SetBinContent(i,j,x);
			m_migrationOnlyStat->SetBinError(i,j,sigma);
		}
		else {
			m_migrationOnlyStat->SetBinContent(i,j,0);
			m_migrationOnlyStat->SetBinError(i,j,0);
		}
	}
	double error;
	double a,b,c,d;
	for(j=1;j<=m_nbins_truth;j++){
		a=signalTruthNominal->GetBinContent(j);
		b= signalTruthNominal->GetBinError(j);
		//m_effOfRecoLevelCutsNominatorOnlyStat->SetBinContent(j,m_migrationOnlyStat->IntegralAndError(j,j,1,m_nbins_reco,error));
		m_effOfRecoLevelCutsNominatorOnlyStat->SetBinContent(j,m_migrationOnlyStat->IntegralAndError(1,m_nbins_reco,j,j,error));
		m_effOfRecoLevelCutsNominatorOnlyStat->SetBinError(j,error);
		//m_signalTruthOnlyStat->SetBinContent(j,gRandom->Gaus(a - effOfRecoLevelCutsNominatorNominal->GetBinContent(j),sqrt(b*b - pow(effOfRecoLevelCutsNominatorNominal->GetBinError(j),2))) + m_effOfRecoLevelCutsNominatorOnlyStat->GetBinContent(j));
		
		//cout << "Smearing truthOnlyStat in bin: " << i << endl;
		m_signalTruthOnlyStat->SetBinContent(j,functions::getRandomContent(a - effOfRecoLevelCutsNominatorNominal->GetBinContent(j),sqrt(b*b - pow(effOfRecoLevelCutsNominatorNominal->GetBinError(j),2))) + m_effOfRecoLevelCutsNominatorOnlyStat->GetBinContent(j));
		//cout << "Smearing truthOnlyStat in bin: " << i << " done." <<endl;
		//cout << m_gap << a << " " << effOfRecoLevelCutsNominatorNominal->GetBinContent(j) << endl;
		//cout << m_gap << b << " " << effOfRecoLevelCutsNominatorNominal->GetBinError(j) << endl;
		//cout << m_gap << m_signalTruthOnlyStat->GetBinContent(j) << " " << signalTruthNominal->GetBinContent(j) << endl;
		//cout << endl;
		//cout << m_gap << m_effOfRecoLevelCutsNominatorOnlyStat->GetBinContent(j) << " " << effOfRecoLevelCutsNominatorNominal->GetBinContent(j) << endl;
		//cout << m_gap << m_effOfRecoLevelCutsNominatorOnlyStat->GetBinError(j) << " " << effOfRecoLevelCutsNominatorNominal->GetBinError(j) << endl;
		
	}
	for(j=1;j<=m_nbins_reco;j++){
		a=signalRecoNominal->GetBinContent(j);
		b= signalRecoNominal->GetBinError(j);
		//m_effOfTruthLevelCutsNominatorOnlyStat->SetBinContent(j,m_migrationOnlyStat->IntegralAndError(1,m_nbins_truth,j,j,error));
		m_effOfTruthLevelCutsNominatorOnlyStat->SetBinContent(j,m_migrationOnlyStat->IntegralAndError(j,j,1,m_nbins_truth,error));
		m_effOfTruthLevelCutsNominatorOnlyStat->SetBinError(j,error);
		//m_signalRecoOnlyStat->SetBinContent(j,gRandom->Gaus(a - effOfTruthLevelCutsNominatorNominal->GetBinContent(j),sqrt(b*b - pow(effOfTruthLevelCutsNominatorNominal->GetBinError(j),2))) + m_effOfTruthLevelCutsNominatorOnlyStat->GetBinContent(j));
		
		c=effOfTruthLevelCutsNominatorNominal->GetBinContent(j);
		d=effOfTruthLevelCutsNominatorNominal->GetBinError(j);
		
		//cout << "Smearing signal recoOnlyStat in bin: " << j << " mu: " << a - c<< " sigma: " << sqrt(b*b - pow(d,2))<< endl;
		//cout << a << " " << b << " " << c << " " << d << endl;
		m_signalRecoOnlyStat->SetBinContent(j,functions::getRandomContent(a - c,sqrt(b*b - d*d)) + m_effOfTruthLevelCutsNominatorOnlyStat->GetBinContent(j));
		//cout << "Smearing signal recoOnlyStat in bin: " << j << " Done." << endl;
		//cout << m_gap << a << " " << effOfTruthLevelCutsNominatorNominal->GetBinContent(j) << endl;
		//cout << m_gap << b << " " << effOfTruthLevelCutsNominatorNominal->GetBinError(j) << endl;
		//cout << m_gap << m_signalRecoOnlyStat->GetBinContent(j) << " " << signalRecoNominal->GetBinContent(j) << endl;
		//cout << endl;
		//cout << m_gap << m_effOfTruthLevelCutsNominatorOnlyStat->GetBinContent(j) << " " << effOfTruthLevelCutsNominatorNominal->GetBinContent(j) << endl;
		//cout << m_gap << m_effOfTruthLevelCutsNominatorOnlyStat->GetBinError(j) << " " << effOfTruthLevelCutsNominatorNominal->GetBinError(j) << endl;
	}
	//TH1D* hx= m_migrationOnlyStat->ProjectionX();
	//TH1D* hy= m_migrationOnlyStat->ProjectionY();
	//for(int i=1;i<=m_nbins_reco;i++)cout << m_effOfTruthLevelCutsNominatorOnlyStat->GetBinContent(i) << " " 
	//<< m_effOfRecoLevelCutsNominatorOnlyStat->GetBinContent(i) << " " 
	//<< hx->GetBinContent(i) << " " 
	//<< hy->GetBinContent(i) << endl;
	
}

void Pseudoexperiment::prepareBkgMCStat(TH1D* bkgMCNominal){
	for(int i=1;i<=m_nbins_reco;i++)m_bkgMCOnlyStat->SetBinContent(i, functions::getRandomContent(bkgMCNominal->GetBinContent(i),bkgMCNominal->GetBinError(i)));
}
void Pseudoexperiment::prepareABCDInputsStat(vector<TH1D*>& data16RegionsNominal,vector<TH1D*>& MC16RegionsNominal){
	for(int i=0;i<15;i++){
		for(int ibin=1;ibin<=m_nbins_reco;ibin++){
			m_ABCDInputsMCOnlyStat[i]->SetBinContent(ibin,functions::getRandomContent(MC16RegionsNominal[i]->GetBinContent(ibin),MC16RegionsNominal[i]->GetBinError(ibin)));
			double data=data16RegionsNominal[i]->GetBinContent(ibin);
			if(data<100.) m_ABCDInputsDataOnlyStat[i]->SetBinContent(ibin,gRandom->Poisson(data));
			else m_ABCDInputsDataOnlyStat[i]->SetBinContent(ibin,gRandom->Gaus(data,sqrt(data)));
		}
	}
}

void Pseudoexperiment::writePseudoexperiment(TFile*f,TString foldername){
	f->cd();
	f->mkdir(foldername);
	f->cd(foldername);
	m_dataOnlyStat->Write(m_variable_name+"_"+ "DataOnlyStat");
	m_dataOnlyMCStat->Write(m_variable_name+"_"+ "DataOnlyMCStat");
	m_dataOnlySys->Write(m_variable_name+"_"+ "DataOnlySys");
	m_dataStatAndSys->Write(m_variable_name+"_"+ "DataStatAndSys");
	m_signalRecoOnlyStat->Write(m_variable_name+"_"+"SignalRecoOnlyStat");
	m_signalRecoOnlySys->Write(m_variable_name+"_"+"SignalRecoOnlySys");
	m_signalRecoStatAndSys->Write(m_variable_name+"_"+"SignalRecoStatAndSys");
	m_bkgMCOnlyStat->Write(m_variable_name+"_"+"bkgMCOnlyStat");
	m_bkgMCOnlySys->Write(m_variable_name+"_"+"bkgMCOnlySys");
	m_bkgMCStatAndSys->Write(m_variable_name+"_"+"bkgMCStatAndSys");
	m_bkgMultijetOnlyStat->Write(m_variable_name+"_"+"MultijetOnlyStat");
	m_bkgMultijetOnlySys->Write(m_variable_name+"_"+"MultijetOnlySys");
	m_bkgMultijetStatAndSys->Write(m_variable_name+"_"+"MultijetStatAndSys");
	m_bkgAllOnlyStat->Write(m_variable_name+"_"+"bkgAllOnlyStat");
	m_bkgAllOnlySys->Write(m_variable_name+"_"+"bkgAllStatAndSys");
	m_bkgAllStatAndSys->Write(m_variable_name+"_"+"bkgAllStatAndSys");
	m_pseudodataMCStat->Write(m_variable_name+"_"+"PseudodataMCStat");
	m_pseudodataDataStat->Write(m_variable_name+"_"+"PseudodataDataStat");
	m_pseudodataSys->Write(m_variable_name+"_"+"PseudodataSys");
	m_pseudodataMCStatAndSys->Write(m_variable_name+"_"+"PseudodataMCStatAndSys");
	m_pseudodataDataStatAndSys->Write(m_variable_name+"_"+"PseudodataDataStatAndSys");
	m_pseudodataDataStatMCStatAndSys->Write(m_variable_name+"_"+"PseudodataDataStatMCStatAndSys");
	
	m_migrationOnlyStat->Write(m_variable_name+"_"+"migrationOnlyStat");
	m_migrationOnlySys->Write(m_variable_name+"_"+"migrationOnlySys");
	m_migrationStatAndSys->Write(m_variable_name+"_"+"migrationStatAndSys");
	m_signalTruthOnlyStat->Write(m_variable_name+"_"+"SignalTruthOnlyStat");
	m_signalTruthOnlySys->Write(m_variable_name+"_"+"SignalTruthOnlySys");
	m_signalTruthStatAndSys->Write(m_variable_name+"_"+"SignalTruthStatAndSys");
	m_effOfRecoLevelCutsNominatorOnlyStat->Write(m_variable_name+"_"+"effOfRecoLevelCutsNominatorOnlyStat");
	m_effOfRecoLevelCutsNominatorOnlySys->Write(m_variable_name+"_"+"effOfRecoLevelCutsNominatorOnlySys");
	m_effOfRecoLevelCutsNominatorStatAndSys->Write(m_variable_name+"_"+"effOfRecoLevelCutsNominatorStatAndSys");
	m_effOfTruthLevelCutsNominatorOnlyStat->Write(m_variable_name+"_"+"effOfTruthLevelCutsNominatorOnlyStat");
	m_effOfTruthLevelCutsNominatorOnlySys->Write(m_variable_name+"_"+"effOfTruthLevelCutsNominatorOnlySys");
	m_effOfTruthLevelCutsNominatorStatAndSys->Write(m_variable_name+"_"+"effOfTruthLevelCutsNominatorStatAndSys");
	m_histLumi->Write("lumi");
}
Pseudoexperiment::Pseudoexperiment(TFile* f, TString experiment_name, TString variable_name): m_gap("                  "){
	m_variable_name=variable_name;
	m_dataOnlyStat = (TH1D*)f->Get(experiment_name+"/" + m_variable_name+"_"+ "DataOnlyStat"); 
	m_dataOnlyMCStat = (TH1D*)f->Get(experiment_name+"/" + m_variable_name+"_"+ "DataOnlyMCStat"); 
	m_dataOnlySys = (TH1D*)f->Get(experiment_name+"/" + m_variable_name+"_"+ "DataOnlySys");
	m_dataStatAndSys = (TH1D*)f->Get(experiment_name+"/" + m_variable_name+"_"+ "DataStatAndSys");
	m_signalRecoOnlyStat = (TH1D*)f->Get(experiment_name+"/" + m_variable_name+"_"+ "SignalRecoOnlyStat");
	m_signalRecoOnlySys = (TH1D*)f->Get(experiment_name+"/" + m_variable_name+"_"+ "SignalRecoOnlySys");
	m_signalRecoStatAndSys = (TH1D*)f->Get(experiment_name+"/" + m_variable_name+"_"+ "SignalRecoStatAndSys");
	m_bkgMCOnlyStat = (TH1D*)f->Get(experiment_name+"/" + m_variable_name+"_"+ "bkgMCOnlyStat");
	m_bkgMCOnlySys = (TH1D*)f->Get(experiment_name+"/" + m_variable_name+"_"+ "bkgMCOnlySys");
	m_bkgMCStatAndSys = (TH1D*)f->Get(experiment_name+"/" + m_variable_name+"_"+ "bkgMCStatAndSys");
	m_bkgMultijetOnlyStat = (TH1D*)f->Get(experiment_name+"/" + m_variable_name+"_"+ "MultijetOnlyStat");
	m_bkgMultijetOnlySys = (TH1D*)f->Get(experiment_name+"/" + m_variable_name+"_"+ "MultijetOnlySys");
	m_bkgMultijetStatAndSys = (TH1D*)f->Get(experiment_name+"/" + m_variable_name+"_"+ "MultijetStatAndSys");
	m_bkgAllOnlyStat = (TH1D*)f->Get(experiment_name+"/" + m_variable_name+"_"+ "bkgAllOnlyStat");
	m_bkgAllOnlySys = (TH1D*)f->Get(experiment_name+"/" + m_variable_name+"_"+ "bkgAllOnlySys");
	m_bkgAllStatAndSys = (TH1D*)f->Get(experiment_name+"/" + m_variable_name+"_"+ "bkgAllStatAndSys");
	m_pseudodataMCStat = (TH1D*)f->Get(experiment_name+"/" + m_variable_name+"_"+ "PseudodataMCStat");
	m_pseudodataDataStat = (TH1D*)f->Get(experiment_name+"/" + m_variable_name+"_"+ "PseudodataDataStat");
	m_pseudodataSys = (TH1D*)f->Get(experiment_name+"/" + m_variable_name+"_"+ "PseudodataSys");
	m_pseudodataMCStatAndSys = (TH1D*)f->Get(experiment_name+"/" + m_variable_name+"_"+ "PseudodataMCStatAndSys");
	m_pseudodataDataStatAndSys = (TH1D*)f->Get(experiment_name+"/" + m_variable_name+"_"+ "PseudodataDataStatAndSys");
	m_pseudodataDataStatMCStatAndSys = (TH1D*)f->Get(experiment_name+"/" + m_variable_name+"_"+ "PseudodataDataStatMCStatAndSys");
	
	m_migrationOnlyStat = (TH2D*)f->Get(experiment_name+"/" + m_variable_name+"_"+ "migrationOnlyStat");
	m_migrationOnlySys = (TH2D*)f->Get(experiment_name+"/" + m_variable_name+"_"+ "migrationOnlySys");
	m_migrationStatAndSys = (TH2D*)f->Get(experiment_name+"/" + m_variable_name+"_"+ "migrationStatAndSys");
	m_signalTruthOnlyStat = (TH1D*)f->Get(experiment_name+"/" + m_variable_name+"_"+ "SignalTruthOnlyStat");
	m_signalTruthOnlySys = (TH1D*)f->Get(experiment_name+"/" + m_variable_name+"_"+ "SignalTruthOnlySys");
	m_signalTruthStatAndSys = (TH1D*)f->Get(experiment_name+"/" + m_variable_name+"_"+ "SignalTruthStatAndSys");
	m_effOfRecoLevelCutsNominatorOnlyStat = (TH1D*)f->Get(experiment_name+"/" + m_variable_name+"_"+ "effOfRecoLevelCutsNominatorOnlyStat");
	m_effOfRecoLevelCutsNominatorOnlySys = (TH1D*)f->Get(experiment_name+"/" + m_variable_name+"_"+ "effOfRecoLevelCutsNominatorOnlySys");
	m_effOfRecoLevelCutsNominatorStatAndSys = (TH1D*)f->Get(experiment_name+"/" + m_variable_name+"_"+ "effOfRecoLevelCutsNominatorStatAndSys");
	m_effOfTruthLevelCutsNominatorOnlyStat = (TH1D*)f->Get(experiment_name+"/" + m_variable_name+"_"+ "effOfTruthLevelCutsNominatorOnlyStat");
	m_effOfTruthLevelCutsNominatorOnlySys = (TH1D*)f->Get(experiment_name+"/" + m_variable_name+"_"+ "effOfTruthLevelCutsNominatorOnlySys");
	m_effOfTruthLevelCutsNominatorStatAndSys = (TH1D*)f->Get(experiment_name+"/" + m_variable_name+"_"+ "effOfTruthLevelCutsNominatorStatAndSys");
	
	m_histLumi = (TH1D*)f->Get(experiment_name+"/lumi");
	
	m_migrationOnlyStat->SetName(experiment_name + m_variable_name+"_"+ "migrationOnlyStat");
	m_migrationOnlySys->SetName(experiment_name + m_variable_name+"_"+ "migrationOnlySys");
	m_migrationStatAndSys->SetName(experiment_name + m_variable_name+"_"+ "migrationStatAndSys");
	//cout << m_gap << m_migrationOnlyStat->Integral() << endl;
}
