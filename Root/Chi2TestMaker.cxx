#include "TTbarCovarianceCalculator/Chi2TestMaker.h"
#include "TTbarHelperFunctions/histManipFunctions.h"
#include "TTbarHelperFunctions/functions.h"
#include "TTbarHelperFunctions/TtbarDiffCrossSection_exception.h"

#include "TMath.h"
#include "TH1.h"

#include <iostream>
#include <iomanip>      // std::setprecision

ClassImp(Chi2TestMaker)

//----------------------------------------------------------------------

bool Chi2TestMaker::checkSetup() const {
  if(!m_distr1 || !m_distr2 || !m_cov1 || !m_cov2) return false;
  
  const int nbins = m_distr1->GetNcols();
  
  if(nbins!=m_distr2->GetNcols() ||
     1!=m_distr1->GetNrows()     ||
     1!=m_distr2->GetNrows()     ||
     nbins!=m_cov1->GetNcols()   ||
     nbins!=m_cov1->GetNrows()   ||
     nbins!=m_cov2->GetNcols()   ||
     nbins!=m_cov2->GetNrows()) {
       std::cout << nbins << " " << m_distr2->GetNcols() << " " << m_distr1->GetNcols() << " " << m_cov1->GetNcols() << " " << m_cov2->GetNcols() << " " << m_cov1->GetNrows() << " " << m_cov2->GetNrows() << std::endl;
       return false;
     }
  
  return true;
}

//----------------------------------------------------------------------

void Chi2TestMaker::calculateInverseCov() {
  m_inverseCov = std::make_unique<TMatrixT<double>>(*m_cov1);
  *m_inverseCov += *m_cov2;
  std::cout << "Printing covariances: " << std::endl;
  m_cov1->Print();
  m_cov2->Print();
  std::cout << "Printing correlations: " << std::endl;
  functions::makeCorrelationMatrix(*m_cov1)->Print();
  functions::makeCorrelationMatrix(*m_cov2)->Print();
  
  //std::cout << "Printing sum of covariances: " << std::endl;
  //m_inverseCov->Print();
  
  m_inverseCov->Invert();
  //std::cout << "Printing inverse covariance: " << std::endl;
  //m_inverseCov->Print();
}

//----------------------------------------------------------------------

// Function used to reduce number of degrees of freedom
void Chi2TestMaker::removeBin(const int index) {
  
  if(!checkSetup()) {
    throw TtbarDiffCrossSection_exception("Chi2TestMaker: Inconsistent settings in Chi2TestMaker.");
  }
  
  int i0 = index-1;
  
  const int nbins = m_distr1->GetNcols();
  
  if(nbins==1) {
    std::cout << "WARNING: Chi2TestMaker::removeBin() requested for distribution with one bin. Bin will not be removed." << std::endl;
    return;
  }
  
  TMatrixT<double> distr1(1,nbins-1);
  TMatrixT<double> distr2(1,nbins-1);
  TMatrixT<double> cov1(nbins-1,nbins-1);
  TMatrixT<double> cov2(nbins-1,nbins-1);
  
  int k=0;
  for(int i=0;i<nbins;i++) {
    if(i==i0) continue;
    distr1[0][k] = (*m_distr1)[0][i];
    distr2[0][k] = (*m_distr2)[0][i];
    
    int l=0;
    for(int j=0;j<nbins;j++) {
      if(j==i0) continue;
      cov1[l][k] = (*m_cov1)[j][i];
      cov2[l][k] = (*m_cov2)[j][i];
      l++;
    }
    k++;
  }
  
  m_distr1 = std::make_unique<TMatrixT<double>>(distr1);
  m_distr2 = std::make_unique<TMatrixT<double>>(distr2);
  m_cov1 = std::make_unique<TMatrixT<double>>(cov1);
  m_cov2 = std::make_unique<TMatrixT<double>>(cov2);
  
}

//----------------------------------------------------------------------

void Chi2TestMaker::chi2Test() {
  if(!checkSetup()) {
    throw TtbarDiffCrossSection_exception("Chi2TestMaker: Inconsistent settings in Chi2TestMaker.");
  }
  
  calculateInverseCov();
  
  TMatrixT<double> difference = *m_distr1;
  difference -=*m_distr2;
  
  std::cout << "Printing distributions: " << std::endl;
  std::cout << "Distr1:" << std::endl;
  m_distr1->Print();
  std::cout << "Distr2:" << std::endl;
  m_distr2->Print();
  std::cout << "Difference:" << std::endl;
  difference.Print();
  
  m_chi2 = functions::chi2Statistics(difference, *m_inverseCov);
  m_ndf = m_distr1->GetNcols();
  m_pValue = TMath::Prob(m_chi2,m_ndf);
  
}

//----------------------------------------------------------------------

void Chi2TestMaker::setDistr1(const std::vector<double>& distr) {
  m_distr1=std::make_unique<TMatrixT<double>>(functions::tMatrixFromVector(distr));
}

//----------------------------------------------------------------------
void Chi2TestMaker::setDistr1(const TVectorD& distr) {
  m_distr1=std::make_unique<TMatrixT<double>>(functions::tMatrixFromVector(distr));
}

//----------------------------------------------------------------------

void Chi2TestMaker::setDistr1(const TH1* distr) {
  m_distr1=std::make_unique<TMatrixT<double>>(functions::tMatrixFromHistogram(distr));
}

//----------------------------------------------------------------------

void Chi2TestMaker::setDistr1(const TMatrixT<double>& distr){
  m_distr1=std::make_unique<TMatrixT<double>>(distr);
}

//----------------------------------------------------------------------

void Chi2TestMaker::setDistr2(const std::vector<double>& distr) {
  m_distr2=std::make_unique<TMatrixT<double>>(functions::tMatrixFromVector(distr));
}

//----------------------------------------------------------------------

void Chi2TestMaker::setDistr2(const TVectorD& distr) {
  m_distr2=std::make_unique<TMatrixT<double>>(functions::tMatrixFromVector(distr));
}

//----------------------------------------------------------------------

void Chi2TestMaker::setDistr2(const TH1* distr) {
  m_distr2=std::make_unique<TMatrixT<double>>(functions::tMatrixFromHistogram(distr));
}

//----------------------------------------------------------------------

void Chi2TestMaker::setDistr2(const TMatrixT<double>& distr){
  m_distr2=std::make_unique<TMatrixT<double>>(distr);
}

//----------------------------------------------------------------------
