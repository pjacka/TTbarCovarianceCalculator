#include "TTbarCovarianceCalculator/OneSysHistos.h"
#include "TTbarHelperFunctions/functions.h"
#include "TTbarHelperFunctions/histManipFunctions.h"
ClassImp(OneSysHistos)

OneSysHistos::OneSysHistos(const TString& chain_name,const TString& variable,TFile* file){
  m_variable=variable;
  
  loadHistos(chain_name,variable,file);
  
}
void OneSysHistos::write(TFile* f, const TString& chain_name,const TString& variable_name){
  f->mkdir(chain_name);
  f->cd(chain_name);
  data->Write(variable_name + "_data");
  signal_reco ->Write(variable_name + "_signal_reco"); 
  bkg_MC  ->Write(variable_name + "_bkg_MC");                    
  bkg_Multijet ->Write(variable_name + "_bkg_Multijet");               
  bkg_all    ->Write(variable_name + "_bkg_all");                 
  pseudodata  ->Write(variable_name + "_pseudodata");                
  migration   ->Write(variable_name + "_migration");                
  effOfTruthLevelCutsNominator->Write(variable_name + "_EffOfTruthLevelCutsNominator");
  effOfRecoLevelCutsNominator->Write(variable_name + "_EffOfRecoLevelCutsNominator"); 
  signal_truth->Write(variable_name + "_signal_truth");
  
  vector<TString> regionsNames={"A","B","C","D","E","F","G","H","I","J","K","L","M","N","O"};
  
  f->mkdir(chain_name + "/SidebandRegions/");
  f->cd(chain_name + "/SidebandRegions"); 
  for(int i=0;i<15;i++) sidebandRegions_data[i]->Write(variable_name + "_data_"+regionsNames[i]); 
  for(int i=0;i<15;i++) sidebandRegions_MC[i]->Write(variable_name + "_MC_"+regionsNames[i]); 
}
void OneSysHistos::loadHistos(const TString& chain_name,const TString& variable, TFile* file){
  data			      = (TH1D*)file->Get(chain_name + "/" + variable + "_data");
  signal_reco                 = (TH1D*)file->Get(chain_name + "/" + variable + "_signal_reco");
  bkg_MC                      = (TH1D*)file->Get(chain_name + "/" + variable + "_bkg_MC");
  bkg_Multijet                = (TH1D*)file->Get(chain_name + "/" + variable + "_bkg_Multijet");
  bkg_all                     = (TH1D*)file->Get(chain_name + "/" + variable + "_bkg_all");
  pseudodata                  = (TH1D*)file->Get(chain_name + "/" + variable + "_pseudodata");
  migration                   = (TH2D*)file->Get(chain_name + "/" + variable + "_migration");
  effOfTruthLevelCutsNominator= (TH1D*)file->Get(chain_name + "/" + variable + "_EffOfTruthLevelCutsNominator");
  effOfRecoLevelCutsNominator = (TH1D*)file->Get(chain_name + "/" + variable + "_EffOfRecoLevelCutsNominator");
  signal_truth		      = (TH1D*)file->Get(chain_name + "/" + variable + "_signal_truth");
  
  vector<TString> regionsNames={"A","B","C","D","E","F","G","H","I","J","K","L","M","N","O"};
  
  sidebandRegions_data.resize(15);
  sidebandRegions_MC.resize(15);
  
  for(int i=0;i<15;i++) sidebandRegions_data[i]=(TH1D*)file->Get(chain_name+"/SidebandRegions/" + variable + "_data_" + regionsNames[i]);
  for(int i=0;i<15;i++) sidebandRegions_MC[i]=(TH1D*)file->Get(chain_name+"/SidebandRegions/" + variable + "_MC_" + regionsNames[i]);
 
 }

OneSysHistos::OneSysHistos(const vector<OneSysHistos*>& vec){
  int nhistos=vec.size();
  vector<TH1D*> helpvec(nhistos);
  for(int i=0;i<nhistos;i++)helpvec[i]=vec[i]->signal_reco;
  TH1D* h_template = functions::createEmptyConcatenatedHistogram(helpvec);
  
  signal_reco		      =(TH1D*)h_template->Clone();
  bkg_MC                      =(TH1D*)h_template->Clone();
  bkg_Multijet                =(TH1D*)h_template->Clone();
  bkg_all                     =(TH1D*)h_template->Clone();
  pseudodata                  =(TH1D*)h_template->Clone();
  effOfTruthLevelCutsNominator=(TH1D*)h_template->Clone();
  effOfRecoLevelCutsNominator =(TH1D*)h_template->Clone();
  signal_truth		      =(TH1D*)h_template->Clone();
  
  functions::FillConcatenatedHistogram(signal_reco,helpvec);
  
  if(vec[0]->data){
    //cout << vec[0]->data << endl;
    data=(TH1D*)h_template->Clone();
    for(int i=0;i<nhistos;i++)helpvec[i]=vec[i]->data;
    functions::FillConcatenatedHistogram(data,helpvec);
  
  }
  
  for(int i=0;i<nhistos;i++)helpvec[i]=vec[i]->bkg_MC;
  functions::FillConcatenatedHistogram(bkg_MC,helpvec);
  
  for(int i=0;i<nhistos;i++)helpvec[i]=vec[i]->bkg_Multijet;
  functions::FillConcatenatedHistogram(bkg_Multijet,helpvec);
  
  for(int i=0;i<nhistos;i++)helpvec[i]=vec[i]->bkg_all;
  functions::FillConcatenatedHistogram(bkg_all,helpvec);
  
  for(int i=0;i<nhistos;i++)helpvec[i]=vec[i]->pseudodata;
  functions::FillConcatenatedHistogram(pseudodata,helpvec);
  
  for(int i=0;i<nhistos;i++)helpvec[i]=vec[i]->effOfTruthLevelCutsNominator;
  functions::FillConcatenatedHistogram(effOfTruthLevelCutsNominator,helpvec);
  
  for(int i=0;i<nhistos;i++)helpvec[i]=vec[i]->effOfRecoLevelCutsNominator;
  functions::FillConcatenatedHistogram(effOfRecoLevelCutsNominator,helpvec);
  
  for(int i=0;i<nhistos;i++)helpvec[i]=vec[i]->signal_truth;
  functions::FillConcatenatedHistogram(signal_truth,helpvec);
 
  
  vector<TH2D*> helpvec2(nhistos);
  for(int i=0;i<nhistos;i++)helpvec2[i]=vec[i]->migration;
  migration = functions::createEmptyConcatenatedHistogram(helpvec2);
  functions::FillConcatenatedHistogram(migration,helpvec2);
  
  sidebandRegions_data.resize(15);
  sidebandRegions_MC.resize(15);
  
  if(vec[0]->sidebandRegions_MC[0]){
    for(int iRegion=0;iRegion<15;iRegion++){
      sidebandRegions_MC[iRegion]=(TH1D*)h_template->Clone();
      for(int i=0;i<nhistos;i++)helpvec[i]=vec[i]->sidebandRegions_MC[iRegion];
      functions::FillConcatenatedHistogram(sidebandRegions_MC[iRegion],helpvec);
    
    }
  }
  
  if(vec[0]->sidebandRegions_data[0]){
    for(int iRegion=0;iRegion<15;iRegion++){
      sidebandRegions_data[iRegion]=(TH1D*)h_template->Clone();
      for(int i=0;i<nhistos;i++)helpvec[i]=vec[i]->sidebandRegions_data[iRegion];
      functions::FillConcatenatedHistogram(sidebandRegions_data[iRegion],helpvec);
    }
  }
  
}

void OneSysHistos::rebin1Dhist(TH1D* hist,const int& nbins,const vector<double>& binning){
  TH1D* helphist=0;
  helphist = (TH1D*)hist->Rebin(nbins,"",&binning[0]);
  delete hist;
  hist= helphist;
}
