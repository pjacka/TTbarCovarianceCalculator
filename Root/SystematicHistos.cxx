#include "TTbarCovarianceCalculator/SystematicHistos.h"
#include "TTbarHelperFunctions/functions.h"
#include "TTbarHelperFunctions/histManipFunctions.h"
#include "TTbarCovarianceCalculator/CovarianceCalculatorFunctions.h"
ClassImp(SystematicHistos)

SystematicHistos::SystematicHistos(TFile* file,const TString& variable, const TTBarConfig& ttbarConfig){
  this->init(file,variable,ttbarConfig);
  
  
}
void SystematicHistos::init(TFile* file,const TString& variable, const TTBarConfig& ttbarConfig){

  m_variable=variable;
  
  m_fileSystematics=file;

  m_ttbarConfig = &ttbarConfig;

  prepareListOfSystematics();
  loadHistos();
 
  //prepareRelativeShifts();
  //printRelativeShifts();
}


SystematicHistos::SystematicHistos(const vector<SystematicHistos*>& vec){
  const int nhistos=vec.size();
  m_variable="concatenated";
  m_nsys_all=vec[0]->m_nsys_all;
  m_nsys_pairs=vec[0]->m_nsys_pairs;
  m_nsys_single=vec[0]->m_nsys_single;
  m_nsys_PDF=vec[0]->m_nsys_PDF;
  m_nsys_signalModeling=vec[0]->m_nsys_signalModeling;
  
  m_sys_names_all=vec[0]->m_sys_names_all;
  m_sys_names_pairs=vec[0]->m_sys_names_pairs;
  m_sys_names_single=vec[0]->m_sys_names_single;
  m_sys_names_PDF=vec[0]->m_sys_names_PDF;
  m_sys_names_signalModeling=vec[0]->m_sys_names_signalModeling;
  
  
  vector<OneSysHistos*> helpvec(nhistos);
  vector<OneSysHistos*> helpvec2(nhistos);
  for(int i=0;i<nhistos;i++)helpvec[i]=vec[i]->m_nominalHistos;
  m_nominalHistos = new OneSysHistos(helpvec);
  
  m_sysHistosPaired.resize(m_nsys_pairs);
  m_sysHistosSingle.resize(m_nsys_single);
  m_sysHistosPDF.resize(m_nsys_PDF);;
  
  for(int isys=0;isys<m_nsys_pairs;isys++){
    for(int i=0;i<nhistos;i++)helpvec[i]=vec[i]->m_sysHistosPaired[isys].first;
    for(int i=0;i<nhistos;i++)helpvec2[i]=vec[i]->m_sysHistosPaired[isys].second;
    m_sysHistosPaired[isys]=make_pair(new OneSysHistos(helpvec),new OneSysHistos(helpvec2));
    
  }
  for(int isys=0;isys<m_nsys_single;isys++){
    for(int i=0;i<nhistos;i++)helpvec[i]=vec[i]->m_sysHistosSingle[isys];
    m_sysHistosSingle[isys]=new OneSysHistos(helpvec);
  }
  for(int isys=0;isys<m_nsys_PDF;isys++){
    for(int i=0;i<nhistos;i++)helpvec[i]=vec[i]->m_sysHistosPDF[isys];
    m_sysHistosPDF[isys]=new OneSysHistos(helpvec);
  }
  for(int isys=0;isys<m_nsys_signalModeling;isys++){
    for(int i=0;i<nhistos;i++)helpvec[i]=vec[i]->m_sysHistosSignalModeling[m_sys_names_signalModeling[isys]];
    m_sysHistosSignalModeling[m_sys_names_signalModeling[isys]]=new OneSysHistos(helpvec);
  }
}



void SystematicHistos::prepareListOfSystematics(){
  functions::GetListOfNames(m_sys_names_all,m_fileSystematics,TDirectory::Class());
  auto it = std::find (m_sys_names_all.begin(), m_sys_names_all.end(), "Sherpa");
  if(it!=m_sys_names_all.end())m_sys_names_all.erase(it);
  m_nsys_all=m_sys_names_all.size();
  for(int isys=0;isys<m_nsys_all;isys++){
    cout << m_sys_names_all[isys] << endl;
  }
  functions::classifySysNames(m_sys_names_all,m_sys_names_pairs, m_sys_names_single, m_sys_names_PDF,m_sys_names_signalModeling);
  m_nsys_pairs=m_sys_names_pairs.size();
  m_nsys_single=m_sys_names_single.size();
  m_nsys_PDF=m_sys_names_PDF.size();
  m_nsys_signalModeling=m_sys_names_signalModeling.size();
  
}

void SystematicHistos::loadHistos(){
  
  m_sysHistosPaired.resize(m_nsys_pairs);
  m_sysHistosSingle.resize(m_nsys_single);
  m_sysHistosPDF.resize(m_nsys_PDF);
  
  m_nominalHistos = new OneSysHistos("nominal",m_variable,m_fileSystematics);
  
  for(int isys=0;isys<m_nsys_pairs;isys++) m_sysHistosPaired[isys]=make_pair( new OneSysHistos(m_sys_names_pairs[isys].first,m_variable,m_fileSystematics), new OneSysHistos(m_sys_names_pairs[isys].second,m_variable,m_fileSystematics) );
  for(int isys=0;isys<m_nsys_single;isys++) m_sysHistosSingle[isys] = new OneSysHistos(m_sys_names_single[isys],m_variable,m_fileSystematics);
  for(int isys=0;isys<m_nsys_PDF;isys++) m_sysHistosPDF[isys] = new OneSysHistos(m_sys_names_PDF[isys],m_variable,m_fileSystematics);
  for(int isys=0;isys<m_nsys_signalModeling;isys++) {
    std::cout << "SystematicHistos::loadHistos: Loading modeling histos. index: " << isys << " name: " << m_sys_names_signalModeling[isys] << std::endl;
    m_sysHistosSignalModeling[m_sys_names_signalModeling[isys]] = new OneSysHistos(m_sys_names_signalModeling[isys],m_variable,m_fileSystematics);
  }
  
}
void SystematicHistos::write(TFile* f){
  
  m_nominalHistos->write(f,"nominal",m_variable);

  for(int isys=0;isys<m_nsys_pairs;isys++) {
    //cout << isys << " " << m_nsys_pairs << " " << m_sysHistosPaired.size() << " " << m_sysHistosPaired[isys].first->signal_reco->Integral() << " " << m_sysHistosPaired[isys].first->signal_reco->Integral()<< endl;
    m_sysHistosPaired[isys].first->write(f,m_sys_names_pairs[isys].first,m_variable);
    m_sysHistosPaired[isys].second->write(f,m_sys_names_pairs[isys].second,m_variable);
  
  }
  for(int isys=0;isys<m_nsys_single;isys++) m_sysHistosSingle[isys]->write(f,m_sys_names_single[isys],m_variable);
  for(int isys=0;isys<m_nsys_PDF;isys++) m_sysHistosPDF[isys]->write(f,m_sys_names_PDF[isys],m_variable);
  for(int isys=0;isys<m_nsys_signalModeling;isys++) m_sysHistosSignalModeling[m_sys_names_signalModeling[isys]]->write(f,m_sys_names_signalModeling[isys],m_variable);
  
  
}

void SystematicHistos::prepareRelativeShiftsSignalModeling(){
  cout << endl << "Preparing shifts for signal modeling: " << endl;
  for(int isys=1;isys<m_nsys_PDF;isys++){
    cout << m_sys_names_PDF[isys] << endl;
    m_shifts_PDF.push_back(functions::calculateRelativeShiftsForAbsoluteSpectra(*m_sysHistosPDF[0],*m_sysHistosPDF[isys],"modeling"));
    m_shifts_PDF_alternative.push_back(functions::calculateRelativeShiftsForAbsoluteSpectra(*m_sysHistosPDF[isys],*m_sysHistosPDF[0],"modeling"));
    m_shifts_relative_PDF.push_back(functions::calculateRelativeShiftsForRelativeSpectra(*m_sysHistosPDF[0],*m_sysHistosPDF[isys],"modeling"));
    m_shifts_relative_PDF_alternative.push_back(functions::calculateRelativeShiftsForRelativeSpectra(*m_sysHistosPDF[isys],*m_sysHistosPDF[0],"modeling"));
  }
  for(int isys=0;isys<m_nsys_signalModeling;isys++){
    //cout << m_sys_names_signalModeling[isys] << endl;
    
    
    OneSysHistos* nom = m_nominalHistos;
    if(m_sys_names_signalModeling[isys]==m_ttbarConfig->hardScatteringDirectory()) {
      if(m_sysHistosSignalModeling.find(m_ttbarConfig->PhPy8MECoffDirectory()) != std::end(m_sysHistosSignalModeling)) {
	nom = m_sysHistosSignalModeling.at(m_ttbarConfig->PhPy8MECoffDirectory());
      }
      else std::cout << "Warning: PhPy8MECoffDirectory not found on the list of signal modeling systematics. Using nominal instead." << std::endl;
    }
    
    m_shifts_signalModeling[m_sys_names_signalModeling[isys]] = functions::calculateRelativeShiftsForAbsoluteSpectra(*nom,*m_sysHistosSignalModeling[m_sys_names_signalModeling[isys]],"modeling");
    m_shifts_signalModeling_alternative[m_sys_names_signalModeling[isys]] = functions::calculateRelativeShiftsForAbsoluteSpectra(*m_sysHistosSignalModeling[m_sys_names_signalModeling[isys]],*nom,"modeling");
    m_shifts_relative_signalModeling[m_sys_names_signalModeling[isys]] = functions::calculateRelativeShiftsForRelativeSpectra(*nom,*m_sysHistosSignalModeling[m_sys_names_signalModeling[isys]],"modeling");
    m_shifts_relative_signalModeling_alternative[m_sys_names_signalModeling[isys]] = functions::calculateRelativeShiftsForRelativeSpectra(*m_sysHistosSignalModeling[m_sys_names_signalModeling[isys]],*nom,"modeling");
  }
  
}

//----------------------------------------------------------------------

void SystematicHistos::prepareRelativeShifts(){
  cout << endl << "Preparing shifts: " << endl;
  
  TH1D* hAbs1,*hRel1;
  TH1D* hAbs2,*hRel2;
  
  
  for(int isys=0;isys<m_nsys_pairs;isys++){
    //cout << m_sys_names_pairs[isys].first << endl;
    hAbs1 = functions::calculateRelativeShiftsForAbsoluteSpectra(*m_nominalHistos, *m_sysHistosPaired[isys].first);
    hRel1 = functions::calculateRelativeShiftsForRelativeSpectra(*m_nominalHistos, *m_sysHistosPaired[isys].first);
    hAbs2 = functions::calculateRelativeShiftsForAbsoluteSpectra(*m_nominalHistos, *m_sysHistosPaired[isys].second);
    hRel2 = functions::calculateRelativeShiftsForRelativeSpectra(*m_nominalHistos, *m_sysHistosPaired[isys].second);
    
    //cout << hAbs1->Integral() <<  endl;
    //cout << hRel1->Integral() <<  endl;
    //cout << hRel2->Integral() <<  endl;
    //cout << hAbs2->Integral() <<  endl;
    m_shifts_paired.push_back(make_pair(hAbs1,hAbs2));
    m_shifts_relative_paired.push_back(make_pair(hRel1,hRel2));
  }
  for(int isys=0;isys<m_nsys_single;isys++){
    //cout << m_sys_names_single[isys] << endl;
    m_shifts_single.push_back(functions::calculateRelativeShiftsForAbsoluteSpectra(*m_nominalHistos, *m_sysHistosSingle[isys]));
    m_shifts_relative_single.push_back(functions::calculateRelativeShiftsForRelativeSpectra(*m_nominalHistos, *m_sysHistosSingle[isys]));
  }
  
  prepareRelativeShiftsSignalModeling();
  
}

//----------------------------------------------------------------------

void SystematicHistos::printRelativeShifts(){
  cout << m_shifts_paired.size() << endl;
  const int nbins=m_shifts_paired[0].first->GetNbinsX();
  for(int isys=0;isys<m_nsys_pairs;isys++){
    cout << m_sys_names_pairs[isys].first << ":";
    for(int ibin=1;ibin<=nbins;ibin++) cout << fixed << setprecision(3) <<  "    " << m_shifts_paired[isys].first->GetBinContent(ibin) << " " << m_shifts_paired[isys].second->GetBinContent(ibin);
    cout << endl;
    
    
  }
  
  for(int isys=0;isys<m_nsys_single;isys++){
    cout << m_sys_names_single[isys] << ":";
    for(int ibin=1;ibin<=nbins;ibin++) cout << fixed << setprecision(3) <<  "    " << m_shifts_single[isys]->GetBinContent(ibin);
    cout << endl;
  }
  cout << m_nsys_PDF << " " << m_shifts_PDF.size() << endl;
  for(int isys=1;isys<m_nsys_PDF;isys++){
    cout << m_sys_names_PDF[isys] << ":";
    for(int ibin=1;ibin<=nbins;ibin++) cout << fixed << setprecision(3) <<  "    " << m_shifts_PDF[isys-1]->GetBinContent(ibin);
    cout << endl;
    
    
  }
  
  for(int isys=0;isys<m_nsys_signalModeling;isys++){
    cout << m_sys_names_signalModeling[isys] << ":";
    for(int ibin=1;ibin<=nbins;ibin++) cout << fixed << setprecision(3) <<  "    " << m_shifts_signalModeling[m_sys_names_signalModeling[isys]]->GetBinContent(ibin);
    cout << endl;
  }
  
}
