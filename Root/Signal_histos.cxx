#include "TTbarCovarianceCalculator/Signal_histos.h"
#include "TTbarHelperFunctions/functions.h"
#include "TTbarHelperFunctions/histManipFunctions.h"
ClassImp(Signal_histos)

Signal_histos::Signal_histos(TString path_to_histos): m_path_to_histos(path_to_histos)
{}

Signal_histos::Signal_histos(TString variable,TString Level,TFile* file, TString path_to_histos): m_path_to_histos(path_to_histos)
{
	loadHistos(variable,Level, file);
}


void Signal_histos::loadHistos(TString variable,TString Level,TFile* file){
	cout << "Loading " << variable << " histos at " << Level << endl;
	TString filename=file->GetName();
	filename.ReplaceAll("/","");
	reco = (TH1D*)file->Get(m_path_to_histos + variable + "_RecoLevel");
	//cout << reco->GetName() << endl;
	truth = (TH1D*)file->Get(m_path_to_histos + Level + "CutsPassed_noRecoLevelCuts_" + variable + "_" + Level);
	migration = (TH2D*)file->Get(m_path_to_histos + Level + "CutsPassed_RecoLevelCutsPassed_" + variable + "_migration");
	truth_normalized = (TH1D*)truth->Clone();
	if(variable.Contains("concatenated")){
	  vector<int> nbins;
	  if(Level.Contains("Parton"))nbins={7,8,8,8,7,8,10,8,8,7,4,7,7,6,10}; // number of bins of single variables
	  if(Level.Contains("Particle"))nbins={8,8,7,8,10,8,8,7,4,7,7,6,10};
	  functions::NormalizeConcatenatedHistogram(truth_normalized,nbins);

	}
	else truth_normalized->Scale(1./truth_normalized->Integral());
	
	reco_cuts_correction = migration->ProjectionY("nominator_for_efficiency",1,migration->GetNbinsX(),"e");
	truth_cuts_correction = migration->ProjectionX("nominator_for_acceptance",1,migration->GetNbinsY(),"e");
	reco_cuts_correction->Divide(truth);
	truth_cuts_correction->Divide(reco);
	
	
	
	reco->SetName(filename + reco->GetName());
	truth->SetName(filename + truth->GetName());
	migration->SetName(filename + migration->GetName());
	reco_cuts_correction->SetName(filename + reco_cuts_correction->GetName());
	truth_cuts_correction->SetName(filename + truth_cuts_correction->GetName());
	response = new RooUnfoldResponse(0,0,migration);
	
	//cout << reco->GetName() << endl;
	//cout << truth->GetName() << endl;
	//cout << migration->GetName() << endl;
	//cout << reco_cuts_correction->GetName() << endl;
	//cout << truth_cuts_correction->GetName() << endl;
}

Signal_histos::Signal_histos(const TString variable,const TString level,const TString foldername,TFile* file, TString path_to_histos): m_path_to_histos(path_to_histos)
{
	loadHistos(variable,level,foldername,file);
}
void Signal_histos::loadHistos(const TString variable,const TString level,const TString foldername,TFile* file){
	cout << "Loading " << variable << " histos at " << level << endl;
	TString name=variable;
	name.ReplaceAll("_fine","");
	cout << file->GetName() << endl;
	cout << foldername + "/"+name + "_signal_reco" << endl;
	reco = (TH1D*)file->Get(foldername + "/"+name + "_signal_reco");
	cout << reco->GetName() << endl;
	multijet= (TH1D*)file->Get(foldername + "/"+name + "_bkg_Multijet");
	cout << multijet->GetName() << endl;
	truth= (TH1D*)file->Get(foldername + "/"+name + "_signal_truth");
	migration=(TH2D*)file->Get(foldername + "/"+name + "_migration");
	response=new RooUnfoldResponse(0,0,migration);
	reco_cuts_correction = (TH1D*)file->Get(foldername + "/"+name + "_EffOfRecoLevelCutsNominator");
	reco_cuts_correction->Divide(truth);
	truth_cuts_correction = (TH1D*)file->Get(foldername + "/"+name + "_EffOfTruthLevelCutsNominator");
	truth_cuts_correction->Divide(reco);
	
	truth_normalized = (TH1D*)truth->Clone();
	if(variable.Contains("concatenated")){
	  vector<int> nbins;
	  if(level.Contains("Parton"))nbins={7,8,8,8,7,8,10,8,8,7,4,7,7,6,10}; // number of bins of single variables
	  if(level.Contains("Particle"))nbins={8,8,7,8,10,8,8,7,4,7,7,6,10};
	  functions::NormalizeConcatenatedHistogram(truth_normalized,nbins);

	}
	else truth_normalized->Scale(1./truth_normalized->Integral());
	cout << "End Of Signal_histos::loadHistos() function" << endl;
}

void Signal_histos::rebinHistos(int nbinsx,Double_t* binsx,int nbinsy,Double_t* binsy){
	reco = (TH1D*)reco->Rebin(nbinsx,reco->GetName(),binsx);
	TH2D* migration_rebinned = functions::Rebin2D(migration,nbinsx,binsx,nbinsy,binsy,migration->GetName());
	//delete migration;
	migration = migration_rebinned;
	truth = (TH1D*)truth->Rebin(nbinsy,truth->GetName(),binsy);
	truth_normalized = (TH1D*)truth->Clone();
	truth_normalized->Scale(1./truth_normalized->Integral());
	//delete reco_cuts_correction;
	//delete truth_cuts_correction;
	reco_cuts_correction = migration->ProjectionY(reco_cuts_correction->GetName(),1,migration->GetNbinsX(),"e");
	truth_cuts_correction = migration->ProjectionX(truth_cuts_correction->GetName(),1,migration->GetNbinsY(),"e");
	reco_cuts_correction->Divide(truth);
	truth_cuts_correction->Divide(reco);
	//delete response;
	response = new RooUnfoldResponse(0,0,functions::ProduceHistogramWithSwitchedAxis(migration));
	//response = new RooUnfoldResponse(0,0,migration);
	//cout << reco_cuts_correction->Integral()/reco_cuts_correction->GetNbinsX() << " " << truth_cuts_correction->Integral()/truth_cuts_correction->GetNbinsX() << endl;
}

void Signal_histos::scaleHistos(const double SF){
	reco->Scale(SF);
	migration->Scale(SF);
	truth->Scale(SF);

}
void Signal_histos::divideByBinWidth(){
	functions::DivideByBinWidth(truth);
	functions::DivideByBinWidth(truth_normalized);
}

void Signal_histos::cloneHistos(Signal_histos* cloned,TString name){
	cloned->reco=(TH1D*)reco->Clone((TString)reco->GetName() + name);
	cloned->migration=(TH2D*)migration->Clone((TString)migration->GetName() + name);
	//migration->SetName("");
	cloned->multijet=(TH1D*)truth->Clone((TString)multijet->GetName() + name);
	cloned->truth=(TH1D*)truth->Clone((TString)truth->GetName() + name);
	cloned->truth_normalized=(TH1D*)truth_normalized->Clone((TString)truth_normalized->GetName() + name);
	cloned->reco_cuts_correction=(TH1D*)reco_cuts_correction->Clone((TString)reco_cuts_correction->GetName() + name);
	cloned->truth_cuts_correction=(TH1D*)truth_cuts_correction->Clone((TString)truth_cuts_correction->GetName() + name);
	cloned->response=(RooUnfoldResponse*)response->Clone((TString)response->GetName() + name);
}
	
void Signal_histos::smearHistos(){
	static Int_t ncalls=0;
	ncalls++;
	const int nbinsx = migration->GetXaxis()->GetNbins();
	const int nbinsy = migration->GetYaxis()->GetNbins();
	int i,j;
	
	TH1D* reco_cuts_correction_nominal = migration->ProjectionY("x",1,migration->GetNbinsX(),"e");
	TH1D* truth_cuts_correction_nominal = migration->ProjectionX("y",1,migration->GetNbinsY(),"e");
	
	for(i=1;i<=nbinsx;i++)for(j=1;j<=nbinsy;j++)migration->SetBinContent(i,j,gRandom->Gaus(migration->GetBinContent(i,j),migration->GetBinError(i,j)));
	migration->SetName((TString)migration->GetName() + (TString)ncalls);
	
	delete reco_cuts_correction;
	delete truth_cuts_correction;
	reco_cuts_correction = migration->ProjectionY("x",1,migration->GetNbinsX(),"e");
	truth_cuts_correction = migration->ProjectionX("y",1,migration->GetNbinsY(),"e");
	
	for(j=1;j<=nbinsy;j++){
		double error=sqrt(max(0.,pow(truth->GetBinError(j),2) - pow(reco_cuts_correction_nominal->GetBinError(j),2)));
		truth->SetBinContent(j,gRandom->Gaus(truth->GetBinContent(j) - reco_cuts_correction_nominal->GetBinContent(j),error) + reco_cuts_correction->GetBinContent(j));
	}
	
	
	delete truth_normalized;
	truth_normalized=(TH1D*)truth->Clone();
	truth_normalized->Scale(1./truth_normalized->Integral());
	for(i=1;i<=nbinsx;i++){
		double error=sqrt(max(0.,pow(reco->GetBinError(i),2) - pow(truth_cuts_correction_nominal->GetBinError(i),2)));
		reco->SetBinContent(i,gRandom->Gaus(reco->GetBinContent(i) - truth_cuts_correction_nominal->GetBinContent(i),error) + truth_cuts_correction->GetBinContent(i));
	}
	
	reco_cuts_correction->Divide(truth);
	truth_cuts_correction->Divide(reco);
	//cout << endl << endl << migration->GetName() << endl << endl << endl;
	delete response;
	response = new RooUnfoldResponse(0,0,migration);
	
}
