#include "TTbarCovarianceCalculator/GeneratorSystematicsBase.h"
#include "TTbarHelperFunctions/functions.h"
#include "TTbarHelperFunctions/histManipFunctions.h"
#include "TTbarHelperFunctions/namesAndTitles.h"
#include "TTbarCovarianceCalculator/CovarianceCalculatorFunctions.h"
//#include <iostream>



void GeneratorSystematicsBase::init(const TString& mainDir,const TString& inputdir,const TString& outputdir,const TString& path_config_lumi,const TString& signal,bool doSysPDF,int debugLevel,bool is2D){
  
  m_debug=debugLevel;
  
  m_ttbarConfig= std::make_unique<TTBarConfig>(path_config_lumi);
  
  delete gRandom;
  gRandom = new TRandom3;
  
  m_isConcatenated=false;
  m_is2D=is2D;
  
  m_useNewISR = true;
  
  m_outputdir=outputdir;
  m_inputdir=inputdir;
  m_mainDir=mainDir;
  m_signal=signal;
  
  m_accessors.push_back("ME_sys");
  m_colors.push_back(2);
  m_lineStyles.push_back(1);
  m_legend_entries["ME_sys"]="aMcAtNLO+Pythia8";
  m_foldernames["ME_sys"]=m_ttbarConfig->hardScatteringDirectory();
  
  m_accessors.push_back("PartonShower_sys");
  m_colors.push_back(3);
  m_lineStyles.push_back(1);
  m_legend_entries["PartonShower_sys"]="Powheg+Herwig7";
  m_foldernames["PartonShower_sys"]=m_ttbarConfig->partonShoweringDirectory();
  
  if(m_useNewISR) {
    m_accessors.push_back("ISR_muRup_sys");
    m_colors.push_back(4);
    m_lineStyles.push_back(2);
    m_legend_entries["ISR_muRup_sys"]="Powheg+Pythia8 ISR muR up";
    m_foldernames["ISR_muRup_sys"]=m_ttbarConfig->ISRmuRupDirectory();
    
    m_accessors.push_back("ISR_muRdown_sys");
    m_colors.push_back(5);
    m_lineStyles.push_back(2);
    m_legend_entries["ISR_muRdown_sys"]="Powheg+Pythia8 ISR muR down";
    m_foldernames["ISR_muRdown_sys"]=m_ttbarConfig->ISRmuRdownDirectory();
    
    m_accessors.push_back("ISR_muFup_sys");
    m_colors.push_back(6);
    m_lineStyles.push_back(2);
    m_legend_entries["ISR_muFup_sys"]="Powheg+Pythia8 ISR muF up";
    m_foldernames["ISR_muFup_sys"]=m_ttbarConfig->ISRmuFupDirectory();
    
    m_accessors.push_back("ISR_muFdown_sys");
    m_colors.push_back(9);
    m_lineStyles.push_back(2);
    m_legend_entries["ISR_muFdown_sys"]="Powheg+Pythia8 ISR muF down";
    m_foldernames["ISR_muFdown_sys"]=m_ttbarConfig->ISRmuFdownDirectory();
    
    m_accessors.push_back("ISR_var3cUp_sys");
    m_colors.push_back(11);
    m_lineStyles.push_back(2);
    m_legend_entries["ISR_var3cUp_sys"]="Powheg+Pythia8 ISR Var3c up";
    m_foldernames["ISR_var3cUp_sys"]=m_ttbarConfig->ISRVar3cUpDirectory();
    
    m_accessors.push_back("ISR_var3cDown_sys");
    m_colors.push_back(12);
    m_lineStyles.push_back(2);
    m_legend_entries["ISR_var3cDown_sys"]="Powheg+Pythia8 ISR Var3c down";
    m_foldernames["ISR_var3cDown_sys"]=m_ttbarConfig->ISRVar3cDownDirectory();
  }
  else {
    m_accessors.push_back("ISR_up_sys");
    m_colors.push_back(4);
    m_lineStyles.push_back(2);
    m_legend_entries["ISR_up_sys"]="Powheg+Pythia8 ISR up";
    m_foldernames["ISR_up_sys"]=m_ttbarConfig->ISRupDirectory();
    
    m_accessors.push_back("ISR_down_sys");
    m_colors.push_back(6);
    m_lineStyles.push_back(2);
    m_legend_entries["ISR_down_sys"]="Powheg+Pythia8 ISR down";
    m_foldernames["ISR_down_sys"]=m_ttbarConfig->ISRdownDirectory();
  }
  m_accessors.push_back("FSR_up_sys");
  m_colors.push_back(7);
  m_lineStyles.push_back(4);
  m_legend_entries["FSR_up_sys"]="Powheg+Pythia8 FSR up";
  m_foldernames["FSR_up_sys"]=m_ttbarConfig->FSRupDirectory();
  
  m_accessors.push_back("FSR_down_sys");
  m_colors.push_back(8);
  m_lineStyles.push_back(4);
  m_legend_entries["FSR_down_sys"]="Powheg+Pythia8 FSR down";
  m_foldernames["FSR_down_sys"]=m_ttbarConfig->FSRdownDirectory();
  
  
  
  m_ngenSystematics_histos=m_accessors.size();	
  
  
  m_mc_samples_production = m_ttbarConfig->mc_samples_production();
   
  m_lumi = functions::getLumi(*m_ttbarConfig);
  if(m_lumi < 0.)throw TtbarDiffCrossSection_exception((TString)"Error: Unknown mc samples production"); 
    
  m_dirname=mainDir+"/pdf."+m_mc_samples_production+"/"+m_outputdir+"/";
  gSystem->mkdir(m_dirname,true);
    

  
  //m_cross_sec_unit=1000.; // fb
  m_cross_sec_unit=1.; // pb
  m_lumi/=m_cross_sec_unit;
  
  cout << "GeneratorSystematicsBase::GeneratorSystematicsBase: m_lumi = " << m_lumi << endl;
  cout << "GeneratorSystematicsBase::GeneratorSystematicsBase: Using MC production: " << m_mc_samples_production << endl;
  
  
  m_signal_legend_entry="Powheg+Pythia8 nominal";
  
  
  m_doSysPDF=doSysPDF;
  m_nsys_pdf=0;
  m_nPseudoExperiments=100;
  m_file_systematics=0;
}

void GeneratorSystematicsBase::prepareListOfPdfChains(){
	
  // Creating list of pdf systematics
  vector<TString> sys_names_all;
  functions::GetListOfNames(sys_names_all,m_file_systematics,TDirectory::Class());
  const int nsys_names=sys_names_all.size();
  const TString pdf_subname="PDF4LHC15_nlo_30_eigen";
  for(int i=0;i<nsys_names;i++){
    if(!sys_names_all[i].Contains(pdf_subname))continue;
    m_pdfSystematics_names.push_back(sys_names_all[i]);
  }
  m_nsys_pdf=m_pdfSystematics_names.size();
	
}

void GeneratorSystematicsBase::LoadHistos(const TString& variable,const TString& Level){
	
  if(!m_file_systematics)m_file_systematics=new TFile(m_mainDir+"/" + m_inputdir +"/"+variable+"_"+Level+".root");
  if(m_file_systematics->IsZombie()){
    throw TtbarDiffCrossSection_exception((TString)"Error in loading rootfile with systematics from " + m_mainDir+"/" + m_inputdir +"/" + variable + "_" + Level + ".root"); 
    return;
  }
  m_variable_name=variable+"_" + Level;
  TString name=variable;
  TString rootfileoutputpath = m_mainDir+"/root."+ m_mc_samples_production + "/"+m_outputdir+"/genSystematics_using_signal_"+m_signal; 
  cout << "Creating output file: " << rootfileoutputpath+"/" + name + "_" + Level + ".root" << endl;
  gSystem->mkdir(rootfileoutputpath,true);
  m_outputTFile = std::make_unique<TFile>(rootfileoutputpath+"/" + name + "_" + Level + ".root","RECREATE");
  
  
  if (m_is2D){
    functions::load2DBinning(m_file_systematics, variable,m_binning_x,m_binning_y,m_binning_x_truth,m_binning_y_truth);
    
    
  }
 
  
  
  if(variable.Contains("concatenated")){ // for big covariance with all spectra in one big histogram
    if(Level.Contains("Parton"))m_subHistNbins={7,8,8,8,7,8,10,8,8,7,4,7,7,6,10}; // number of bins of single variables
    if(Level.Contains("Particle"))m_subHistNbins={8,8,7,8,10,8,8,7,4,7,7,6,10};
    m_isConcatenated=true; // Letting know that we are using concatenated histogram
    cout << "Number of subhistograms " << m_subHistNbins.size() << endl;
  }
  
  
  m_nominal_histos = new Signal_histos(name,Level,"nominal",m_file_systematics);
  m_MECOffSample_histos = new Signal_histos(name,Level,m_ttbarConfig->PhPy8MECoffDirectory(),m_file_systematics);
  for(int i=0;i<m_ngenSystematics_histos;i++){
    m_genSystematic_histos[m_accessors[i]] = new Signal_histos(name,Level,m_foldernames[m_accessors[i]],m_file_systematics);
  }
  
  if(m_doSysPDF){
    prepareListOfPdfChains();
    //cout << m_nsys_pdf << endl;
    //for(int i=0;i<m_nsys_pdf;i++)cout << m_pdfSystematics_names[i] << endl;
    if(m_nsys_pdf>0){
      m_pdfSystematics_histos.resize(m_nsys_pdf);
      m_pdfCovariancesNormalized.resize(m_nsys_pdf-1);
      m_pdfCovariances.resize(m_nsys_pdf-1);
      m_pdfCovariancesAlternative.resize(m_nsys_pdf-1);
      m_pdfCovariancesNormalizedAlternative.resize(m_nsys_pdf-1);
      for(int i=0;i<m_nsys_pdf;i++)m_pdfSystematics_histos[i] = new Signal_histos(name,Level,m_pdfSystematics_names[i],m_file_systematics);
    }
  }
  cout << "End Of GeneratorSystematicsBase::LoadHistos() function" << endl;
}

void GeneratorSystematicsBase::LoadDataHistos(const TString& variable,const TString& Level){
  if(!m_file_systematics)m_file_systematics = new TFile(m_mainDir+"/" + m_inputdir +"/"+variable+"_"+Level+".root");
  if(m_file_systematics->IsZombie()){
    throw TtbarDiffCrossSection_exception((TString)"Error in loading rootfile with data from " + m_mainDir+"/root/systematics_histos/" + variable + "_" + Level + ".root"); 
    return;
  }
  
  const TString histpath="nominal";
  cout << histpath + "/" + variable + "_data" << endl;
  m_hist_data_reco = (TH1D*)m_file_systematics->Get(histpath + "/" + variable + "_data");
  //cout << m_hist_data_reco->GetName() << endl;
  m_hist_data_reco->Add((TH1D*)m_file_systematics->Get(histpath+ "/" + variable + "_bkg_all"),-1);
  
  m_outputTFile->cd();
  
  m_hist_data_unfolded = this->Unfold(m_hist_data_reco,m_nominal_histos);
  m_hist_data_unfolded_normalized = (TH1D*)m_hist_data_unfolded->Clone();
  if(m_isConcatenated)functions::NormalizeConcatenatedHistogram(m_hist_data_unfolded_normalized,m_subHistNbins);
  else m_hist_data_unfolded_normalized->Scale(1./m_hist_data_unfolded_normalized->Integral());
  
  m_hist_data_unfolded->Scale(1./m_lumi);
  
  functions::DivideByBinWidth(m_hist_data_unfolded_normalized);
  functions::DivideByBinWidth(m_hist_data_unfolded);
  
  delete m_file_systematics;
}


void GeneratorSystematicsBase::RebinHistos(int nbinsx,Double_t* binsx,int nbinsy,Double_t* binsy){
	
  m_nominal_histos->rebinHistos(nbinsx,binsx,nbinsy,binsy);
  for(int i=0;i<m_ngenSystematics_histos;i++){
    m_genSystematic_histos[m_accessors[i]]->rebinHistos(nbinsx,binsx,nbinsy,binsy);
  }

}
void GeneratorSystematicsBase::prepareHistos(){
  m_nominal_histos->divideByBinWidth();
  for(int i=0;i<m_ngenSystematics_histos;i++){
    m_genSystematic_histos[m_accessors[i]]->divideByBinWidth();
  }
  cout << m_nsys_pdf << endl;
  for(int i=0;i<m_nsys_pdf;i++){
    m_pdfSystematics_histos[i]->divideByBinWidth();
  }
	
}
void GeneratorSystematicsBase::CalculateSysErrors(){
	
  CalculatePartonShowerSysErrors();
  CalculateMatrixElementSysErrors();
  CalculateIFSRSysErrors();
  
  if(m_doSysPDF) CalculatePdfSysErrors();
  
  CalculateTotalCovariances();
  
  const int nbins_truth=m_nominal_histos->truth->GetNbinsX();
  
  for(const string& acc : m_accessors) {
    m_genSystematic_errors[acc] = (TH1D*)m_nominal_histos->truth->Clone();
    m_genSystematic_errors_normalized[acc] = (TH1D*)m_nominal_histos->truth->Clone();
  }
  
  m_genSystematic_errors["total"] = (TH1D*)m_nominal_histos->truth->Clone();
  m_genSystematic_errors_normalized["total"] = (TH1D*)m_nominal_histos->truth->Clone();
 
  
  
  const int nsys = m_accessors.size();
  
  for(int i=0;i<nbins_truth;i++) {
    const int ibin = i+1;
    
    const double centralValue=m_hist_data_unfolded->GetBinContent(ibin);
    const double centralValueNormalized=m_hist_data_unfolded_normalized->GetBinContent(ibin);
    
    if(centralValue>0.) {
    
      m_genSystematic_errors["total"]->SetBinContent(ibin,sqrt(m_covariances["Total"][i][i])/centralValue);
      double totalUnc=0.;
      
      for(int isys=0;isys<nsys;isys++) {
	const double mean = m_mean_of_errors[isys][i];
	const double unc = m_errors_of_errors[isys][i];
	totalUnc = sqrt(totalUnc*totalUnc + unc*unc);
	
	m_genSystematic_errors[m_accessors[isys]]->SetBinContent(ibin,mean);
	m_genSystematic_errors[m_accessors[isys]]->SetBinError(ibin,unc);
	
      }
      
    }
    
    if(centralValueNormalized>0.){      
      m_genSystematic_errors_normalized["total"]->SetBinContent(i+1,sqrt(m_covariances["Total_normalized"][i][i])/centralValueNormalized);
      
      double totalUnc=0.;
      
      for(int isys=0;isys<nsys;isys++) {
	double mean = m_mean_of_errors_normalized[isys][i];
	double unc = m_errors_of_errors_normalized[isys][i];
	totalUnc = sqrt(totalUnc*totalUnc + unc*unc);
	
	m_genSystematic_errors_normalized[m_accessors[isys]]->SetBinContent(ibin,mean);
	m_genSystematic_errors_normalized[m_accessors[isys]]->SetBinError(ibin,unc);
	
      }
      
    }
    
  }
  
}

//----------------------------------------------------------------------

void GeneratorSystematicsBase::printUncertainties() {
  cout << endl << "Printing relative errors from not-normalized distributions" << endl;
  
  const int nbins_truth=m_nominal_histos->truth->GetNbinsX();
  
  cout << endl << "Parton shower relative sys errors " << endl;
  for(int i=0;i<nbins_truth;i++) cout << sqrt(m_covariances["PS"][i][i])/m_hist_data_unfolded->GetBinContent(i+1) << " ";
  
  cout << endl << "Matrix element sys errors " << endl;
  for(int i=0;i<nbins_truth;i++) cout << sqrt(m_covariances["ME"][i][i])/m_hist_data_unfolded->GetBinContent(i+1) << " ";
  
  if(m_useNewISR) {
    cout << endl << "ISR muR sys errors " << endl;
    for(int i=0;i<nbins_truth;i++) cout << sqrt(m_covariances["ISR_muR"][i][i])/m_hist_data_unfolded->GetBinContent(i+1) << " ";
    cout << endl << "ISR muF sys errors " << endl;
    for(int i=0;i<nbins_truth;i++) cout << sqrt(m_covariances["ISR_muF"][i][i])/m_hist_data_unfolded->GetBinContent(i+1) << " ";
    cout << endl << "ISR var3c sys errors " << endl;
    for(int i=0;i<nbins_truth;i++) cout << sqrt(m_covariances["ISR_var3c"][i][i])/m_hist_data_unfolded->GetBinContent(i+1) << " ";
  }
  else {
    cout << endl << "ISR sys errors " << endl;
    for(int i=0;i<nbins_truth;i++) cout << sqrt(m_covariances["ISR"][i][i])/m_hist_data_unfolded->GetBinContent(i+1) << " ";
  }
  
  
  cout << endl << "Total gen sys errors " << endl;
  for(int i=0;i<nbins_truth;i++) cout << sqrt(m_covariances["Total"][i][i])/m_hist_data_unfolded->GetBinContent(i+1) << " ";
  cout << endl;
  cout << endl << "Total gen sys errors alternative " << endl;
  for(int i=0;i<nbins_truth;i++) cout << sqrt(m_covariances["Total_Alternative"][i][i])/m_hist_data_unfolded->GetBinContent(i+1) << " ";
  cout << endl;
  
  
  cout << endl << "Printing relative errors from normalized distributions" << endl;
  cout << endl << "Parton shower relative sys errors " << endl;
  for(int i=0;i<nbins_truth;i++) cout << sqrt(m_covariances["PS_normalized"][i][i])/m_hist_data_unfolded_normalized->GetBinContent(i+1) << " ";
  
  cout << endl << "Matrix element sys errors " << endl;
  for(int i=0;i<nbins_truth;i++) cout << sqrt(m_covariances["ME_normalized"][i][i])/m_hist_data_unfolded_normalized->GetBinContent(i+1) << " ";
  
  if(m_useNewISR) {
    cout << endl << "ISR muR sys errors " << endl;
    for(int i=0;i<nbins_truth;i++) cout << sqrt(m_covariances["ISR_muR_normalized"][i][i])/m_hist_data_unfolded->GetBinContent(i+1) << " ";
    cout << endl << "ISR muF sys errors " << endl;
    for(int i=0;i<nbins_truth;i++) cout << sqrt(m_covariances["ISR_muF_normalized"][i][i])/m_hist_data_unfolded->GetBinContent(i+1) << " ";
    cout << endl << "ISR var3c sys errors " << endl;
    for(int i=0;i<nbins_truth;i++) cout << sqrt(m_covariances["ISR_var3c_normalized"][i][i])/m_hist_data_unfolded->GetBinContent(i+1) << " ";
  }
  else {
    cout << endl << "ISR sys errors " << endl;
    for(int i=0;i<nbins_truth;i++) cout << sqrt(m_covariances["ISR_normalized"][i][i])/m_hist_data_unfolded_normalized->GetBinContent(i+1) << " ";
  }
  
  
  cout << endl << "Total gen sys errors " << endl;
  for(int i=0;i<nbins_truth;i++) cout << sqrt(m_covariances["Total_normalized"][i][i])/m_hist_data_unfolded_normalized->GetBinContent(i+1) << " ";
  cout << endl;
  
  cout << endl << "Total gen sys errors alternative " << endl;
  for(int i=0;i<nbins_truth;i++) cout << sqrt(m_covariances["Total_Alternative_normalized"][i][i])/m_hist_data_unfolded_normalized->GetBinContent(i+1) << " ";
  cout << endl;
  
  if(m_doSysPDF){
    cout << endl << "Total pdf sys errors " << endl;
    for(int i=0;i<nbins_truth;i++) cout << sqrt(m_pdfTotalCovariance[i][i])/m_hist_data_unfolded->GetBinContent(i+1) << " ";
    cout << endl;
    
    cout << endl << "Total pdf sys errors alternative" << endl;
    for(int i=0;i<nbins_truth;i++) cout << sqrt(m_pdfTotalCovarianceAlternative[i][i])/m_hist_data_unfolded->GetBinContent(i+1) << " ";
    cout << endl;
    
    cout << endl << "Total pdf sys errors normalized " << endl;
    for(int i=0;i<nbins_truth;i++) cout << sqrt(m_pdfTotalCovarianceNormalized[i][i])/m_hist_data_unfolded_normalized->GetBinContent(i+1) << " ";
    cout << endl;
    
    cout << endl << "Total pdf sys errors normalized alternative" << endl;
    for(int i=0;i<nbins_truth;i++) cout << sqrt(m_pdfTotalCovarianceNormalizedAlternative[i][i])/m_hist_data_unfolded_normalized->GetBinContent(i+1) << " ";
    cout << endl;
  }
  
  cout << endl << "Relative uncertainties for Signal modeling Plus PDF systematics for absolute spectrum" << endl;
  for(int i=0;i<nbins_truth;i++) cout << sqrt(m_covariances["SignalModelingPlusPDFCovariance"][i][i])/m_hist_data_unfolded->GetBinContent(i+1) << " ";
  cout << endl;
  
  cout << endl << "Relative uncertainties for Signal modeling Plus PDF systematics for absolute spectrum: Alternative method" << endl;
  for(int i=0;i<nbins_truth;i++) cout << sqrt(m_covariances["SignalModelingPlusPDFCovariance_Alternative"][i][i])/m_hist_data_unfolded->GetBinContent(i+1) << " ";
  cout << endl;
  
  cout << endl << "Relative uncertainties for Signal modeling Plus PDF systematics for relative spectrum" << endl;
  for(int i=0;i<nbins_truth;i++) cout << sqrt(m_covariances["SignalModelingPlusPDFCovariance_normalized"][i][i])/m_hist_data_unfolded_normalized->GetBinContent(i+1) << " ";
  cout << endl;
  cout << endl << "Relative uncertainties for Signal modeling Plus PDF systematics for relative spectrum: Alternative method" << endl;
  for(int i=0;i<nbins_truth;i++) cout << sqrt(m_covariances["SignalModelingPlusPDFCovariance_Alternative_normalized"][i][i])/m_hist_data_unfolded_normalized->GetBinContent(i+1) << " ";
  cout << endl;
  
  
  cout << endl << "Central values absolute " << endl;
  for(int i=0;i<nbins_truth;i++) cout << m_hist_data_unfolded->GetBinContent(i+1) << " ";
  cout << endl;
  
  cout << endl << "Central values relative " << endl;
  for(int i=0;i<nbins_truth;i++) cout << m_hist_data_unfolded_normalized->GetBinContent(i+1) << " ";
  cout << endl;
  
}

//----------------------------------------------------------------------

void GeneratorSystematicsBase::CalculateTotalCovariances(){
  //cout << "Adding covariances" << endl;
  vector<TMatrixT<double> > covariancesVector;
  covariancesVector.push_back(m_covariances["PS"]);
  covariancesVector.push_back(m_covariances["ME"]);
  if(m_useNewISR) {
    covariancesVector.push_back(m_covariances["ISR_muR"]);
    covariancesVector.push_back(m_covariances["ISR_muF"]);
    covariancesVector.push_back(m_covariances["ISR_var3c"]);
  }
  else {
    covariancesVector.push_back(m_covariances["ISR"]);
  }
  covariancesVector.push_back(m_covariances["FSR"]);
  functions::AddCovariances(covariancesVector,m_covariances["Total"]);
  //cout << "Adding normalized covariances" << endl;
  covariancesVector.clear();
  covariancesVector.push_back(m_covariances["PS_normalized"]);
  covariancesVector.push_back(m_covariances["ME_normalized"]);
  if(m_useNewISR) {
    covariancesVector.push_back(m_covariances["ISR_muR_normalized"]);
    covariancesVector.push_back(m_covariances["ISR_muF_normalized"]);
    covariancesVector.push_back(m_covariances["ISR_var3c_normalized"]);
  }
  else {
    covariancesVector.push_back(m_covariances["ISR_normalized"]);
  }
  covariancesVector.push_back(m_covariances["FSR_normalized"]);
  functions::AddCovariances(covariancesVector,m_covariances["Total_normalized"]);
  //cout << "Adding alternative covariances" << endl;
  covariancesVector.clear();
  covariancesVector.push_back(m_covariances["PS_Alternative"]);
  covariancesVector.push_back(m_covariances["ME_Alternative"]);
  if(m_useNewISR) {
    covariancesVector.push_back(m_covariances["ISR_muR_Alternative"]);
    covariancesVector.push_back(m_covariances["ISR_muF_Alternative"]);
    covariancesVector.push_back(m_covariances["ISR_var3c_Alternative"]);
  }
  else {
    covariancesVector.push_back(m_covariances["ISR_Alternative"]);
  }
  covariancesVector.push_back(m_covariances["FSR_Alternative"]);
  functions::AddCovariances(covariancesVector,m_covariances["Total_Alternative"]);
  //cout << "Adding alternative normalized covariances" << endl;
  covariancesVector.clear();
  covariancesVector.push_back(m_covariances["PS_Alternative_normalized"]);
  covariancesVector.push_back(m_covariances["ME_Alternative_normalized"]);
  if(m_useNewISR) {
    covariancesVector.push_back(m_covariances["ISR_muR_Alternative_normalized"]);
    covariancesVector.push_back(m_covariances["ISR_muF_Alternative_normalized"]);
    covariancesVector.push_back(m_covariances["ISR_var3c_Alternative_normalized"]);
  }
  else {
    covariancesVector.push_back(m_covariances["ISR_Alternative_normalized"]);
  }
  covariancesVector.push_back(m_covariances["FSR_Alternative_normalized"]);
  functions::AddCovariances(covariancesVector,m_covariances["Total_Alternative_normalized"]);
  
  // Creating total covariances for pdf systematics
  
  if(m_doSysPDF){
    functions::AddCovariances(m_pdfCovariances,m_pdfTotalCovariance);
    functions::AddCovariances(m_pdfCovariancesNormalized,m_pdfTotalCovarianceNormalized);
	    
    functions::AddCovariances(m_pdfCovariancesAlternative,m_pdfTotalCovarianceAlternative);
    functions::AddCovariances(m_pdfCovariancesNormalizedAlternative,m_pdfTotalCovarianceNormalizedAlternative);
    
  }
  
  const int nbins=m_covariances["Total"].GetNcols();
  
  m_covariances["SignalModelingPlusPDFCovariance"].ResizeTo(nbins,nbins);
  m_covariances["SignalModelingPlusPDFCovariance"]=m_covariances["Total"];
  if(m_doSysPDF) m_covariances["SignalModelingPlusPDFCovariance"] += m_pdfTotalCovariance;
  m_covariances["SignalModelingPlusPDFCovariance_Alternative"].ResizeTo(nbins,nbins);
  m_covariances["SignalModelingPlusPDFCovariance_Alternative"]=m_covariances["Total_Alternative"];
  if(m_doSysPDF) m_covariances["SignalModelingPlusPDFCovariance_Alternative"] += m_pdfTotalCovarianceAlternative;
  
  m_covariances["SignalModelingPlusPDFCovariance_normalized"].ResizeTo(nbins,nbins);
  m_covariances["SignalModelingPlusPDFCovariance_normalized"]=m_covariances["Total_normalized"];
  if(m_doSysPDF) m_covariances["SignalModelingPlusPDFCovariance_normalized"]+=m_pdfTotalCovarianceNormalized;
  m_covariances["SignalModelingPlusPDFCovariance_Alternative_normalized"].ResizeTo(nbins,nbins);
  m_covariances["SignalModelingPlusPDFCovariance_Alternative_normalized"]=m_covariances["Total_Alternative_normalized"];
  if(m_doSysPDF) m_covariances["SignalModelingPlusPDFCovariance_Alternative_normalized"]+=m_pdfTotalCovarianceNormalizedAlternative;
	
  m_covariances["SignalModelingPlusPDFCovariance"].ResizeTo(nbins,nbins);
  m_covariances["SignalModelingPlusPDFCovariance"]=m_covariances["Total"];
  if(m_doSysPDF) m_covariances["SignalModelingPlusPDFCovariance"] += m_pdfTotalCovariance;
  m_covariances["SignalModelingPlusPDFCovariance_Alternative"].ResizeTo(nbins,nbins);
  m_covariances["SignalModelingPlusPDFCovariance_Alternative"]=m_covariances["Total_Alternative"];
  if(m_doSysPDF) m_covariances["SignalModelingPlusPDFCovariance_Alternative"] += m_pdfTotalCovarianceAlternative;
  
  m_covariances["SignalModelingPlusPDFCovariance_normalized"].ResizeTo(nbins,nbins);
  m_covariances["SignalModelingPlusPDFCovariance_normalized"]=m_covariances["Total_normalized"];
  if(m_doSysPDF) m_covariances["SignalModelingPlusPDFCovariance_normalized"]+=m_pdfTotalCovarianceNormalized;
  m_covariances["SignalModelingPlusPDFCovariance_Alternative_normalized"].ResizeTo(nbins,nbins);
  m_covariances["SignalModelingPlusPDFCovariance_Alternative_normalized"]=m_covariances["Total_Alternative_normalized"];
  if(m_doSysPDF) m_covariances["SignalModelingPlusPDFCovariance_Alternative_normalized"]+=m_pdfTotalCovarianceNormalizedAlternative;
	
}

void GeneratorSystematicsBase::writeCovariances(){

  m_outputTFile->cd();
  
  m_covariances["Total"].Write((TString)"totalCovariance_"+m_variable_name);
  m_covariances["PS"].Write((TString)"PartonShowerCovariance_"+m_variable_name);
  m_covariances["ME"].Write((TString)"MatrixElementCovariance_"+m_variable_name);
  if(m_useNewISR) {
    m_covariances["ISR_muR"].Write((TString)"ISR_muR_Covariance_"+m_variable_name);
    m_covariances["ISR_muF"].Write((TString)"ISR_muF_Covariance_"+m_variable_name);
    m_covariances["ISR_var3c"].Write((TString)"ISR_var3c_Covariance_"+m_variable_name);
  }
  else {
    m_covariances["ISR"].Write((TString)"ISRCovariance_"+m_variable_name);
  }
  m_covariances["FSR"].Write((TString)"FSRCovariance_"+m_variable_name);
  
  m_covariances["Total_normalized"].Write((TString)"totalCovariance_normalized_"+m_variable_name);
  m_covariances["PS_normalized"].Write((TString)"PartonShowerCovariance_normalized_"+m_variable_name);
  m_covariances["ME_normalized"].Write((TString)"MatrixElementCovariance_normalized_"+m_variable_name);
  if(m_useNewISR) {
    m_covariances["ISR_muR_normalized"].Write((TString)"ISR_muR_Covariance_normalized_"+m_variable_name);
    m_covariances["ISR_muF_normalized"].Write((TString)"ISR_muF_Covariance_normalized_"+m_variable_name);
    m_covariances["ISR_var3c_normalized"].Write((TString)"ISR_var3c_Covariance_normalized_"+m_variable_name);
  }
  else {
    m_covariances["ISR_normalized"].Write((TString)"ISRCovariance_normalized_"+m_variable_name);
  }
  m_covariances["FSR_normalized"].Write((TString)"FSRCovariance_normalized_"+m_variable_name);
  
  m_covariances["Total_Alternative"].Write((TString)"totalCovariance_Alternative_"+m_variable_name);
  m_covariances["PS_Alternative"].Write((TString)"PartonShowerCovariance_Alternative_"+m_variable_name);
  m_covariances["ME_Alternative"].Write((TString)"MatrixElementCovariance_Alternative_"+m_variable_name);
  if(m_useNewISR) {
    m_covariances["ISR_muR_Alternative"].Write((TString)"ISR_muR_Covariance_Alternative_"+m_variable_name);
    m_covariances["ISR_muF_Alternative"].Write((TString)"ISR_muF_Covariance_Alternative_"+m_variable_name);
    m_covariances["ISR_var3c_Alternative"].Write((TString)"ISR_var3c_Covariance_Alternative_"+m_variable_name);
  }
  else {
    m_covariances["ISR_Alternative"].Write((TString)"ISRCovariance_Alternative_"+m_variable_name);
  }
  m_covariances["FSR_Alternative"].Write((TString)"FSRCovariance_Alternative_"+m_variable_name);
  
  m_covariances["Total_Alternative_normalized"].Write((TString)"totalCovariance_Alternative_normalized_"+m_variable_name);
  m_covariances["PS_Alternative_normalized"].Write((TString)"PartonShowerCovariance_Alternative_normalized_"+m_variable_name);
  m_covariances["ME_Alternative_normalized"].Write((TString)"MatrixElementCovariance_Alternative_normalized_"+m_variable_name);
  if(m_useNewISR) {
    m_covariances["ISR_muR_Alternative_normalized"].Write((TString)"ISR_muR_Covariance_normalized_Alternative_"+m_variable_name);
    m_covariances["ISR_muF_Alternative_normalized"].Write((TString)"ISR_muF_Covariance_normalized_Alternative_"+m_variable_name);
    m_covariances["ISR_var3c_Alternative_normalized"].Write((TString)"ISR_var3c_Covariance_normalized_Alternative_"+m_variable_name);
  }
  else {
    m_covariances["ISR_Alternative_normalized"].Write((TString)"ISRCovariance_normalized_Alternative_"+m_variable_name);
  }
  m_covariances["FSR_Alternative_normalized"].Write((TString)"FSRCovariance_normalized_Alternative_"+m_variable_name);

  if(m_doSysPDF){
    m_pdfTotalCovariance.Write((TString)"totalPDFCovariance_"+m_variable_name);
    m_pdfTotalCovarianceNormalized.Write((TString)"totalPDFCovariance_normalized_"+m_variable_name);
  
    m_pdfTotalCovarianceAlternative.Write((TString)"totalPDFCovariance_Alternative_"+m_variable_name);
    m_pdfTotalCovarianceNormalizedAlternative.Write((TString)"totalPDFCovariance_Alternative_normalized_"+m_variable_name);
  }
  
  m_covariances["SignalModelingPlusPDFCovariance"].Write((TString)"SignalModelingPlusPDFCovariance_"+m_variable_name);
  m_covariances["SignalModelingPlusPDFCovariance_Alternative"].Write((TString)"SignalModelingPlusPDFCovariance_Alternative_"+m_variable_name);
  
  m_covariances["SignalModelingPlusPDFCovariance_normalized"].Write((TString)"SignalModelingPlusPDFCovariance_normalized_"+m_variable_name);
  m_covariances["SignalModelingPlusPDFCovariance_Alternative_normalized"].Write((TString)"SignalModelingPlusPDFCovariance_Alternative_normalized_"+m_variable_name);

}

void GeneratorSystematicsBase::CalculateIFSRSysErrors(){
  cout << endl << "GeneratorSystematics::CalculateIFSRSysErrors" << endl;

  string sysname_up="FSR_up_sys";
  string sysname_down="FSR_down_sys";
  bool useMultijetBkg=true;
  bool useAlternative=false;

  CalculateIFSRSysErrors(m_genSystematic_histos[sysname_up],m_genSystematic_histos[sysname_down],m_nominal_histos,m_hist_data_unfolded,m_covariances["FSR"],useMultijetBkg,useAlternative);
  CalculateNormalizedIFSRSysErrors(m_genSystematic_histos[sysname_up],m_genSystematic_histos[sysname_down],m_nominal_histos,m_hist_data_unfolded_normalized,m_covariances["FSR_normalized"],useMultijetBkg,useAlternative);
  useAlternative=true;
  CalculateIFSRSysErrors(m_genSystematic_histos[sysname_up],m_genSystematic_histos[sysname_down],m_nominal_histos,m_hist_data_unfolded,m_covariances["FSR_Alternative"],useMultijetBkg,useAlternative);
  CalculateNormalizedIFSRSysErrors(m_genSystematic_histos[sysname_up],m_genSystematic_histos[sysname_down],m_nominal_histos,m_hist_data_unfolded_normalized,m_covariances["FSR_Alternative_normalized"],useMultijetBkg,useAlternative);  
  
  if(m_useNewISR) {
    
    sysname_up="ISR_muRup_sys";
    sysname_down="ISR_muRdown_sys";
    
    useAlternative=false;
    CalculateIFSRSysErrors(m_genSystematic_histos[sysname_up],m_genSystematic_histos[sysname_down],m_nominal_histos,m_hist_data_unfolded,m_covariances["ISR_muR"],useMultijetBkg,useAlternative);
    CalculateNormalizedIFSRSysErrors(m_genSystematic_histos[sysname_up],m_genSystematic_histos[sysname_down],m_nominal_histos,m_hist_data_unfolded_normalized,m_covariances["ISR_muR_normalized"],useMultijetBkg,useAlternative);
    useAlternative=true;
    CalculateIFSRSysErrors(m_genSystematic_histos[sysname_up],m_genSystematic_histos[sysname_down],m_nominal_histos,m_hist_data_unfolded,m_covariances["ISR_muR_Alternative"],useMultijetBkg,useAlternative);
    CalculateNormalizedIFSRSysErrors(m_genSystematic_histos[sysname_up],m_genSystematic_histos[sysname_down],m_nominal_histos,m_hist_data_unfolded_normalized,m_covariances["ISR_muR_Alternative_normalized"],useMultijetBkg,useAlternative);

    sysname_up="ISR_muFup_sys";
    sysname_down="ISR_muFdown_sys";
    
    useAlternative=false;
    CalculateIFSRSysErrors(m_genSystematic_histos[sysname_up],m_genSystematic_histos[sysname_down],m_nominal_histos,m_hist_data_unfolded,m_covariances["ISR_muF"],useMultijetBkg,useAlternative);
    CalculateNormalizedIFSRSysErrors(m_genSystematic_histos[sysname_up],m_genSystematic_histos[sysname_down],m_nominal_histos,m_hist_data_unfolded_normalized,m_covariances["ISR_muF_normalized"],useMultijetBkg,useAlternative);
    useAlternative=true;
    CalculateIFSRSysErrors(m_genSystematic_histos[sysname_up],m_genSystematic_histos[sysname_down],m_nominal_histos,m_hist_data_unfolded,m_covariances["ISR_muF_Alternative"],useMultijetBkg,useAlternative);
    CalculateNormalizedIFSRSysErrors(m_genSystematic_histos[sysname_up],m_genSystematic_histos[sysname_down],m_nominal_histos,m_hist_data_unfolded_normalized,m_covariances["ISR_muF_Alternative_normalized"],useMultijetBkg,useAlternative);
    
    sysname_up="ISR_var3cUp_sys";
    sysname_down="ISR_var3cDown_sys";
    
    useAlternative=false;
    CalculateIFSRSysErrors(m_genSystematic_histos[sysname_up],m_genSystematic_histos[sysname_down],m_nominal_histos,m_hist_data_unfolded,m_covariances["ISR_var3c"],useMultijetBkg,useAlternative);
    CalculateNormalizedIFSRSysErrors(m_genSystematic_histos[sysname_up],m_genSystematic_histos[sysname_down],m_nominal_histos,m_hist_data_unfolded_normalized,m_covariances["ISR_var3c_normalized"],useMultijetBkg,useAlternative);
    useAlternative=true;
    CalculateIFSRSysErrors(m_genSystematic_histos[sysname_up],m_genSystematic_histos[sysname_down],m_nominal_histos,m_hist_data_unfolded,m_covariances["ISR_var3c_Alternative"],useMultijetBkg,useAlternative);
    CalculateNormalizedIFSRSysErrors(m_genSystematic_histos[sysname_up],m_genSystematic_histos[sysname_down],m_nominal_histos,m_hist_data_unfolded_normalized,m_covariances["ISR_var3c_Alternative_normalized"],useMultijetBkg,useAlternative);
    
  }
  else {
    sysname_up="ISR_up_sys";
    sysname_down="ISR_down_sys";
    
    useAlternative=false;
    CalculateIFSRSysErrors(m_genSystematic_histos[sysname_up],m_genSystematic_histos[sysname_down],m_nominal_histos,m_hist_data_unfolded,m_covariances["ISR"],useMultijetBkg,useAlternative);
    CalculateNormalizedIFSRSysErrors(m_genSystematic_histos[sysname_up],m_genSystematic_histos[sysname_down],m_nominal_histos,m_hist_data_unfolded_normalized,m_covariances["ISR_normalized"],useMultijetBkg,useAlternative);
    useAlternative=true;
    CalculateIFSRSysErrors(m_genSystematic_histos[sysname_up],m_genSystematic_histos[sysname_down],m_nominal_histos,m_hist_data_unfolded,m_covariances["ISR_Alternative"],useMultijetBkg,useAlternative);
    CalculateNormalizedIFSRSysErrors(m_genSystematic_histos[sysname_up],m_genSystematic_histos[sysname_down],m_nominal_histos,m_hist_data_unfolded_normalized,m_covariances["ISR_Alternative_normalized"],useMultijetBkg,useAlternative);
  }
}
void GeneratorSystematicsBase::CalculatePartonShowerSysErrors(){
  cout << endl << "GeneratorSystematicsBase::CalculatePartonShowerSysErrors" << endl;
  string sysname="PartonShower_sys";
  bool useMultijetBkg=true;
  CalculateSysErrors(m_genSystematic_histos[sysname],m_nominal_histos,m_hist_data_unfolded,m_covariances["PS"],useMultijetBkg);
  CalculateNormalizedSysErrors(m_genSystematic_histos[sysname],m_nominal_histos,m_hist_data_unfolded_normalized,m_covariances["PS_normalized"],useMultijetBkg);
  
  CalculateSysErrors(m_nominal_histos,m_genSystematic_histos[sysname],m_hist_data_unfolded,m_covariances["PS_Alternative"],useMultijetBkg);
  CalculateNormalizedSysErrors(m_nominal_histos,m_genSystematic_histos[sysname],m_hist_data_unfolded_normalized,m_covariances["PS_Alternative_normalized"],useMultijetBkg);
  
}
void GeneratorSystematicsBase::CalculateMatrixElementSysErrors(){
  cout << endl << "GeneratorSystematicsBase::CalculateMatrixElementSysErrors" << endl;
  string sysname="ME_sys";
  bool useMultijetBkg=true;
  CalculateSysErrors(m_genSystematic_histos[sysname],m_MECOffSample_histos,m_hist_data_unfolded,m_covariances["ME"],useMultijetBkg);
  CalculateNormalizedSysErrors(m_genSystematic_histos[sysname],m_MECOffSample_histos,m_hist_data_unfolded_normalized,m_covariances["ME_normalized"],useMultijetBkg);
  
  CalculateSysErrors(m_MECOffSample_histos,m_genSystematic_histos[sysname],m_hist_data_unfolded,m_covariances["ME_Alternative"],useMultijetBkg);
  CalculateNormalizedSysErrors(m_MECOffSample_histos,m_genSystematic_histos[sysname],m_hist_data_unfolded_normalized,m_covariances["ME_Alternative_normalized"],useMultijetBkg);
  
}



void GeneratorSystematicsBase::CalculatePdfSysErrors(){

  bool useMultijetBkg=true;
  const int nbins=m_nominal_histos->truth->GetNbinsX();
  for(int i=1;i<m_nsys_pdf;i++){
    // Current approach - reco and truth is shifted
    CalculateSysErrors(m_pdfSystematics_histos[i],m_pdfSystematics_histos[0],m_hist_data_unfolded,m_pdfCovariances[i-1],useMultijetBkg);
    CalculateNormalizedSysErrors(m_pdfSystematics_histos[i],m_pdfSystematics_histos[0],m_hist_data_unfolded_normalized,m_pdfCovariancesNormalized[i-1],useMultijetBkg);
    //Alternative approach - migrations are shifted
    CalculateSysErrors(m_pdfSystematics_histos[0],m_pdfSystematics_histos[i],m_hist_data_unfolded,m_pdfCovariancesAlternative[i-1],useMultijetBkg);
    CalculateNormalizedSysErrors(m_pdfSystematics_histos[0],m_pdfSystematics_histos[i],m_hist_data_unfolded_normalized,m_pdfCovariancesNormalizedAlternative[i-1],useMultijetBkg);
    cout << "Printing pdf sys errors for eigenvector " << i << endl;
    for(int ibin=0;ibin<nbins;ibin++) cout << sqrt(m_pdfCovariances[i-1][ibin][ibin])/m_hist_data_unfolded->GetBinContent(ibin+1) << " ";
    cout << endl;
    cout << "Printing normalized pdf sys errors for eigenvector " << i << endl;
    for(int ibin=0;ibin<nbins;ibin++) cout << sqrt(m_pdfCovariancesNormalized[i-1][ibin][ibin])/m_hist_data_unfolded_normalized->GetBinContent(ibin+1) << " ";
    cout << endl;
    cout << "Printing pdf alternative sys errors for eigenvector " << i << endl;
    for(int ibin=0;ibin<nbins;ibin++) cout << sqrt(m_pdfCovariancesAlternative[i-1][ibin][ibin])/m_hist_data_unfolded->GetBinContent(ibin+1) << " ";
    cout << endl;
    cout << "Printing alternative normalized pdf sys errors for eigenvector " << i << endl;
    for(int ibin=0;ibin<nbins;ibin++) cout << sqrt(m_pdfCovariancesNormalizedAlternative[i-1][ibin][ibin])/m_hist_data_unfolded_normalized->GetBinContent(ibin+1) << " ";
    cout << endl << endl;
    
  }

}

TH1D* GeneratorSystematicsBase::CalculateNormalizedSysErrors(Signal_histos* shifted,Signal_histos* nominal,TH1D* unfolded_data,TMatrixT<double>& cov,const bool useMultijet){
  TH1D* hist_reco=(TH1D*)shifted->reco->Clone();
  
  if(useMultijet){
    hist_reco->Add(shifted->multijet);
    hist_reco->Add(nominal->multijet,-1);
    //cout << "CalculateNormalizedSysErrors: Looking on differences between signal reco and signal reco with multijet shift" << endl;
    //for(int i=1;i<=hist_reco->GetNbinsX();i++) cout << (shifted->reco->GetBinContent(i) - hist_reco->GetBinContent(i))/shifted->reco->GetBinContent(i) << " "
    //																								<< (shifted->multijet->GetBinContent(i) - nominal->multijet->GetBinContent(i))/nominal->multijet->GetBinContent(i) << endl;
  }
  TH1D* hist_unfolded = Unfold(hist_reco,nominal);
  if(m_isConcatenated)functions::NormalizeConcatenatedHistogram(hist_unfolded,m_subHistNbins);
  else hist_unfolded->Scale(1./hist_unfolded->Integral());
  functions::DivideByBinWidth(hist_unfolded);
  //if(isParton)hist_unfolded->Scale(2.1882987);
  
  
  //cout << hist_unfolded->GetName() << endl;
  TH1D* errors = (TH1D*) shifted->truth_normalized->Clone();
  const int nbins=errors->GetNbinsX();
  cov.ResizeTo(nbins,nbins);
  //cor.ResizeTo(nbins,nbins);
  //cout << "Sys errors: " << endl;
  double error=0;
  for(int i=1;i<=nbins;i++){
    double truth=shifted->truth_normalized->GetBinContent(i);
    if(truth>0)error = (hist_unfolded->GetBinContent(i) - truth)*(unfolded_data->GetBinContent(i)/truth);
    else error=0;
    
    errors->SetBinContent(i,error);
    //cout << hist_unfolded->GetBinContent(i) - shifted->truth_normalized->GetBinContent(i) << " " << shifted->truth_normalized->GetBinContent(i) << " " << (hist_unfolded->GetBinContent(i) - shifted->truth_normalized->GetBinContent(i))/shifted->truth_normalized->GetBinContent(i) << " " << errors->GetBinContent(i)/unfolded_data->GetBinContent(i) << " " << (hist_unfolded->GetBinContent(i) - nominal->truth_normalized->GetBinContent(i))/nominal->truth_normalized->GetBinContent(i) << endl;
  }
  for(int i=1;i<=nbins;i++)for(int j=1;j<=nbins;j++){
    cov[i-1][j-1] = errors->GetBinContent(i)*errors->GetBinContent(j);
    //cor[i-1][j-1]= cov[i-1][j-1]>=0 ? 1 : -1;
  }
  //cout << endl;
  //for(int i=1;i<=nbins;i++) cout << errors->GetBinContent(i) << " " << nominal->truth_normalized->GetBinContent(i) << " " << hist_unfolded->GetBinContent(i)/nominal->truth_normalized->GetBinContent(i) << endl;
  delete errors;
  delete hist_reco;
  return hist_unfolded;
}



TH1D* GeneratorSystematicsBase::CalculateSysErrors(Signal_histos* shifted,Signal_histos* nominal,TH1D* unfolded_data,TMatrixT<double>& cov,const bool useMultijet){
  //const double phaseSpaceSF= Level=="Parton" ? 2.1882987 : 1.;
  //const bool isParton=m_variable_name.Contains("Parton");
  
  TH1D* hist_reco=(TH1D*)shifted->reco->Clone();
  
  if(useMultijet){
    hist_reco->Add(shifted->multijet);
    hist_reco->Add(nominal->multijet,-1);
  }
  TH1D* hist_unfolded = Unfold(hist_reco,nominal);
  functions::DivideByBinWidth(hist_unfolded);
  //if(isParton)hist_unfolded->Scale(2.1882987);
  
  
  //cout << hist_unfolded->GetName() << endl;
  TH1D* errors = (TH1D*) shifted->truth->Clone();
  const int nbins=errors->GetNbinsX();
  cov.ResizeTo(nbins,nbins);
  //cor.ResizeTo(nbins,nbins);
  //cout << "Sys errors: " << endl;
  double error=0;
  for(int i=1;i<=nbins;i++){
    double truth=shifted->truth->GetBinContent(i);
    if(truth>0)error = (hist_unfolded->GetBinContent(i) - truth)*(unfolded_data->GetBinContent(i)/truth);
    else error=0;
    
    errors->SetBinContent(i,error);
    //cout << hist_unfolded->GetBinContent(i) - shifted->truth->GetBinContent(i) << " " << shifted->truth->GetBinContent(i) << " " << (hist_unfolded->GetBinContent(i) - shifted->truth->GetBinContent(i))/shifted->truth->GetBinContent(i) << " " << errors->GetBinContent(i)/unfolded_data->GetBinContent(i) << " " << (hist_unfolded->GetBinContent(i) - nominal->truth->GetBinContent(i))/nominal->truth->GetBinContent(i) << endl;
  }
  for(int i=1;i<=nbins;i++)for(int j=1;j<=nbins;j++){
    cov[i-1][j-1] = errors->GetBinContent(i)*errors->GetBinContent(j);
    //cor[i-1][j-1]= cov[i-1][j-1]>=0 ? 1 : -1;
  }
  //cout << endl;
  //for(int i=1;i<=nbins;i++) cout << errors->GetBinContent(i) << " " << nominal->truth->GetBinContent(i) << " " << hist_unfolded->GetBinContent(i)/nominal->truth->GetBinContent(i) << endl;
  delete errors;
  delete hist_reco;
  return hist_unfolded;
}

TH1D* GeneratorSystematicsBase::CalculateNormalizedIFSRSysErrors(Signal_histos* histos_up,Signal_histos* histos_down,Signal_histos* nominal,TH1D* unfolded_data,TMatrixT<double>& cov,const bool useMultijet,const bool alternative){
	
  TH1D* hist_reco_up=(TH1D*)histos_up->reco->Clone();
  TH1D* hist_reco_down=(TH1D*)histos_down->reco->Clone();
  TH1D* hist_reco_nominal_up=(TH1D*)nominal->reco->Clone();
  TH1D* hist_reco_nominal_down=(TH1D*)nominal->reco->Clone();
  
  if(useMultijet){
    hist_reco_up->Add(histos_up->multijet);
    hist_reco_up->Add(nominal->multijet,-1);
    hist_reco_down->Add(histos_down->multijet);
    hist_reco_down->Add(nominal->multijet,-1);
    
    hist_reco_nominal_up->Add(nominal->multijet);
    hist_reco_nominal_up->Add(histos_up->multijet,-1);
    
    hist_reco_nominal_down->Add(nominal->multijet);
    hist_reco_nominal_down->Add(histos_up->multijet,-1);
  }
  TH1D* hist_up_unfolded,*hist_down_unfolded;
  
  if(!alternative){
    hist_up_unfolded = Unfold(hist_reco_up,nominal);
    hist_down_unfolded = Unfold(hist_reco_down,nominal);
  }
  else{
    hist_up_unfolded = Unfold(hist_reco_nominal_up,histos_up);
    hist_down_unfolded = Unfold(hist_reco_nominal_down,histos_down);
  }
  
  
  if(m_isConcatenated)functions::NormalizeConcatenatedHistogram(hist_up_unfolded,m_subHistNbins);
  else hist_up_unfolded->Scale(1./hist_up_unfolded->Integral());
  
  if(m_isConcatenated)functions::NormalizeConcatenatedHistogram(hist_down_unfolded,m_subHistNbins);
  else hist_down_unfolded->Scale(1./hist_down_unfolded->Integral());
  
  functions::DivideByBinWidth(hist_up_unfolded);
  functions::DivideByBinWidth(hist_down_unfolded);
  
  //cout << hist_unfolded->GetName() << endl;
  TH1D* errors = (TH1D*)nominal->truth_normalized->Clone();
  const int nbins=errors->GetNbinsX();
  cov.ResizeTo(nbins,nbins);
  //cor.ResizeTo(nbins,nbins);
  //cout << "Sys errors: " << endl;
  double error1,error2,error;
  for(int i=1;i<=nbins;i++){
    if(!alternative){
      double truth_up=histos_up->truth_normalized->GetBinContent(i);
      double truth_down=histos_down->truth_normalized->GetBinContent(i);
      if(truth_up>0)error1= (hist_up_unfolded->GetBinContent(i) - truth_up)/truth_up*unfolded_data->GetBinContent(i);
      else error1=0.;
      if(truth_down>0)error2= (hist_down_unfolded->GetBinContent(i) - truth_down)/truth_down*unfolded_data->GetBinContent(i);
      else error2=0.;
      error=error1-error2;
      if(truth_down > 0 && truth_up > 0)error*=0.5;
    }
    else{
      double truth=nominal->truth_normalized->GetBinContent(i);
      if(truth>0){
	error1= (hist_up_unfolded->GetBinContent(i) - truth)/truth*unfolded_data->GetBinContent(i);
	error2= (hist_down_unfolded->GetBinContent(i) - truth)/truth*unfolded_data->GetBinContent(i);
	error= 0.5*(error1-error2);
      }
      else error=0.;
    }
    errors->SetBinContent(i,error);
  }
  for(int i=1;i<=nbins;i++)for(int j=1;j<=nbins;j++){
    cov[i-1][j-1] = errors->GetBinContent(i)*errors->GetBinContent(j);
    //cor[i-1][j-1]= cov[i-1][j-1]>=0 ? 1 : -1;
  }
  //cout << endl;
  delete errors;
  delete hist_reco_up;
  delete hist_reco_down;
  delete hist_reco_nominal_up;
  delete hist_reco_nominal_down;
  return hist_up_unfolded;
}

TH1D* GeneratorSystematicsBase::CalculateIFSRSysErrors(Signal_histos* histos_up,Signal_histos* histos_down,Signal_histos* nominal,TH1D* unfolded_data,TMatrixT<double>& cov,const bool useMultijet,const bool alternative){
	
  TH1D* hist_reco_up=(TH1D*)histos_up->reco->Clone();
  TH1D* hist_reco_down=(TH1D*)histos_down->reco->Clone();
  
  TH1D* hist_reco_nominal_up=(TH1D*)nominal->reco->Clone();
  TH1D* hist_reco_nominal_down=(TH1D*)nominal->reco->Clone();
  
  if(useMultijet){
    hist_reco_up->Add(histos_up->multijet);
    hist_reco_up->Add(nominal->multijet,-1);
    hist_reco_down->Add(histos_down->multijet);
    hist_reco_down->Add(nominal->multijet,-1);
    
    hist_reco_nominal_up->Add(nominal->multijet);
    hist_reco_nominal_up->Add(histos_up->multijet,-1);
    
    hist_reco_nominal_down->Add(nominal->multijet);
    hist_reco_nominal_down->Add(histos_up->multijet,-1);
  }
  
  TH1D* hist_up_unfolded,*hist_down_unfolded;
  
  if(!alternative){
    hist_up_unfolded = Unfold(hist_reco_up,nominal);
    hist_down_unfolded = Unfold(hist_reco_down,nominal);
  }
  else{
    hist_up_unfolded = Unfold(hist_reco_nominal_up,histos_up);
    hist_down_unfolded = Unfold(hist_reco_nominal_down,histos_down);
  }
  
  functions::DivideByBinWidth(hist_up_unfolded);
  functions::DivideByBinWidth(hist_down_unfolded);
  
  //cout << hist_unfolded->GetName() << endl;
  TH1D* errors = (TH1D*)nominal->truth->Clone();
  const int nbins=errors->GetNbinsX();
  cov.ResizeTo(nbins,nbins);
  
  double error_up,error_down,error;
  for(int i=1;i<=nbins;i++){
    if(!alternative){
      double truth_up=histos_up->truth->GetBinContent(i);
      double truth_down=histos_down->truth->GetBinContent(i);
      error_up= (hist_up_unfolded->GetBinContent(i) - truth_up)/truth_up*unfolded_data->GetBinContent(i);
      error_down= (hist_down_unfolded->GetBinContent(i) - truth_down)/truth_down*unfolded_data->GetBinContent(i);
    }
    else{
      double truth=nominal->truth->GetBinContent(i);
      error_up= (hist_up_unfolded->GetBinContent(i) - truth)/truth*unfolded_data->GetBinContent(i);
      error_down= (hist_down_unfolded->GetBinContent(i) - truth)/truth*unfolded_data->GetBinContent(i);
    }
    error = functions::calculateFSRsys(error_up,error_down);
    
    errors->SetBinContent(i,error);
  }
  
  
  for(int i=1;i<=nbins;i++)for(int j=1;j<=nbins;j++){
    cov[i-1][j-1] = errors->GetBinContent(i)*errors->GetBinContent(j);
    //cor[i-1][j-1]= cov[i-1][j-1]>=0 ? 1 : -1;
  }
  //cout << endl;
  delete errors;
  delete hist_reco_up;
  delete hist_reco_down;
  delete hist_reco_nominal_up;
  delete hist_reco_nominal_down;
  return hist_up_unfolded;
	
}
void GeneratorSystematicsBase::calculateCorrelationMatrices() {
  for(auto it=m_covariances.begin();it!=m_covariances.end();it++){
    functions::calculateCorrelationMatrix(it->second,m_correlations[it->first]);
  }
}

void GeneratorSystematicsBase::DeleteObjects(){
  m_genSystematic_histos.clear();
}

TH1D* GeneratorSystematicsBase::Unfold(TH1D* distr_to_unfold,Signal_histos* nominal){
  TH1D* hist_unfolded = functions::UnfoldBayes(distr_to_unfold,nominal->truth_cuts_correction,nominal->reco_cuts_correction,nominal->response,4,0);
  return hist_unfolded;
}
void GeneratorSystematicsBase::prepareReplicas(int nPseudoExperiments,bool shiftNominal){
  cout << "GeneratorSystematicsBase: Preparing " << nPseudoExperiments << " pseudoexperiments" << endl;
  m_nPseudoExperiments=nPseudoExperiments;
  const int nsys=m_accessors.size();
  
  m_nominal_replicas.resize(m_nPseudoExperiments);
  m_MECOffSample_replicas.resize(m_nPseudoExperiments);
  
  m_sys_replicas.resize(nsys);
  for(int isys=0;isys<nsys;isys++)m_sys_replicas[isys].resize(m_nPseudoExperiments);
  
  stringstream s;
  
  for(int i=0;i<m_nPseudoExperiments;i++) {
    m_nominal_replicas[i] = new Signal_histos();
    s << "nominal_" << i;
    m_nominal_histos->cloneHistos(m_nominal_replicas[i],s.str().c_str());
    s.str("");
    if(shiftNominal) m_nominal_replicas[i]->smearHistos();
    m_nominal_replicas[i]->divideByBinWidth();
    
    m_MECOffSample_replicas[i] = new Signal_histos();
    m_MECOffSample_histos->cloneHistos(m_MECOffSample_replicas[i],(s.str()+"_MECOff").c_str());
    if(shiftNominal) m_MECOffSample_replicas[i]->smearHistos();
    m_MECOffSample_replicas[i]->divideByBinWidth();
    
  }	
  for(int isys=0;isys<nsys;isys++) for(int i=0;i<m_nPseudoExperiments;i++) {
    s << m_accessors[isys] << "_" << i;
    m_sys_replicas[isys][i] = new Signal_histos();
    //cout << s.str().c_str() << endl;
    m_genSystematic_histos[m_accessors[isys]]->cloneHistos(m_sys_replicas[isys][i],s.str().c_str());
    s.str("");
    m_sys_replicas[isys][i]->smearHistos();
    m_sys_replicas[isys][i]->divideByBinWidth();
  }
  cout << "GeneratorSystematicsBase: Pseudoexperiments prepared" << endl;
}
void GeneratorSystematicsBase::deleteReplicas(){
	
  for(int i=0;i<m_nPseudoExperiments;i++){
    delete m_nominal_replicas[i];
    delete m_MECOffSample_replicas[i];
  }
  const int nsys=m_accessors.size();
  for(int isys=0;isys<nsys;isys++)for(int i=0;i<m_nPseudoExperiments;i++)delete m_sys_replicas[isys][i];
}
void GeneratorSystematicsBase::runPseudoexperiments(vector<vector<double> >& average,vector<vector<double> >& errors,vector<vector<double> >& average_normalized,vector<vector<double> >& errors_normalized){
  cout << "INFO: Running GeneratorSystematicsBase::runPseudoexperiments(...) " << endl;
  const int nsys=m_accessors.size();
  errors.resize(nsys);
  errors_normalized.resize(nsys);
  
  
  average.resize(nsys);
  average_normalized.resize(nsys);
  vector<vector<double> > mean(nsys),mean2(nsys);
  vector<vector<double> > mean_normalized(nsys),mean2_normalized(nsys);
  
  const int nbins = m_nominal_replicas[0]->truth->GetXaxis()->GetNbins();
  
  for(int isys=0;isys<nsys;isys++){
    mean[isys].resize(nbins);
    mean2[isys].resize(nbins);
    mean_normalized[isys].resize(nbins);
    mean2_normalized[isys].resize(nbins);
    average[isys].resize(nbins);
    average_normalized[isys].resize(nbins);
    errors[isys].resize(nbins);
    errors_normalized[isys].resize(nbins);
    for(int i=0;i<m_nPseudoExperiments;i++){
      TH1D* unfolded = isys==0 ? Unfold(m_sys_replicas[isys][i]->reco,m_MECOffSample_replicas[i]) : Unfold(m_sys_replicas[isys][i]->reco,m_nominal_replicas[i]);
      TH1D* unfolded_normalized=(TH1D*)unfolded->Clone();
      if(m_isConcatenated) functions::NormalizeConcatenatedHistogram(unfolded_normalized,m_subHistNbins);
      else unfolded_normalized->Scale(1./unfolded_normalized->Integral());
      functions::DivideByBinWidth(unfolded_normalized);
      functions::DivideByBinWidth(unfolded);
      for(int ibin=0;ibin<nbins;ibin++){
	double x=(unfolded->GetBinContent(ibin+1) - m_sys_replicas[isys][i]->truth->GetBinContent(ibin+1))/m_sys_replicas[isys][i]->truth->GetBinContent(ibin+1);
	mean[isys][ibin]+=x;
	mean2[isys][ibin]+=(x*x);
	double y=(unfolded_normalized->GetBinContent(ibin+1) - m_sys_replicas[isys][i]->truth_normalized->GetBinContent(ibin+1))/m_sys_replicas[isys][i]->truth_normalized->GetBinContent(ibin+1);
	mean_normalized[isys][ibin]+=y;
	mean2_normalized[isys][ibin]+=(y*y);
      }
      delete unfolded;
      delete unfolded_normalized;
    }
    for(int ibin=0;ibin<nbins;ibin++){
      mean[isys][ibin]/=m_nPseudoExperiments;
      mean2[isys][ibin]/=m_nPseudoExperiments;
      mean_normalized[isys][ibin]/=m_nPseudoExperiments;
      mean2_normalized[isys][ibin]/=m_nPseudoExperiments;
      errors[isys][ibin] = sqrt(mean2[isys][ibin] - mean[isys][ibin]*mean[isys][ibin]);
      errors_normalized[isys][ibin] = sqrt(mean2_normalized[isys][ibin] - mean_normalized[isys][ibin]*mean_normalized[isys][ibin]);
      
      average[isys][ibin]=mean[isys][ibin];
      average_normalized[isys][ibin]=mean_normalized[isys][ibin];
      
      
    }
    
    
  }
  cout << "INFO: Reaching end of GeneratorSystematicsBase::doPseudoexperiments(...) function" << endl;
}
void GeneratorSystematicsBase::runPseudoexperiments() {
  runPseudoexperiments(m_mean_of_errors,m_errors_of_errors,m_mean_of_errors_normalized ,m_errors_of_errors_normalized);
	
}
