#include "TTbarCovarianceCalculator/GeneratorSystematics.h"
#include "TTbarHelperFunctions/functions.h"
#include "TTbarHelperFunctions/histManipFunctions.h"
#include "TTbarHelperFunctions/namesAndTitles.h"
#include "TTbarHelperFunctions/plottingFunctions.h"

ClassImp(GeneratorSystematics)
//#include <iostream>


void GeneratorSystematics::drawErrorsComparison(){
  vector<TH1D*> histos;
  auto leg = make_shared<TLegend>(0.5,0.55,0.88,0.91);
  leg->SetTextSize(0.032);
  
  const int nsys = m_accessors.size();
  for(int isys=0;isys<nsys;isys++) {
    histos.push_back(m_genSystematic_errors[m_accessors[isys]]);
  }
  
  TString ytitle="Relative uncertainty";
  
  for(unsigned int i=0;i<histos.size();i++) {
    histos[i]->SetLineColor(m_colors[i]);
    histos[i]->SetMarkerColor(m_colors[i]);
    histos[i]->SetLineStyle(m_lineStyles[i]);
    histos[i]->SetLineWidth(2);
    histos[i]->GetYaxis()->SetTitle(ytitle);
    NamesAndTitles::setXtitle(histos[i],m_variable_name);
    //functions::SetErrorsToZero(histos[i]);
    leg->AddEntry(histos[i],m_legend_entries[m_accessors[i]]);
  }
  
  double ymin=-0.1;
  double ymax=0.2;
  
  functions::plotHistogramsWithFixedYRange(histos,leg.get(),m_dirname,m_variable_name+"_errors.pdf", "",ymin,ymax);
  functions::plotHistogramsWithFixedYRange(histos,leg.get(),m_dirname,m_variable_name+"_errors.png", "",ymin,ymax);
  
  leg->SetY1(0.91);
  
  for(int isys=0;isys<nsys;isys++) {
    histos={m_genSystematic_errors[m_accessors[isys]]};
    leg->Clear();
    leg->AddEntry(m_genSystematic_errors[m_accessors[isys]],m_legend_entries[m_accessors[isys]]);
    functions::plotHistogramsWithFixedYRange(histos,leg.get(),m_dirname,m_variable_name+"_errors_" + m_foldernames[m_accessors[isys]], "",ymin,ymax);
    functions::plotHistogramsWithFixedYRange(histos,leg.get(),m_dirname,m_variable_name+"_errors_" + m_foldernames[m_accessors[isys]] + ".png", "",ymin,ymax);
  }
	
}

//----------------------------------------------------------------------

void GeneratorSystematics::drawTruthHistComparison(){
	
  cout << endl << "Drawing histos at truth level" << endl;
  vector<TH1D*> histos;
  for(int i=0;i<m_ngenSystematics_histos;i++){
    histos.push_back((TH1D*)m_genSystematic_histos[m_accessors[i]]->truth->Clone());
  }
  histos.push_back((TH1D*)m_nominal_histos->truth->Clone());
  //for(int i=0;i<(int)histos.size();i++) cout << histos[i]->GetName() << endl;
  
  TString xtitle=m_variable_name;
  //xtitle.ReplaceAll("Level","");
  xtitle.ReplaceAll("Particle","Particle ");
  xtitle.ReplaceAll("Parton","Parton ");
  xtitle.ReplaceAll("_fine","");
  xtitle.ReplaceAll("_"," ");
  xtitle.ReplaceAll("reco","");
  //xtitle += " Reco level";
  TString ytitle="";
  
  TLegend* leg = new TLegend(0.45,0.45,0.92,0.92);
  for(int i=0;i<m_ngenSystematics_histos;i++){
    //cout << m_unfolded_genSystematic_histos.size() << " " << i << endl;
    //cout << m_unfolded_genSystematic_histos[m_accessors[i]]->GetName() << endl;
    //if(m_accessors[i].find("ISR")!=std::string::npos)continue;
    histos[i]->GetYaxis()->SetTitle(ytitle);
    histos[i]->GetXaxis()->SetTitle(xtitle);
    histos[i]->SetLineColor(m_colors[i]);
    histos[i]->SetMarkerColor(m_colors[i]);
    histos[i]->SetLineStyle(m_lineStyles[i]);
    histos[i]->SetLineWidth(1.2);
    leg->AddEntry(histos[i],m_legend_entries[m_accessors[i]]);
  }
  histos[histos.size()-1]->SetLineWidth(1.5);
  leg->AddEntry(m_nominal_histos->truth,m_signal_legend_entry);
  drawHistComparison(histos[histos.size()-1],histos,leg,m_dirname,m_variable_name+"_truth",(TString)"SYS / NOMINAL",0.41,1.59);
}



void GeneratorSystematics::drawRecoCorrectionsComparison(){
	
  cout << endl << "Drawing histos at truth level" << endl;
  vector<TH1D*> histos;
  for(int i=0;i<m_ngenSystematics_histos;i++){
    histos.push_back((TH1D*)m_genSystematic_histos[m_accessors[i]]->reco_cuts_correction->Clone());
  }
  histos.push_back((TH1D*)m_nominal_histos->reco_cuts_correction->Clone());
  
  TString xtitle=m_variable_name;
  //xtitle.ReplaceAll("Level","");
  xtitle.ReplaceAll("Particle","Particle ");
  xtitle.ReplaceAll("Parton","Parton ");
  xtitle.ReplaceAll("_fine","");
  xtitle.ReplaceAll("_"," ");
  xtitle.ReplaceAll("reco","");
  //xtitle += " Reco level";
  TString ytitle="Correction";
  
  //for(int i=0;i<(int)histos.size();i++) cout << histos[i]->GetName() << endl;
  TLegend* leg = new TLegend(0.515,0.55,0.92,0.92);
  for(int i=0;i<m_ngenSystematics_histos;i++){
    //cout << m_unfolded_genSystematic_histos.size() << " " << i << endl;
    //cout << m_unfolded_genSystematic_histos[m_accessors[i]]->GetName() << endl;
    //if(m_accessors[i].find("ISR")!=std::string::npos)continue;
    histos[i]->SetLineColor(m_colors[i]);
    histos[i]->SetMarkerColor(m_colors[i]);
    histos[i]->SetLineStyle(m_lineStyles[i]);
    histos[i]->GetYaxis()->SetTitle(ytitle);
    histos[i]->GetXaxis()->SetTitle(xtitle);
    histos[i]->SetLineWidth(1.2);
    leg->AddEntry(histos[i],m_legend_entries[m_accessors[i]]);
  }
  histos[histos.size()-1]->SetLineWidth(1.5);
  leg->AddEntry(m_nominal_histos->truth,m_signal_legend_entry);
  drawHistComparison(histos[histos.size()-1],histos,leg,m_dirname,m_variable_name+"_recoCorrections",(TString)"SYS / NOMINAL",0.41,1.59);
}

//----------------------------------------------------------------------

void GeneratorSystematics::drawTruthCorrectionsComparison(){
	
  cout << endl << "Drawing histos at truth level" << endl;
  vector<TH1D*> histos;
  for(int i=0;i<m_ngenSystematics_histos;i++){
    histos.push_back((TH1D*)m_genSystematic_histos[m_accessors[i]]->truth_cuts_correction->Clone());
  }
  histos.push_back((TH1D*)m_nominal_histos->truth_cuts_correction->Clone());
  
  TString xtitle=m_variable_name;
  xtitle.ReplaceAll("Level","");
  xtitle.ReplaceAll("Particle","");
  xtitle.ReplaceAll("Parton","");
  xtitle.ReplaceAll("_fine","");
  xtitle.ReplaceAll("_"," ");
  xtitle.ReplaceAll("reco","");
  xtitle += " Reco level";
  TString ytitle="Correction";
  //for(int i=0;i<(int)histos.size();i++) cout << histos[i]->GetName() << endl;
  TLegend* leg = new TLegend(0.515,0.55,0.92,0.92);
  for(int i=0;i<m_ngenSystematics_histos;i++){
    //cout << m_unfolded_genSystematic_histos.size() << " " << i << endl;
    //cout << m_unfolded_genSystematic_histos[m_accessors[i]]->GetName() << endl;
    //if(m_accessors[i].find("ISR")!=std::string::npos)continue;
    histos[i]->SetLineColor(m_colors[i]);
    histos[i]->SetMarkerColor(m_colors[i]);
    histos[i]->SetLineStyle(m_lineStyles[i]);
    histos[i]->GetYaxis()->SetTitle(ytitle);
    histos[i]->GetXaxis()->SetTitle(xtitle);
    histos[i]->SetLineWidth(1.2);
    leg->AddEntry(histos[i],m_legend_entries[m_accessors[i]]);
  }
  histos[histos.size()-1]->SetLineWidth(1.5);
  leg->AddEntry(m_nominal_histos->truth,m_signal_legend_entry);
  drawHistComparison(histos[histos.size()-1],histos,leg,m_dirname,m_variable_name+"_truthCorrections",(TString)"SYS / NOMINAL",0.41,1.59);
}

//----------------------------------------------------------------------

void GeneratorSystematics::drawRecoLevelHistComparison(){
	
  cout << endl << "Drawing histos at reco level" << endl;
  vector<TH1D*> histos;
  for(int i=0;i<m_ngenSystematics_histos;i++){
    histos.push_back((TH1D*)m_genSystematic_histos[m_accessors[i]]->reco->Clone());
  }
  histos.push_back((TH1D*)m_nominal_histos->reco->Clone());
  
  TString xtitle=m_variable_name;
  xtitle.ReplaceAll("Level","");
  xtitle.ReplaceAll("Particle","");
  xtitle.ReplaceAll("Parton","");
  xtitle.ReplaceAll("_fine","");
  xtitle.ReplaceAll("_"," ");
  xtitle.ReplaceAll("reco","");
  xtitle += " Reco level";
  TString ytitle="";
  
  //for(int i=0;i<(int)histos.size();i++) cout << histos[i]->GetName() << endl;
  TLegend* leg = new TLegend(0.515,0.55,0.92,0.92);
  for(int i=0;i<m_ngenSystematics_histos;i++){
    //cout << m_unfolded_genSystematic_histos.size() << " " << i << endl;
    //cout << m_unfolded_genSystematic_histos[m_accessors[i]]->GetName() << endl;
    //if(m_accessors[i].find("ISR")!=std::string::npos)continue;
    functions::DivideByBinWidth(histos[i]);
    histos[i]->SetLineColor(m_colors[i]);
    histos[i]->SetMarkerColor(m_colors[i]);
    histos[i]->SetLineStyle(m_lineStyles[i]);
    histos[i]->GetYaxis()->SetTitle(ytitle);
    histos[i]->GetXaxis()->SetTitle(xtitle);
    histos[i]->SetLineWidth(1.2);
    leg->AddEntry(histos[i],m_legend_entries[m_accessors[i]]);
  }
  histos[histos.size()-1]->SetLineWidth(1.5);
  functions::DivideByBinWidth(histos[histos.size()-1]);
  leg->AddEntry(m_nominal_histos->truth,m_signal_legend_entry);
  drawHistComparison(histos[histos.size()-1],histos,leg,m_dirname,m_variable_name+"_reco",(TString)"SYS / NOMINAL",0.41,1.59);
	
	
}

//----------------------------------------------------------------------

void GeneratorSystematics::drawUnfoldedHistComparison(){
  cout << endl << "Drawing unfolded histos" << endl;
  vector<TH1D*> histos;
  for(int i=0;i<m_ngenSystematics_histos;i++){
    //TH1D* h=(TH1D*)Unfold(m_nominal_histos->reco,m_genSystematic_histos[m_accessors[i]])->Clone();
    //TH1D* h=(TH1D*)Unfold(m_genSystematic_histos[m_accessors[i]]->reco,m_genSystematic_histos[m_accessors[i]])->Clone();
    TH1D* h=(TH1D*)Unfold(m_genSystematic_histos[m_accessors[i]]->reco,m_nominal_histos)->Clone();
    functions::DivideByBinWidth(h);
    histos.push_back((TH1D*)h->Clone());
  }
  //const int nbins = m_nominal_histos->truth->GetNbinsX();
  //cout << "Ratios ME: ";
  //for(int i=1;i<=nbins;i++) cout << (histos[0]->GetBinContent(i) - m_nominal_histos->truth->GetBinContent(i))/m_nominal_histos->truth->GetBinContent(i) << " ";
  //cout << endl;
  //cout << "Ratios PS+Hadronization: ";
  //for(int i=1;i<=nbins;i++) cout << (histos[1]->GetBinContent(i) - m_nominal_histos->truth->GetBinContent(i))/m_nominal_histos->truth->GetBinContent(i) << " ";
  //cout << endl;
  //cout << "Ratios FSR: ";
  //for(int i=1;i<=nbins;i++) cout << (histos[2]->GetBinContent(i) - m_nominal_histos->truth->GetBinContent(i))/m_nominal_histos->truth->GetBinContent(i) << " ";
  //cout << endl;
  histos.push_back((TH1D*)m_nominal_histos->truth->Clone());
  
  TString xtitle=m_variable_name;
  //xtitle.ReplaceAll("Level","");
  xtitle.ReplaceAll("Particle","Particle ");
  xtitle.ReplaceAll("Parton","Parton ");
  xtitle.ReplaceAll("_fine","");
  xtitle.ReplaceAll("_"," ");
  xtitle.ReplaceAll("reco","");
  TString ytitle="";
  
  TLegend* leg = new TLegend(0.515,0.55,0.92,0.92);
  for(int i=0;i<m_ngenSystematics_histos;i++){
    //cout << m_unfolded_genSystematic_histos.size() << " " << i << endl;
    //cout << m_unfolded_genSystematic_histos[m_accessors[i]]->GetName() << endl;
    //if(m_accessors[i].find("ISR")!=std::string::npos)continue;
    histos[i]->SetLineColor(m_colors[i]);
    histos[i]->SetMarkerColor(m_colors[i]);
    histos[i]->GetYaxis()->SetTitle(ytitle);
    histos[i]->GetXaxis()->SetTitle(xtitle);
    histos[i]->SetLineStyle(m_lineStyles[i]);
    histos[i]->SetLineWidth(1.2);
    leg->AddEntry(histos[i],m_legend_entries[m_accessors[i]]);
  }
  histos[histos.size()-1]->SetLineWidth(1.5);
  leg->AddEntry(m_nominal_histos->truth,m_signal_legend_entry);
  drawHistComparison(histos[histos.size()-1],histos,leg,m_dirname,m_variable_name+"_unfolded",(TString)"SYS / NOMINAL",0.41,1.59);
	
}

//----------------------------------------------------------------------

void GeneratorSystematics::drawNormalizedErrorsComparison(){
  vector<TH1D*> histos;
  auto leg = make_shared<TLegend>(0.5,0.55,0.88,0.91);
  leg->SetFillColor(0);
  leg->SetFillStyle(0);
  leg->SetBorderSize(0);
  leg->SetTextFont(42);
  leg->SetTextSize(0.039);
	
  const int nsys = m_accessors.size();
  for(int isys=0;isys<nsys;isys++) {
    histos.push_back(m_genSystematic_errors_normalized[m_accessors[isys]]);
  }
  
  TString ytitle="Relative uncertainty";
  
  for(unsigned int i=0;i<histos.size();i++) {
    histos[i]->SetLineColor(m_colors[i]);
    histos[i]->SetMarkerColor(m_colors[i]);
    histos[i]->SetLineStyle(m_lineStyles[i]);
    histos[i]->SetLineWidth(2);
    histos[i]->GetYaxis()->SetTitle(ytitle);
    NamesAndTitles::setXtitle(histos[i],m_variable_name);
    //functions::SetErrorsToZero(histos[i]);
    leg->AddEntry(histos[i],m_legend_entries[m_accessors[i]]);
  }
  
  double ymin=-0.08;
  double ymax=0.15;
  functions::plotHistogramsWithFixedYRange(histos,leg.get(),m_dirname,m_variable_name+"_errors_normalized", "",ymin,ymax);
  functions::plotHistogramsWithFixedYRange(histos,leg.get(),m_dirname,m_variable_name+"_errors_normalized.png", "",ymin,ymax);
  
  leg->SetY1(0.91);
  
  for(int isys=0;isys<nsys;isys++) {
    histos={m_genSystematic_errors_normalized[m_accessors[isys]]};
    leg->Clear();
    leg->AddEntry(m_genSystematic_errors_normalized[m_accessors[isys]],m_legend_entries[m_accessors[isys]]);
    functions::plotHistogramsWithFixedYRange(histos,leg.get(),m_dirname,m_variable_name+"_errors_normalized_" + m_foldernames[m_accessors[isys]], "",ymin,ymax);
    functions::plotHistogramsWithFixedYRange(histos,leg.get(),m_dirname,m_variable_name+"_errors_normalized_" + m_foldernames[m_accessors[isys]] + ".png", "",ymin,ymax);
  }

}

//----------------------------------------------------------------------

void GeneratorSystematics::drawNormalizedTruthHistComparison(){
  cout << endl << "Drawing normalized histos at truth level" << endl;
  vector<TH1D*> histos;
  for(int i=0;i<m_ngenSystematics_histos;i++){
    histos.push_back((TH1D*)m_genSystematic_histos[m_accessors[i]]->truth_normalized->Clone());
  }
  histos.push_back((TH1D*)m_nominal_histos->truth_normalized->Clone());
  //for(int i=0;i<(int)histos.size();i++) cout << histos[i]->GetName() << endl;
  
  TString xtitle=m_variable_name;
  //xtitle.ReplaceAll("Level","");
  xtitle.ReplaceAll("Particle","Particle ");
  xtitle.ReplaceAll("Parton","Parton ");
  xtitle.ReplaceAll("_fine","");
  xtitle.ReplaceAll("_"," ");
  xtitle.ReplaceAll("reco","");
  //xtitle += " Reco level";
  TString ytitle="";
  
  TLegend* leg = new TLegend(0.45,0.45,0.92,0.92);
  for(int i=0;i<m_ngenSystematics_histos;i++){
    //cout << m_unfolded_genSystematic_histos.size() << " " << i << endl;
    //cout << m_unfolded_genSystematic_histos[m_accessors[i]]->GetName() << endl;
    //if(m_accessors[i].find("ISR")!=std::string::npos)continue;
    histos[i]->GetYaxis()->SetTitle(ytitle);
    histos[i]->GetXaxis()->SetTitle(xtitle);
    histos[i]->SetLineColor(m_colors[i]);
    histos[i]->SetMarkerColor(m_colors[i]);
    histos[i]->SetLineStyle(m_lineStyles[i]);
    histos[i]->SetLineWidth(1.2);
    leg->AddEntry(histos[i],m_legend_entries[m_accessors[i]]);
  }
  histos[histos.size()-1]->SetLineWidth(1.5);
  leg->AddEntry(m_nominal_histos->truth_normalized,m_signal_legend_entry);
  drawHistComparison(histos[histos.size()-1],histos,leg,m_dirname,m_variable_name+"_truth_normalized",(TString)"SYS / NOMINAL",0.41,1.59);
}

//----------------------------------------------------------------------

void GeneratorSystematics::drawNormalizedUnfoldedHistComparison(){
  cout << endl << "Drawing normalized unfolded histos" << endl;
  vector<TH1D*> histos;
  for(int i=0;i<m_ngenSystematics_histos;i++){
    //TH1D* h=(TH1D*)Unfold(m_nominal_histos->reco,m_genSystematic_histos[m_accessors[i]])->Clone();
    //TH1D* h=(TH1D*)Unfold(m_genSystematic_histos[m_accessors[i]]->reco,m_genSystematic_histos[m_accessors[i]])->Clone();
    TH1D* h=(TH1D*)Unfold(m_genSystematic_histos[m_accessors[i]]->reco,m_nominal_histos)->Clone();
    h->Scale(1./h->Integral());
    functions::DivideByBinWidth(h);
    histos.push_back((TH1D*)h->Clone());
  }
  //const int nbins = m_nominal_histos->truth_normalized->GetNbinsX();
  //cout << "Ratios ME: ";
  //for(int i=1;i<=nbins;i++) cout << (histos[0]->GetBinContent(i) - m_nominal_histos->truth_normalized->GetBinContent(i))/m_nominal_histos->truth_normalized->GetBinContent(i) << " ";
  //cout << endl;
  //cout << "Ratios PS+Hadronization: ";
  //for(int i=1;i<=nbins;i++) cout << (histos[1]->GetBinContent(i) - m_nominal_histos->truth_normalized->GetBinContent(i))/m_nominal_histos->truth_normalized->GetBinContent(i) << " ";
  //cout << endl;
  //cout << "Ratios FSR: ";
  //for(int i=1;i<=nbins;i++) cout << (histos[2]->GetBinContent(i) - m_nominal_histos->truth_normalized->GetBinContent(i))/m_nominal_histos->truth_normalized->GetBinContent(i) << " ";
  //cout << endl;
  histos.push_back((TH1D*)m_nominal_histos->truth_normalized->Clone());
  
  TString xtitle=m_variable_name;
  //xtitle.ReplaceAll("Level","");
  xtitle.ReplaceAll("Particle","Particle ");
  xtitle.ReplaceAll("Parton","Parton ");
  xtitle.ReplaceAll("_fine","");
  xtitle.ReplaceAll("_"," ");
  xtitle.ReplaceAll("reco","");
  TString ytitle="";
  
  TLegend* leg = new TLegend(0.515,0.55,0.92,0.92);
  for(int i=0;i<m_ngenSystematics_histos;i++){
    //cout << m_unfolded_genSystematic_histos.size() << " " << i << endl;
    //cout << m_unfolded_genSystematic_histos[m_accessors[i]]->GetName() << endl;
    //if(m_accessors[i].find("ISR")!=std::string::npos)continue;
    histos[i]->SetLineColor(m_colors[i]);
    histos[i]->SetMarkerColor(m_colors[i]);
    histos[i]->GetYaxis()->SetTitle(ytitle);
    histos[i]->GetXaxis()->SetTitle(xtitle);
    histos[i]->SetLineStyle(m_lineStyles[i]);
    histos[i]->SetLineWidth(1.2);
    leg->AddEntry(histos[i],m_legend_entries[m_accessors[i]]);
  }
  histos[histos.size()-1]->SetLineWidth(1.5);
  leg->AddEntry(m_nominal_histos->truth,m_signal_legend_entry);
  drawHistComparison(histos[histos.size()-1],histos,leg,m_dirname,m_variable_name+"_unfolded_normalized",(TString)"SYS / NOMINAL",0.41,1.59);
}

//----------------------------------------------------------------------

void GeneratorSystematics::drawNormalizedRecoLevelHistComparison(){
	
  cout << endl << "Drawing normalized histos at reco level" << endl;
  vector<TH1D*> histos;
  for(int i=0;i<m_ngenSystematics_histos;i++){
    histos.push_back((TH1D*)m_genSystematic_histos[m_accessors[i]]->reco->Clone());
    histos[i]->Scale(1./histos[i]->Integral());
    functions::DivideByBinWidth(histos[i]);
  }
  histos.push_back((TH1D*)m_nominal_histos->reco->Clone());
  histos[histos.size()-1]->Scale(1./histos[histos.size()-1]->Integral());
  functions::DivideByBinWidth(histos[histos.size()-1]);
  
  
  TString xtitle=m_variable_name;
  xtitle.ReplaceAll("Level","");
  xtitle.ReplaceAll("Particle","");
  xtitle.ReplaceAll("Parton","");
  xtitle.ReplaceAll("_fine","");
  xtitle.ReplaceAll("_"," ");
  xtitle.ReplaceAll("reco","");
  xtitle += " Reco level";
  TString ytitle="";
  
  //for(int i=0;i<(int)histos.size();i++) cout << histos[i]->GetName() << endl;
  TLegend* leg = new TLegend(0.515,0.55,0.92,0.92);
  for(int i=0;i<m_ngenSystematics_histos;i++){
    //cout << m_unfolded_genSystematic_histos.size() << " " << i << endl;
    //cout << m_unfolded_genSystematic_histos[m_accessors[i]]->GetName() << endl;
    //if(m_accessors[i].find("ISR")!=std::string::npos)continue;
    functions::DivideByBinWidth(histos[i]);
    histos[i]->SetLineColor(m_colors[i]);
    histos[i]->SetMarkerColor(m_colors[i]);
    histos[i]->SetLineStyle(m_lineStyles[i]);
    histos[i]->GetYaxis()->SetTitle(ytitle);
    histos[i]->GetXaxis()->SetTitle(xtitle);
    histos[i]->SetLineWidth(1.2);
    leg->AddEntry(histos[i],m_legend_entries[m_accessors[i]]);
  }
  histos[histos.size()-1]->SetLineWidth(1.5);
  functions::DivideByBinWidth(histos[histos.size()-1]);
  leg->AddEntry(m_nominal_histos->truth,m_signal_legend_entry);
  drawHistComparison(histos[histos.size()-1],histos,leg,m_dirname,m_variable_name+"_reco_normalized",(TString)"SYS / NOMINAL",0.41,1.59);
	
}

//----------------------------------------------------------------------

void GeneratorSystematics::drawMigrationMatrices(){
  cout << endl << "Drawing migration matrices" << endl;
  
  vector<TH2D*> histos;
  for(int i=0;i<m_ngenSystematics_histos;i++){
    histos.push_back((TH2D*)m_genSystematic_histos[m_accessors[i]]->migration->Clone());
  }
  histos.push_back((TH2D*)m_nominal_histos->migration->Clone());
  
  TString x=m_variable_name;
  TString level;
  if(m_variable_name.Contains("Parton"))level="Parton";
  if(m_variable_name.Contains("Particle"))level="Particle";
  x.ReplaceAll("Level","");
  x.ReplaceAll("Particle","");
  x.ReplaceAll("Parton","");
  x.ReplaceAll("_fine","");
  x.ReplaceAll("_"," ");
  x.ReplaceAll("reco","");
  TString xtitle = x + " Reco level";
  TString ytitle= x + " " + level + " level";
  cout << xtitle << endl;
  cout << ytitle << endl;
  
  for(unsigned int i=0;i<histos.size();i++){
    histos[i]->SetStats(0);
    histos[i]->GetXaxis()->SetTitle(xtitle);
    histos[i]->GetYaxis()->SetTitle(ytitle);
    functions::NormalizeRows(histos[i]);
    histos[i]->Scale(100);
  }
  
  TCanvas* c = new TCanvas("c");
  histos[0]->Draw("text00");
  c->SetGrid();
  c->SaveAs(m_dirname + m_variable_name+"_migration_ME.pdf");
  histos[1]->Draw("text00");
  c->SaveAs(m_dirname + m_variable_name+"_migration_PS_Hadronization.pdf");
  histos[2]->Draw("text00");
  c->SaveAs(m_dirname + m_variable_name+"_migration_FSR.pdf");
  histos[3]->Draw("text00");
  c->SaveAs(m_dirname + m_variable_name+"_migration_nominal.pdf");
  delete c;
}

//----------------------------------------------------------------------

void GeneratorSystematics::drawHistComparison(TH1D* nominal, vector<TH1D*>& hist,TLegend* leg,TString dirname,TString name, TString y2name, double y2min, double y2max, TString lumi,bool /**/){
  hist.push_back(nominal);
  functions::plotHistograms(hist,leg,dirname,name,nominal, y2name, y2min, y2max, lumi);
}

//----------------------------------------------------------------------

void GeneratorSystematics::drawHistComparison(TH1D* nominal, unordered_map<string,TH1D*>& hist,TLegend* leg,TString dirname,TString name, TString y2name, double y2min, double y2max, TString lumi,bool /**/){
  vector<TH1D*> histos;
  for(auto it=hist.begin();it!=hist.end();it++) histos.push_back(it->second);
  histos.push_back(nominal);
  functions::plotHistograms(histos,leg,dirname,name,nominal, y2name, y2min, y2max, lumi);
}

