#include "TTbarCovarianceCalculator/PseudoexperimentsCalculator.h"
#include "TTbarHelperFunctions/functions.h"
#include "TTbarHelperFunctions/histManipFunctions.h"
#include "TTbarHelperFunctions/namesAndTitles.h"
#include "TTbarHelperFunctions/TTBarConfig.h"
ClassImp(PseudoexperimentsCalculator)

PseudoexperimentsCalculator::PseudoexperimentsCalculator(SystematicHistos* sysHistos,const TString& config_lumi,const TString& config_sysnames,const TString& mainDir,const TString& variable,const TString& level,int debugLevel): m_gap("                             ")
{
  this->init(sysHistos,config_lumi,config_sysnames,mainDir,variable,level,debugLevel);
  
  
}

//--------------------------------------------------------------------------------

void PseudoexperimentsCalculator::init(SystematicHistos* sysHistos,const TString& config_lumi_string,const TString& config_sysnames,const TString& mainDir,const TString& variable,const TString& level,int debugLevel){
  
  m_debug=debugLevel;

  m_sysHistos=sysHistos;
  m_nbins_reco=m_sysHistos->m_nominalHistos->signal_reco->GetNbinsX();
  m_nbins_truth=m_sysHistos->m_nominalHistos->signal_truth->GetNbinsX();
  if (m_debug>0 ) cout << "PseudoexperimentsCalculator: Number of bins: reco level: " << m_nbins_reco << " truth level: " << m_nbins_truth << endl;
  
  m_nsys_all = m_sysHistos->m_sys_names_all.size();
  m_nsys_pairs = m_sysHistos->m_sys_names_pairs.size();
  m_nsys_single = m_sysHistos->m_sys_names_single.size();
  m_nsys_pdf = m_sysHistos->m_sys_names_PDF.size()-1;
  
  m_sys_chain_names_all = m_sysHistos->m_sys_names_all;
  m_sys_chain_names_single = m_sysHistos->m_sys_names_single;
  m_sys_chain_names_pdf = m_sysHistos->m_sys_names_PDF;
  m_sys_chain_names_paired = m_sysHistos->m_sys_names_pairs;
  
  m_hist_template=(TH1D*)m_sysHistos->m_nominalHistos->signal_reco->Clone();
  m_hist_template->Reset();
  
  m_hist_template_truth=(TH1D*)m_sysHistos->m_nominalHistos->signal_truth->Clone();
  m_hist_template_truth->Reset();
  
  m_config_sys_names = new TEnv(config_sysnames);
  m_mainDir=mainDir;
  
  m_ttbarConfig= std::make_unique<TTBarConfig>(config_lumi_string);
  
  m_lumi = functions::getLumi(*m_ttbarConfig);
  m_lumi_string = NamesAndTitles::getLumiString(*m_ttbarConfig);
  m_variable=variable;
  m_level=level;
  m_pseudoexperimentCounter=0;
  m_histLumi = new TH1D("histLumi","",1,0,1);
  m_useBootstrapHistos=0;
}

//--------------------------------------------------------------------------------

void PseudoexperimentsCalculator::prepareShiftsForOnePseudoExperiment(){
	if (m_debug>0 ) cout << "PseudoexperimentsCalculator: Preparing shifts for one pseudo-experiment" << endl;
	m_pseudoexperimentCounter++; // Counting pseudoexperiments
	setShiftsToZero(); // All shifts in stored in vectors are set to 0.
	prepareDataStatShifts(); // Statistical shifts are prepared
	addShiftsForPairedSystematics();
	addShiftsForSingleSystematics();
	addShiftsForSignalModeling();
	normalizeShifts();
	if (m_debug>0 ) cout << m_gap << "Done." << endl;
}

//--------------------------------------------------------------------------------

void PseudoexperimentsCalculator::allocateMemoryForPseudoExperiments(){ // Function to resize vectors. Should be used in initialization phase only before pseudo-experiments are launched.
  
  m_data_shifts.resize(m_nbins_reco);
  m_data_shifts_16_regions.resize(15);
  for(int i=0;i<15;i++)m_data_shifts_16_regions[i].resize(m_nbins_reco);
  m_signal_shifts_onlyStat.resize(m_nbins_reco);
  m_bkg_MC_shifts_onlyStat.resize(m_nbins_reco);
  m_bkg_All_shifts_onlyStat.resize(m_nbins_reco);
  m_pseudodata_shifts_onlyStat.resize(m_nbins_reco);
  
  m_data_shifts_onlySys.resize(m_nbins_reco);
  m_signal_shifts_onlySys.resize(m_nbins_reco);
  m_bkg_MC_shifts_onlySys.resize(m_nbins_reco);
  m_bkg_All_shifts_onlySys.resize(m_nbins_reco);
  m_pseudodata_shifts_onlySys.resize(m_nbins_reco);
  m_MC_16_regions_shifts_onlySys.resize(15);
  m_data_16_regions_shifts_onlySys.resize(15);
  
  for(int i=0;i<15;i++) m_MC_16_regions_shifts_onlySys[i].resize(m_nbins_reco);
  for(int i=0;i<15;i++) m_data_16_regions_shifts_onlySys[i].resize(m_nbins_reco);
  

  m_migration_shifts_onlySys.resize(m_nbins_reco);
  m_EffOfRecoLevelCutsNominator_shifts_onlySys.resize(m_nbins_truth);
  m_EffOfTruthLevelCutsNominator_shifts_onlySys.resize(m_nbins_reco);
  m_signal_truth_shifts_onlySys.resize(m_nbins_truth);
  for(int i=0;i<m_nbins_reco;i++) m_migration_shifts_onlySys[i].resize(m_nbins_truth);

  m_signalModelingShifts.resize(m_nbins_truth);
  m_signalModelingShiftsAlternative.resize(m_nbins_truth);

}

//--------------------------------------------------------------------------------

void PseudoexperimentsCalculator::setShiftsToZero(){ // All shifts in stored in vectors are set to 0.
  std::fill(m_data_shifts.begin(), m_data_shifts.end(), 0);
  std::fill(m_data_shifts_onlySys.begin(), m_data_shifts_onlySys.end(), 0);
  std::fill(m_signal_shifts_onlySys.begin(), m_signal_shifts_onlySys.end(), 0);
  std::fill(m_bkg_MC_shifts_onlySys.begin(), m_bkg_MC_shifts_onlySys.end(), 0);
  std::fill(m_bkg_All_shifts_onlySys.begin(), m_bkg_All_shifts_onlySys.end(), 0);
  std::fill(m_pseudodata_shifts_onlySys.begin(), m_pseudodata_shifts_onlySys.end(), 0);
 
  for(int iRegion=0;iRegion<15;iRegion++){
    std::fill(m_data_16_regions_shifts_onlySys[iRegion].begin(), m_data_16_regions_shifts_onlySys[iRegion].end(), 0);
    std::fill(m_MC_16_regions_shifts_onlySys[iRegion].begin(), m_MC_16_regions_shifts_onlySys[iRegion].end(), 0);
  }
  for(int i=0;i<m_nbins_reco;i++) std::fill(m_migration_shifts_onlySys[i].begin(),m_migration_shifts_onlySys[i].end(),0);
  std::fill(m_EffOfRecoLevelCutsNominator_shifts_onlySys.begin(),m_EffOfRecoLevelCutsNominator_shifts_onlySys.end(),0);
  std::fill(m_EffOfTruthLevelCutsNominator_shifts_onlySys.begin(),m_EffOfTruthLevelCutsNominator_shifts_onlySys.end(),0);
  std::fill(m_signal_truth_shifts_onlySys.begin(),m_signal_truth_shifts_onlySys.end(),0);
  
  std::fill(m_signalModelingShifts.begin(),m_signalModelingShifts.end(),0);
  std::fill(m_signalModelingShiftsAlternative.begin(),m_signalModelingShiftsAlternative.end(),0);
}

//--------------------------------------------------------------------------------

void PseudoexperimentsCalculator::prepareDataStatShifts(){
  int ibin;
  if(m_useBootstrapHistos){  // Shifts are loaded directly from TH1DBootstrap classes if bootstrap histograms are used 
    int iReplica = m_pseudoexperimentCounter-1;
    TH1D* helphist = (TH1D*)m_bootstrapData->GetReplica(iReplica);
    for(int i=0;i<m_nbins_reco;i++){
      ibin=i+1;
      m_data_shifts[i]=helphist->GetBinContent(ibin);
    }
    for(int iRegion=0;iRegion<15;iRegion++){
      helphist = (TH1D*)m_bootstrapSidebandRegionsData[iRegion]->GetReplica(iReplica);
      for(int i=0;i<m_nbins_reco;i++){
	ibin=i+1;
	m_data_shifts_16_regions[iRegion][i]=helphist->GetBinContent(ibin);
      }
    }
  }
  else { // Data are smeared with Poisson distribution if Ndata < 100, otherwise, Gaussian shifts are used
    for(int i=0;i<m_nbins_reco;i++){
      ibin=i+1;
      if(m_sysHistos->m_nominalHistos->data->GetBinContent(ibin)<100.) m_data_shifts[i]=gRandom->Poisson(m_sysHistos->m_nominalHistos->data->GetBinContent(ibin));
      else m_data_shifts[i]=gRandom->Gaus(m_sysHistos->m_nominalHistos->data->GetBinContent(ibin),sqrt(m_sysHistos->m_nominalHistos->data->GetBinContent(ibin)));
    }
    for(int iRegion=0;iRegion<15;iRegion++)for(int i=0;i<m_nbins_reco;i++){
      ibin=i+1;
      if(m_sysHistos->m_nominalHistos->sidebandRegions_data[iRegion]->GetBinContent(ibin)<100.)m_data_shifts_16_regions[iRegion][i]=gRandom->Poisson(m_sysHistos->m_nominalHistos->sidebandRegions_data[iRegion]->GetBinContent(ibin));
      else m_data_shifts_16_regions[iRegion][i]=gRandom->Gaus(m_sysHistos->m_nominalHistos->sidebandRegions_data[iRegion]->GetBinContent(ibin),sqrt(m_sysHistos->m_nominalHistos->sidebandRegions_data[iRegion]->GetBinContent(ibin)));
    }
  }
}

//--------------------------------------------------------------------------------------

void PseudoexperimentsCalculator::addShiftForSingleSystematicBranch(const OneSysHistos* sysHistos, const OneSysHistos* nominalHistos,bool shiftLumi,const double& y){
  
  int i(0),j(0),ibin(0),jbin(0);
   
  if(shiftLumi){ // Shifting value of luminosity. Needed only for alternative approach of propagating uncertainties (shifting unfolding corrections)
	
    if (m_debug>2 ) {
      cout << m_gap << "Calculating shift for luminosity " << endl;
      std::cout << "Rel. shift: " << (sysHistos->signal_reco->Integral() - nominalHistos->signal_reco->Integral())/nominalHistos->signal_reco->Integral() << " y: " << y << " m_lumi: " << m_lumi << std::endl;
    }
    m_lumi_shifted=(1.+(sysHistos->signal_reco->Integral() - nominalHistos->signal_reco->Integral())/nominalHistos->signal_reco->Integral()*y)*m_lumi;
    if (m_debug>2 ) cout << m_gap << m_lumi_shifted << " " << m_lumi << endl;
  
  }
  // Preparing shifts for reco level spectra
  for(i=0;i<m_nbins_reco;i++){
    ibin=i+1;
    // Signal shifts should be taken with negative sign if using standard procedure to propagate systematics (Shifting data and keeping unfolding corrections nominal)
    m_data_shifts_onlySys[i]-= (sysHistos->data->GetBinContent(ibin) - nominalHistos->data->GetBinContent(ibin))*y; 
    m_signal_shifts_onlySys[i]+= (sysHistos->signal_reco->GetBinContent(ibin) - nominalHistos->signal_reco->GetBinContent(ibin))*y; 
    m_bkg_MC_shifts_onlySys[i]+= (sysHistos->bkg_MC->GetBinContent(ibin) - nominalHistos->bkg_MC->GetBinContent(ibin))*y;
    m_bkg_All_shifts_onlySys[i]+= (sysHistos->bkg_all->GetBinContent(ibin) - nominalHistos->bkg_all->GetBinContent(ibin))*y;
    m_pseudodata_shifts_onlySys[i]+= (sysHistos->pseudodata->GetBinContent(ibin) - nominalHistos->pseudodata->GetBinContent(ibin))*y;
  }
  // Preparing shifts for the multijet background estimate. Shifting MC distributions in sideband regions. 
  for(int iRegion=0;iRegion<15;iRegion++) for(i=0;i<m_nbins_reco;i++){ 
    ibin=i+1;
    m_data_16_regions_shifts_onlySys[iRegion][i]+= (sysHistos->sidebandRegions_data[iRegion]->GetBinContent(ibin) - nominalHistos->sidebandRegions_data[iRegion]->GetBinContent(ibin))*y;
    m_MC_16_regions_shifts_onlySys[iRegion][i]+= (sysHistos->sidebandRegions_MC[iRegion]->GetBinContent(ibin) - nominalHistos->sidebandRegions_MC[iRegion]->GetBinContent(ibin))*y;
  }
  
  // Preparing shifts for migration matrix, acceptances and efficiencies
  for(i=0;i<m_nbins_reco;i++){
    ibin=i+1;
    for(j=0;j<m_nbins_truth;j++){
      jbin=j+1;
      m_migration_shifts_onlySys[i][j]+= (sysHistos->migration->GetBinContent(ibin,jbin) - nominalHistos->migration->GetBinContent(ibin,jbin))*y; // Warning! Shifted migration matrix could have negative values. Need to procced carefuly!
    }
    m_EffOfTruthLevelCutsNominator_shifts_onlySys[i] += (sysHistos->effOfTruthLevelCutsNominator->GetBinContent(ibin) - nominalHistos->effOfTruthLevelCutsNominator->GetBinContent(ibin))*y;
  }
  for(i=0;i<m_nbins_truth;i++) {
    int ibin=i+1;
    m_EffOfRecoLevelCutsNominator_shifts_onlySys[i] += (sysHistos->effOfRecoLevelCutsNominator->GetBinContent(ibin) - nominalHistos->effOfRecoLevelCutsNominator->GetBinContent(ibin))*y;
    m_signal_truth_shifts_onlySys[i] += (sysHistos->signal_truth->GetBinContent(ibin) - nominalHistos->signal_truth->GetBinContent(ibin))*y; // This should be always zero with current configuration.
  }
}
//--------------------------------------------------------------------------------------

void PseudoexperimentsCalculator::addShiftsForPairedSystematics(){ // Adding shifts for systematic branches with asymetric 
  double x,y;
  
  const OneSysHistos* nominalHistos=m_sysHistos->m_nominalHistos;
  for(int isys=0;isys<m_nsys_pairs;isys++){ // Loop over systematic branches
    x=gRandom->Gaus(); // Single Gaussian number with mu=0, sigma=1 is generated for each systematic branch 
    y=abs(x); 	       // Absolute value of gaussian number is used as multiplicative factor for the shifts: random shift = y*sigma, where sigma is sigma_up or sigma_down depending on sign of x
    
    const OneSysHistos* sysHistosFirst=m_sysHistos->m_sysHistosPaired[isys].first; // First is down.
    const OneSysHistos* sysHistosSecond=m_sysHistos->m_sysHistosPaired[isys].second;
    
    bool shiftLumi = (m_sys_chain_names_paired[isys].first=="luminosity_1down"); // Need to check what is the current name of the luminosity branch
    
    // 50% of pseudoexperiments will get shift up and 50% will get shift down
    if(x>=0){ // if x >= 0: sysHistosFirst are used
      this->addShiftForSingleSystematicBranch(sysHistosFirst, nominalHistos,shiftLumi, y);
    } // end if x >= 0
    else { // if x < 0
      this->addShiftForSingleSystematicBranch(sysHistosSecond, nominalHistos,shiftLumi, y);
    } // End if x < 0
  } // End of loop over systematic branches
}

//-----------------------------------------------------------------------------

void PseudoexperimentsCalculator::addShiftsForSingleSystematics(){
  double x;
  OneSysHistos* nominalHistos=m_sysHistos->m_nominalHistos;
  for(int isys=0;isys<m_nsys_single;isys++){
    OneSysHistos* sysHistos=m_sysHistos->m_sysHistosSingle[isys];
    x=gRandom->Gaus();
    this->addShiftForSingleSystematicBranch(sysHistos, nominalHistos,false, x);
  }
}

//-----------------------------------------------------------------------------

void PseudoexperimentsCalculator::addShiftsForSignalModeling(){
  
  addShiftForSignalModeling(m_ttbarConfig->hardScatteringDirectory());
  addShiftForSignalModeling(m_ttbarConfig->partonShoweringDirectory());
  
  bool useNewISR = true;
  
  if(useNewISR) {
    addShiftForSignalModeling("hdamp");
    addShiftForSignalModeling(m_ttbarConfig->ISRmuRupDirectory(),m_ttbarConfig->ISRmuRdownDirectory());
    addShiftForSignalModeling(m_ttbarConfig->ISRmuFupDirectory(),m_ttbarConfig->ISRmuFdownDirectory());
    addShiftForSignalModeling(m_ttbarConfig->ISRVar3cUpDirectory(),m_ttbarConfig->ISRVar3cDownDirectory());
  }
  else {
    addShiftForSignalModeling(m_ttbarConfig->ISRupDirectory(),m_ttbarConfig->ISRdownDirectory());
  }
  addShiftForSignalModeling(m_ttbarConfig->FSRupDirectory(),m_ttbarConfig->FSRdownDirectory());
  
  const int nsysPDF=m_sysHistos->m_nsys_PDF-1;
  for(int isys=0;isys<nsysPDF;isys++){
    const double x = gRandom->Gaus();
    addShiftForSignalModeling(x,m_sysHistos->m_shifts_PDF[isys],m_sysHistos->m_shifts_PDF_alternative[isys]);
  }
  
}

//-----------------------------------------------------------------------------

void PseudoexperimentsCalculator::addShiftForSignalModeling(const TString& name){
  addShiftForSignalModeling(gRandom->Gaus(),m_sysHistos->m_shifts_signalModeling[name],m_sysHistos->m_shifts_signalModeling_alternative[name]);
}

//-----------------------------------------------------------------------------

void PseudoexperimentsCalculator::addShiftForSignalModeling(const TString& name_up,const TString& name_down){
  const double x = gRandom->Gaus();
  const double y = std::abs(x);
  const TString& name = x > 0. ? name_up : name_down; // Playing lotery if to use shift up or shift down
  addShiftForSignalModeling(y,m_sysHistos->m_shifts_signalModeling[name],m_sysHistos->m_shifts_signalModeling_alternative[name]);
  
}

//-----------------------------------------------------------------------------

void PseudoexperimentsCalculator::addShiftForSignalModeling(const double x,TH1D* hshift,TH1D* hshiftAlternative){
  int ibin(0);  
  for(int i=0;i<m_nbins_truth;i++){
    ibin=i+1;
    m_signalModelingShifts[i] += x*hshift->GetBinContent(ibin);
    m_signalModelingShiftsAlternative[i] += x*hshiftAlternative->GetBinContent(ibin);
  }
}

//-----------------------------------------------------------------------------

void PseudoexperimentsCalculator::normalizeShifts(){
  int ibin;
  OneSysHistos* nominalHistos=m_sysHistos->m_nominalHistos;
  for(int i=0;i<m_nbins_reco;i++){
    ibin=i+1;
    m_data_shifts_onlySys[i]/=nominalHistos->data->GetBinContent(ibin);
    m_signal_shifts_onlySys[i]/=nominalHistos->signal_reco->GetBinContent(ibin);
    m_bkg_MC_shifts_onlySys[i]/=nominalHistos->bkg_MC->GetBinContent(ibin);
    m_bkg_All_shifts_onlySys[i]/=nominalHistos->bkg_all->GetBinContent(ibin);
    m_pseudodata_shifts_onlySys[i]/=nominalHistos->pseudodata->GetBinContent(ibin);
    m_EffOfTruthLevelCutsNominator_shifts_onlySys[i]/=nominalHistos->effOfTruthLevelCutsNominator->GetBinContent(ibin);
  }
  for(int i=0;i<m_nbins_truth;i++){
    int ibin=i+1;
    m_EffOfRecoLevelCutsNominator_shifts_onlySys[i]/=nominalHistos->effOfRecoLevelCutsNominator->GetBinContent(ibin);
    m_signal_truth_shifts_onlySys[i]/=nominalHistos->signal_truth->GetBinContent(ibin);
  }
  for(int iRegion=0;iRegion<15;iRegion++)for(int i=0;i<m_nbins_reco;i++){
    ibin=i+1;
    m_MC_16_regions_shifts_onlySys[iRegion][i]/=nominalHistos->sidebandRegions_MC[iRegion]->GetBinContent(ibin);
    m_data_16_regions_shifts_onlySys[iRegion][i]/=nominalHistos->sidebandRegions_data[iRegion]->GetBinContent(ibin);
  }
}

//-----------------------------------------------------------------------------

void PseudoexperimentsCalculator::preparePseudoexperiment(Pseudoexperiment* pseudoExperiment){
  if (m_debug>1) cout << "PseudoexperimentsCalculator::preparePseudoexperiment" << endl;
  OneSysHistos* nominalHistos=m_sysHistos->m_nominalHistos;
  if (m_debug>2) cout << "PseudoexperimentsCalculator: Preparing stat part of the pseudoexperiment" << endl;
  prepareStatPartOfPseudoexperiment(pseudoExperiment);
  if (m_debug>2) cout << "PseudoexperimentsCalculator: Preparing stat part of the pseudoexperiment done." << endl;
  
  if (m_debug>2) cout << "PseudoexperimentsCalculator: Preparing ABCD estimates" << endl;
  functions::Calculate_ABCD16_estimate(nominalHistos->sidebandRegions_data,nominalHistos->sidebandRegions_MC,pseudoExperiment->m_bkgMultijetOnlyStat);
  if (m_debug>2) cout << "PseudoexperimentsCalculator: Preparing ABCD estimates done." << endl;
  
  prepareSysPartOfPseudoexperiment(pseudoExperiment);
  
  preparePseudodataForPseudoexperiment(pseudoExperiment);
  
  prepareSignalModelingPartOfPseudoexperiment(pseudoExperiment);
  if (m_debug>1) cout << "PseudoexperimentsCalculator::preparePseudoexperiment finished" << endl;
}

//-----------------------------------------------------------------------------

void PseudoexperimentsCalculator::prepareStatPartOfPseudoexperiment(Pseudoexperiment* pseudoExperiment){
  OneSysHistos* nominalHistos=m_sysHistos->m_nominalHistos;
  if(m_useBootstrapHistos){ // For the bootstrap method, bootstrapped migrations are not available, so they are smeared always with the standard method
    int iReplica=m_pseudoexperimentCounter-1;
    TH1D* helphist=(TH1D*)m_bootstrapData->GetReplica(iReplica);
    for(int ibin=1;ibin<=m_nbins_reco;ibin++)pseudoExperiment->m_dataOnlyStat->SetBinContent(ibin,helphist->GetBinContent(ibin));
    helphist=(TH1D*)m_bootstrapMC->GetReplica(iReplica);
    for(int ibin=1;ibin<=m_nbins_reco;ibin++)pseudoExperiment->m_pseudodataMCStat->SetBinContent(ibin,helphist->GetBinContent(ibin));
    
    pseudoExperiment->prepareSignalMCStat(nominalHistos->migration,nominalHistos->effOfRecoLevelCutsNominator,nominalHistos->signal_reco,nominalHistos->effOfTruthLevelCutsNominator,nominalHistos->signal_truth);
    helphist=(TH1D*)m_bootstrap_signal->GetReplica(iReplica);
    for(int ibin=1;ibin<=m_nbins_reco;ibin++)pseudoExperiment->m_signalRecoOnlyStat->SetBinContent(ibin,helphist->GetBinContent(ibin));
    
    helphist=(TH1D*)m_bootstrap_bkg_MC->GetReplica(iReplica);
    for(int ibin=1;ibin<=m_nbins_reco;ibin++)pseudoExperiment->m_bkgMCOnlyStat->SetBinContent(ibin,helphist->GetBinContent(ibin));
    
    for(int iRegion=0;iRegion<15;iRegion++){
      helphist=(TH1D*)m_bootstrapSidebandRegionsData[iRegion]->GetReplica(iReplica);
      for(int ibin=1;ibin<=m_nbins_reco;ibin++)pseudoExperiment->m_ABCDInputsDataOnlyStat[iRegion]->SetBinContent(ibin,helphist->GetBinContent(ibin));
      helphist=(TH1D*)m_bootstrapSidebandRegionsMC[iRegion]->GetReplica(iReplica);
      for(int ibin=1;ibin<=m_nbins_reco;ibin++)pseudoExperiment->m_ABCDInputsMCOnlyStat[iRegion]->SetBinContent(ibin,helphist->GetBinContent(ibin));
    }
    //pseudoExperiment->prepareABCDInputsStat(nominalHistos->sidebandRegions_data,nominalHistos->sidebandRegions_MC);
  }
  else{
    if (m_debug>2) cout << "Preparing data stat" << endl;
    pseudoExperiment->prepareDataStat(nominalHistos->data);
    if (m_debug>2) cout << "Preparing data stat done" << endl;
    if (m_debug>2) cout << "Preparing signal MC stat" << endl;
    pseudoExperiment->prepareSignalMCStat(nominalHistos->migration,nominalHistos->effOfRecoLevelCutsNominator,nominalHistos->signal_reco,nominalHistos->effOfTruthLevelCutsNominator,nominalHistos->signal_truth);
    if (m_debug>2) cout << "Preparing signal MC stat done" << endl;
    pseudoExperiment->prepareBkgMCStat(nominalHistos->bkg_MC);
    pseudoExperiment->prepareABCDInputsStat(nominalHistos->sidebandRegions_data,nominalHistos->sidebandRegions_MC);
  }
}

//-----------------------------------------------------------------------------

void PseudoexperimentsCalculator::prepareSysPartOfPseudoexperiment(Pseudoexperiment* pseudoExperiment){
  OneSysHistos* nominalHistos=m_sysHistos->m_nominalHistos;
  int ibin,jbin;
  // Adding shifted luminosity into histogram
  pseudoExperiment->m_histLumi->SetBinContent(1,m_lumi_shifted);
  m_histLumi->SetBinContent(1,m_lumi_shifted);
  double error;
  
  // Calculating shifted migrations
  for(int i=0;i<m_nbins_reco;i++){
    ibin=i+1;
    const double& shiftDataRel = m_data_shifts_onlySys[i]; // Shift is relative
    for(int j=0;j<m_nbins_truth;j++){
      jbin=j+1;
      const double shift=m_migration_shifts_onlySys[i][j]; // shift is absolute, no relative!!!
      const double nominal=nominalHistos->migration->GetBinContent(ibin,jbin);
      const double stat=pseudoExperiment->m_migrationOnlyStat->GetBinContent(ibin,jbin);
      pseudoExperiment->m_migrationOnlySys->SetBinContent(ibin,jbin,nominalHistos->migration->GetBinContent(ibin,jbin) + shift);
      if(pseudoExperiment->m_migrationOnlySys->GetBinContent(ibin,jbin)<=0) pseudoExperiment->m_migrationOnlySys->SetBinContent(ibin,jbin,0.);
      else pseudoExperiment->m_migrationOnlySys->SetBinContent(ibin,jbin,pseudoExperiment->m_migrationOnlySys->GetBinContent(ibin,jbin)*(1.+shiftDataRel));
      
      if(nominal>0 && stat >0)pseudoExperiment->m_migrationStatAndSys->SetBinContent(ibin,jbin,stat*(1. + shift/nominal));
      else pseudoExperiment->m_migrationStatAndSys->SetBinContent(ibin,jbin,pseudoExperiment->m_migrationOnlyStat->GetBinContent(ibin,jbin) + shift); 
      pseudoExperiment->m_migrationStatAndSys->SetBinContent(ibin,jbin,pseudoExperiment->m_migrationStatAndSys->GetBinContent(ibin,jbin)*(1.+shiftDataRel));
      
      
      if(pseudoExperiment->m_migrationStatAndSys->GetBinContent(ibin,jbin)<0)pseudoExperiment->m_migrationStatAndSys->SetBinContent(ibin,jbin,0.); // remove negative elements from migration matrix (they should not be there)
    }
  }
  for(int i=0;i<m_nbins_reco;i++){
    ibin=i+1;
    const double& shiftDataRel = m_data_shifts_onlySys[i]; // Shift is relative
    
    // Calculating shifted acceptances
    pseudoExperiment->m_effOfTruthLevelCutsNominatorOnlySys->SetBinContent(ibin,pseudoExperiment->m_migrationOnlySys->IntegralAndError(ibin,ibin,1,m_nbins_reco,error));
    pseudoExperiment->m_effOfTruthLevelCutsNominatorStatAndSys->SetBinContent(ibin,pseudoExperiment->m_migrationStatAndSys->IntegralAndError(ibin,ibin,1,m_nbins_reco,error));
    // Calculating shifted signal reco
    pseudoExperiment->m_signalRecoOnlySys->SetBinContent(ibin,nominalHistos->signal_reco->GetBinContent(ibin)*(1.+m_signal_shifts_onlySys[i])*(1.+shiftDataRel));
    pseudoExperiment->m_signalRecoStatAndSys->SetBinContent(ibin,pseudoExperiment->m_signalRecoOnlyStat->GetBinContent(ibin)*(1.+m_signal_shifts_onlySys[i])*(1.+shiftDataRel));
    // Calculating shifted MC background
    pseudoExperiment->m_bkgMCOnlySys->SetBinContent(ibin,nominalHistos->bkg_MC->GetBinContent(ibin)*(1.+m_bkg_MC_shifts_onlySys[i])*(1.+shiftDataRel));
    pseudoExperiment->m_bkgMCStatAndSys->SetBinContent(ibin,pseudoExperiment->m_bkgMCOnlyStat->GetBinContent(ibin)*(1.+m_bkg_MC_shifts_onlySys[i])*(1.+shiftDataRel));
    
     
  }
  for(int i=0;i<m_nbins_truth;i++){
    ibin=i+1;
    // Calculating shifted efficiencies
    pseudoExperiment->m_effOfRecoLevelCutsNominatorOnlySys->SetBinContent(ibin,pseudoExperiment->m_migrationOnlySys->IntegralAndError(1,m_nbins_reco,ibin,ibin,error));
    pseudoExperiment->m_effOfRecoLevelCutsNominatorStatAndSys->SetBinContent(ibin,pseudoExperiment->m_migrationStatAndSys->IntegralAndError(1,m_nbins_reco,ibin,ibin,error));
    // Calculating shifted MC truth
    pseudoExperiment->m_signalTruthOnlySys->SetBinContent(ibin,nominalHistos->signal_truth->GetBinContent(ibin)*(1.+m_signal_truth_shifts_onlySys[i]));
    pseudoExperiment->m_signalTruthStatAndSys->SetBinContent(ibin,pseudoExperiment->m_signalTruthOnlyStat->GetBinContent(ibin)*(1.+m_signal_truth_shifts_onlySys[i]));
  }
  
  
  // Calculating shifted distributions in the sideband regions
  for(int iRegion=0;iRegion<15;iRegion++)for(int i=0;i<m_nbins_reco;i++) {
    ibin=i+1;
    const double& shiftDataRel = m_data_16_regions_shifts_onlySys[iRegion][i]; // Shift is relative
    pseudoExperiment->m_ABCDInputsMCOnlySys[iRegion]->SetBinContent(ibin, nominalHistos->sidebandRegions_MC[iRegion]->GetBinContent(ibin)*(1.+m_MC_16_regions_shifts_onlySys[iRegion][i])*(1.+shiftDataRel));
    pseudoExperiment->m_ABCDInputsMCStatAndSys[iRegion]->SetBinContent(ibin,pseudoExperiment->m_ABCDInputsMCOnlyStat[iRegion]->GetBinContent(ibin)*(1.+m_MC_16_regions_shifts_onlySys[iRegion][i])*(1.+shiftDataRel));
    pseudoExperiment->m_ABCDInputsDataOnlySys[iRegion]->SetBinContent(ibin, nominalHistos->sidebandRegions_data[iRegion]->GetBinContent(ibin)*(1.+m_MC_16_regions_shifts_onlySys[iRegion][i])*(1.+shiftDataRel));
    pseudoExperiment->m_ABCDInputsDataStatAndSys[iRegion]->SetBinContent(ibin,pseudoExperiment->m_ABCDInputsDataOnlyStat[iRegion]->GetBinContent(ibin)*(1.+m_MC_16_regions_shifts_onlySys[iRegion][i])*(1.+shiftDataRel));
  }
  // Calculating shifted ABCD estimates
  functions::Calculate_ABCD16_estimate(pseudoExperiment->m_ABCDInputsDataOnlyStat,pseudoExperiment->m_ABCDInputsMCOnlyStat,pseudoExperiment->m_bkgMultijetOnlyStat);
  functions::Calculate_ABCD16_estimate(nominalHistos->sidebandRegions_data,pseudoExperiment->m_ABCDInputsMCOnlySys,pseudoExperiment->m_bkgMultijetOnlySys);
  functions::Calculate_ABCD16_estimate(pseudoExperiment->m_ABCDInputsDataOnlyStat,pseudoExperiment->m_ABCDInputsMCStatAndSys,pseudoExperiment->m_bkgMultijetStatAndSys);

  
}

//-----------------------------------------------------------------------------
//  
void PseudoexperimentsCalculator::preparePseudodataForPseudoexperiment(Pseudoexperiment* pseudoExperiment){
  OneSysHistos* nominalHistos=m_sysHistos->m_nominalHistos;
  pseudoExperiment->m_bkgAllOnlyStat->Reset();
  pseudoExperiment->m_bkgAllOnlySys->Reset();
  pseudoExperiment->m_bkgAllStatAndSys->Reset();
  pseudoExperiment->m_bkgAllOnlyStat->Add(pseudoExperiment->m_bkgMCOnlyStat);
  pseudoExperiment->m_bkgAllOnlyStat->Add(pseudoExperiment->m_bkgMultijetOnlyStat);
  pseudoExperiment->m_bkgAllOnlySys->Add(pseudoExperiment->m_bkgMCOnlySys);
  pseudoExperiment->m_bkgAllOnlySys->Add(pseudoExperiment->m_bkgMultijetOnlySys);
  pseudoExperiment->m_bkgAllStatAndSys->Add(pseudoExperiment->m_bkgMCStatAndSys);
  pseudoExperiment->m_bkgAllStatAndSys->Add(pseudoExperiment->m_bkgMultijetStatAndSys);
  
  pseudoExperiment->m_pseudodataMCStat->Reset();
  pseudoExperiment->m_pseudodataDataStat->Reset();
  pseudoExperiment->m_pseudodataSys->Reset();
  pseudoExperiment->m_pseudodataMCStatAndSys->Reset();
  pseudoExperiment->m_pseudodataDataStatMCStatAndSys->Reset();
  pseudoExperiment->m_pseudodataMCStat->Add(pseudoExperiment->m_signalRecoOnlyStat);
  pseudoExperiment->m_pseudodataMCStat->Add(pseudoExperiment->m_bkgMCOnlyStat);
  pseudoExperiment->m_pseudodataMCStat->Add(pseudoExperiment->m_bkgMultijetOnlyStat);
  pseudoExperiment->m_pseudodataSys->Add(pseudoExperiment->m_signalRecoOnlySys);
  pseudoExperiment->m_pseudodataSys->Add(pseudoExperiment->m_bkgMCOnlySys);
  pseudoExperiment->m_pseudodataSys->Add(pseudoExperiment->m_bkgMultijetOnlySys);
  pseudoExperiment->m_pseudodataMCStatAndSys->Add(pseudoExperiment->m_signalRecoStatAndSys);
  pseudoExperiment->m_pseudodataMCStatAndSys->Add(pseudoExperiment->m_bkgMCStatAndSys);
  pseudoExperiment->m_pseudodataMCStatAndSys->Add(pseudoExperiment->m_bkgMultijetStatAndSys);
  int ibin;
  double bincontentNominal,bincontentDataStat;
  double pseudodataNominal,pseudodataSys,pseudodataMCStat,pseudodataMCStatAndSys;
  
  
  for(int i=0;i<m_nbins_reco;i++) {
    ibin=i+1;
    bincontentNominal=nominalHistos->data->GetBinContent(ibin);
    bincontentDataStat=pseudoExperiment->m_dataOnlyStat->GetBinContent(ibin);
    pseudodataNominal=nominalHistos->pseudodata->GetBinContent(ibin);
    pseudodataSys=pseudoExperiment->m_pseudodataSys->GetBinContent(ibin);
    pseudodataMCStat=pseudoExperiment->m_pseudodataMCStat->GetBinContent(ibin);
    pseudodataMCStatAndSys=pseudoExperiment->m_pseudodataMCStatAndSys->GetBinContent(ibin);
    
    // Inverting sign of shifts for pseudodata
    pseudoExperiment->m_pseudodataSys->SetBinContent(ibin,2.*pseudodataNominal - pseudodataSys); // Pseudodata have to be shifted with opposite sign
    pseudoExperiment->m_pseudodataMCStat->SetBinContent(ibin,2.*pseudodataNominal - pseudodataMCStat); // Pseudodata have to be shifted with opposite sign
    pseudoExperiment->m_pseudodataMCStatAndSys->SetBinContent(ibin,2.*pseudodataNominal - pseudodataMCStatAndSys); // Pseudodata have to be shifted with opposite sign
    
    
    if(bincontentNominal>0){
      pseudoExperiment->m_pseudodataDataStatAndSys->SetBinContent(ibin,pseudoExperiment->m_pseudodataSys->GetBinContent(ibin)*(1.+(bincontentDataStat-bincontentNominal)/bincontentNominal) );
      pseudoExperiment->m_pseudodataDataStatMCStatAndSys->SetBinContent(ibin,pseudoExperiment->m_pseudodataMCStatAndSys->GetBinContent(ibin)*(1.+(bincontentDataStat-bincontentNominal)/bincontentNominal) );
      pseudoExperiment->m_pseudodataDataStat->SetBinContent(ibin,pseudodataNominal*(1.+(bincontentDataStat-bincontentNominal)/bincontentNominal) );
    }
    if(pseudodataNominal>0){
      pseudoExperiment->m_dataOnlyMCStat->SetBinContent(ibin,bincontentNominal*(1.-(pseudodataMCStat - pseudodataNominal)/pseudodataNominal));
      pseudoExperiment->m_dataOnlySys->SetBinContent(ibin,bincontentNominal*(1.-(pseudodataSys - pseudodataNominal)/pseudodataNominal)); // Data have to be shifted with opposite sign
      pseudoExperiment->m_dataStatAndSys->SetBinContent(ibin,bincontentDataStat*(1.-(pseudodataSys - pseudodataNominal)/pseudodataNominal)); // Data have to be shifted with opposite sign
      pseudoExperiment->m_dataStatMCStatAndSys->SetBinContent(ibin,pseudoExperiment->m_dataStatAndSys->GetBinContent(ibin)*(1.-(pseudodataMCStat - pseudodataNominal)/pseudodataNominal)); // Data have to be shifted with opposite sign
    }
  }
  
  
  
}

//-----------------------------------------------------------------------------

void PseudoexperimentsCalculator::prepareSignalModelingPartOfPseudoexperiment(Pseudoexperiment* pseudoExperiment){
  
  for(int i=0;i<m_nbins_truth;i++){
    pseudoExperiment->m_signalModelingShifts[i] = m_signalModelingShifts[i];
    pseudoExperiment->m_signalModelingShiftsAlternative[i] = m_signalModelingShiftsAlternative[i];
    //std::cout << "signalModeling shifts: " << i+1 << ": " << m_signalModelingShifts[i] << std::endl;;
  }
  
}

//-----------------------------------------------------------------------------

// Nominal pseudoexperiment should be loaded only in initialization phase.
void PseudoexperimentsCalculator::prepareNominalPseudoexperiment(Pseudoexperiment* pseudoExperiment){
  int ibin;
  OneSysHistos* nominalHistos=m_sysHistos->m_nominalHistos;
  for(int i=0;i<m_nbins_reco;i++){
    ibin=i+1;
    pseudoExperiment->m_dataOnlyStat->SetBinContent(ibin,nominalHistos->data->GetBinContent(ibin) );
    pseudoExperiment->m_dataOnlyMCStat->SetBinContent(ibin,nominalHistos->data->GetBinContent(ibin) );
    pseudoExperiment->m_dataOnlySys->SetBinContent(ibin,nominalHistos->data->GetBinContent(ibin) );
    pseudoExperiment->m_dataStatAndSys->SetBinContent(ibin,nominalHistos->data->GetBinContent(ibin) );
    pseudoExperiment->m_dataStatMCStatAndSys->SetBinContent(ibin,nominalHistos->data->GetBinContent(ibin) );
    pseudoExperiment->m_signalRecoOnlyStat->SetBinContent(ibin,nominalHistos->signal_reco->GetBinContent(ibin) );
    pseudoExperiment->m_signalRecoOnlySys->SetBinContent(ibin,nominalHistos->signal_reco->GetBinContent(ibin) );
    pseudoExperiment->m_signalRecoStatAndSys->SetBinContent(ibin,nominalHistos->signal_reco->GetBinContent(ibin) );
    pseudoExperiment->m_bkgMCOnlyStat->SetBinContent(ibin,nominalHistos->bkg_MC->GetBinContent(ibin) );
    pseudoExperiment->m_bkgMCOnlySys->SetBinContent(ibin,nominalHistos->bkg_MC->GetBinContent(ibin) );
    pseudoExperiment->m_bkgMCStatAndSys->SetBinContent(ibin,nominalHistos->bkg_MC->GetBinContent(ibin) );
    pseudoExperiment->m_bkgMultijetOnlyStat->SetBinContent(ibin,nominalHistos->bkg_Multijet->GetBinContent(ibin) );
    pseudoExperiment->m_bkgMultijetOnlySys->SetBinContent(ibin,nominalHistos->bkg_Multijet->GetBinContent(ibin) );
    pseudoExperiment->m_bkgMultijetStatAndSys->SetBinContent(ibin,nominalHistos->bkg_Multijet->GetBinContent(ibin) );
    pseudoExperiment->m_bkgAllOnlyStat->SetBinContent(ibin,nominalHistos->bkg_all->GetBinContent(ibin) );
    pseudoExperiment->m_bkgAllOnlySys->SetBinContent(ibin,nominalHistos->bkg_all->GetBinContent(ibin) );
    pseudoExperiment->m_bkgAllStatAndSys->SetBinContent(ibin,nominalHistos->bkg_all->GetBinContent(ibin) );
    pseudoExperiment->m_pseudodataMCStat->SetBinContent(ibin,nominalHistos->pseudodata->GetBinContent(ibin) );
    pseudoExperiment->m_pseudodataDataStat->SetBinContent(ibin,nominalHistos->pseudodata->GetBinContent(ibin) );
    pseudoExperiment->m_pseudodataSys->SetBinContent(ibin,nominalHistos->pseudodata->GetBinContent(ibin) );
    pseudoExperiment->m_pseudodataMCStatAndSys->SetBinContent(ibin,nominalHistos->pseudodata->GetBinContent(ibin) );
    pseudoExperiment->m_pseudodataDataStatMCStatAndSys->SetBinContent(ibin,nominalHistos->pseudodata->GetBinContent(ibin) );
    
    
    pseudoExperiment->m_effOfTruthLevelCutsNominatorOnlyStat->SetBinContent(ibin,nominalHistos->effOfTruthLevelCutsNominator->GetBinContent(ibin) );
    pseudoExperiment->m_effOfTruthLevelCutsNominatorOnlySys->SetBinContent(ibin,nominalHistos->effOfTruthLevelCutsNominator->GetBinContent(ibin) );
    pseudoExperiment->m_effOfTruthLevelCutsNominatorStatAndSys->SetBinContent(ibin,nominalHistos->effOfTruthLevelCutsNominator->GetBinContent(ibin) );
  }
  for(int i=0;i<m_nbins_truth;i++){
    ibin=i+1;
    
    pseudoExperiment->m_signalTruthOnlyStat->SetBinContent(ibin,nominalHistos->signal_truth->GetBinContent(ibin) );
    pseudoExperiment->m_signalTruthOnlySys->SetBinContent(ibin,nominalHistos->signal_truth->GetBinContent(ibin) );
    pseudoExperiment->m_signalTruthStatAndSys->SetBinContent(ibin,nominalHistos->signal_truth->GetBinContent(ibin) );
    pseudoExperiment->m_effOfRecoLevelCutsNominatorOnlyStat->SetBinContent(ibin,nominalHistos->effOfRecoLevelCutsNominator->GetBinContent(ibin) );
    pseudoExperiment->m_effOfRecoLevelCutsNominatorOnlySys->SetBinContent(ibin,nominalHistos->effOfRecoLevelCutsNominator->GetBinContent(ibin) );
    pseudoExperiment->m_effOfRecoLevelCutsNominatorStatAndSys->SetBinContent(ibin,nominalHistos->effOfRecoLevelCutsNominator->GetBinContent(ibin) );
  }
  pseudoExperiment->m_histLumi->SetBinContent(1,m_lumi);

  for(int i=0;i<m_nbins_reco;i++)for(int j=0;j<m_nbins_truth;j++){
    pseudoExperiment->m_migrationOnlyStat->SetBinContent(i+1,j+1,nominalHistos->migration->GetBinContent(i+1,j+1));
    pseudoExperiment->m_migrationOnlySys->SetBinContent(i+1,j+1,nominalHistos->migration->GetBinContent(i+1,j+1));
    pseudoExperiment->m_migrationStatAndSys->SetBinContent(i+1,j+1,nominalHistos->migration->GetBinContent(i+1,j+1));
  }

}

//-----------------------------------------------------------------------------

TH1D* PseudoexperimentsCalculator::Calculate_ABCD16_estimate(const vector<TH1D*>& data,const vector<TH1D*>& MC,bool calculate_errors){
  TH1D* result=(TH1D*)data[0]->Clone();
  //this->Calculate_ABCD16_estimate(data,MC,result,calculate_errors);
  functions::Calculate_ABCD16_estimate(data,MC,result,calculate_errors);
  return result;
}

//-----------------------------------------------------------------------------
// Bootstrap histograms can replace Data stat and MC stat shifts if requested
void PseudoexperimentsCalculator::loadBootstrapHistos(TFile*f,const TString& variable){
  if (m_debug>1) cout << "PseudoexperimentsCalculator: Loading Bootstrap Histos" << endl;
  this->useBootstrap();
  const TString nominal="nominal/";
  m_bootstrapData= (TH1DBootstrap*)f->Get(nominal+variable+"_boots_data");
  m_bootstrapMC= (TH1DBootstrap*)f->Get(nominal+variable+"_boots_pseudodata");
  m_bootstrap_signal= (TH1DBootstrap*)f->Get(nominal+variable+"_boots_signal");
  m_bootstrap_bkg_MC= (TH1DBootstrap*)f->Get(nominal+variable+"_boots_bkg_MC");
  m_bootstrapSidebandRegionsData.resize(15);
  m_bootstrapSidebandRegionsMC.resize(15);
  
  char x='A';
  for(int i=0;i<15;i++){
   if (m_debug>2) cout << "PseudoexperimentsCalculator::loadBootstrapHistos: Region: " << x << endl;
   m_bootstrapSidebandRegionsData[i]=(TH1DBootstrap*)f->Get(nominal+"SidebandRegions/" + variable+"_data_"+x);
   m_bootstrapSidebandRegionsMC[i]=(TH1DBootstrap*)f->Get(nominal+"SidebandRegions/" + variable+"_MC_"+x);
   if (m_debug>2) cout << "PseudoexperimentsCalculator::loadBootstrapHistos: Integral: " <<  m_bootstrapSidebandRegionsMC[i]->GetNominal()->Integral() << endl;
   x++;
  }
}

