#include "TTbarCovarianceCalculator/ClosureTestMaker.h"
#include "TTbarCovarianceCalculator/CovarianceCalculator.h"
#include "TTbarCovarianceCalculator/CovarianceFromToys.h"
#include "TTbarHelperFunctions/functions.h"
#include "TTbarHelperFunctions/histManipFunctions.h"
#include "TTbarHelperFunctions/TtbarDiffCrossSection_exception.h"

#include "../RooUnfold/src/RooUnfoldResponse.h"

ClassImp(ClosureTestMaker)

ClosureTestMaker::ClosureTestMaker() : m_regParam(4), m_nToys(100000), m_debug(0) {
  m_rooUnfold2 = std::make_unique<MyUnfold>();
}

//----------------------------------------------------------------------

void ClosureTestMaker::initializeShiftedHistos() {
  m_reco1Shifted = std::unique_ptr<TH1D>{(TH1D*)m_reco1->Clone()};
  m_truth1Shifted = std::unique_ptr<TH1D>{(TH1D*)m_truth1->Clone()};
  m_migration1Shifted = std::unique_ptr<TH2D>{(TH2D*)m_migration1->Clone()};
  
  m_reco2Shifted = std::unique_ptr<TH1D>{(TH1D*)m_reco2->Clone()};
  m_accNumerator2Shifted = std::unique_ptr<TH1D>{(TH1D*)m_accNumerator2->Clone()};
  m_truth2Shifted = std::unique_ptr<TH1D>{(TH1D*)m_truth2->Clone()};
  m_effNumerator2Shifted = std::unique_ptr<TH1D>{(TH1D*)m_effNumerator2->Clone()};
  m_migration2Shifted = std::unique_ptr<TH2D>{(TH2D*)m_migration2->Clone()};
  
  m_unfoldedShifted = std::unique_ptr<TH1D>{(TH1D*)m_truth1->Clone()};
  
  m_difference = std::unique_ptr<TH1D>{(TH1D*)m_truth1->Clone()};
  m_differenceShifted = std::unique_ptr<TH1D>{(TH1D*)m_truth1->Clone()};
  
  m_cov = std::make_unique<CovarianceFromToys>(m_truth1->GetXaxis()->GetNbins());
}

//----------------------------------------------------------------------

void ClosureTestMaker::initRooUnfold(const TH2D* migration) {
  if(m_debug>1) cout << "Initialize rooUnfold" << endl;
  m_response2 = std::make_unique<RooUnfoldResponse>(nullptr,nullptr,migration,"","");
  m_rooUnfold2->SetResponse(m_response2.get());
  m_rooUnfold2->SetRegParm(m_regParam);
  m_rooUnfold2->SetVerbose(0);
}

//----------------------------------------------------------------------

void ClosureTestMaker::makeCentralValues() {
  
  initRooUnfold(m_migration2.get());
  
  m_unfolded = std::unique_ptr<TH1D>{(TH1D*)m_truth1->Clone()};
  
  auto reco1Clone = std::unique_ptr<TH1D>{(TH1D*)m_reco1->Clone()};
  
  functions::UnfoldBayes(m_unfolded.get(),
			   reco1Clone.get(),
			   m_reco2.get(),
			   m_accNumerator2.get(),
			   m_truth2.get(),
			   m_effNumerator2.get(),
			   m_rooUnfold2.get());
		
  const int nbins = m_truth1->GetXaxis()->GetNbins();
  if(m_debug>1) std::cout << "Difference:";
  for(int i=1;i<=nbins;i++) {
    m_difference->SetBinContent(i,m_truth1->GetBinContent(i) - m_unfolded->GetBinContent(i));
    if(m_debug>1) std::cout << " " << m_difference->GetBinContent(i);
  }
  if(m_debug>1) cout << endl;
}

//----------------------------------------------------------------------

void ClosureTestMaker::runPseudoExperiments() {
  
  initializeShiftedHistos();
  makeCentralValues();
  
  int nprint = 10000;
  for(int i=0;i<m_nToys;i++) {
    if((i+1)%nprint == 0) std::cout << i+1 << "/" << m_nToys << " [" << (i+1.)/m_nToys*100 << "%]" << std::endl;
    
    preparePseudoExperiment();
    unfoldPseudoexperiment();
    addPseudoexperiment();
  }
  finalizePseudoexperiments();
  chi2Test();
}

//----------------------------------------------------------------------

void ClosureTestMaker::preparePseudoExperiment() {
  if(m_debug>3) cout << "Preparing pseudoexperiment" << endl;
  
  functions::coherentSmearing(m_reco1.get(), m_migration1.get(), m_truth1.get(), m_reco1Shifted.get(), m_migration1Shifted.get(), m_truth1Shifted.get());
  functions::coherentSmearing(m_reco2.get(), m_migration2.get(), m_truth2.get(), m_reco2Shifted.get(), m_migration2Shifted.get(), m_truth2Shifted.get());
  
  functions::getAccNumerator(m_migration2Shifted.get(),m_accNumerator2Shifted.get());
  functions::getEffNumerator(m_migration2Shifted.get(),m_effNumerator2Shifted.get());

}

//----------------------------------------------------------------------

void ClosureTestMaker::unfoldPseudoexperiment() {
  if(m_debug>3) cout << "Unfolding pseudoexperiment" << endl;
  initRooUnfold(m_migration2Shifted.get());
  
  functions::UnfoldBayes(m_unfoldedShifted.get(),
			   m_reco1Shifted.get(),
			   m_reco2Shifted.get(),
			   m_accNumerator2Shifted.get(),
			   m_truth2Shifted.get(),
			   m_effNumerator2Shifted.get(),
			   m_rooUnfold2.get());
			   
  const int nbins = m_truth1Shifted->GetXaxis()->GetNbins();
  if(m_debug>3) std::cout << "Difference shifted:";
  for(int i=1;i<=nbins;i++) {
    m_differenceShifted->SetBinContent(i,m_truth1Shifted->GetBinContent(i) - m_unfoldedShifted->GetBinContent(i));
    if(m_debug>2) std::cout << " " << m_differenceShifted->GetBinContent(i);
  }
  if(m_debug>2) cout << endl;
}

//----------------------------------------------------------------------

void ClosureTestMaker::addPseudoexperiment() {
  if(m_debug>3) cout << "Adding result of pseudoexperiment into covariance calculator" << endl;
  m_cov->addToy(m_differenceShifted.get());
}

//----------------------------------------------------------------------

void ClosureTestMaker::finalizePseudoexperiments() {
  m_cov->finalize();
}

//----------------------------------------------------------------------

void ClosureTestMaker::chi2Test() {
  if(m_debug>1) cout << "Running chi2Test()" << endl;
  const int nbins = m_difference->GetXaxis()->GetNbins();
  
  vector<double> x = functions::binContentsFromHistogram(m_difference.get());
  
  TMatrixT<double> xLeft(1,nbins);
  TMatrixT<double> xRight(nbins,1);
  for(int i=0;i<nbins;i++) {
    xLeft[0][i] = x[i];
    xRight[i][0] = x[i];
  }
  
  
  m_covariance = m_cov->getCovariance();
  
  m_covariance->Print();
  TMatrixT<double> covarianceInverse = *m_covariance;
  covarianceInverse = covarianceInverse.Invert();
  covarianceInverse.Print();
  xLeft.Print();

  m_chi2 = (xLeft*covarianceInverse*xRight)[0][0];
  m_pValue = TMath::Prob(m_chi2,nbins);
  
}

//----------------------------------------------------------------------
