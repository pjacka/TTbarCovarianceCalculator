#ifdef __CINT__

#include "TTbarCovarianceCalculator/CovarianceCalculatorFunctions.h"
#include "TTbarCovarianceCalculator/AsymmetricErrorsCalculator.h"
#include "TTbarCovarianceCalculator/ClosureTestMaker.h"
#include "TTbarCovarianceCalculator/Chi2TestMaker.h"
#include "TTbarCovarianceCalculator/CovarianceCalculator.h"
#include "TTbarCovarianceCalculator/CovarianceFromToys.h"
#include "TTbarCovarianceCalculator/GeneratorSystematicsBase.h"
#include "TTbarCovarianceCalculator/GeneratorSystematics.h"
#include "TTbarCovarianceCalculator/OneSysHistos.h"
#include "TTbarCovarianceCalculator/Pseudoexperiment.h"
#include "TTbarCovarianceCalculator/PseudoexperimentsCalculator.h"
#include "TTbarCovarianceCalculator/Signal_histos.h"
#include "TTbarCovarianceCalculator/SystematicHistos.h"
#include "TTbarCovarianceCalculator/SystematicsTableMaker.h"

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
//#pragma link C++ nestedclass;

#pragma link C++ namespace functions+;


#pragma link C++ class AsymmetricErrorsCalculator+;
#pragma link C++ class ClosureTestMaker+;
#pragma link C++ class Chi2TestMaker+;
#pragma link C++ class CovarianceCalculator+;
#pragma link C++ class CovarianceFromToys+;
#pragma link C++ class GeneratorSystematicsBase+;
#pragma link C++ class GeneratorSystematics+;
#pragma link C++ class OneSysHistos+;
#pragma link C++ class Pseudoexperiment+;
#pragma link C++ class PseudoexperimentsCalculator+;
#pragma link C++ class Signal_histos+;
#pragma link C++ class SystematicHistos+;
#pragma link C++ class SystematicsTableMaker+;

#endif // __CINT__
