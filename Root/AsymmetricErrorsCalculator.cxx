#include "TTbarCovarianceCalculator/AsymmetricErrorsCalculator.h"
#include "TTbarHelperFunctions/functions.h"
#include "TTbarHelperFunctions/histManipFunctions.h"
#include "TTbarHelperFunctions/namesAndTitles.h"
#include "TTbarHelperFunctions/plottingFunctions.h"

#include "TVectorD.h"

ClassImp(AsymmetricErrorsCalculator)

AsymmetricErrorsCalculator::AsymmetricErrorsCalculator(SystematicHistos* sysHistos,TString config_lumi,TString config_sysnames,TString mainDir,TString variable,TString level,int debugLevel,bool is1D){
  this->init(sysHistos,config_lumi,config_sysnames,mainDir,variable,level,debugLevel,is1D);
  
  
}
void AsymmetricErrorsCalculator::init(SystematicHistos* sysHistos,TString config_lumi_string,TString config_sysnames,TString mainDir,TString variable,TString level,int debugLevel,bool is1D){

  m_debug = debugLevel;

  m_useNewISR = true;

  m_sysHistos=sysHistos;
  m_nbins_reco=m_sysHistos->m_nominalHistos->signal_reco->GetNbinsX();
  m_nbins_truth=m_sysHistos->m_nominalHistos->signal_truth->GetNbinsX();
  //cout << m_nbins_reco << endl;
  
  m_nsys_all = m_sysHistos->m_sys_names_all.size();
  m_nsys_pairs = m_sysHistos->m_sys_names_pairs.size();
  m_nsys_single = m_sysHistos->m_sys_names_single.size();
  m_nsys_pdf = m_sysHistos->m_sys_names_PDF.size()-1;
  m_nsys_modeling = m_sysHistos->m_sys_names_signalModeling.size();
  if(m_nsys_pdf<0) m_nsys_pdf=0;
  
  m_sys_chain_names_all = m_sysHistos->m_sys_names_all;
  m_sys_chain_names_single = m_sysHistos->m_sys_names_single;
  m_sys_chain_names_pdf = m_sysHistos->m_sys_names_PDF;
  m_sys_chain_names_paired = m_sysHistos->m_sys_names_pairs;
  m_sys_chain_names_modeling = m_sysHistos->m_sys_names_signalModeling;
  
  m_hist_template=(TH1D*)m_sysHistos->m_nominalHistos->signal_reco->Clone();
  m_hist_template->Reset();
  
  m_hist_template_truth=(TH1D*)m_sysHistos->m_nominalHistos->signal_truth->Clone();
  m_hist_template_truth->Reset();
  
  m_config_sys_names = new TEnv(config_sysnames);
  m_mainDir=mainDir;
  
  m_ttbarConfig = std::make_unique<TTBarConfig>(config_lumi_string);
  
  m_mc_samples_production = m_ttbarConfig->mc_samples_production();
  m_lumi = functions::getLumi(*m_ttbarConfig);
  if(m_lumi<0.)throw TtbarDiffCrossSection_exception("Error: Unknown samples production!");
  
  m_doSignalModeling = m_ttbarConfig->doSignalModeling();
  
  m_printUncertaintyLowThreshold=0.01;
  
  stringstream ss;
  m_lumi < 10 ? ss << setprecision(2) : ss << setprecision(3); 
  ss << m_lumi;
 
  m_lumi_string = NamesAndTitles::getLumiString(*m_ttbarConfig);
  m_variable=variable;
  m_level=level;
  
  
  m_is1D=is1D;;
  
  
  
}


void AsymmetricErrorsCalculator::load2Dbinning(TFile* f){
  if(!m_is1D){
    
    TVectorD* helpvec = (TVectorD*)f->Get( (TString)"nominal/histInfo/"+m_variable+"_binning_x");
    const int nbinsx=helpvec->GetNrows()-1;
    TVectorD* helpvec_truth = (TVectorD*)f->Get( (TString)"nominal/histInfo/"+m_variable+"_binning_truth_x");
    const int nbinsx_truth=helpvec_truth->GetNrows()-1;
    
    m_binning2D_x.resize(nbinsx+1);
    m_binning2D_truth_x.resize(nbinsx_truth+1);
    for(int i=0;i<=nbinsx;i++)m_binning2D_x[i]=(*helpvec)[i];
    for(int i=0;i<=nbinsx_truth;i++)m_binning2D_truth_x[i]=(*helpvec_truth)[i];
    delete helpvec;
    
    m_binning2D_y.resize(nbinsx);
    m_binning2D_truth_y.resize(nbinsx_truth);
    for(int i=0;i<nbinsx;i++){
      TString name = Form( ( (TString)"nominal/histInfo/"+m_variable+"_binning_y_inXbin%i").Data(),i+1);
      TVectorD* helpvec = (TVectorD*)f->Get(name);
      const int nbinsy=helpvec->GetNrows()-1;
      m_binning2D_y[i].resize(nbinsy+1);
      for(int j=0;j<=nbinsy;j++)m_binning2D_y[i][j]=(*helpvec)[j];
      delete helpvec;
    }
    for(int i=0;i<nbinsx_truth;i++){
      
      TString name = Form( ( (TString)"nominal/histInfo/"+m_variable+"_binning_truth_y_inXbin%i").Data(),i+1);
      TVectorD* helpvec = (TVectorD*)f->Get(name);
      const int nbinsy=helpvec->GetNrows()-1;
      m_binning2D_truth_y[i].resize(nbinsy+1);
      for(int j=0;j<=nbinsy;j++)m_binning2D_truth_y[i][j]=(*helpvec)[j];
      delete helpvec;
    }
    
    
    
  }
  else cout << "Error: Attempt to load 2D binning while is1D option is set to true!" << endl;
  
  
}

//----------------------------------------------------------------------

void AsymmetricErrorsCalculator::loadNDbinning(const TString& path_config_binning) {
  m_histogramConverterND = std::make_unique<HistogramNDto1DConverter>(path_config_binning.Data(),m_debug);
  TString levelShort=m_level;
  levelShort.ReplaceAll("Level","");
  m_histogramConverterND->initializeOptBinning((m_variable+"_"+levelShort+"_truth").Data());
  m_histogramConverterND->initialize1DBinning();
  m_histogramConverterND->print();
}

//----------------------------------------------------------------------

void AsymmetricErrorsCalculator::loadMultiDPlotsSettings(const TString& path) {
  m_plotSettingsND = std::make_unique<MultiDimensionalPlotsSettings>(path.Data());
}

//----------------------------------------------------------------------

void AsymmetricErrorsCalculator::loadStatisticalUncertainties(TFile* f){
  if (m_debug > 0 ) cout << "Info: Loading stat. and MC stat uncertainties. " << endl;
  TString shortName=m_variable;
  shortName.ReplaceAll("_fine","");
  TH1D* h_centralValues=(TH1D*)f->Get(shortName + "_centralValues");
  TH1D* h_centralValuesNormalized=(TH1D*)f->Get(shortName + "_centralValuesNormalized");
  if (m_debug > 1 ) { 
    cout << "CentralValues absolute: Integral: " << h_centralValues->Integral() << endl;
    cout << "CentralValues normalized: Integral: " << h_centralValuesNormalized->Integral() << endl;
    
  }
  
  std::unique_ptr<TMatrixT<double> > covStatCombined{ (TMatrixT<double>*)f->Get(shortName + "_covDataBasedOnlyStat") };
  std::unique_ptr<TMatrixT<double> > covStatCombinedNormalized { (TMatrixT<double>*)f->Get(shortName + "_covDataBasedOnlyStatNormalized") };
  
  std::unique_ptr<TMatrixT<double> > covStat{ (TMatrixT<double>*)f->Get(shortName + "_covDataBasedOnlyDataStat") };
  std::unique_ptr<TMatrixT<double> > covStatNormalized{ (TMatrixT<double>*)f->Get(shortName + "_covDataBasedOnlyDataStatNormalized") };
  std::unique_ptr<TMatrixT<double> > covMCStat{ (TMatrixT<double>*)f->Get(shortName + "_covDataBasedOnlyMCStat") };
  std::unique_ptr<TMatrixT<double> > covMCStatNormalized{ (TMatrixT<double>*)f->Get(shortName + "_covDataBasedOnlyMCStatNormalized") };
  
  std::unique_ptr<TMatrixT<double> > covMCStatWithMultijetFixed{ (TMatrixT<double>*)f->Get(shortName + "_covDataBasedOnlyMCStatWithMultijetFixed") };
  std::unique_ptr<TMatrixT<double> > covMCStatWithMultijetFixedNormalized{ (TMatrixT<double>*)f->Get(shortName + "_covDataBasedOnlyMCStatWithMultijetFixedNormalized") };
  
  std::unique_ptr<TMatrixT<double> > covOnlyMultijetBkgStat{ (TMatrixT<double>*)f->Get(shortName + "_covDataBasedOnlyMultijetBkgStat") };
  std::unique_ptr<TMatrixT<double> > covOnlyMultijetBkgStatNormalized{ (TMatrixT<double>*)f->Get(shortName + "_covDataBasedOnlyMultijetBkgStatNormalized") };
  
  std::unique_ptr<TMatrixT<double> > covSys{ (TMatrixT<double>*)f->Get(shortName + "_covDataBasedSysAndSigMod") };
  std::unique_ptr<TMatrixT<double> > covSysNormalized{ (TMatrixT<double>*)f->Get(shortName + "_covDataBasedSysAndSigModNormalized") };
  
  std::unique_ptr<TMatrixT<double> > covTotal{ (TMatrixT<double>*)f->Get(shortName + "_covDataBasedStatSysAndSigMod") };
  std::unique_ptr<TMatrixT<double> > covTotalNormalized{ (TMatrixT<double>*)f->Get(shortName + "_covDataBasedStatSysAndSigModNormalized") };
  
  
  
  //cout << 100.*sqrt((*covStat)[0][0])/h_centralValues->GetBinContent(1) << endl;
  //cout << 100.*sqrt((*covStatNormalized)[0][0])/h_centralValuesNormalized->GetBinContent(1) << endl;
  //cout << 100.*sqrt((*covMCStat)[0][0])/h_centralValues->GetBinContent(1)<< endl;
  //cout << 100.*sqrt((*covMCStatNormalized)[0][0])/h_centralValuesNormalized->GetBinContent(1) << endl;
  
  m_total_uncertainties.resize(m_nbins_truth);
  m_total_uncertainties_normalized.resize(m_nbins_truth);
  m_sys_uncertainties_total.resize(m_nbins_truth);
  m_sys_uncertainties_normalized_total.resize(m_nbins_truth);
  m_sys_uncertainties_stat_combined.resize(m_nbins_truth);
  m_sys_uncertainties_normalized_stat_combined.resize(m_nbins_truth);
  m_sys_uncertainties_stat.resize(m_nbins_truth);
  m_sys_uncertainties_normalized_stat.resize(m_nbins_truth);
  m_sys_uncertainties_MCstat.resize(m_nbins_truth);
  m_sys_uncertainties_normalized_MCstat.resize(m_nbins_truth);
  m_sys_uncertainties_MCstatWithMultijetFixed.resize(m_nbins_truth);
  m_sys_uncertainties_normalized_MCstatWithMultijetFixed.resize(m_nbins_truth);
  m_sys_uncertainties_multijetBkg_stat.resize(m_nbins_truth);
  m_sys_uncertainties_normalized_multijetBkg_stat.resize(m_nbins_truth);
  
  m_central_values.resize(m_nbins_truth);
  m_central_values_normalized.resize(m_nbins_truth);
  
  for(int i=0;i<m_nbins_truth;i++){
    const double centralValue=h_centralValues->GetBinContent(i+1);
    const double centralValueNormalized=h_centralValuesNormalized->GetBinContent(i+1);
    if(centralValue>0.){
      m_sys_uncertainties_stat_combined[i]=sqrt((*covStatCombined)[i][i])/centralValue*100;
      m_total_uncertainties[i]=sqrt((*covTotal)[i][i])/centralValue*100;
      m_sys_uncertainties_total[i]=sqrt((*covSys)[i][i])/centralValue*100;
      m_sys_uncertainties_stat[i]=sqrt((*covStat)[i][i])/centralValue*100;
      m_sys_uncertainties_MCstat[i]=sqrt((*covMCStat)[i][i])/centralValue*100;
      m_sys_uncertainties_multijetBkg_stat[i]=sqrt((*covOnlyMultijetBkgStat)[i][i])/centralValue*100;
      m_sys_uncertainties_MCstatWithMultijetFixed[i]=sqrt((*covMCStatWithMultijetFixed)[i][i])/centralValue*100;
    }
    else{
      m_sys_uncertainties_stat_combined[i]=0.;
      m_total_uncertainties[i]=0.;
      m_sys_uncertainties_total[i]=0.;
      m_sys_uncertainties_stat[i]=0.;
      m_sys_uncertainties_MCstat[i]=0.;
      m_sys_uncertainties_multijetBkg_stat[i]=0.;
      m_sys_uncertainties_MCstatWithMultijetFixed[i]=0.;
    }
    if(centralValueNormalized>0.){
      m_sys_uncertainties_normalized_stat_combined[i]=sqrt((*covStatCombinedNormalized)[i][i])/centralValueNormalized*100;
      m_total_uncertainties_normalized[i]=sqrt((*covTotalNormalized)[i][i])/centralValueNormalized*100;
      m_sys_uncertainties_normalized_total[i]=sqrt((*covSysNormalized)[i][i])/centralValueNormalized*100;
      m_sys_uncertainties_normalized_stat[i]=sqrt((*covStatNormalized)[i][i])/centralValueNormalized*100;
      m_sys_uncertainties_normalized_MCstat[i]=sqrt((*covMCStatNormalized)[i][i])/centralValueNormalized*100;
      m_sys_uncertainties_normalized_multijetBkg_stat[i]=sqrt((*covOnlyMultijetBkgStatNormalized)[i][i])/centralValueNormalized*100;
      m_sys_uncertainties_normalized_MCstatWithMultijetFixed[i]=sqrt((*covMCStatWithMultijetFixedNormalized)[i][i])/centralValueNormalized*100;
    }
    else{
      m_sys_uncertainties_normalized_stat_combined[i]=0.;
      m_total_uncertainties_normalized[i]=0.;
      m_sys_uncertainties_normalized_total[i]=0.;
      m_sys_uncertainties_normalized_stat[i]=0.;
      m_sys_uncertainties_normalized_MCstat[i]=0.;
      m_sys_uncertainties_normalized_multijetBkg_stat[i]=0.;
      m_sys_uncertainties_normalized_MCstatWithMultijetFixed[i]=0.;
    }
    m_central_values[i]=centralValue;
    m_central_values_normalized[i]=centralValueNormalized;
    
    if (m_debug > 1 ) cout << "Stat. unc. in bin " << i+1 << " " << m_sys_uncertainties_stat[i] << " " << m_sys_uncertainties_normalized_stat[i] << " " << m_sys_uncertainties_MCstat[i] << " " << m_sys_uncertainties_normalized_MCstat[i] << " " << endl;
  }
  delete h_centralValues;
  delete h_centralValuesNormalized;
  delete f;
}

// ------------------------------------------------------------------------------------------------------

void AsymmetricErrorsCalculator::prepareSysUncertainties(){ // Function to prepare systematic variation corresponding to all sources of uncertainties: Data stat., MC stat., detector systematics and signal modeling
  if (m_debug > 0 ) cout << "Entering function AsymmetricErrorsCalculator::prepareSysUncertainties()" << endl;
	
  this->initializeUncertaintiesVectors();
  
  this->initializeSummaryTables();

  this->calculateSystematicsPaired();
  
  this->calculateSystematicsSingle();
  
  if(m_doSignalModeling) this->calculateSystematicsSignalModeling();
  
  this->finalizeSummaryUncertainties();
  
}

// ------------------------------------------------------------------------------------------------------

void AsymmetricErrorsCalculator::initializeSummaryTables(){ // Function to initialize maps used for summary tables
  
  m_summaryNames.clear();
  m_summaryNames.push_back("Total systematics");
  m_summaryNames.push_back("Luminosity");
  m_summaryNames.push_back("Jets");
  
  m_summaryNames.push_back("Top-tagging");
  m_summaryNames.push_back("TopTagSF Dijet Modelling");
  m_summaryNames.push_back("TopTagSF Gammajet Modelling");
  m_summaryNames.push_back("TopTagSF Hadronisation");
  m_summaryNames.push_back("TopTagSF ME");
  m_summaryNames.push_back("TopTagSF Radiation");
  m_summaryNames.push_back("TopTagSF Bkg SF");
  m_summaryNames.push_back("TopTagSF Signal SF");
  m_summaryNames.push_back("TopTagSF TagEffUnc");
  m_summaryNames.push_back("TopTagSF bTag");
  
  m_summaryNames.push_back("JES");
  m_summaryNames.push_back("JER");
  m_summaryNames.push_back("JMS");
  m_summaryNames.push_back("JMR");
  m_summaryNames.push_back("Flavor tagging");
  m_summaryNames.push_back("Flavor tagging B");
  m_summaryNames.push_back("Flavor tagging C");
  m_summaryNames.push_back("Flavor tagging Light");
  m_summaryNames.push_back("Flavor tagging extrap");
  m_summaryNames.push_back("Background");
  m_summaryNames.push_back("Signal modelling");
  m_summaryNames.push_back("PDF");
  m_summaryNames.push_back("Leptons");
  m_summaryNames.push_back("MET/PU");
  
  
  for(unsigned int i=0;i<m_summaryNames.size();i++){
    m_uncertainties_for_summary_table_absolute[m_summaryNames[i]].first.resize(m_nbins_truth);
    m_uncertainties_for_summary_table_absolute[m_summaryNames[i]].second.resize(m_nbins_truth);
    m_uncertainties_for_summary_table_relative[m_summaryNames[i]].first.resize(m_nbins_truth);
    m_uncertainties_for_summary_table_relative[m_summaryNames[i]].second.resize(m_nbins_truth);
  }
  
}

// ------------------------------------------------------------------------------------------------------

const TString AsymmetricErrorsCalculator::getSummaryName(const TString& name){ 
  // Input: Name of systematic uncertainty provided by systematic tools (tree names). 
  // Output: Should return string compatible with m_summaryNames vector.
  
  TString summary_name;
    
  if     (name.Contains("luminosity"))            summary_name="Luminosity";
  else if(name.Contains("bTagSF_B_"))             summary_name="Flavor tagging B";
  else if(name.Contains("bTagSF_C_"))             summary_name="Flavor tagging C";
  else if(name.Contains("bTagSF_Light_"))         summary_name="Flavor tagging Light";
  else if(name.Contains("bTagSF_extrapolation_")) summary_name="Flavor tagging extrap";
  else if(name.BeginsWith("topTagSF_")) {
    if(name.Contains("Dijet_Modelling")) summary_name="TopTagSF Dijet Modelling";
    else if(name.Contains("Gammajet_Modelling")) summary_name="TopTagSF Gammajet Modelling";
    else if(name.Contains("Hadronisation")) summary_name="TopTagSF Hadronisation";
    else if(name.Contains("MatrixElement")) summary_name="TopTagSF ME";
    else if(name.Contains("Radiation")) summary_name="TopTagSF Radiation";
    else if(name.Contains("BGSF")) summary_name="TopTagSF Bkg SF";
    else if(name.Contains("SigSF")) summary_name="TopTagSF Signal SF";
    else if(name.Contains("TagEffUnc")) summary_name="TopTagSF TagEffUnc";
    else summary_name="TopTagSF bTag";
  }
  else if(name.Contains("JET_")){
    if(name.Contains("_JMR_"))       summary_name="JMR";
    else if(name.Contains("_JMS_")) summary_name="JMS";
    else if(name.Contains("JER"))      summary_name="JER";
    else                               summary_name="JES";
  }
  else if(name.Contains("_JMR_")) summary_name="JMR";
  else if(name.Contains("MUON") || name.BeginsWith("EG_")) summary_name="Leptons";
  else if(name.Contains("MET") || name=="jvt_1down" || name=="jvf_1down" || name=="pileup_1down") summary_name="MET/PU";
  else if(name=="singletop_tchan_1down" || name=="singletop_Wt_1down" ||name=="ttW_1down" ||name=="ttZ_1down" || name=="ttH_1down" || name=="ttbar_nonallhad_1down") summary_name="Background";
  else summary_name="";
  return summary_name;
}

// ------------------------------------------------------------------------------------------------------

void AsymmetricErrorsCalculator::initializeUncertaintiesVectors(){ // Initialize vectors used in calculations
  
  m_sys_uncertainties_paired.resize(m_nsys_pairs);
  m_sys_uncertainties_single.resize(m_nsys_single);
  m_sys_uncertainties_pdf.resize(m_nsys_pdf);
  m_sys_uncertainties_normalized_paired.resize(m_nsys_pairs);
  m_sys_uncertainties_normalized_single.resize(m_nsys_single);
  m_sys_uncertainties_normalized_pdf.resize(m_nsys_pdf);
  
  m_sys_uncertainties_bkg_all.first.resize(m_nbins_truth);
  m_sys_uncertainties_bkg_all.second.resize(m_nbins_truth);
  m_sys_uncertainties_detector_all.first.resize(m_nbins_truth);
  m_sys_uncertainties_detector_all.second.resize(m_nbins_truth);
  
  m_sys_uncertainties_bkg_all_normalized.first.resize(m_nbins_truth);
  m_sys_uncertainties_bkg_all_normalized.second.resize(m_nbins_truth);
  m_sys_uncertainties_detector_all_normalized.first.resize(m_nbins_truth);
  m_sys_uncertainties_detector_all_normalized.second.resize(m_nbins_truth);
  
  
}

// ------------------------------------------------------------------------------------------------------

void AsymmetricErrorsCalculator::calculateSystematicsPaired(){ // Function to calculate detector systematic uncertainties for sources with two shifts available
  
  for(int i=0;i<m_nsys_pairs;i++){ // Loop over all sources
    TString name=m_sys_chain_names_paired[i].first;
    //bool isLumiUnc=false;
    //if(name.Contains("luminosity"))isLumiUnc=true;
    //cout << endl << name << endl;
    //calculateOneSystematicsPaired(i,m_sys_uncertainties_paired[i].first,m_sys_uncertainties_paired[i].second,m_sys_uncertainties_normalized_paired[i].first,m_sys_uncertainties_normalized_paired[i].second,isLumiUnc);
    calculateOneSystematicsPaired(m_sys_uncertainties_paired[i].first,m_sys_uncertainties_paired[i].second,m_sysHistos->m_shifts_paired[i].first, m_sysHistos->m_shifts_paired[i].second);
    calculateOneSystematicsPaired(m_sys_uncertainties_normalized_paired[i].first,m_sys_uncertainties_normalized_paired[i].second,m_sysHistos->m_shifts_relative_paired[i].first, m_sysHistos->m_shifts_relative_paired[i].second);
    
    TString shortname=name;
    shortname.ReplaceAll("__1down","");
    if (m_debug > 1 ) {
      if( shortname.Contains("topTagSF_") || shortname.Contains("luminosity")) {
	cout << shortname << endl;
	for(int ibin=0;ibin<m_nbins_truth;ibin++) {
	  int  j=ibin+1;
	  if(!std::isfinite(m_sys_uncertainties_paired[i].first[ibin]) || !std::isfinite(m_sys_uncertainties_paired[i].second[ibin])) {
	    std::cout << "Error: nan found" << std::endl;
	  }
	  cout << "shift down: " <<  m_sys_uncertainties_paired[i].first[ibin] << " shift up: " <<  m_sys_uncertainties_paired[i].second[ibin] << " " << m_sysHistos->m_shifts_paired[i].first->GetBinContent(j)*100 << " " << m_sysHistos->m_shifts_paired[i].second->GetBinContent(j)*100 << endl;
	
	}
      }
    }
    const TString& summary_name = getSummaryName(name);
    if( std::find(m_summaryNames.begin(),m_summaryNames.end(),summary_name)==m_summaryNames.end() ) { 
      throw TtbarDiffCrossSection_exception("Error in getSummaryName function: Found incompatible name with m_summaryNames vector! name = " + name);
    }
    
    if (m_debug > 1 ) cout << "Adding systematics with summary name: "<< summary_name <<endl;
    for(int ibin=0;ibin<m_nbins_truth;ibin++) {
      //cout << "name: " << name << " summary_name: " << summary_name << " ibin: " << ibin << " vector size: " <<  m_sys_uncertainties_paired.size() << " inner_size: " << m_sys_uncertainties_paired[i].first.size() << endl;
      m_uncertainties_for_summary_table_absolute[summary_name].first[ibin]+=pow(max(max(-m_sys_uncertainties_paired[i].first[ibin],0.),-m_sys_uncertainties_paired[i].second[ibin]),2);
      m_uncertainties_for_summary_table_absolute[summary_name].second[ibin]+=pow(max(max(m_sys_uncertainties_paired[i].first[ibin],0.),m_sys_uncertainties_paired[i].second[ibin]),2);
      m_uncertainties_for_summary_table_relative[summary_name].first[ibin]+=pow(max(max(-m_sys_uncertainties_normalized_paired[i].first[ibin],0.),-m_sys_uncertainties_normalized_paired[i].second[ibin]),2);
      m_uncertainties_for_summary_table_relative[summary_name].second[ibin]+=pow(max(max(m_sys_uncertainties_normalized_paired[i].first[ibin],0.),m_sys_uncertainties_normalized_paired[i].second[ibin]),2);
      
      
      
    }
    if (m_debug > 1 ) cout << "Done adding systematics" << endl;
    
    if(name.Contains("singletop_tchan") || name.Contains("singletop_Wt") || name.Contains("ttW") || name.Contains("ttZ") || name.Contains("ttH")) {
      for(int ibin=0;ibin<m_nbins_truth;ibin++) {
	m_sys_uncertainties_bkg_all.first[ibin]+=pow(m_sys_uncertainties_paired[i].first[ibin],2);
	m_sys_uncertainties_bkg_all.second[ibin]+=pow(m_sys_uncertainties_paired[i].second[ibin],2);
	m_sys_uncertainties_bkg_all_normalized.first[ibin]+=pow(m_sys_uncertainties_normalized_paired[i].first[ibin],2);
	m_sys_uncertainties_bkg_all_normalized.second[ibin]+=pow(m_sys_uncertainties_normalized_paired[i].second[ibin],2);
      }
    }
    else{
      for(int ibin=0;ibin<m_nbins_truth;ibin++) {
	m_sys_uncertainties_detector_all.first[ibin]+=pow(m_sys_uncertainties_paired[i].first[ibin],2);
	m_sys_uncertainties_detector_all.second[ibin]+=pow(m_sys_uncertainties_paired[i].second[ibin],2);
	m_sys_uncertainties_detector_all_normalized.first[ibin]+=pow(m_sys_uncertainties_normalized_paired[i].first[ibin],2);
	m_sys_uncertainties_detector_all_normalized.second[ibin]+=pow(m_sys_uncertainties_normalized_paired[i].second[ibin],2);
      }
    }
  }
  
  
}

// ------------------------------------------------------------------------------------------------------

void AsymmetricErrorsCalculator::calculateSystematicsSingle(){ // Function to calculate detector systematic uncertainties for sources related with just one available shift
  
  for(int i=0;i<m_nsys_single;i++){ // Loop over all sources
    TString name=m_sys_chain_names_single[i];	
    const TString& summary_name = getSummaryName(name);
    if( std::find(m_summaryNames.begin(),m_summaryNames.end(),summary_name)==m_summaryNames.end() ) { 
      throw TtbarDiffCrossSection_exception("Error in getSummaryName function: Found incompatible name with m_summaryNames vector! name = " + name);
    }
    
    calculateOneSystematicsSingle(m_sys_uncertainties_single[i],m_sysHistos->m_shifts_single[i]);
    calculateOneSystematicsSingle(m_sys_uncertainties_normalized_single[i],m_sysHistos->m_shifts_relative_single[i]);
    
    for(int ibin=0;ibin<m_nbins_truth;ibin++){
      m_sys_uncertainties_detector_all.first[ibin]+=pow(m_sys_uncertainties_single[i][ibin],2);
      m_sys_uncertainties_detector_all.second[ibin]+=pow(m_sys_uncertainties_single[i][ibin],2);
      m_sys_uncertainties_detector_all_normalized.first[ibin]+=pow(m_sys_uncertainties_normalized_single[i][ibin],2);
      m_sys_uncertainties_detector_all_normalized.second[ibin]+=pow(m_sys_uncertainties_normalized_single[i][ibin],2);
      
      m_uncertainties_for_summary_table_absolute[summary_name].first[ibin]+=pow(m_sys_uncertainties_single[i][ibin],2);
      m_uncertainties_for_summary_table_absolute[summary_name].second[ibin]+=pow(m_sys_uncertainties_single[i][ibin],2);
      m_uncertainties_for_summary_table_relative[summary_name].first[ibin]+=pow(m_sys_uncertainties_normalized_single[i][ibin],2);
      m_uncertainties_for_summary_table_relative[summary_name].second[ibin]+=pow(m_sys_uncertainties_normalized_single[i][ibin],2);
    }
  }
  
}
// -------------------------------------------------------------------------------------------------------

void AsymmetricErrorsCalculator::calculateSystematicsSignalModeling(){ // Function to calculate signal modeling uncertainties including PDF
  
  for(int i=0;i<m_nsys_pdf;i++){
    calculateOneSystematicsSingle(m_sys_uncertainties_pdf[i],m_sysHistos->m_shifts_PDF[i]);
    calculateOneSystematicsSingle(m_sys_uncertainties_normalized_pdf[i],m_sysHistos->m_shifts_relative_PDF[i]);
  }
  
  // Helper functions to calculate shifts
  auto sysFunc = [this](const TString& dirname) { 
    calculateOneSystematicsSingle(m_sys_uncertainties_signal_modeling[dirname],m_sysHistos->m_shifts_signalModeling[dirname]);
    calculateOneSystematicsSingle(m_sys_uncertainties_normalized_signal_modeling[dirname],m_sysHistos->m_shifts_relative_signalModeling[dirname]);
  };
  
  auto asymmSysFunc = [this](const TString& dirname, const TString& dirnameUp, const TString& dirnameDown ) {
    m_sys_uncertainties_signal_modeling[dirname].resize(m_nbins_truth);
    m_sys_uncertainties_normalized_signal_modeling[dirname].resize(m_nbins_truth);
    for(int ibin=0;ibin<m_nbins_truth;ibin++) {
      m_sys_uncertainties_signal_modeling[dirname][ibin] = 
	functions::calculateFSRsys(m_sys_uncertainties_signal_modeling[dirnameUp][ibin],m_sys_uncertainties_signal_modeling[dirnameDown][ibin]);
      m_sys_uncertainties_normalized_signal_modeling[dirname][ibin] = 
	functions::calculateFSRsys(m_sys_uncertainties_normalized_signal_modeling[dirnameUp][ibin],m_sys_uncertainties_normalized_signal_modeling[dirnameDown][ibin]);
    }
  };
  
  sysFunc(m_ttbarConfig->hardScatteringDirectory());
  sysFunc(m_ttbarConfig->partonShoweringDirectory());
  
  if(m_useNewISR) {
    sysFunc("hdamp");
    sysFunc(m_ttbarConfig->ISRmuRupDirectory());
    sysFunc(m_ttbarConfig->ISRmuRdownDirectory());
    sysFunc(m_ttbarConfig->ISRmuFupDirectory());
    sysFunc(m_ttbarConfig->ISRmuFdownDirectory());
    sysFunc(m_ttbarConfig->ISRVar3cUpDirectory());
    sysFunc(m_ttbarConfig->ISRVar3cDownDirectory());
    
    asymmSysFunc("ISRmuR",m_ttbarConfig->ISRmuRupDirectory(),m_ttbarConfig->ISRmuRdownDirectory());
    asymmSysFunc("ISRmuF",m_ttbarConfig->ISRmuFupDirectory(),m_ttbarConfig->ISRmuFdownDirectory());
    asymmSysFunc("ISRVar3c",m_ttbarConfig->ISRVar3cUpDirectory(),m_ttbarConfig->ISRVar3cDownDirectory());
    
    m_sys_uncertainties_signal_modeling["ISR"].resize(m_nbins_truth);
    m_sys_uncertainties_normalized_signal_modeling["ISR"].resize(m_nbins_truth);
    for(int ibin=0;ibin<m_nbins_truth;ibin++) {
      m_sys_uncertainties_signal_modeling["ISR"][ibin] += pow(m_sys_uncertainties_signal_modeling["ISRmuR"][ibin],2);
      m_sys_uncertainties_signal_modeling["ISR"][ibin] += pow(m_sys_uncertainties_signal_modeling["ISRmuF"][ibin],2);
      m_sys_uncertainties_signal_modeling["ISR"][ibin] += pow(m_sys_uncertainties_signal_modeling["ISRVar3c"][ibin],2);
      m_sys_uncertainties_signal_modeling["ISR"][ibin] = sqrt(m_sys_uncertainties_signal_modeling["ISR"][ibin]);
      
      m_sys_uncertainties_normalized_signal_modeling["ISR"][ibin] += pow(m_sys_uncertainties_normalized_signal_modeling["ISRmuR"][ibin],2);
      m_sys_uncertainties_normalized_signal_modeling["ISR"][ibin] += pow(m_sys_uncertainties_normalized_signal_modeling["ISRmuF"][ibin],2);
      m_sys_uncertainties_normalized_signal_modeling["ISR"][ibin] += pow(m_sys_uncertainties_normalized_signal_modeling["ISRVar3c"][ibin],2);
      m_sys_uncertainties_normalized_signal_modeling["ISR"][ibin] = sqrt(m_sys_uncertainties_normalized_signal_modeling["ISR"][ibin]);
    }
    
  }
  else {
    sysFunc(m_ttbarConfig->ISRupDirectory());
    sysFunc(m_ttbarConfig->ISRdownDirectory());
    asymmSysFunc("ISR",m_ttbarConfig->ISRupDirectory(),m_ttbarConfig->ISRdownDirectory());
  }
  sysFunc(m_ttbarConfig->FSRupDirectory());
  sysFunc(m_ttbarConfig->FSRdownDirectory());
  asymmSysFunc("FSR",m_ttbarConfig->FSRupDirectory(),m_ttbarConfig->FSRdownDirectory());
  
  for(int ibin=0;ibin<m_nbins_truth;ibin++){
    m_uncertainties_for_summary_table_absolute["Signal modelling"].first[ibin] +=pow(m_sys_uncertainties_signal_modeling[m_ttbarConfig->hardScatteringDirectory()][ibin],2);
    m_uncertainties_for_summary_table_absolute["Signal modelling"].second[ibin]+=pow(m_sys_uncertainties_signal_modeling[m_ttbarConfig->hardScatteringDirectory()][ibin],2);
    m_uncertainties_for_summary_table_absolute["Signal modelling"].first[ibin] +=pow(m_sys_uncertainties_signal_modeling[m_ttbarConfig->partonShoweringDirectory()][ibin],2);
    m_uncertainties_for_summary_table_absolute["Signal modelling"].second[ibin]+=pow(m_sys_uncertainties_signal_modeling[m_ttbarConfig->partonShoweringDirectory()][ibin],2);
    m_uncertainties_for_summary_table_absolute["Signal modelling"].first[ibin] +=pow(m_sys_uncertainties_signal_modeling["ISR"][ibin],2);
    m_uncertainties_for_summary_table_absolute["Signal modelling"].second[ibin]+=pow(m_sys_uncertainties_signal_modeling["ISR"][ibin],2);
    m_uncertainties_for_summary_table_absolute["Signal modelling"].first[ibin] +=pow(m_sys_uncertainties_signal_modeling["FSR"][ibin],2);
    m_uncertainties_for_summary_table_absolute["Signal modelling"].second[ibin]+=pow(m_sys_uncertainties_signal_modeling["FSR"][ibin],2);
    
    for(int i=0;i<m_nsys_pdf;i++){
      m_uncertainties_for_summary_table_absolute["PDF"].first[ibin]+=pow(m_sys_uncertainties_pdf[i][ibin],2);
      m_uncertainties_for_summary_table_absolute["PDF"].second[ibin]+=pow(m_sys_uncertainties_pdf[i][ibin],2);
    }
    m_uncertainties_for_summary_table_absolute["Signal modelling"].first[ibin] +=m_uncertainties_for_summary_table_absolute["PDF"].first[ibin];
    m_uncertainties_for_summary_table_absolute["Signal modelling"].second[ibin]+=m_uncertainties_for_summary_table_absolute["PDF"].second[ibin];
    
    m_uncertainties_for_summary_table_relative["Signal modelling"].first[ibin] +=pow(m_sys_uncertainties_normalized_signal_modeling[m_ttbarConfig->hardScatteringDirectory()][ibin],2);
    m_uncertainties_for_summary_table_relative["Signal modelling"].second[ibin]+=pow(m_sys_uncertainties_normalized_signal_modeling[m_ttbarConfig->hardScatteringDirectory()][ibin],2);
    m_uncertainties_for_summary_table_relative["Signal modelling"].first[ibin] +=pow(m_sys_uncertainties_normalized_signal_modeling[m_ttbarConfig->partonShoweringDirectory()][ibin],2);
    m_uncertainties_for_summary_table_relative["Signal modelling"].second[ibin]+=pow(m_sys_uncertainties_normalized_signal_modeling[m_ttbarConfig->partonShoweringDirectory()][ibin],2);
    m_uncertainties_for_summary_table_relative["Signal modelling"].first[ibin] +=pow(m_sys_uncertainties_normalized_signal_modeling["ISR"][ibin],2);
    m_uncertainties_for_summary_table_relative["Signal modelling"].second[ibin]+=pow(m_sys_uncertainties_normalized_signal_modeling["ISR"][ibin],2);
    m_uncertainties_for_summary_table_relative["Signal modelling"].first[ibin] +=pow(m_sys_uncertainties_normalized_signal_modeling["FSR"][ibin],2);
    m_uncertainties_for_summary_table_relative["Signal modelling"].second[ibin]+=pow(m_sys_uncertainties_normalized_signal_modeling["FSR"][ibin],2);
    
    for(int i=0;i<m_nsys_pdf;i++){
      m_uncertainties_for_summary_table_relative["PDF"].first[ibin]+=pow(m_sys_uncertainties_normalized_pdf[i][ibin],2);
      m_uncertainties_for_summary_table_relative["PDF"].second[ibin]+=pow(m_sys_uncertainties_normalized_pdf[i][ibin],2);
    }
    
    m_uncertainties_for_summary_table_relative["Signal modelling"].first[ibin] +=m_uncertainties_for_summary_table_relative["PDF"].first[ibin];
    m_uncertainties_for_summary_table_relative["Signal modelling"].second[ibin]+=m_uncertainties_for_summary_table_relative["PDF"].second[ibin];    
  }
}

void AsymmetricErrorsCalculator::finalizeSummaryUncertainties() { 
  
  for(int ibin=0;ibin<m_nbins_truth;ibin++){
    m_sys_uncertainties_detector_all.first[ibin]=sqrt(m_sys_uncertainties_detector_all.first[ibin]);
    m_sys_uncertainties_detector_all.second[ibin]=sqrt(m_sys_uncertainties_detector_all.second[ibin]);
    m_sys_uncertainties_bkg_all.first[ibin]=sqrt(m_sys_uncertainties_bkg_all.first[ibin]);
    m_sys_uncertainties_bkg_all.second[ibin]=sqrt(m_sys_uncertainties_bkg_all.second[ibin]);
    m_sys_uncertainties_detector_all_normalized.first[ibin]=sqrt(m_sys_uncertainties_detector_all_normalized.first[ibin]);
    m_sys_uncertainties_detector_all_normalized.second[ibin]=sqrt(m_sys_uncertainties_detector_all_normalized.second[ibin]);
    m_sys_uncertainties_bkg_all_normalized.first[ibin]=sqrt(m_sys_uncertainties_bkg_all_normalized.first[ibin]);
    m_sys_uncertainties_bkg_all_normalized.second[ibin]=sqrt(m_sys_uncertainties_bkg_all_normalized.second[ibin]);
  }
  
  for(const auto& it : m_uncertainties_for_summary_table_absolute) {
    const TString& name = it.first;
    if(name!="Total systematics" && name!="Top-tagging" && name!="Jets" && name!="Flavor tagging") for(int ibin=0;ibin<m_nbins_truth;ibin++){
      m_uncertainties_for_summary_table_absolute["Total systematics"].first[ibin]+=it.second.first[ibin];
      m_uncertainties_for_summary_table_absolute["Total systematics"].second[ibin]+=it.second.second[ibin];
    }
    
    if(name.BeginsWith("TopTagSF ")) {
      for(int ibin=0;ibin<m_nbins_truth;ibin++){
	m_uncertainties_for_summary_table_absolute["Top-tagging"].first[ibin]+=it.second.first[ibin];
	m_uncertainties_for_summary_table_absolute["Top-tagging"].second[ibin]+=it.second.second[ibin];
      }
    }
    
    //if(name=="JES" || name=="JER" || name=="JMS" || name=="JMR" || name.BeginsWith("TopTagSF ")) for(int ibin=0;ibin<m_nbins_truth;ibin++){
    if(name=="JES" || name=="JER" || name=="JMS" || name=="JMR" ) for(int ibin=0;ibin<m_nbins_truth;ibin++){
      m_uncertainties_for_summary_table_absolute["Jets"].first[ibin]+=it.second.first[ibin];
      m_uncertainties_for_summary_table_absolute["Jets"].second[ibin]+=it.second.second[ibin];
    }
    if(name.Contains("Flavor tagging") && name!="Flavor tagging") for(int ibin=0;ibin<m_nbins_truth;ibin++){
      m_uncertainties_for_summary_table_absolute["Flavor tagging"].first[ibin]+=it.second.first[ibin];
      m_uncertainties_for_summary_table_absolute["Flavor tagging"].second[ibin]+=it.second.second[ibin];
    }
    
  }
  
      
  for(const auto& it : m_uncertainties_for_summary_table_relative) {
    const TString& name = it.first;
    if(name!="Total systematics" && name!="Top-tagging" && name!="Jets" && name!="Flavor tagging") for(int ibin=0;ibin<m_nbins_truth;ibin++){
      m_uncertainties_for_summary_table_relative["Total systematics"].first[ibin]+=it.second.first[ibin];
      m_uncertainties_for_summary_table_relative["Total systematics"].second[ibin]+=it.second.second[ibin];
    }
    
    if(name.BeginsWith("TopTagSF ")) {
      for(int ibin=0;ibin<m_nbins_truth;ibin++){
	m_uncertainties_for_summary_table_relative["Top-tagging"].first[ibin]+=it.second.first[ibin];
	m_uncertainties_for_summary_table_relative["Top-tagging"].second[ibin]+=it.second.second[ibin];
      }
    }
    
    //if(name=="JES" || name=="JER" || name=="JMS" || name=="JMR" || name.BeginsWith("TopTagSF ")) for(int ibin=0;ibin<m_nbins_truth;ibin++){
    if(name=="JES" || name=="JER" || name=="JMS" || name=="JMR" ) for(int ibin=0;ibin<m_nbins_truth;ibin++){
      m_uncertainties_for_summary_table_relative["Jets"].first[ibin]+=it.second.first[ibin];
      m_uncertainties_for_summary_table_relative["Jets"].second[ibin]+=it.second.second[ibin];
    }
    if(name.Contains("Flavor tagging") && name!="Flavor tagging") for(int ibin=0;ibin<m_nbins_truth;ibin++){
      m_uncertainties_for_summary_table_relative["Flavor tagging"].first[ibin]+=it.second.first[ibin];
      m_uncertainties_for_summary_table_relative["Flavor tagging"].second[ibin]+=it.second.second[ibin];
    }
    
  }
  
  m_uncertainties_for_summary_table_absolute["Total"].first.resize(m_nbins_truth);
  m_uncertainties_for_summary_table_absolute["Total"].second.resize(m_nbins_truth);
  m_uncertainties_for_summary_table_relative["Total"].first.resize(m_nbins_truth);
  m_uncertainties_for_summary_table_relative["Total"].second.resize(m_nbins_truth);
  
  m_uncertainties_for_summary_table_absolute["Total"] = {m_total_uncertainties,m_total_uncertainties};
  m_uncertainties_for_summary_table_relative["Total"] = {m_total_uncertainties_normalized,m_total_uncertainties_normalized};
  
  
  
  //m_uncertainties_for_summary_table_absolute["Total"] = m_uncertainties_for_summary_table_absolute["Total systematics"];
  //m_uncertainties_for_summary_table_relative["Total"] = m_uncertainties_for_summary_table_relative["Total systematics"];
  
  //for(int ibin=0;ibin<m_nbins_truth;ibin++){
    //m_uncertainties_for_summary_table_absolute["Total"].first[ibin]+=pow(m_sys_uncertainties_stat[ibin],2);
    //m_uncertainties_for_summary_table_absolute["Total"].second[ibin]+=pow(m_sys_uncertainties_stat[ibin],2);
    //m_uncertainties_for_summary_table_relative["Total"].first[ibin]+=pow(m_sys_uncertainties_normalized_stat[ibin],2);
    //m_uncertainties_for_summary_table_relative["Total"].second[ibin]+=pow(m_sys_uncertainties_normalized_stat[ibin],2);
  //}
  //for(int ibin=0;ibin<m_nbins_truth;ibin++){
    //m_uncertainties_for_summary_table_absolute["Total"].first[ibin]+=pow(m_sys_uncertainties_MCstat[ibin],2);
    //m_uncertainties_for_summary_table_absolute["Total"].second[ibin]+=pow(m_sys_uncertainties_MCstat[ibin],2);
    //m_uncertainties_for_summary_table_relative["Total"].first[ibin]+=pow(m_sys_uncertainties_normalized_MCstat[ibin],2);
    //m_uncertainties_for_summary_table_relative["Total"].second[ibin]+=pow(m_sys_uncertainties_normalized_MCstat[ibin],2);
  //}
  
  
  // Making square roots to get uncertainties from squared sums
  for(auto& it : m_uncertainties_for_summary_table_absolute) {
  //for(auto it=m_uncertainties_for_summary_table_absolute.begin();it!=m_uncertainties_for_summary_table_absolute.end();it++){
    if(it.first != "Total") for(int ibin=0;ibin<m_nbins_truth;ibin++){
      it.second.first[ibin]=sqrt(it.second.first[ibin]);
      it.second.second[ibin]=sqrt(it.second.second[ibin]);
    }
    m_hist_uncertainties_absolute[it.first] = histFromSummaryUncertainties(it.second);
    m_hist_uncertainties_absolute[it.first + " up"] = histFromSummaryUncertainties(it.second.first);
    m_hist_uncertainties_absolute[it.first + " down"] = histFromSummaryUncertainties(it.second.second);
  }
  for(auto& it : m_uncertainties_for_summary_table_relative) {
    if(it.first != "Total") for(int ibin=0;ibin<m_nbins_truth;ibin++){
      it.second.first[ibin]=sqrt(it.second.first[ibin]);
      it.second.second[ibin]=sqrt(it.second.second[ibin]);
    }
    m_hist_uncertainties_relative[it.first] = histFromSummaryUncertainties(it.second);
    m_hist_uncertainties_relative[it.first + " up"] = histFromSummaryUncertainties(it.second.first);
    m_hist_uncertainties_relative[it.first + " down"] = histFromSummaryUncertainties(it.second.second);
  }
  
  
  for(const auto& it : m_sys_uncertainties_signal_modeling) {
    m_hist_uncertainties_absolute[it.first]=histFromSummaryUncertainties(it.second);
  }
  
  m_hist_uncertainties_absolute["Combined Statistics"] = histFromSummaryUncertainties(m_sys_uncertainties_stat_combined);
  m_hist_uncertainties_absolute["Statistics"] = histFromSummaryUncertainties(m_sys_uncertainties_stat);
  m_hist_uncertainties_absolute["MC Statistics"] = histFromSummaryUncertainties(m_sys_uncertainties_MCstat);
  m_hist_uncertainties_absolute["MC Stat. w/o multijet"] = histFromSummaryUncertainties(m_sys_uncertainties_MCstatWithMultijetFixed);
  m_hist_uncertainties_absolute["Multijet Bkg stat"] = histFromSummaryUncertainties(m_sys_uncertainties_multijetBkg_stat);
  
  for(const auto& it : m_sys_uncertainties_normalized_signal_modeling) {
    m_hist_uncertainties_relative[it.first]=histFromSummaryUncertainties(it.second);
  }
  
  m_hist_uncertainties_relative["Combined Statistics"] = histFromSummaryUncertainties(m_sys_uncertainties_normalized_stat_combined);
  m_hist_uncertainties_relative["Statistics"] = histFromSummaryUncertainties(m_sys_uncertainties_normalized_stat);
  m_hist_uncertainties_relative["MC Statistics"] = histFromSummaryUncertainties(m_sys_uncertainties_normalized_MCstat);
  m_hist_uncertainties_relative["MC Stat. w/o multijet"] = histFromSummaryUncertainties(m_sys_uncertainties_normalized_MCstatWithMultijetFixed);
  m_hist_uncertainties_relative["Multijet Bkg stat"] = histFromSummaryUncertainties(m_sys_uncertainties_normalized_multijetBkg_stat);
  
  if (m_debug > 1 ) {
    
    cout << "Printing uncertainty from Flavor tagging: absolute" << endl;
    for(int ibin=0;ibin<m_nbins_truth;ibin++){
      cout << "+" << m_uncertainties_for_summary_table_absolute["Flavor tagging"].first[ibin] << " -" << m_uncertainties_for_summary_table_absolute["Flavor tagging"].second[ibin] << " % " << endl;
      
    }
    cout << "Printing uncertainty from Flavor tagging: relative" << endl;
    for(int ibin=0;ibin<m_nbins_truth;ibin++){
      cout << "+" << m_uncertainties_for_summary_table_relative["Flavor tagging"].first[ibin] << " -" << m_uncertainties_for_summary_table_relative["Flavor tagging"].second[ibin] << " % " << endl;
      
    }
    
  }
  
}

// -------------------------------------------------------------------------------------------------------




//void AsymmetricErrorsCalculator::calculateOneSystematicsPaired(vector<double>& errdown,vector<double>& errup,TH1D* shift_down, TH1D* shift_up){
  //int ibin=1;
  //const double min_value=1.e-6;
  //errup.resize(m_nbins_truth);
  //errdown.resize(m_nbins_truth);
  
  //for(int i=0;i<m_nbins_truth;i++){
    //errup[i]=100.*shift_up->GetBinContent(i+1);
    //errdown[i]=100.*shift_down->GetBinContent(i+1);
  //}
    
  
//}



void AsymmetricErrorsCalculator::calculateOneSystematicsPaired(vector<double>& errdown,vector<double>& errup,TH1D* shift_down, TH1D* shift_up){ // Function to extract systematic variations from histograms
  int ibin=1;
  const double min_value=1.e-6;
  errup.resize(m_nbins_truth);
  errdown.resize(m_nbins_truth);
  
  bool whichIsUp=true; // false == shift_down, true = shift_up
  double shiftUp,shiftDown;
  
  for(ibin=1;ibin<=m_nbins_truth;ibin++){
    if((std::abs(shift_up->GetBinContent(ibin)) > min_value) || (std::abs(shift_down->GetBinContent(ibin)) > min_value) ){
      shiftUp=shift_up->GetBinContent(ibin);
      shiftDown=shift_down->GetBinContent(ibin);
      
      if(std::abs(shiftUp) >= std::abs(shiftDown)){
	if(shiftUp > min_value) whichIsUp = true;
	else whichIsUp = false;
      }
      else{
	if(shiftDown > min_value)whichIsUp=false;
	else whichIsUp=true;
      }
      break;
    }
  }
  
  if(ibin==m_nbins_truth+1)whichIsUp=true;
  int i;
  for(ibin=1;ibin<=m_nbins_truth;ibin++){
    i=ibin-1;
    if(whichIsUp){
      shiftUp=shift_up->GetBinContent(ibin);
      shiftDown=shift_down->GetBinContent(ibin);
    }
    else{
      shiftUp=shift_down->GetBinContent(ibin);
      shiftDown=shift_up->GetBinContent(ibin);
    }
    if(shiftUp*shiftDown < 0.){
      errup[i]=100.*shiftUp;
      errdown[i]=100.*shiftDown;
    }
    else if(shiftUp > shiftDown){
      errup[i]=50.*(shiftUp + shiftDown);
      errdown[i]=0.;
    }
    else{
      errdown[i]=50.*(shiftUp + shiftDown);
      errup[i]=0.;
    }
    
  }
  
}

//-------------------------------------------------------------------------------------

void AsymmetricErrorsCalculator::calculateOneSystematicsSingle(vector<double>& err,TH1D* shift){ // Function to extract systematic variations from histogram
  err.resize(m_nbins_truth);
  for(int i=0;i<m_nbins_truth;i++)err[i]=100.*shift->GetBinContent(i+1);
  
  //int imax=0;
  //double max=-1.;
  //for(int i=0;i<m_nbins_truth;i++){
    //if(std::abs(err[i])>max)imax=i;
  //}
  //if(err[imax]<0)for(int i=0;i<m_nbins_truth;i++)err[i]*=-1.;
    
  
}

//-------------------------------------------------------------------------------------

void AsymmetricErrorsCalculator::initializeLatexTable(ofstream& textfile){
  
  textfile << "\\resizebox{\\textwidth}{!}{" << endl;; 
  textfile << "\\begin{tabular}{l";
  for(int i=0;i<m_nbins_truth;i++)textfile << "c";
  textfile << "}" << endl;
  textfile <<  "\\hline" << endl;;
  
}

//-------------------------------------------------------------------------------------

void AsymmetricErrorsCalculator::printBinningInfoIntoLatexTable(ofstream& textfile,const TString& unit) {
  
  if(unit=="") textfile << "Bins ";
  else textfile << "Bins [" << unit << "]";
  
  bool isND=m_variable.Contains("_vs_");
  
  if(isND) {
    for(int i=0;i<m_nbins_truth;i++) {
      int bin{-1};
      std::cout << "Getting axis ID" <<std::endl;
      std::string id = m_histogramConverterND->getAxisID(i+1,bin);
      std::cout << "ID: " << id << " bin: " << bin << std::endl;
      const std::vector<double> lowEdges = m_histogramConverterND->getBinLowEdges(id,bin);
      const std::vector<double> upEdges = m_histogramConverterND->getBinUpEdges(id,bin);
      std::cout << "Done." <<std::endl;
      textfile << " & ";
      for(size_t j=0;j<lowEdges.size();j++) {
	std::cout << "[" << lowEdges[j] << "," << upEdges[j] << "]";
	
	textfile << "[" << lowEdges[j] << "," << upEdges[j] << "]";
	if(j<lowEdges.size()-1) textfile << "\\otimes";
	
      }
      std::cout << std::endl;
    }
    
    
  }
  else {
    for(int i=0;i<m_nbins_truth;i++) textfile << " & [" << m_hist_template_truth->GetBinLowEdge(i+1)<< "," << m_hist_template_truth->GetBinLowEdge(i+2) << "]";
  }
  
  
  
  textfile << " \\\\ \\hline" << endl;
}

//-------------------------------------------------------------------------------------

void AsymmetricErrorsCalculator::printCentralValuesIntoLatexTable(ofstream& textfile,const vector<double>& centralValues,const TString& crossSection,const int& precision){
  
  textfile<< setprecision(precision) << crossSection;
  for(int i=0;i<m_nbins_truth;i++) {
    
    double z=0;
    int k=0;
    functions::splitDouble(centralValues[i],z,k);
    
    textfile << " & $" << z;
    if(k!=0)textfile << "\\cdot 10^{" << k << "}$ ";
    else textfile << "$ ";
  }
  textfile << " \\\\" << endl;
  
}

//-------------------------------------------------------------------------------------

void AsymmetricErrorsCalculator::printOveralUncertaintiesIntoLatexTable(ofstream& textfile, const map<TString,pair<vector<double>,vector<double> > >& uncertainties_for_summary_table, const vector<double>& sys_uncertainties_stat, int& irow ) {
  TString category="";
  
  irow++;
  printOneUncertaintyWithAbsoluteValues(textfile,"Total Uncertainty",category,uncertainties_for_summary_table.at("Total").first,uncertainties_for_summary_table.at("Total").second,irow % 2);
  irow++;
  printOneUncertaintySymmetrized(textfile,"Statistics",category,sys_uncertainties_stat,irow % 2);
  irow++;
  printOneUncertaintyWithAbsoluteValues(textfile,"Systematics",category,uncertainties_for_summary_table.at("Total systematics").first,uncertainties_for_summary_table.at("Total systematics").second,m_irowAbsolute % 2);
  textfile << " \\hline \\hline " << endl;
}

//-------------------------------------------------------------------------------------

void AsymmetricErrorsCalculator::printOveralUncertaintiesIntoLatexTable(ofstream& textfile, const std::vector<double>& uncertainties_total, const std::vector<double>& uncertainties_sys, const std::vector<double>& uncertainties_stat,int& irow ) {
  TString category="";
  irow++;
  printOneUncertaintySymmetrized(textfile,"Total Uncertainty",category,uncertainties_total,irow % 2);
  irow++;
  printOneUncertaintySymmetrized(textfile,"Statistics",category,uncertainties_stat,irow % 2);
  irow++;
  printOneUncertaintySymmetrized(textfile,"Systematics",category,uncertainties_sys,irow % 2);
  
}


//-------------------------------------------------------------------------------------

TString AsymmetricErrorsCalculator::getCategory(const TString& name){
  TString category="";
  
  if (m_debug > 1 ) cout << "sys name: " << name << endl;
  
  if(name.Contains("CategoryReduction")) category="JETS";
  else if(name.Contains("bTagSF")) category="FTAG";
  else if(name.Contains("MUON") || name.BeginsWith("EG_")) category="LEP";
  else if(name.Contains("MET") || name=="jvt_1down" || name=="jvf_1down" || name=="pileup_1down" ) category="MET/PU";
  else if(name=="singletop_tchan_1down" || name=="singletop_Wt_1down" ||name=="ttW_1down" ||name=="ttZ_1down" || name=="ttH_1down" ) category="BKG";
  else category="";
  
  return category;
}

//--------------------------------------------------------------------------------------
void AsymmetricErrorsCalculator::printPairedSystematicUncertaintiesIntoLatexTable(ofstream& textfileAbsolute,ofstream& textfileNormalized){
  
  for(int index=0;index<m_nsys_pairs;index++){
    const TString& name = m_sys_chain_names_paired[index].first;
    const TString& category = this->getCategory(name);
    m_irowAbsolute++;
    m_irowRelative++;
    printOneUncertainty(textfileAbsolute,name,category,m_sys_uncertainties_paired[index].first,m_sys_uncertainties_paired[index].second,m_irowAbsolute % 2);
    printOneUncertainty(textfileNormalized,name,category,m_sys_uncertainties_normalized_paired[index].first,m_sys_uncertainties_normalized_paired[index].second,m_irowRelative % 2);
  }
  
}

//-------------------------------------------------------------------------------------


void AsymmetricErrorsCalculator::printOneSidedSystematicUncertaintiesIntoLatexTable(ofstream& textfileAbsolute,ofstream& textfileNormalized){
  
  for(int index=0;index<m_nsys_single;index++){
    const TString& name = m_sys_chain_names_single[index];
    const TString &category = this->getCategory(name);
    m_irowAbsolute++;
    m_irowRelative++;
    printOneUncertaintySymmetrized(textfileAbsolute,name,category,m_sys_uncertainties_single[index],m_irowAbsolute % 2);
    printOneUncertaintySymmetrized(textfileNormalized,name,category,m_sys_uncertainties_normalized_single[index],m_irowRelative % 2);	
  }
  
  
  
}


//-------------------------------------------------------------------------------------

void AsymmetricErrorsCalculator::printSignalModelingUncertaintiesIntoLatexTable(ofstream& textfileAbsolute,ofstream& textfileNormalized){
  
  TString category="";
  if(m_doSignalModeling){
    for(int index=0;index<m_nsys_pdf;index++){
      TString name = m_sys_chain_names_pdf[index+1];
      category="PDF";
      m_irowAbsolute++;
      m_irowRelative++;
      printOneUncertaintySymmetrized(textfileAbsolute,name,category,m_sys_uncertainties_pdf[index],m_irowAbsolute % 2);
      printOneUncertaintySymmetrized(textfileNormalized,name,category,m_sys_uncertainties_normalized_pdf[index],m_irowRelative % 2);	
    }
    
    category="MOD";
    m_irowAbsolute++;
    printOneUncertaintySymmetrized(textfileAbsolute,"Alternative hard-scattering model",category, m_sys_uncertainties_signal_modeling[m_ttbarConfig->hardScatteringDirectory()],m_irowAbsolute % 2);
    
    m_irowAbsolute++;
    printOneUncertaintySymmetrized(textfileAbsolute,"Alternative parton-shower model",category, m_sys_uncertainties_signal_modeling[m_ttbarConfig->partonShoweringDirectory()],m_irowAbsolute % 2);
    
    m_irowAbsolute++;
    printOneUncertaintySymmetrized(textfileAbsolute,"ISR/FSR + scale",category, m_sys_uncertainties_signal_modeling["ISR"],m_irowAbsolute % 2);
    
    m_irowAbsolute++;
    printOneUncertaintySymmetrized(textfileAbsolute,"FSR",category, m_sys_uncertainties_signal_modeling["FSR"],m_irowAbsolute % 2);
    
    m_irowRelative++;
    printOneUncertaintySymmetrized(textfileNormalized,"Alternative hard-scattering model",category, m_sys_uncertainties_normalized_signal_modeling[m_ttbarConfig->hardScatteringDirectory()],m_irowRelative % 2);
    
    m_irowRelative++;
    printOneUncertaintySymmetrized(textfileNormalized,"Alternative parton-shower mode",category, m_sys_uncertainties_normalized_signal_modeling[m_ttbarConfig->partonShoweringDirectory()],m_irowRelative % 2);
    
    m_irowRelative++;
    printOneUncertaintySymmetrized(textfileNormalized,"ISR/FSR + scale",category, m_sys_uncertainties_normalized_signal_modeling["ISR"],m_irowRelative % 2);
    
    m_irowRelative++;
    printOneUncertaintySymmetrized(textfileNormalized,"FSR",category, m_sys_uncertainties_normalized_signal_modeling["FSR"],m_irowRelative % 2);
  }
  
  category="MOD";
  m_irowAbsolute++;
  m_irowRelative++;
  printOneUncertaintySymmetrized(textfileAbsolute,"Monte Carlo sample statistics",category,m_sys_uncertainties_MCstat,m_irowAbsolute % 2);
  printOneUncertaintySymmetrized(textfileNormalized,"Monte Carlo sample statistics",category,m_sys_uncertainties_normalized_MCstat,m_irowRelative % 2);
  
  
  
}

//-------------------------------------------------------------------------------------

void AsymmetricErrorsCalculator::finalizeLatexTable(ofstream& textfile){
  
  textfile << "\\hline" << endl;
  textfile << "\\end{tabular}" << endl;
  textfile << "}" << endl;; 
  
}

//-------------------------------------------------------------------------------------

void AsymmetricErrorsCalculator::printDetailedTablesOfUncertainties(ofstream& textfileAbsolute,ofstream& textfileNormalized){
  // Function to print uncertainties to tables: REVISION NEEDED! Will be done after we have a full set of uncertainties.
  
  // Making latex tables heathers
  this->initializeLatexTable(textfileAbsolute);
  this->initializeLatexTable(textfileNormalized);
  
  
  
  TString unit = NamesAndTitles::getDifferentialVariableUnit(m_variable);
  unit = "$\\mathrm{"+unit+"}$";
 
  // Printing bin edges into latex tables
  this->printBinningInfoIntoLatexTable(textfileAbsolute,unit);
  this->printBinningInfoIntoLatexTable(textfileNormalized,unit);
  
  
  TString diffVariable = NamesAndTitles::getDifferentialVariableLatex(m_variable);
  TString inverseUnit = NamesAndTitles::getInverseUnit(m_variable);
  if(inverseUnit!="") inverseUnit="\\mathrm{" + inverseUnit + "}";
  diffVariable.ReplaceAll("#","\\");
  
  TString crossSectionAbsolute{""};
  TString crossSectionNormalized{""};
  
  if (inverseUnit != "") crossSectionAbsolute =  (TString)"$\\frac{d\\sigma}{" +diffVariable+"}"+ " [\\mathrm{fb}\\cdot " + inverseUnit +"]$";
  else                   crossSectionAbsolute =  (TString)"$\\frac{d\\sigma}{" +diffVariable+"}"+ " [\\mathrm{fb}]$";
  
  if(m_variable=="total_cross_section" || diffVariable=="") crossSectionAbsolute = "$\\sigma [\\mathrm{fb}]$";
  
  if (inverseUnit != "") crossSectionNormalized = (TString)"$\\frac{1}{\\sigma} " +"\\frac{d \\sigma}{" +diffVariable+"}"+ " ["+ inverseUnit +"]$";
  else                   crossSectionNormalized = (TString)"$\\frac{1}{\\sigma} " +"\\frac{d \\sigma}{" +diffVariable+"}$";
  
  
  //const TString& crossSectionAbsolute = (unit=="") ? "$d\\sigma / d X$ [pb]" : "$d\\sigma / d X$ [pb/" + unit + "]";
  //const TString& crossSectionNormalized = (unit=="") ? "$1 / \\sigma  d\\sigma / d X$ [pb]" : "$1 / \\sigma  d\\sigma / d X$ [pb/" + unit + "]";
  const int& precision = 3;
  
  // Printing unfolded central values into latex tables
  this->printCentralValuesIntoLatexTable(textfileAbsolute,m_central_values,crossSectionAbsolute,precision);
  this->printCentralValuesIntoLatexTable(textfileNormalized,m_central_values_normalized,crossSectionNormalized,precision);
  
  TString category="";
  
  m_irowAbsolute=1;
  m_irowRelative=1;
  
  // Printing total, statistical and overal systematic uncertainties into latex tables
  printOveralUncertaintiesIntoLatexTable(textfileAbsolute,
                                         m_total_uncertainties,
					 m_sys_uncertainties_total,
					 m_sys_uncertainties_stat,
					 m_irowAbsolute);
  printOveralUncertaintiesIntoLatexTable(textfileNormalized,
                                         m_total_uncertainties_normalized,
					 m_sys_uncertainties_normalized_total,
					 m_sys_uncertainties_normalized_stat,
					 m_irowRelative);
  //this->printOveralUncertaintiesIntoLatexTable(textfileAbsolute, m_uncertainties_for_summary_table_absolute, m_sys_uncertainties_stat , m_irowAbsolute );
  //this->printOveralUncertaintiesIntoLatexTable(textfileNormalized, m_uncertainties_for_summary_table_relative, m_sys_uncertainties_normalized_stat , m_irowRelative );
  
  // Printing individual systematic uncertainties
  this->printOneSidedSystematicUncertaintiesIntoLatexTable(textfileAbsolute,textfileNormalized);
  this->printPairedSystematicUncertaintiesIntoLatexTable(textfileAbsolute,textfileNormalized);
  this->printSignalModelingUncertaintiesIntoLatexTable(textfileAbsolute,textfileNormalized);
  
  this->finalizeLatexTable(textfileAbsolute);
  this->finalizeLatexTable(textfileNormalized);
  
}
void AsymmetricErrorsCalculator::printSummaryTables(ofstream& textfileSummaryAbsolute,ofstream& textfileSummaryNormalized){
  
  // Printing individual uncertainties into summary tables!
  
  this->initializeLatexTable(textfileSummaryAbsolute);
  this->initializeLatexTable(textfileSummaryNormalized);
  
  TString unit = NamesAndTitles::getDifferentialVariableUnit(m_variable);
  unit = "$\\mathrm{"+unit+"}$";
  
  this->printBinningInfoIntoLatexTable(textfileSummaryAbsolute,unit);
  this->printBinningInfoIntoLatexTable(textfileSummaryNormalized,unit);
  
  TString diffVariable = NamesAndTitles::getDifferentialVariableLatex(m_variable);
  TString inverseUnit = NamesAndTitles::getInverseUnit(m_variable);
  if(inverseUnit!="") inverseUnit="\\mathrm{" + inverseUnit + "}";
  diffVariable.ReplaceAll("#","\\");
  
  TString crossSectionAbsolute{""};
  TString crossSectionNormalized{""};
  
  if (inverseUnit != "") crossSectionAbsolute =  (TString)"$\\frac{d\\sigma}{" +diffVariable+"}"+ " [\\mathrm{fb}\\cdot " + inverseUnit +"]$";
  else                   crossSectionAbsolute =  (TString)"$\\frac{d\\sigma}{" +diffVariable+"}"+ " [\\mathrm{fb}]$";
  
  if(m_variable=="total_cross_section" || diffVariable=="") crossSectionAbsolute = "$\\sigma [\\mathrm{fb}]$";
  
  if (inverseUnit != "") crossSectionNormalized = (TString)"$\\frac{1}{\\sigma} " +"\\frac{d \\sigma}{" +diffVariable+"}"+ " ["+ inverseUnit +"]$";
  else                   crossSectionNormalized = (TString)"$\\frac{1}{\\sigma} " +"\\frac{d \\sigma}{" +diffVariable+"}$";
  
  
  //const TString& crossSectionAbsolute = (unit=="") ? "$d\\sigma / d X$ [pb]" : "$d\\sigma / d X$ [pb/" + unit + "]";
  //const TString& crossSectionNormalized = (unit=="") ? "$1 / \\sigma  d\\sigma / d X$ [pb]" : "$1 / \\sigma  d\\sigma / d X$ [pb/" + unit + "]";
  const int& precision = 3;
  
  // Printing unfolded central values into latex tables
  this->printCentralValuesIntoLatexTable(textfileSummaryAbsolute,m_central_values,crossSectionAbsolute,precision);
  this->printCentralValuesIntoLatexTable(textfileSummaryNormalized,m_central_values_normalized,crossSectionNormalized,precision);
  
  // Printing total, statistical and overal systematic uncertainties into latex tables
  printOveralUncertaintiesIntoLatexTable(textfileSummaryAbsolute,
                                         m_total_uncertainties,
					 m_sys_uncertainties_total,
					 m_sys_uncertainties_stat,
					 m_irowAbsolute);
  printOveralUncertaintiesIntoLatexTable(textfileSummaryNormalized,
                                         m_total_uncertainties_normalized,
					 m_sys_uncertainties_normalized_total,
					 m_sys_uncertainties_normalized_stat,
					 m_irowRelative);
  
  //this->printOveralUncertaintiesIntoLatexTable(textfileSummaryAbsolute, m_uncertainties_for_summary_table_absolute, m_sys_uncertainties_stat , m_irowAbsolute );
  //this->printOveralUncertaintiesIntoLatexTable(textfileSummaryNormalized, m_uncertainties_for_summary_table_relative, m_sys_uncertainties_normalized_stat , m_irowRelative );
  
  TString category="";
  
  m_irowAbsolute=1;
  m_irowRelative=1;
  
  for(auto it=m_uncertainties_for_summary_table_absolute.begin();it!=m_uncertainties_for_summary_table_absolute.end();it++){
    if(it->first.Contains("Total") || it->first.Contains("Signal modelling") || it->first == "Flavor tagging" || it->first == "Jets") continue;
    m_irowAbsolute++;
    printOneUncertaintyWithAbsoluteValues(textfileSummaryAbsolute,it->first,category,it->second.first,it->second.second,m_irowAbsolute % 2);
  }
  for(auto it=m_uncertainties_for_summary_table_relative.begin();it!=m_uncertainties_for_summary_table_relative.end();it++){
    if(it->first.Contains("Total") || it->first.Contains("Signal modelling") || it->first == "Flavor tagging" || it->first == "Jets") continue;
    m_irowRelative++;
    printOneUncertaintyWithAbsoluteValues(textfileSummaryNormalized,it->first,category,it->second.first,it->second.second,m_irowRelative % 2);
  }
  
  category="";
  m_irowAbsolute++;
  m_irowRelative++;
  printOneUncertaintySymmetrized(textfileSummaryAbsolute,"Monte Carlo sample statistics",category,m_sys_uncertainties_MCstat,m_irowAbsolute % 2);
  printOneUncertaintySymmetrized(textfileSummaryNormalized,"Monte Carlo sample statistics",category,m_sys_uncertainties_normalized_MCstat,m_irowRelative % 2);
  
  if(m_doSignalModeling){
    m_irowAbsolute++;
    printOneUncertaintySymmetrized(textfileSummaryAbsolute,"Alternative hard-scattering model",category, m_sys_uncertainties_signal_modeling[m_ttbarConfig->hardScatteringDirectory()],m_irowAbsolute % 2);
    m_irowAbsolute++;
    printOneUncertaintySymmetrized(textfileSummaryAbsolute,"Alternative parton-shower model",category, m_sys_uncertainties_signal_modeling[m_ttbarConfig->partonShoweringDirectory()],m_irowAbsolute % 2);
    m_irowAbsolute++;
    printOneUncertaintySymmetrized(textfileSummaryAbsolute,"ISR/FSR + scale",category, m_sys_uncertainties_signal_modeling["ISR"],m_irowAbsolute % 2);
    m_irowAbsolute++;
    printOneUncertaintySymmetrized(textfileSummaryAbsolute,"FSR",category, m_sys_uncertainties_signal_modeling["FSR"],m_irowAbsolute % 2);
    m_irowRelative++;
    printOneUncertaintySymmetrized(textfileSummaryNormalized,"Alternative hard-scattering model",category, m_sys_uncertainties_normalized_signal_modeling[m_ttbarConfig->hardScatteringDirectory()],m_irowRelative % 2);
    m_irowRelative++;
    printOneUncertaintySymmetrized(textfileSummaryNormalized,"Alternative parton-shower model",category, m_sys_uncertainties_normalized_signal_modeling[m_ttbarConfig->partonShoweringDirectory()],m_irowRelative % 2);
    m_irowRelative++;
    printOneUncertaintySymmetrized(textfileSummaryNormalized,"ISR/FSR + scale",category, m_sys_uncertainties_normalized_signal_modeling["ISR"],m_irowRelative % 2);
    m_irowRelative++;
    printOneUncertaintySymmetrized(textfileSummaryNormalized,"FSR",category, m_sys_uncertainties_normalized_signal_modeling["FSR"],m_irowRelative % 2);
    
  }
  
  this->finalizeLatexTable(textfileSummaryAbsolute);
  this->finalizeLatexTable(textfileSummaryNormalized);
  
  
}

void AsymmetricErrorsCalculator::printOneUncertaintyWithAbsoluteValues(ofstream& textfile,TString name,TString category,const vector<double>& errdown,const vector<double>& errup,bool useColor){
	
  name.ReplaceAll("__1up","");
  name.ReplaceAll("__1down","");
  name.ReplaceAll("_1up","");
  name.ReplaceAll("_1down","");
  if(name.EndsWith("_down"))name.ReplaceAll("_down","");
  if(name.EndsWith("_up"))name.ReplaceAll("_up","");
  
  const int precision = -log10(m_printUncertaintyLowThreshold);
  
  TString sys_name = m_config_sys_names->GetValue(name,"");
  if(sys_name==""){
	  name.ReplaceAll("_","\\_");
	  sys_name=name;
  }
  if(sys_name=="luminosity")sys_name="Luminosity";
  
  sys_name.ReplaceAll("\\\\","\\");
  
  if(category!="") sys_name="("+category+") " + sys_name;
  
  if(useColor) textfile << "\\rowcolor{Gray}" << endl;
  textfile << sys_name << " $[\\%]$";
  textfile << fixed << setprecision(precision);
  
  //cout << sys_name << endl;
  for(int ibin=0;ibin<m_nbins_truth;ibin++) {
    //cout << ibin+1 << " up: " << errup[ibin] << " down: " << errdown[ibin] << endl;

    textfile << " & ";
    if(errup[ibin]<=m_printUncertaintyLowThreshold && errdown[ibin] <=m_printUncertaintyLowThreshold){
      textfile << " $-$ ";
    }
    else{
      textfile << " $^{";
      errup[ibin] > m_printUncertaintyLowThreshold ? textfile << "+" << errup[ibin] : textfile << "-";
      textfile << "}_{";
      errdown[ibin] > m_printUncertaintyLowThreshold ? textfile << "-" << errdown[ibin] :  textfile << "-";
      textfile << "}$";
    }
  }
  textfile << " \\\\" << endl;
}

void AsymmetricErrorsCalculator::printOneUncertainty(ofstream& textfile,TString name,TString category,const vector<double>& errdown,const vector<double>& errup,bool useColor){
	
  name.ReplaceAll("__1up","");
  name.ReplaceAll("__1down","");
  name.ReplaceAll("_1up","");
  name.ReplaceAll("_1down","");
  if(name.EndsWith("_down"))name.ReplaceAll("_down","");
  if(name.EndsWith("_up"))name.ReplaceAll("_up","");
  
  const int precision = -log10(m_printUncertaintyLowThreshold);
  
  TString sys_name = m_config_sys_names->GetValue(name,"");
  if(sys_name==""){
	  name.ReplaceAll("_","\\_");
	  sys_name=name;
  }
  if(sys_name=="luminosity") sys_name="Luminosity";
  
  sys_name.ReplaceAll("\\\\","\\");
  
  if(category!="") sys_name="("+category+") " + sys_name;
  
  if(useColor) textfile << "\\rowcolor{Gray}" << endl;
  textfile << sys_name << " $[\\%]$";
  textfile << fixed << setprecision(precision);
  
  //cout << sys_name << endl;
  for(int ibin=0;ibin<m_nbins_truth;ibin++) {
    //cout << ibin+1 << " up: " << errup[ibin] << " down: " << errdown[ibin] << endl;
    
    
    
    
    textfile << " & ";
    
    if(std::abs(errup[ibin])<=m_printUncertaintyLowThreshold && std::abs(errdown[ibin] <=m_printUncertaintyLowThreshold)){
      textfile << " $-$ ";
    }
    else{
      textfile << " $^{";
      if(errup[ibin]>0){
	errup[ibin] > m_printUncertaintyLowThreshold ? textfile << "+" << errup[ibin] : textfile << "-";
      }
      else{
	errup[ibin] < -m_printUncertaintyLowThreshold ? textfile << "-" << std::abs(errup[ibin]) : textfile << "-";
      }
      
      textfile << "}_{";
      
      if(errdown[ibin]>0){
	errdown[ibin] > m_printUncertaintyLowThreshold ? textfile << "+" << errdown[ibin] : textfile << "-";
      }
      else{
	errdown[ibin] < -m_printUncertaintyLowThreshold ? textfile << "-" << std::abs(errdown[ibin]) : textfile << "-";
      }
      textfile << "}$";
    }
  }
  textfile << " \\\\" << endl;
}


void AsymmetricErrorsCalculator::printOneUncertaintySymmetrized(ofstream& textfile,TString name,TString category,const vector<double>& err,bool useColor){
  
  name.ReplaceAll("__1up","");
  name.ReplaceAll("__1down","");
  name.ReplaceAll("_1up","");
  name.ReplaceAll("_1down","");
  if(name.EndsWith("_down"))name.ReplaceAll("_down","");
  if(name.EndsWith("_up"))name.ReplaceAll("_up","");
  
  const int precision = -log10(m_printUncertaintyLowThreshold);
  
  TString sys_name = m_config_sys_names->GetValue(name,"");
  if(sys_name==""){
	  name.ReplaceAll("_","\\_");
	  sys_name=name;
  }
  if(sys_name=="luminosity")sys_name="Luminosity";
  
  sys_name.ReplaceAll("\\\\","\\");
  
  if(category!="") sys_name="("+category+") " + sys_name;
  
  if(useColor) textfile << "\\rowcolor{Gray}" << endl;
  textfile << sys_name << " $[\\%]$" ;
  textfile << fixed << setprecision(precision);
  

  for(int ibin=0;ibin<m_nbins_truth;ibin++) {
    textfile << " & ";

    if(std::abs(err[ibin])<=m_printUncertaintyLowThreshold){
      textfile << " $-$ ";
    }
    else{
      
      if(err[ibin]>m_printUncertaintyLowThreshold){
	textfile << " $\\pm " << err[ibin] << "$ ";
      }
      else{
	textfile << "$ \\mp " << -err[ibin] << "$ ";
      }
      
    }
  }
  textfile << " \\\\" << endl;
  
  
  
}

TH1D* AsymmetricErrorsCalculator::histFromSummaryUncertainties(const pair<vector<double>,vector<double> >& uncertainties){
  TH1D* h=(TH1D*)m_hist_template_truth->Clone();
  for(int i=0;i<m_nbins_truth;i++){
    h->SetBinContent(i+1, (abs(uncertainties.first[i])+abs(uncertainties.second[i]))/2);
  }
  return h;
}
TH1D* AsymmetricErrorsCalculator::histFromSummaryUncertainties(const vector<double>& uncertainties){
  TH1D* h=(TH1D*)m_hist_template_truth->Clone();
  for(int i=0;i<m_nbins_truth;i++){
    h->SetBinContent(i+1, (uncertainties[i]));
  }
  return h;
  
}
void AsymmetricErrorsCalculator::plotUncertainties(const TString& foldername,map<TString,TH1D*>& histmap,TString crossSection){
  vector<TH1D*> histos;
  TString sysname;
  
  double legX1=0.43;
  double legY1=0.91;
  double legX2=0.9;
    
  if(!m_is1D)legY1=0.93;
  
  double legY2=legY1;
  
  for(auto it=histmap.begin();it!=histmap.end();it++){
    it->second->GetYaxis()->SetTitle("Relative Uncertainty [%]");
    NamesAndTitles::setXtitle(it->second,m_variable);
  }
  
  
  //TLegend *leg = new TLegend(0.465,0.7,0.925,0.92);
  auto leg = std::make_unique<TLegend>(legX1,legY1,legX2,legY2);
  leg->SetNColumns(2);
  leg->SetFillColor(0);
  if(m_is1D) leg->SetFillStyle(0);
  leg->SetBorderSize(0);
  leg->SetTextFont(42);
  leg->SetTextSize(0.039);
  
  
  TString dirname=m_mainDir+"/pdf."+m_mc_samples_production+"/"+foldername+"/";
  TString name=m_variable;
  name.ReplaceAll("_fine","");
  if(crossSection=="Absolute")name += "_abs";
  if(crossSection=="Relative")name += "_rel";
  TString figurename=m_level + "_" + name;
  bool use_logscale=false;
  
  
  sysname="Total";histos.push_back(histmap[sysname]);histmap[sysname]->SetLineColor(kGray+3);histmap[sysname]->SetFillColor(kBlue-10);histmap[sysname]->SetMarkerStyle(1);histmap[sysname]->SetLineWidth(3);histmap[sysname]->SetFillStyle(1001);histmap[sysname]->SetLineStyle(1);leg->AddEntry(histmap[sysname],"Total Uncert.","f");
  sysname="Statistics";histos.push_back(histmap[sysname]);histmap[sysname]->SetLineColor(kBlue-9);histmap[sysname]->SetFillColor(kBlue-8);histmap[sysname]->SetMarkerStyle(1);histmap[sysname]->SetLineWidth(0);histmap[sysname]->SetFillStyle(1001);histmap[sysname]->SetLineStyle(1);leg->AddEntry(histmap[sysname],"Stat. Uncert.","f");
  sysname="MC Statistics";histos.push_back(histmap[sysname]);histmap[sysname]->SetLineColor(kGray+3);histmap[sysname]->SetMarkerStyle(20);histmap[sysname]->SetLineWidth(3);histmap[sysname]->SetLineStyle(4);leg->AddEntry(histmap[sysname],"MC & Multijet Stat.","l");
  sysname="Flavor tagging";
  
      if(strstr(figurename,"abs"))
    {
    TString tag_comb="Cont80_77";
    int nbins =   histmap[sysname]->GetNbinsX();

    cout << "    h_" << m_variable << "_flavour_abs_"<< tag_comb <<" = r.TH1F(\"" << m_variable << "_flavour_abs_"<< tag_comb <<"\", \"" << m_variable << "_flavour_abs_"<< tag_comb <<"\", " << nbins << ", 0, " << nbins << ")" << endl << endl;

    

    for (int i=1;i<=nbins;i++){
    cout << "    h_"<< m_variable <<"_flavour_abs_" << tag_comb << ".SetBinContent(" << i << ", "<< histmap[sysname]->GetBinContent(i)  <<")"<< endl;
    
    }

    } 
  
  histos.push_back(histmap[sysname]);histmap[sysname]->SetLineColor(kBlue+1);histmap[sysname]->SetMarkerStyle(1);histmap[sysname]->SetLineWidth(3);histmap[sysname]->SetLineStyle(2);leg->AddEntry(histmap[sysname],"Flavour Tagging","l");
  if(m_doSignalModeling){sysname="Signal modelling";histos.push_back(histmap[sysname]);histmap[sysname]->SetLineColor(kOrange-4);histmap[sysname]->SetMarkerStyle(20);histmap[sysname]->SetLineWidth(3);histmap[sysname]->SetLineStyle(1);leg->AddEntry(histmap[sysname],"Signal modelling","l");}
  //if(m_doSignalModeling){sysname=m_ttbarConfig->hardScatteringDirectory();histos.push_back(histmap[sysname]);histmap[sysname]->SetLineColor(kOrange-4);histmap[sysname]->SetMarkerStyle(20);histmap[sysname]->SetLineWidth(3);histmap[sysname]->SetLineStyle(1);leg->AddEntry(histmap[sysname],"Hard scattering","l");}
  //if(m_doSignalModeling){sysname=m_ttbarConfig->partonShoweringDirectory();histos.push_back(histmap[sysname]);histmap[sysname]->SetLineColor(kGreen+3);histmap[sysname]->SetMarkerStyle(20);histmap[sysname]->SetLineWidth(3);histmap[sysname]->SetLineStyle(1);leg->AddEntry(histmap[sysname],"Parton shower","l");}
  //if(m_doSignalModeling){sysname="ISR";histos.push_back(histmap[sysname]);histmap[sysname]->SetLineColor(kPink+10);histmap[sysname]->SetMarkerStyle(20);histmap[sysname]->SetLineWidth(3);histmap[sysname]->SetLineStyle(1);leg->AddEntry(histmap[sysname],"ISR","l");}
  //if(m_doSignalModeling){sysname="FSR";histos.push_back(histmap[sysname]);histmap[sysname]->SetLineColor(kBlack);histmap[sysname]->SetMarkerStyle(20);histmap[sysname]->SetLineWidth(3);histmap[sysname]->SetLineStyle(1);leg->AddEntry(histmap[sysname],"#alpha_{s} FSR","l");}
  //if(m_doSignalModeling){sysname="PDF";histos.push_back(histmap[sysname]);histmap[sysname]->SetLineColor(kGreen);histmap[sysname]->SetMarkerStyle(20);histmap[sysname]->SetLineWidth(3);histmap[sysname]->SetLineStyle(1);leg->AddEntry(histmap[sysname],"PDF","l");}
  sysname="Jets";histos.push_back(histmap[sysname]);histmap[sysname]->SetLineColor(kRed);histmap[sysname]->SetMarkerStyle(1);histmap[sysname]->SetLineWidth(3);histmap[sysname]->SetLineStyle(1);leg->AddEntry(histmap[sysname],"Jets","l");
  sysname="Top-tagging";histos.push_back(histmap[sysname]);histmap[sysname]->SetLineColor(kGreen+3);histmap[sysname]->SetMarkerStyle(20);histmap[sysname]->SetLineWidth(3);histmap[sysname]->SetLineStyle(2);leg->AddEntry(histmap[sysname],"Top-tagging","l");
  //sysname="Luminosity";histos.push_back(histmap[sysname]);histmap[sysname]->SetLineColor(kGreen+3);histmap[sysname]->SetMarkerStyle(20);histmap[sysname]->SetLineWidth(3);histmap[sysname]->SetLineStyle(2);leg->AddEntry(histmap[sysname],"Luminosity","l");
  const double height = 0.045;

  leg->SetY1( leg->GetY2() - height * leg->GetNRows() );
  cout << " LEGEND Y2: " << leg->GetY2() << endl;
  if(m_is1D) leg->SetColumnSeparation( 0.10 );


  for(unsigned int i=0;i<histos.size();i++) {
    histos[i]->GetYaxis()->SetTitle("Relative Uncertainty [%]");
    NamesAndTitles::setXtitle(histos[i],m_variable);
    //if(m_variable.Contains("ttbar_mass")) {
      //TString s=histos[i]->GetXaxis()->GetTitle();
      //s.ReplaceAll("TeV","GeV");
      //histos[i]->GetXaxis()->SetTitle(s);
    //}
  }
  TString info,info2;
  if(m_level=="PartonLevel")info="#bf{Fiducial parton level}";
  if(m_level=="ParticleLevel")info="#bf{Fiducial particle level}";
  if(crossSection=="Absolute")info2 = "#bf{Absolute cross-section}";
  if(crossSection=="Relative")info2 = "#bf{Normalized cross-section}";
  
  
  TString dirname2=dirname;
  dirname2.ReplaceAll("pdf","png");
  TString dirname3=dirname;
  dirname3.ReplaceAll("pdf","root");
  gSystem->mkdir(dirname,true);
  gSystem->mkdir(dirname2,true);
  //gSystem->mkdir(dirname3,true);
  std::pair<double,double> yRange = functions::getYRange(histos,false,0.0,0.5);
  
  if(m_is1D) {
    //functions::plotHistograms(histos,leg.get(),dirname,figurename + ".pdf" ,m_lumi_string,use_logscale,info,info2);
    //functions::plotHistograms(histos,leg.get(),dirname2,figurename + ".png" ,m_lumi_string,use_logscale,info,info2);
    functions::plotHistogramsWithFixedYRange(histos,leg.get(),dirname,figurename + ".pdf" ,m_lumi_string,yRange.first,yRange.second,use_logscale,info,info2);
    functions::plotHistogramsWithFixedYRange(histos,leg.get(),dirname2,figurename + ".png" ,m_lumi_string,yRange.first,yRange.second,use_logscale,info,info2);
  }
  else {
    plotNDhistograms(histos, leg.get(), dirname, figurename,info,info2);
  }
  
  
  
  // Make plots with signal modelling only
  if(m_doSignalModeling){
    
    leg = std::make_unique<TLegend>(legX1,legY1,legX2,legY2);
    leg->SetNColumns(2);
    leg->SetFillColor(0);
    if(m_is1D) leg->SetFillStyle(0);
    leg->SetBorderSize(0);
    leg->SetTextFont(42);
    leg->SetTextSize(0.039);
    
    histos.clear();
    sysname="Signal modelling";histmap[sysname]->SetLineColor(kGray+3);histos.push_back(histmap[sysname]);histmap[sysname]->SetFillColor(kBlue-10);histmap[sysname]->SetMarkerStyle(1);histmap[sysname]->SetLineWidth(3);histmap[sysname]->SetFillStyle(1001);histmap[sysname]->SetLineStyle(1);leg->AddEntry(histmap[sysname],"Total modelling","f");
    TH1D* h=(TH1D*)histmap[sysname]->Clone();h->Scale(-1);histos.push_back(h);
    sysname=m_ttbarConfig->hardScatteringDirectory();histos.push_back(histmap[sysname]);histmap[sysname]->SetLineColor(kOrange-4);histmap[sysname]->SetMarkerStyle(20);histmap[sysname]->SetLineWidth(3);histmap[sysname]->SetLineStyle(1);leg->AddEntry(histmap[sysname],"Hard scattering","l");
    sysname=m_ttbarConfig->partonShoweringDirectory();histos.push_back(histmap[sysname]);histmap[sysname]->SetLineColor(kGreen+3);histmap[sysname]->SetMarkerStyle(20);histmap[sysname]->SetLineWidth(3);histmap[sysname]->SetLineStyle(1);leg->AddEntry(histmap[sysname],"Parton shower","l");
    sysname="ISR";histos.push_back(histmap[sysname]);histmap[sysname]->SetLineColor(kPink+10);histmap[sysname]->SetMarkerStyle(20);histmap[sysname]->SetLineWidth(3);histmap[sysname]->SetLineStyle(1);leg->AddEntry(histmap[sysname],"ISR","l");
    //sysname="ISR_up";histos.push_back(histmap[sysname]);histmap[sysname]->SetLineColor(kGray+2);histmap[sysname]->SetMarkerStyle(20);histmap[sysname]->SetLineWidth(3);histmap[sysname]->SetLineStyle(2);leg->AddEntry(histmap[sysname],"ISR up","l");
    //sysname="ISR_down";histos.push_back(histmap[sysname]);histmap[sysname]->SetLineColor(kGray+2);histmap[sysname]->SetMarkerStyle(20);histmap[sysname]->SetLineWidth(3);histmap[sysname]->SetLineStyle(4);leg->AddEntry(histmap[sysname],"ISR down","l");
    sysname="FSR";histos.push_back(histmap[sysname]);histmap[sysname]->SetLineColor(kGreen);histmap[sysname]->SetMarkerStyle(20);histmap[sysname]->SetLineWidth(3);histmap[sysname]->SetLineStyle(1);leg->AddEntry(histmap[sysname],"#alpha_{s} FSR","l");
    sysname="hdamp";histos.push_back(histmap[sysname]);histmap[sysname]->SetLineColor(kGray+2);histmap[sysname]->SetMarkerStyle(20);histmap[sysname]->SetLineWidth(3);histmap[sysname]->SetLineStyle(1);leg->AddEntry(histmap[sysname],"hdamp","l");
    sysname="PDF";histos.push_back(histmap[sysname]);histmap[sysname]->SetLineColor(kBlue);histmap[sysname]->SetMarkerStyle(20);histmap[sysname]->SetLineWidth(3);histmap[sysname]->SetLineStyle(1);leg->AddEntry(histmap[sysname],"PDF","l");
    
    std::pair<double,double> yRange = functions::getYRange(histos,false,0.1,0.7);
    
    //const double height = 0.05;
    leg->SetY1( leg->GetY1() - height*leg->GetNRows() );
    if(m_is1D) leg->SetColumnSeparation( 0.10 );
    
    if(m_is1D) {
      functions::plotHistogramsWithFixedYRange(histos,leg.get(),dirname,figurename + "_SIGNALMOD.pdf" ,m_lumi_string,yRange.first,yRange.second,use_logscale,info,info2);
      functions::plotHistogramsWithFixedYRange(histos,leg.get(),dirname2,figurename + "_SIGNALMOD.png" ,m_lumi_string,yRange.first,yRange.second,use_logscale,info,info2);
    }
    else {
      plotNDhistograms(histos, leg.get(), dirname, figurename+"_SIGNALMOD",info,info2);
    }
    delete h;
    
    
    
    leg = std::make_unique<TLegend>(legX1,legY1,legX2,legY2);
    leg->SetNColumns(2);
    leg->SetFillColor(0);
    if(m_is1D) leg->SetFillStyle(0);
    leg->SetBorderSize(0);
    leg->SetTextFont(42);
    leg->SetTextSize(0.039);
    
    histos.clear();
   
    if(m_useNewISR) {
      sysname=m_ttbarConfig->ISRmuRupDirectory();histos.push_back(histmap[sysname]);histmap[sysname]->SetLineColor(kRed);histmap[sysname]->SetMarkerStyle(20);histmap[sysname]->SetLineWidth(3);histmap[sysname]->SetLineStyle(2);leg->AddEntry(histmap[sysname],"ISR 0.5#mu_{R}","l");
      sysname=m_ttbarConfig->ISRmuRdownDirectory();histos.push_back(histmap[sysname]);histmap[sysname]->SetLineColor(kRed+2);histmap[sysname]->SetMarkerStyle(20);histmap[sysname]->SetLineWidth(3);histmap[sysname]->SetLineStyle(2);leg->AddEntry(histmap[sysname],"ISR 2#mu_{R}","l");

      sysname=m_ttbarConfig->ISRmuFupDirectory();histos.push_back(histmap[sysname]);histmap[sysname]->SetLineColor(kBlue);histmap[sysname]->SetMarkerStyle(20);histmap[sysname]->SetLineWidth(3);histmap[sysname]->SetLineStyle(2);leg->AddEntry(histmap[sysname],"ISR 0.5#mu_{F}","l");
      sysname=m_ttbarConfig->ISRmuFdownDirectory();histos.push_back(histmap[sysname]);histmap[sysname]->SetLineColor(kBlue+2);histmap[sysname]->SetMarkerStyle(20);histmap[sysname]->SetLineWidth(3);histmap[sysname]->SetLineStyle(2);leg->AddEntry(histmap[sysname],"ISR 2#mu_{F}","l");

      sysname=m_ttbarConfig->ISRVar3cUpDirectory();histos.push_back(histmap[sysname]);histmap[sysname]->SetLineColor(kGreen);histmap[sysname]->SetMarkerStyle(20);histmap[sysname]->SetLineWidth(3);histmap[sysname]->SetLineStyle(2);leg->AddEntry(histmap[sysname],"ISR var3cUp","l");
      sysname=m_ttbarConfig->ISRVar3cDownDirectory();histos.push_back(histmap[sysname]);histmap[sysname]->SetLineColor(kGreen+2);histmap[sysname]->SetMarkerStyle(20);histmap[sysname]->SetLineWidth(3);histmap[sysname]->SetLineStyle(2);leg->AddEntry(histmap[sysname],"ISR var3cDown","l");

    }
    else {
      sysname=m_ttbarConfig->ISRupDirectory();histos.push_back(histmap[sysname]);histmap[sysname]->SetLineColor(kRed);histmap[sysname]->SetMarkerStyle(20);histmap[sysname]->SetLineWidth(3);histmap[sysname]->SetLineStyle(2);leg->AddEntry(histmap[sysname],"More ISR","l");
      sysname=m_ttbarConfig->ISRdownDirectory();histos.push_back(histmap[sysname]);histmap[sysname]->SetLineColor(kRed+2);histmap[sysname]->SetMarkerStyle(20);histmap[sysname]->SetLineWidth(3);histmap[sysname]->SetLineStyle(2);leg->AddEntry(histmap[sysname],"Less ISR","l");
    }
    sysname=m_ttbarConfig->FSRupDirectory();histos.push_back(histmap[sysname]);histmap[sysname]->SetLineColor(kGreen);histmap[sysname]->SetMarkerStyle(20);histmap[sysname]->SetLineWidth(3);histmap[sysname]->SetLineStyle(1);leg->AddEntry(histmap[sysname],"More FSR","l");
    sysname=m_ttbarConfig->FSRdownDirectory();histos.push_back(histmap[sysname]);histmap[sysname]->SetLineColor(kBlue);histmap[sysname]->SetMarkerStyle(20);histmap[sysname]->SetLineWidth(3);histmap[sysname]->SetLineStyle(1);leg->AddEntry(histmap[sysname],"Less FSR","l");

    
    yRange = functions::getYRange(histos,false,0.1,0.7);
    
    //const double height = 0.05;
    leg->SetY1( leg->GetY1() - height*leg->GetNRows() );
    if(m_is1D) leg->SetColumnSeparation( 0.10 );
    
    
    if(m_is1D){
      functions::plotHistogramsWithFixedYRange(histos,leg.get(),dirname,figurename + "_FSR.pdf" ,m_lumi_string,yRange.first,yRange.second,use_logscale,info,info2);
      functions::plotHistogramsWithFixedYRange(histos,leg.get(),dirname2,figurename + "_FSR.png" ,m_lumi_string,yRange.first,yRange.second,use_logscale,info,info2);
    }
    else{
      plotNDhistograms(histos, leg.get(), dirname, figurename+"_FSR",info,info2);
    }
    
    
    
  }
  // Make plots with large-R jets systematics only
  leg = std::make_unique<TLegend>(legX1,legY1,legX2,legY2);
  leg->SetNColumns(2);
  leg->SetFillColor(0);
  if(m_is1D) leg->SetFillStyle(0);
  leg->SetBorderSize(0);
  leg->SetTextFont(42);
  leg->SetTextSize(0.039);
  
  histos.clear();
  sysname="Jets";histmap[sysname]->SetLineColor(kGray+3);histos.push_back(histmap[sysname]);histmap[sysname]->SetFillColor(kBlue-10);histmap[sysname]->SetMarkerStyle(1);histmap[sysname]->SetLineWidth(3);histmap[sysname]->SetFillStyle(1001);histmap[sysname]->SetLineStyle(1);leg->AddEntry(histmap[sysname],"Jets total","l");
  sysname="JMS";histos.push_back(histmap[sysname]);histmap[sysname]->SetLineColor(kRed);histmap[sysname]->SetMarkerStyle(1);histmap[sysname]->SetLineWidth(3);histmap[sysname]->SetLineStyle(2);leg->AddEntry(histmap[sysname],"JMS","l");
  sysname="JMR";histos.push_back(histmap[sysname]);histmap[sysname]->SetLineColor(kGreen+2);histmap[sysname]->SetMarkerStyle(1);histmap[sysname]->SetLineWidth(3);histmap[sysname]->SetLineStyle(2);leg->AddEntry(histmap[sysname],"JMR","l");
  sysname="JES";histos.push_back(histmap[sysname]);histmap[sysname]->SetLineColor(kBlue);histmap[sysname]->SetMarkerStyle(1);histmap[sysname]->SetLineWidth(3);histmap[sysname]->SetLineStyle(2);leg->AddEntry(histmap[sysname],"JES","l");
  sysname="JER";histos.push_back(histmap[sysname]);histmap[sysname]->SetLineColor(kOrange-4);histmap[sysname]->SetMarkerStyle(1);histmap[sysname]->SetLineWidth(3);histmap[sysname]->SetLineStyle(2);leg->AddEntry(histmap[sysname],"JER","l");
  //sysname="Top-tagging";histos.push_back(histmap[sysname]);histmap[sysname]->SetLineColor(kPink+10);histmap[sysname]->SetMarkerStyle(1);histmap[sysname]->SetLineWidth(3);histmap[sysname]->SetLineStyle(1);leg->AddEntry(histmap[sysname],"Top-tagging","l");

  
  leg->SetY1( leg->GetY1() - height*leg->GetNRows() );
  if(m_is1D) leg->SetColumnSeparation( 0.10 );
  
  if(m_is1D){
    functions::plotHistograms(histos,leg.get(),dirname,figurename + "_JETS.pdf" ,m_lumi_string,use_logscale,info,info2);
    functions::plotHistograms(histos,leg.get(),dirname2,figurename + "_JETS.png" ,m_lumi_string,use_logscale,info,info2);
  }
  else{
    plotNDhistograms(histos, leg.get(), dirname, figurename+"_JETS",info,info2);
  }
 
  

  // Make plots with Flavor tagging systematics only
  leg = std::make_unique<TLegend>(legX1,legY1,legX2,legY2);
  leg->SetNColumns(2);
  leg->SetFillColor(0);
  if(m_is1D) leg->SetFillStyle(0);
  leg->SetBorderSize(0);
  leg->SetTextFont(42);
  leg->SetTextSize(0.039);
  
  histos.clear();
  sysname="Flavor tagging";histos.push_back(histmap[sysname]);histmap[sysname]->SetLineColor(kBlack);histmap[sysname]->SetFillColor(kBlue-10);histmap[sysname]->SetMarkerStyle(1);histmap[sysname]->SetLineWidth(3);histmap[sysname]->SetFillStyle(1001);histmap[sysname]->SetLineStyle(1);leg->AddEntry(histmap[sysname],"Flavor Tagging","f");
  sysname="Flavor tagging B";
  histos.push_back(histmap[sysname]);histmap[sysname]->SetLineColor(kRed);histmap[sysname]->SetMarkerStyle(1);histmap[sysname]->SetLineWidth(3);histmap[sysname]->SetLineStyle(2);leg->AddEntry(histmap[sysname],"B","l");
  sysname="Flavor tagging C";histos.push_back(histmap[sysname]);histmap[sysname]->SetLineColor(kOrange-4);histmap[sysname]->SetMarkerStyle(1);histmap[sysname]->SetLineWidth(3);histmap[sysname]->SetLineStyle(4);leg->AddEntry(histmap[sysname],"C","l");
  sysname="Flavor tagging Light";histos.push_back(histmap[sysname]);histmap[sysname]->SetLineColor(kBlue);histmap[sysname]->SetMarkerStyle(1);histmap[sysname]->SetLineWidth(3);histmap[sysname]->SetLineStyle(4);leg->AddEntry(histmap[sysname],"Light","l");
  sysname="Flavor tagging extrap";histos.push_back(histmap[sysname]);histmap[sysname]->SetLineColor(kGreen+2);histmap[sysname]->SetMarkerStyle(1);histmap[sysname]->SetLineWidth(3);histmap[sysname]->SetLineStyle(4);leg->AddEntry(histmap[sysname],"Extrap","l");
  leg->SetY1( leg->GetY1() - height*leg->GetNRows() );
  if(m_is1D) leg->SetColumnSeparation( 0.10 );

  if(m_is1D) {
    functions::plotHistograms(histos,leg.get(),dirname,figurename + "_FLAVOR_TAGGING.pdf" ,m_lumi_string,use_logscale,info,info2);
    functions::plotHistograms(histos,leg.get(),dirname2,figurename + "_FLAVOR_TAGGING.png" ,m_lumi_string,use_logscale,info,info2);
  }
  else {
    plotNDhistograms(histos, leg.get(), dirname, figurename+"_FLAVOR_TAGGING",info,info2);
  }
  
  
  // Make plots with top-tagging systematics only
  leg = std::make_unique<TLegend>(legX1,legY1,legX2,legY2);
  leg->SetNColumns(2);
  leg->SetFillColor(0);
  if(m_is1D) leg->SetFillStyle(0);
  leg->SetBorderSize(0);
  leg->SetTextFont(42);
  leg->SetTextSize(0.039);
  
  histos.clear();
  //sysname="Top-tagging";histos.push_back(histmap[sysname]);histmap[sysname]->SetLineColor(kBlack);histmap[sysname]->SetFillColor(kBlue-10);histmap[sysname]->SetMarkerStyle(1);histmap[sysname]->SetLineWidth(3);histmap[sysname]->SetFillStyle(1001);histmap[sysname]->SetLineStyle(1);leg->AddEntry(histmap[sysname],"Top Tagging","f");
  //sysname="TopTagSF Dijet Modelling";histos.push_back(histmap[sysname]);histmap[sysname]->SetLineColor(kRed);histmap[sysname]->SetMarkerStyle(1);histmap[sysname]->SetLineWidth(3);histmap[sysname]->SetLineStyle(2);leg->AddEntry(histmap[sysname],"Dijet Modelling","l");
  //sysname="TopTagSF Gammajet Modelling";histos.push_back(histmap[sysname]);histmap[sysname]->SetLineColor(kOrange-4);histmap[sysname]->SetMarkerStyle(1);histmap[sysname]->SetLineWidth(3);histmap[sysname]->SetLineStyle(4);leg->AddEntry(histmap[sysname],"Gammajet Modelling","l");
  sysname="TopTagSF Hadronisation up";histos.push_back(histmap[sysname]);histmap[sysname]->SetLineColor(kBlue);histmap[sysname]->SetMarkerStyle(1);histmap[sysname]->SetLineWidth(3);histmap[sysname]->SetLineStyle(4);leg->AddEntry(histmap[sysname],"Hadronisation 1","l");
  sysname="TopTagSF Hadronisation down";histos.push_back(histmap[sysname]);histmap[sysname]->SetLineColor(kBlue);histmap[sysname]->SetMarkerStyle(1);histmap[sysname]->SetLineWidth(3);histmap[sysname]->SetLineStyle(3);leg->AddEntry(histmap[sysname],"Hadronisation 2","l");
  sysname="TopTagSF ME up";histos.push_back(histmap[sysname]);histmap[sysname]->SetLineColor(kGreen+2);histmap[sysname]->SetMarkerStyle(1);histmap[sysname]->SetLineWidth(3);histmap[sysname]->SetLineStyle(4);leg->AddEntry(histmap[sysname],"ME 1","l");
  sysname="TopTagSF ME down";histos.push_back(histmap[sysname]);histmap[sysname]->SetLineColor(kGreen+2);histmap[sysname]->SetMarkerStyle(1);histmap[sysname]->SetLineWidth(3);histmap[sysname]->SetLineStyle(3);leg->AddEntry(histmap[sysname],"ME 2","l");
  //sysname="TopTagSF Bkg SF up";histos.push_back(histmap[sysname]);histmap[sysname]->SetLineColor(kOrange);histmap[sysname]->SetMarkerStyle(1);histmap[sysname]->SetLineWidth(3);histmap[sysname]->SetLineStyle(4);leg->AddEntry(histmap[sysname],"Bkg SF up","l");
  //sysname="TopTagSF Bkg SF down";histos.push_back(histmap[sysname]);histmap[sysname]->SetLineColor(kOrange);histmap[sysname]->SetMarkerStyle(1);histmap[sysname]->SetLineWidth(3);histmap[sysname]->SetLineStyle(3);leg->AddEntry(histmap[sysname],"Bkg SF down","l");
  sysname="TopTagSF Signal SF up";histos.push_back(histmap[sysname]);histmap[sysname]->SetLineColor(kPink+10);histmap[sysname]->SetMarkerStyle(1);histmap[sysname]->SetLineWidth(3);histmap[sysname]->SetLineStyle(4);leg->AddEntry(histmap[sysname],"Signal SF up","l");
  sysname="TopTagSF Signal SF down";histos.push_back(histmap[sysname]);histmap[sysname]->SetLineColor(kPink+10);histmap[sysname]->SetMarkerStyle(1);histmap[sysname]->SetLineWidth(3);histmap[sysname]->SetLineStyle(3);leg->AddEntry(histmap[sysname],"Signal SF down","l");
  //sysname="TopTagSF TagEffUnc";histos.push_back(histmap[sysname]);histmap[sysname]->SetLineColor(kYellow+2);histmap[sysname]->SetMarkerStyle(1);histmap[sysname]->SetLineWidth(3);histmap[sysname]->SetLineStyle(4);leg->AddEntry(histmap[sysname],"Taggging eff","l");
  //sysname="TopTagSF bTag up";histos.push_back(histmap[sysname]);histmap[sysname]->SetLineColor(kOrange-4);histmap[sysname]->SetMarkerStyle(1);histmap[sysname]->SetLineWidth(3);histmap[sysname]->SetLineStyle(4);leg->AddEntry(histmap[sysname],"B-tagging up","l");
  //sysname="TopTagSF bTag down";histos.push_back(histmap[sysname]);histmap[sysname]->SetLineColor(kOrange-4);histmap[sysname]->SetMarkerStyle(1);histmap[sysname]->SetLineWidth(3);histmap[sysname]->SetLineStyle(3);leg->AddEntry(histmap[sysname],"B-tagging down","l");
  leg->SetY1( leg->GetY1() - height*leg->GetNRows() );
  if(m_is1D) leg->SetColumnSeparation( 0.10 );

  if(m_is1D) {
    functions::plotHistograms(histos,leg.get(),dirname,figurename + "_TOP_TAGGING.pdf" ,m_lumi_string,use_logscale,info,info2);
    functions::plotHistograms(histos,leg.get(),dirname2,figurename + "_TOP_TAGGING.png" ,m_lumi_string,use_logscale,info,info2);
  }
  else {
    plotNDhistograms(histos, leg.get(), dirname, figurename+"_TOP_TAGGING",info,info2);
  }
  
  


  // Make plots with MC stat. uncertainties only
  leg = std::make_unique<TLegend>(legX1,legY1,legX2,legY2);
  leg->SetNColumns(2);
  leg->SetFillColor(0);
  if(m_is1D) leg->SetFillStyle(0);
  leg->SetBorderSize(0);
  leg->SetTextFont(42);
  leg->SetTextSize(0.039);
  
  histos.clear();
  
  sysname="Combined Statistics";
  histos.push_back(histmap[sysname]);histmap[sysname]->SetLineColor(kBlack);histmap[sysname]->SetFillColor(kBlue-10);histmap[sysname]->SetMarkerStyle(1);histmap[sysname]->SetLineWidth(3);histmap[sysname]->SetFillStyle(1001);histmap[sysname]->SetLineStyle(1);leg->AddEntry(histmap[sysname],"Combined Stat. Unc.","f");
  //sysname="MC Statistics";histos.push_back(histmap[sysname]);histmap[sysname]->SetLineColor(kMagenta);histmap[sysname]->SetMarkerStyle(20);histmap[sysname]->SetLineWidth(3);histmap[sysname]->SetLineStyle(1);leg->AddEntry(histmap[sysname],"MC Stat. Total","l");
  sysname="Multijet Bkg stat";histos.push_back(histmap[sysname]);histmap[sysname]->SetLineColor(kGreen+2);histmap[sysname]->SetMarkerStyle(20);histmap[sysname]->SetLineWidth(3);histmap[sysname]->SetLineStyle(2);leg->AddEntry(histmap[sysname],"Multijet Stat.","l");
  sysname="MC Stat. w/o multijet";histos.push_back(histmap[sysname]);histmap[sysname]->SetLineColor(kRed);histmap[sysname]->SetMarkerStyle(20);histmap[sysname]->SetLineWidth(3);histmap[sysname]->SetLineStyle(4);leg->AddEntry(histmap[sysname],"MC Stat. w/o Multijet","l");
  sysname="Statistics";histos.push_back(histmap[sysname]);histmap[sysname]->SetLineColor(kBlue);histmap[sysname]->SetFillColor(0);;histmap[sysname]->SetMarkerStyle(1);histmap[sysname]->SetLineWidth(3);histmap[sysname]->SetLineStyle(2);leg->AddEntry(histmap[sysname],"Data Stat.","l"); 

  for(unsigned int i=0;i<histos.size();i++) {
    histos[i]->GetYaxis()->SetTitle("Relative Uncertainty [%]");
    NamesAndTitles::setXtitle(histos[i],m_variable);
  }


  leg->SetY1( leg->GetY1() - height*leg->GetNRows() );
  if(m_is1D) leg->SetColumnSeparation( 0.10 );
  
  if(m_is1D) {
    functions::plotHistograms(histos,leg.get(),dirname,figurename + "_STAT.pdf" ,m_lumi_string,use_logscale,info,info2);
    functions::plotHistograms(histos,leg.get(),dirname2,figurename + "_STAT.png" ,m_lumi_string,use_logscale,info,info2);
  }
  else {
    plotNDhistograms(histos, leg.get(), dirname, figurename+"_STAT",info,info2);
  }
  
}

void AsymmetricErrorsCalculator::plotUncertainties(const TString& foldername){
  if (m_debug > 0 ) cout << "Plotting uncertainties" << endl;
  
  plotUncertainties(foldername,m_hist_uncertainties_absolute,"Absolute");
  plotUncertainties(foldername,m_hist_uncertainties_relative,"Relative");
}

//----------------------------------------------------------------------

void AsymmetricErrorsCalculator::plotNDhistograms(vector<TH1D*>& histos, TLegend* leg, const TString& dirname, const TString& figurename, const TString& info1, const TString& info2) const {
  
  const int nhistos = histos.size();
  vector<vector<TH1D*>> projections(nhistos);
  for(int i=0;i<nhistos;i++) {
    projections[i] = m_histogramConverterND->makeProjections(histos[i],Form("_%i",i));
    for(TH1D* proj : projections[i]) functions::copyHistStyle(histos[i],proj);
  }
  const int nprojections = projections[0].size();
  
  std::vector<TString> generalInfo = { NamesAndTitles::ATLASString(m_lumi_string),
				       NamesAndTitles::energyLumiString(m_lumi_string),
				       info1,
				       info2};
  const int nlinesInfo=generalInfo.size();
  
  int nPerLine(0),nlines(0);
  functions::getGridSize(nprojections,nlines,nPerLine);
  
  const double canvas_xsize = (1000./3) * nPerLine;
  const double canvas_ysize = 175.*(nlines+1);
  
  m_plotSettingsND->initialize(canvas_xsize, canvas_ysize, nlines, nPerLine, nlinesInfo);
  
  auto can = std::unique_ptr<TCanvas>(m_plotSettingsND->makeCanvas("can"));
  can->cd();
  vector<vector<shared_ptr<TPad>>> pads = m_plotSettingsND->makeArrayOfPads(nprojections);
  
  std::vector<TString> xtitles = NamesAndTitles::getXtitles(m_variable);
  
  int iproj=0;
  for(int iline=0;iline<nlines;iline++) {
    int nInLine = pads[iline].size();
    
    for(int iInLine=0;iInLine<nInLine;iInLine++) {
      TPad* currentPad = pads[iline][iInLine].get();
      
      vector<TH1D*> histos_proj(nhistos);
      for(int i=0;i<nhistos;i++) {
	histos_proj[i] = projections[i][iproj];
	m_plotSettingsND->setHistLabelStyle(histos_proj[i]);
      }
      
      if(iInLine ==0 && iline ==0) { 
	histos_proj[0]->GetYaxis()->SetTitle(histos[0]->GetYaxis()->GetTitle());
      }
      else {
	histos_proj[0]->GetYaxis()->SetTitle("");
      }
      if(iline != nlines-1) { 
	histos_proj[0]->GetXaxis()->SetTitle("");
      }
      else {
	histos_proj[0]->GetXaxis()->SetTitle(xtitles.back());
      }
      
      currentPad->cd();
      
      std::vector<TString> externalBinInfo = m_histogramConverterND->getExternalBinsInfo(iproj);
      TPaveText* paveText = functions::makePaveText(externalBinInfo, m_plotSettingsND->textSizePad(), 0.35, 0.75, 0.93); // xmin,xmax,ymax
      paveText->SetFillStyle(0);
      
      double max = functions::getMaximum(histos_proj);
      double min = functions::getMinimum(histos_proj);
      if(min>0) min = 0.;
      if(min<0) min= min - (max-min)*0.1;
      if(externalBinInfo.size()==1) max = max + (max - min)*0.4;
      else max = max + (max - min)*0.8;
      
      
      
      
      //if(min<0) max*=3;
      //else max*=2;
      histos_proj[0]->GetYaxis()->SetRangeUser(min,max);
      
      
      histos_proj[0]->Draw();
      for(int i=1;i<nhistos;i++) {
	histos_proj[i]->Draw("same");
      }
      
      paveText->Draw();
      
      currentPad->RedrawAxis("g"); 
      currentPad->RedrawAxis();
      
      iproj++;
    }
  }
  
  can->cd();
  TPaveText* paveTextGeneralInfo = m_plotSettingsND->makePaveText(generalInfo);
  paveTextGeneralInfo->Draw();
  leg->Draw();
  
  can->Update();
  
  int ncolumns=0;
  if(nhistos<5) ncolumns=1;
  else if(nhistos<9) ncolumns=2;
  else ncolumns=3;
  
  leg->SetNColumns(ncolumns);
  
  double xmin(0.3),xmax(0.98);
  if(ncolumns==1) xmin=0.65;
  else if (ncolumns==2) xmin=0.5;
  
  m_plotSettingsND->setLegendAttributes(leg,xmin,xmax);
  
  
  //can->Modified();
  
  TString dirname_png=dirname;
  dirname_png.ReplaceAll("pdf.","png.");
  
  can->SaveAs(dirname+"/"+figurename+".pdf");
  can->SaveAs(dirname_png+"/"+figurename+".png");
  
  for(int i=0;i<nhistos;i++) for(TH1D* proj : projections[i]) {
    delete proj;
  }
  
}

