#include "TTbarCovarianceCalculator/CovarianceCalculator.h"
#include "TTbarHelperFunctions/functions.h"
#include "TTbarHelperFunctions/histManipFunctions.h"
#include "TTbarCovarianceCalculator/CovarianceCalculatorFunctions.h"
ClassImp(CovarianceCalculator)

CovarianceCalculator::CovarianceCalculator(Pseudoexperiment* pseudoexperiment,const TString& level,const int& debugLevel) :m_debug(debugLevel), m_gap("                      ")
{
  if (m_debug>0 ) cout << "CovarianceCalculator: Running constructor." << endl;
  
  m_nbins_reco=pseudoexperiment->m_dataOnlyStat->GetNbinsX();
  m_nbins_truth=pseudoexperiment->m_signalTruthOnlyStat->GetNbinsX();
  std::cout << "CovarianceCalculator: nbins_truth: " << m_nbins_truth << " nbins_reco: " << m_nbins_reco << std::endl;
  m_isConcatenated=false;
  m_signalModelingLoaded=false;
  
  m_rooUnfoldNominal = new MyUnfold();
  m_rooUnfoldStat = new MyUnfold();
  m_rooUnfoldStatAndSys = new MyUnfold();
  
  setupUnfolder(m_rooUnfoldNominal);
  setupUnfolder(m_rooUnfoldStat);
  setupUnfolder(m_rooUnfoldStatAndSys);
  
  
  m_histUnfoldedDataBasedOnlyDataStat 	       = (TH1D*)pseudoexperiment->m_signalTruthOnlyStat->Clone();
  m_histUnfoldedDataBasedOnlyMCStat 	       = (TH1D*)pseudoexperiment->m_signalTruthOnlyStat->Clone();
  m_histUnfoldedDataBasedOnlyMCStatReco 	       = (TH1D*)pseudoexperiment->m_signalTruthOnlyStat->Clone();
  m_histUnfoldedDataBasedOnlyStat 	       = (TH1D*)pseudoexperiment->m_signalTruthOnlyStat->Clone();
  m_histUnfoldedDataBasedOnlySys 	       = (TH1D*)pseudoexperiment->m_signalTruthOnlyStat->Clone();
  m_histUnfoldedDataBasedOnlySigMod 	       = (TH1D*)pseudoexperiment->m_signalTruthOnlyStat->Clone();
  m_histUnfoldedDataBasedStatAndSys 	       = (TH1D*)pseudoexperiment->m_signalTruthOnlyStat->Clone();
  m_histUnfoldedDataBasedSysAndSigMod 	       = (TH1D*)pseudoexperiment->m_signalTruthOnlyStat->Clone();
  m_histUnfoldedDataBasedStatMCStatRecoAndSys  = (TH1D*)pseudoexperiment->m_signalTruthOnlyStat->Clone();
  m_histUnfoldedDataBasedStatAndSysAlternative = (TH1D*)pseudoexperiment->m_signalTruthOnlyStat->Clone();
  m_histUnfoldedDataBasedStatSysAndSigMod      = (TH1D*)pseudoexperiment->m_signalTruthOnlyStat->Clone();
  m_histUnfoldedDataBasedStatMCStatRecoSysAndSigMod  = (TH1D*)pseudoexperiment->m_signalTruthOnlyStat->Clone();
  m_histUnfoldedDataBasedStatSysAndSigModAlternative = (TH1D*)pseudoexperiment->m_signalTruthOnlyStat->Clone();
  m_histUnfoldedMCBasedOnlyDataStat 	       = (TH1D*)pseudoexperiment->m_signalTruthOnlyStat->Clone();
  m_histUnfoldedMCBasedOnlyMCStat 	       = (TH1D*)pseudoexperiment->m_signalTruthOnlyStat->Clone();
  m_histUnfoldedMCBasedOnlyMCStatReco 	       = (TH1D*)pseudoexperiment->m_signalTruthOnlyStat->Clone();
  m_histUnfoldedMCBasedOnlyStat 	       = (TH1D*)pseudoexperiment->m_signalTruthOnlyStat->Clone();
  m_histUnfoldedMCBasedOnlySys 		       = (TH1D*)pseudoexperiment->m_signalTruthOnlyStat->Clone();
  m_histUnfoldedMCBasedStatAndSys 	       = (TH1D*)pseudoexperiment->m_signalTruthOnlyStat->Clone();
  m_histUnfoldedMCBasedSysAndSigMod 	       = (TH1D*)pseudoexperiment->m_signalTruthOnlyStat->Clone();
  m_histUnfoldedMCBasedStatAndSysAlternative   = (TH1D*)pseudoexperiment->m_signalTruthOnlyStat->Clone();
  m_histUnfoldedMCBasedStatSysAndSigMod	       = (TH1D*)pseudoexperiment->m_signalTruthOnlyStat->Clone();
  m_histUnfoldedMCBasedStatSysAndSigModAlternative   = (TH1D*)pseudoexperiment->m_signalTruthOnlyStat->Clone();
  
  m_histUnfoldedDataBasedOnlyDataStatNormalized 	       = (TH1D*)pseudoexperiment->m_signalTruthOnlyStat->Clone();
  m_histUnfoldedDataBasedOnlyMCStatNormalized 	       = (TH1D*)pseudoexperiment->m_signalTruthOnlyStat->Clone();
  m_histUnfoldedDataBasedOnlyMCStatRecoNormalized 	       = (TH1D*)pseudoexperiment->m_signalTruthOnlyStat->Clone();
  m_histUnfoldedDataBasedOnlyStatNormalized 	       = (TH1D*)pseudoexperiment->m_signalTruthOnlyStat->Clone();
  m_histUnfoldedDataBasedOnlySysNormalized 	       = (TH1D*)pseudoexperiment->m_signalTruthOnlyStat->Clone();
  m_histUnfoldedDataBasedOnlySigModNormalized 	       = (TH1D*)pseudoexperiment->m_signalTruthOnlyStat->Clone();
  m_histUnfoldedDataBasedStatAndSysNormalized 	       = (TH1D*)pseudoexperiment->m_signalTruthOnlyStat->Clone();
  m_histUnfoldedDataBasedSysAndSigModNormalized 	       = (TH1D*)pseudoexperiment->m_signalTruthOnlyStat->Clone();
  m_histUnfoldedDataBasedStatMCStatRecoAndSysNormalized = (TH1D*)pseudoexperiment->m_signalTruthOnlyStat->Clone();
  m_histUnfoldedDataBasedStatAndSysAlternativeNormalized = (TH1D*)pseudoexperiment->m_signalTruthOnlyStat->Clone();
  m_histUnfoldedDataBasedStatSysAndSigModNormalized 	       = (TH1D*)pseudoexperiment->m_signalTruthOnlyStat->Clone();
  m_histUnfoldedDataBasedStatMCStatRecoSysAndSigModNormalized = (TH1D*)pseudoexperiment->m_signalTruthOnlyStat->Clone();
  m_histUnfoldedDataBasedStatSysAndSigModAlternativeNormalized = (TH1D*)pseudoexperiment->m_signalTruthOnlyStat->Clone();
  m_histUnfoldedMCBasedOnlyDataStatNormalized 	       = (TH1D*)pseudoexperiment->m_signalTruthOnlyStat->Clone();
  m_histUnfoldedMCBasedOnlyMCStatNormalized 	       = (TH1D*)pseudoexperiment->m_signalTruthOnlyStat->Clone();
  m_histUnfoldedMCBasedOnlyMCStatRecoNormalized 	       = (TH1D*)pseudoexperiment->m_signalTruthOnlyStat->Clone();
  m_histUnfoldedMCBasedOnlyStatNormalized 	       = (TH1D*)pseudoexperiment->m_signalTruthOnlyStat->Clone();
  m_histUnfoldedMCBasedOnlySysNormalized 	       = (TH1D*)pseudoexperiment->m_signalTruthOnlyStat->Clone();
  m_histUnfoldedMCBasedStatAndSysNormalized 	       = (TH1D*)pseudoexperiment->m_signalTruthOnlyStat->Clone();
  m_histUnfoldedMCBasedSysAndSigModNormalized 	       = (TH1D*)pseudoexperiment->m_signalTruthOnlyStat->Clone();
  m_histUnfoldedMCBasedStatAndSysAlternativeNormalized = (TH1D*)pseudoexperiment->m_signalTruthOnlyStat->Clone();
  m_histUnfoldedMCBasedStatSysAndSigModNormalized 	       = (TH1D*)pseudoexperiment->m_signalTruthOnlyStat->Clone();
  m_histUnfoldedMCBasedStatSysAndSigModAlternativeNormalized = (TH1D*)pseudoexperiment->m_signalTruthOnlyStat->Clone();
  
  m_histUnfoldedDataBasedOnlyMultijetBkgStat = (TH1D*)pseudoexperiment->m_signalTruthOnlyStat->Clone();
  m_histUnfoldedDataBasedOnlyMultijetBkgStatNormalized = (TH1D*)pseudoexperiment->m_signalTruthOnlyStat->Clone();
  
  m_histUnfoldedDataBasedOnlyMCStatWithMultijetFixed =(TH1D*)pseudoexperiment->m_signalTruthOnlyStat->Clone();
  m_histUnfoldedDataBasedOnlyMCStatWithMultijetFixedNormalized = (TH1D*)pseudoexperiment->m_signalTruthOnlyStat->Clone();
  
  m_effOfRecoLevelOnlyStat                               = (TH1D*)pseudoexperiment->m_signalTruthOnlyStat->Clone();
  m_effOfRecoLevelOnlySys                                = (TH1D*)pseudoexperiment->m_signalTruthOnlyStat->Clone();
  m_effOfRecoLevelStatAndSys                             = (TH1D*)pseudoexperiment->m_signalTruthOnlyStat->Clone();
  m_effOfTruthLevelOnlyStat                               = (TH1D*)pseudoexperiment->m_dataOnlyStat->Clone();
  m_effOfTruthLevelOnlySys                                = (TH1D*)pseudoexperiment->m_dataOnlyStat->Clone();
  m_effOfTruthLevelStatAndSys                             = (TH1D*)pseudoexperiment->m_dataOnlyStat->Clone();
  
  
  m_recoDataBasedOnlyDataStat          = (TH1D*)pseudoexperiment->m_dataOnlyStat->Clone();
  m_recoDataBasedOnlyMCStat            = (TH1D*)pseudoexperiment->m_dataOnlyStat->Clone();
  m_recoDataBasedOnlyMCStatReco            = (TH1D*)pseudoexperiment->m_dataOnlyStat->Clone();
  m_recoDataBasedOnlyStat              = (TH1D*)pseudoexperiment->m_dataOnlyStat->Clone();
  m_recoDataBasedOnlySys               = (TH1D*)pseudoexperiment->m_dataOnlyStat->Clone();
  m_recoDataBasedStatAndSys            = (TH1D*)pseudoexperiment->m_dataOnlyStat->Clone();
  m_recoDataBasedStatMCStatAndSys      = (TH1D*)pseudoexperiment->m_dataOnlyStat->Clone();
  m_recoDataBasedStatAndSysAlternative = (TH1D*)pseudoexperiment->m_dataOnlyStat->Clone();
  m_recoDataBasedOnlyMultijetBkgStat   = (TH1D*)pseudoexperiment->m_dataOnlyStat->Clone();
  m_recoDataBasedOnlyMCStatWithMultijetFixed = (TH1D*)pseudoexperiment->m_dataOnlyStat->Clone();
  m_recoMCBasedOnlyDataStat            = (TH1D*)pseudoexperiment->m_dataOnlyStat->Clone();
  m_recoMCBasedOnlyMCStat              = (TH1D*)pseudoexperiment->m_dataOnlyStat->Clone();
  m_recoMCBasedOnlyMCStatReco              = (TH1D*)pseudoexperiment->m_dataOnlyStat->Clone();
  m_recoMCBasedOnlyStat                = (TH1D*)pseudoexperiment->m_dataOnlyStat->Clone();
  m_recoMCBasedOnlySys                 = (TH1D*)pseudoexperiment->m_dataOnlyStat->Clone();
  m_recoMCBasedStatAndSys              = (TH1D*)pseudoexperiment->m_dataOnlyStat->Clone();
  m_recoMCBasedStatAndSysAlternative   = (TH1D*)pseudoexperiment->m_dataOnlyStat->Clone();
  
  
  m_covFromToys["DataBasedOnlyDataStat"] = std::make_shared<CovarianceFromToys>(m_nbins_truth);
  m_covFromToys["DataBasedOnlyMCStat"] = std::make_shared<CovarianceFromToys>(m_nbins_truth);
  m_covFromToys["DataBasedOnlyMCStatReco"] = std::make_shared<CovarianceFromToys>(m_nbins_truth);
  m_covFromToys["DataBasedOnlyStat"] = std::make_shared<CovarianceFromToys>(m_nbins_truth);
  m_covFromToys["DataBasedOnlySys"] = std::make_shared<CovarianceFromToys>(m_nbins_truth);
  m_covFromToys["DataBasedOnlySigMod"] = std::make_shared<CovarianceFromToys>(m_nbins_truth);
  m_covFromToys["DataBasedStatAndSys"] = std::make_shared<CovarianceFromToys>(m_nbins_truth);
  m_covFromToys["DataBasedSysAndSigMod"] = std::make_shared<CovarianceFromToys>(m_nbins_truth);
  m_covFromToys["DataBasedStatMCStatRecoAndSys"] = std::make_shared<CovarianceFromToys>(m_nbins_truth);
  m_covFromToys["DataBasedStatAndSysAlternative"] = std::make_shared<CovarianceFromToys>(m_nbins_truth);
  m_covFromToys["DataBasedStatSysAndSigMod"] = std::make_shared<CovarianceFromToys>(m_nbins_truth);
  m_covFromToys["DataBasedStatMCStatRecoSysAndSigMod"] = std::make_shared<CovarianceFromToys>(m_nbins_truth);
  m_covFromToys["DataBasedStatSysAndSigModAlternative"] = std::make_shared<CovarianceFromToys>(m_nbins_truth);
  
  m_covFromToys["MCBasedOnlyDataStat"] = std::make_shared<CovarianceFromToys>(m_nbins_truth);
  m_covFromToys["MCBasedOnlyMCStat"] = std::make_shared<CovarianceFromToys>(m_nbins_truth);
  m_covFromToys["MCBasedOnlyMCStatReco"] = std::make_shared<CovarianceFromToys>(m_nbins_truth);
  m_covFromToys["MCBasedOnlyStat"] = std::make_shared<CovarianceFromToys>(m_nbins_truth);
  m_covFromToys["MCBasedOnlySys"] = std::make_shared<CovarianceFromToys>(m_nbins_truth);
  m_covFromToys["MCBasedStatAndSys"] = std::make_shared<CovarianceFromToys>(m_nbins_truth);
  m_covFromToys["MCBasedSysAndSigMod"] = std::make_shared<CovarianceFromToys>(m_nbins_truth);
  m_covFromToys["MCBasedStatAndSysAlternative"] = std::make_shared<CovarianceFromToys>(m_nbins_truth);
  m_covFromToys["MCBasedStatSysAndSigMod"] = std::make_shared<CovarianceFromToys>(m_nbins_truth);
  m_covFromToys["MCBasedStatSysAndSigModAlternative"] = std::make_shared<CovarianceFromToys>(m_nbins_truth);
  
  m_covFromToys["DataBasedOnlyDataStatNormalized"] = std::make_shared<CovarianceFromToys>(m_nbins_truth);
  m_covFromToys["DataBasedOnlyMCStatNormalized"] = std::make_shared<CovarianceFromToys>(m_nbins_truth);
  m_covFromToys["DataBasedOnlyMCStatRecoNormalized"] = std::make_shared<CovarianceFromToys>(m_nbins_truth);
  m_covFromToys["DataBasedOnlyStatNormalized"] = std::make_shared<CovarianceFromToys>(m_nbins_truth);
  m_covFromToys["DataBasedOnlySysNormalized"] = std::make_shared<CovarianceFromToys>(m_nbins_truth);
  m_covFromToys["DataBasedOnlySigModNormalized"] = std::make_shared<CovarianceFromToys>(m_nbins_truth);
  m_covFromToys["DataBasedStatAndSysNormalized"] = std::make_shared<CovarianceFromToys>(m_nbins_truth);
  m_covFromToys["DataBasedSysAndSigModNormalized"] = std::make_shared<CovarianceFromToys>(m_nbins_truth);
  m_covFromToys["DataBasedStatMCStatRecoAndSysNormalized"] = std::make_shared<CovarianceFromToys>(m_nbins_truth);
  m_covFromToys["DataBasedStatAndSysAlternativeNormalized"] = std::make_shared<CovarianceFromToys>(m_nbins_truth);
  m_covFromToys["DataBasedStatSysAndSigModNormalized"] = std::make_shared<CovarianceFromToys>(m_nbins_truth);
  m_covFromToys["DataBasedStatMCStatRecoSysAndSigModNormalized"] = std::make_shared<CovarianceFromToys>(m_nbins_truth);
  m_covFromToys["DataBasedStatSysAndSigModAlternativeNormalized"] = std::make_shared<CovarianceFromToys>(m_nbins_truth);
  
  m_covFromToys["MCBasedOnlyDataStatNormalized"] = std::make_shared<CovarianceFromToys>(m_nbins_truth);
  m_covFromToys["MCBasedOnlyMCStatNormalized"] = std::make_shared<CovarianceFromToys>(m_nbins_truth);
  m_covFromToys["MCBasedOnlyMCStatRecoNormalized"] = std::make_shared<CovarianceFromToys>(m_nbins_truth);
  m_covFromToys["MCBasedOnlyStatNormalized"] = std::make_shared<CovarianceFromToys>(m_nbins_truth);
  m_covFromToys["MCBasedOnlySysNormalized"] = std::make_shared<CovarianceFromToys>(m_nbins_truth);
  m_covFromToys["MCBasedStatAndSysNormalized"] = std::make_shared<CovarianceFromToys>(m_nbins_truth);
  m_covFromToys["MCBasedSysAndSigModNormalized"] = std::make_shared<CovarianceFromToys>(m_nbins_truth);
  m_covFromToys["MCBasedStatAndSysAlternativeNormalized"] = std::make_shared<CovarianceFromToys>(m_nbins_truth);
  m_covFromToys["MCBasedStatSysAndSigModNormalized"] = std::make_shared<CovarianceFromToys>(m_nbins_truth);
  m_covFromToys["MCBasedStatSysAndSigModAlternativeNormalized"] = std::make_shared<CovarianceFromToys>(m_nbins_truth);
  
  m_covFromToys["DataBasedOnlyMultijetBkgStat"] = std::make_shared<CovarianceFromToys>(m_nbins_truth);
  m_covFromToys["DataBasedOnlyMultijetBkgStatNormalized"] = std::make_shared<CovarianceFromToys>(m_nbins_truth);
  m_covFromToys["DataBasedOnlyMCStatWithMultijetFixed"] = std::make_shared<CovarianceFromToys>(m_nbins_truth);
  m_covFromToys["DataBasedOnlyMCStatWithMultijetFixedNormalized"] = std::make_shared<CovarianceFromToys>(m_nbins_truth);
  
  m_covFromToys["EffOfRecoLevelOnlyStat"] = std::make_shared<CovarianceFromToys>(m_nbins_truth);
  m_covFromToys["EffOfRecoLevelOnlySys"] = std::make_shared<CovarianceFromToys>(m_nbins_truth);
  m_covFromToys["EffOfRecoLevelStatAndSys"] = std::make_shared<CovarianceFromToys>(m_nbins_truth);
  
  m_covFromToys["EffOfTruthLevelOnlyStat"] = std::make_shared<CovarianceFromToys>(m_nbins_reco);
  m_covFromToys["EffOfTruthLevelOnlySys"] = std::make_shared<CovarianceFromToys>(m_nbins_reco);
  m_covFromToys["EffOfTruthLevelStatAndSys"] = std::make_shared<CovarianceFromToys>(m_nbins_reco);
  
  m_covFromToys["MultijetBkgOnlyStat"] = std::make_shared<CovarianceFromToys>(m_nbins_reco);
  m_covFromToys["MultijetBkgOnlySys"] = std::make_shared<CovarianceFromToys>(m_nbins_reco);
  m_covFromToys["MultijetBkgStatAndSys"] = std::make_shared<CovarianceFromToys>(m_nbins_reco);
  
  //if(level.Contains("Parton")) m_SFToTtbarCrossSection=2.1882987;
  if(level.Contains("Parton")) m_SFToTtbarCrossSection=1.;
  else m_SFToTtbarCrossSection=1.;
  m_level="PartonLevel";
  if (m_debug>0 ) cout << "CovarianceCalculator: nbins of distribution " << m_nbins_truth << endl;
}
void CovarianceCalculator::setupUnfolder(MyUnfold* rooUnfold){
  rooUnfold->SetRegParm( 4 );
  rooUnfold->SetNToys( 1000);			
  rooUnfold->SetVerbose( 0 );
  rooUnfold->SetSmoothing( 0 );
   
}

void CovarianceCalculator::setNominalResponse(Pseudoexperiment* nominal){
  //m_nominalResponse = new RooUnfoldResponse(0,0,functions::ProduceHistogramWithSwitchedAxis(nominal->m_migrationOnlyStat),"","");
  m_nominalResponse = new RooUnfoldResponse(0,0,nominal->m_migrationOnlyStat,"","");
  m_nominalResponse->UseOverflow(false);
  m_responseOnlyStat = new RooUnfoldResponse(0,0,nominal->m_migrationOnlyStat,"","");
  m_responseOnlyStat->UseOverflow(false);
  m_responseStatAndSys = new RooUnfoldResponse(0,0,nominal->m_migrationOnlyStat,"","");
  m_responseStatAndSys->UseOverflow(false);
  m_rooUnfoldNominal->SetResponse(m_nominalResponse);
  m_rooUnfoldStat->SetResponse(m_responseOnlyStat);
  m_rooUnfoldStatAndSys->SetResponse(m_responseStatAndSys);
}
void CovarianceCalculator::calculateCentralValues(Pseudoexperiment* nominal){
  if (m_debug>0 ) cout << "CovarianceCalculator: Preparing central values." << endl;
  TH1D* recoOnlyStat = (TH1D*)nominal->m_dataOnlyStat->Clone();
  recoOnlyStat->Add(nominal->m_bkgAllOnlyStat,-1);
  TH1D* hist_unfolded = (TH1D*)nominal->m_signalTruthOnlyStat->Clone();
  //TH1D* hist_unfolded = functions::UnfoldBayes(recoOnlyStat,nominal->m_signalRecoOnlyStat,nominal->m_effOfTruthLevelCutsNominatorOnlyStat,
			//nominal->m_signalTruthOnlyStat,nominal->m_effOfRecoLevelCutsNominatorOnlyStat,m_nominalResponse,4,0);
  functions::UnfoldBayes(hist_unfolded,recoOnlyStat,nominal->m_signalRecoOnlyStat,nominal->m_effOfTruthLevelCutsNominatorOnlyStat,
			nominal->m_signalTruthOnlyStat,nominal->m_effOfRecoLevelCutsNominatorOnlyStat,m_rooUnfoldNominal);
  hist_unfolded->Scale(1./m_lumi);
  hist_unfolded->Scale(m_SFToTtbarCrossSection);
  const double integral=hist_unfolded->Integral();
  functions::DivideByBinWidth(hist_unfolded);
  m_centralValues = (TH1D*)hist_unfolded->Clone();
  m_centralValuesNormalized= (TH1D*)hist_unfolded->Clone();
  m_centralValuesNormalized->Scale(1./integral);
  if (m_debug>0 ) {
    cout <<  m_gap << "Absolute: Central values:" << endl;
    for(int ibin=1;ibin<=m_nbins_truth;ibin++){
      cout << m_gap << hist_unfolded->GetBinContent(ibin)/m_SFToTtbarCrossSection << endl ;	
    }
    cout <<  m_gap << "Relative: Central values:" << endl;
    for(int ibin=1;ibin<=m_nbins_truth;ibin++){
      cout << m_gap << m_centralValuesNormalized->GetBinContent(ibin) << endl ;	
    }
    
    cout << m_gap << "Done." << endl;
  }
  
  delete recoOnlyStat;
}

void CovarianceCalculator::addPseudoexperiment(Pseudoexperiment* pseudoexperiment,Pseudoexperiment* nominal){
  
  m_pseudoexperiment=pseudoexperiment;
  m_nominal=nominal;
  
  this->loadRecoLevelPseudoexperiment(); // Load smeared reco level histograms
  
  this->prepareUnfolding(); // Load smeared responses
 
  this->unfoldPseudoexperiment(); // Unfold all reco level pseudoexperiments

  this->applyPostUnfoldingShifts(); // Applying post-unfolding shifts for signal modeling uncertainties																									    
  
  
  
  m_lumi_shifted=m_pseudoexperiment->m_histLumi->GetBinContent(1); // Load smeared luminosity
  if (m_debug>2 )cout << m_gap << "Lumi_shifted: " << m_lumi_shifted << " Relative shift: " << (m_lumi_shifted - m_lumi)/m_lumi << endl;
  
  this->prepareNormalizedDistributions();
  
  this->scaleAbsoluteDistributions();// Divide by luminosity and by bin widths
  
  this->makeMCBasedResultsProportionalToData(); // MCBased results are scaled to central values
  
  
  
  
  this->prepareSpecialPseudoexperiments(); // Loading additional pseudoexperiments
  
  
  this->fillPseudoexperimentIntoCovariances();
  
  delete m_responseOnlyStat;
  delete m_responseStatAndSys;
}


void CovarianceCalculator::loadRecoLevelPseudoexperiment(){
  
  functions::copyBinContentsAndErrors(m_pseudoexperiment->m_dataOnlyStat,m_recoDataBasedOnlyDataStat);
  m_recoDataBasedOnlyDataStat->Add(m_nominal->m_bkgAllOnlyStat,-1);
  
  functions::copyBinContentsAndErrors(m_nominal->m_dataOnlyStat,m_recoDataBasedOnlyMCStat);
  m_recoDataBasedOnlyMCStat->Add(m_pseudoexperiment->m_bkgAllOnlyStat,-1);
  
  functions::copyBinContentsAndErrors(m_pseudoexperiment->m_dataOnlyMCStat,m_recoDataBasedOnlyMCStatReco);
  m_recoDataBasedOnlyMCStatReco->Add(m_nominal->m_bkgAllOnlyStat,-1);
  
  functions::copyBinContentsAndErrors(m_pseudoexperiment->m_dataOnlyStat,m_recoDataBasedOnlyStat);
  m_recoDataBasedOnlyStat->Add(m_pseudoexperiment->m_bkgAllOnlyStat,-1);

  functions::copyBinContentsAndErrors(m_pseudoexperiment->m_dataOnlySys,m_recoDataBasedOnlySys);
  m_recoDataBasedOnlySys->Add(m_nominal->m_bkgAllOnlyStat,-1);
  
  functions::copyBinContentsAndErrors(m_pseudoexperiment->m_dataStatAndSys,m_recoDataBasedStatAndSys);
  m_recoDataBasedStatAndSys->Add(m_pseudoexperiment->m_bkgAllOnlyStat,-1);
  
  functions::copyBinContentsAndErrors(m_pseudoexperiment->m_dataStatMCStatAndSys,m_recoDataBasedStatMCStatAndSys);
  m_recoDataBasedStatMCStatAndSys->Add(m_nominal->m_bkgAllOnlyStat,-1);
  
  functions::copyBinContentsAndErrors(m_pseudoexperiment->m_dataOnlyStat,m_recoDataBasedStatAndSysAlternative);
  m_recoDataBasedStatAndSysAlternative->Add(m_pseudoexperiment->m_bkgAllStatAndSys,-1);
  
  
  functions::copyBinContentsAndErrors(m_pseudoexperiment->m_pseudodataDataStat,m_recoMCBasedOnlyDataStat);
  m_recoMCBasedOnlyDataStat->Add(m_nominal->m_bkgAllOnlyStat,-1);
  
  functions::copyBinContentsAndErrors(m_nominal->m_pseudodataDataStat,m_recoMCBasedOnlyMCStat);
  m_recoMCBasedOnlyMCStat->Add(m_pseudoexperiment->m_bkgAllOnlyStat,-1);
  
  functions::copyBinContentsAndErrors(m_pseudoexperiment->m_pseudodataMCStat,m_recoMCBasedOnlyMCStatReco);
  m_recoMCBasedOnlyMCStatReco->Add(m_nominal->m_bkgAllOnlyStat,-1);
  
  functions::copyBinContentsAndErrors(m_pseudoexperiment->m_pseudodataDataStat,m_recoMCBasedOnlyStat);
  m_recoMCBasedOnlyStat->Add(m_pseudoexperiment->m_bkgAllOnlyStat,-1);

  functions::copyBinContentsAndErrors(m_pseudoexperiment->m_pseudodataSys,m_recoMCBasedOnlySys);
  m_recoMCBasedOnlySys->Add(m_nominal->m_bkgAllOnlyStat,-1);
  
  functions::copyBinContentsAndErrors(m_pseudoexperiment->m_pseudodataDataStatAndSys,m_recoMCBasedStatAndSys);
  m_recoMCBasedStatAndSys->Add(m_pseudoexperiment->m_bkgAllOnlyStat,-1);
  
  functions::copyBinContentsAndErrors(m_pseudoexperiment->m_pseudodataDataStat,m_recoMCBasedStatAndSysAlternative);
  m_recoMCBasedStatAndSysAlternative->Add(m_pseudoexperiment->m_bkgAllStatAndSys,-1);
  
  functions::copyBinContentsAndErrors(m_nominal->m_dataOnlyStat,m_recoDataBasedOnlyMultijetBkgStat);
  m_recoDataBasedOnlyMultijetBkgStat->Add(m_pseudoexperiment->m_bkgMultijetOnlyStat,-1);
  m_recoDataBasedOnlyMultijetBkgStat->Add(m_nominal->m_bkgMCOnlyStat,-1);
  
  functions::copyBinContentsAndErrors(m_nominal->m_dataOnlyStat,m_recoDataBasedOnlyMCStatWithMultijetFixed);
  m_recoDataBasedOnlyMCStatWithMultijetFixed->Add(m_nominal->m_bkgMultijetOnlyStat,-1);
  m_recoDataBasedOnlyMCStatWithMultijetFixed->Add(m_pseudoexperiment->m_bkgMCOnlyStat,-1);
  
  
}

void CovarianceCalculator::prepareUnfolding(){
  m_responseOnlyStat = new RooUnfoldResponse(0,0,m_pseudoexperiment->m_migrationOnlyStat,"","");
  m_responseStatAndSys = new RooUnfoldResponse(0,0,m_pseudoexperiment->m_migrationStatAndSys,"","");
  
  m_rooUnfoldStat->SetResponse(m_responseOnlyStat);
  m_rooUnfoldStatAndSys->SetResponse(m_responseStatAndSys);
}

void CovarianceCalculator::unfoldPseudoexperiment(){

  functions::UnfoldBayes(m_histUnfoldedDataBasedOnlyDataStat,m_recoDataBasedOnlyDataStat,m_nominal->m_signalRecoOnlyStat,m_nominal->m_effOfTruthLevelCutsNominatorOnlyStat, m_nominal->m_signalTruthOnlyStat,m_nominal->m_effOfRecoLevelCutsNominatorOnlyStat,m_rooUnfoldNominal);  
  functions::UnfoldBayes(m_histUnfoldedDataBasedOnlyMCStat,m_recoDataBasedOnlyMCStat,m_pseudoexperiment->m_signalRecoOnlyStat,m_pseudoexperiment->m_effOfTruthLevelCutsNominatorOnlyStat, m_pseudoexperiment->m_signalTruthOnlyStat,m_pseudoexperiment->m_effOfRecoLevelCutsNominatorOnlyStat,m_rooUnfoldStat);
  functions::UnfoldBayes(m_histUnfoldedDataBasedOnlyMCStatReco,m_recoDataBasedOnlyMCStatReco,m_nominal->m_signalRecoOnlyStat,m_nominal->m_effOfTruthLevelCutsNominatorOnlyStat, m_nominal->m_signalTruthOnlyStat,m_nominal->m_effOfRecoLevelCutsNominatorOnlyStat,m_rooUnfoldNominal);
  functions::UnfoldBayes(m_histUnfoldedDataBasedOnlyStat,m_recoDataBasedOnlyStat,m_pseudoexperiment->m_signalRecoOnlyStat,m_pseudoexperiment->m_effOfTruthLevelCutsNominatorOnlyStat, m_pseudoexperiment->m_signalTruthOnlyStat,m_pseudoexperiment->m_effOfRecoLevelCutsNominatorOnlyStat,m_rooUnfoldStat);
  functions::UnfoldBayes(m_histUnfoldedDataBasedOnlySys,m_recoDataBasedOnlySys,m_nominal->m_signalRecoOnlyStat,m_nominal->m_effOfTruthLevelCutsNominatorOnlyStat,m_nominal->m_signalTruthOnlyStat,m_nominal->m_effOfRecoLevelCutsNominatorOnlyStat,m_rooUnfoldNominal);
  functions::UnfoldBayes(m_histUnfoldedDataBasedStatAndSys,m_recoDataBasedStatAndSys,m_pseudoexperiment->m_signalRecoOnlyStat,m_pseudoexperiment->m_effOfTruthLevelCutsNominatorOnlyStat, m_pseudoexperiment->m_signalTruthOnlyStat,m_pseudoexperiment->m_effOfRecoLevelCutsNominatorOnlyStat,m_rooUnfoldStat);
  functions::UnfoldBayes(m_histUnfoldedDataBasedStatMCStatRecoAndSys,m_recoDataBasedStatMCStatAndSys,m_nominal->m_signalRecoOnlyStat,m_nominal->m_effOfTruthLevelCutsNominatorOnlyStat, m_nominal->m_signalTruthOnlyStat,m_nominal->m_effOfRecoLevelCutsNominatorOnlyStat,m_rooUnfoldStat);
  functions::UnfoldBayes(m_histUnfoldedDataBasedStatAndSysAlternative,m_recoDataBasedStatAndSysAlternative,m_pseudoexperiment->m_signalRecoStatAndSys,m_pseudoexperiment->m_effOfTruthLevelCutsNominatorStatAndSys, m_pseudoexperiment->m_signalTruthOnlyStat,m_pseudoexperiment->m_effOfRecoLevelCutsNominatorStatAndSys,m_rooUnfoldStatAndSys);
																							  
  functions::UnfoldBayes(m_histUnfoldedMCBasedOnlyDataStat,m_recoMCBasedOnlyDataStat,m_nominal->m_signalRecoOnlyStat,m_nominal->m_effOfTruthLevelCutsNominatorOnlyStat, m_nominal->m_signalTruthOnlyStat,m_nominal->m_effOfRecoLevelCutsNominatorOnlyStat,m_rooUnfoldNominal);
  functions::UnfoldBayes(m_histUnfoldedMCBasedOnlyMCStat,m_recoMCBasedOnlyMCStat,m_pseudoexperiment->m_signalRecoOnlyStat,m_pseudoexperiment->m_effOfTruthLevelCutsNominatorOnlyStat, m_pseudoexperiment->m_signalTruthOnlyStat,m_pseudoexperiment->m_effOfRecoLevelCutsNominatorOnlyStat,m_rooUnfoldStat);
  functions::UnfoldBayes(m_histUnfoldedMCBasedOnlyMCStatReco,m_recoMCBasedOnlyMCStatReco,m_nominal->m_signalRecoOnlyStat,m_nominal->m_effOfTruthLevelCutsNominatorOnlyStat, m_nominal->m_signalTruthOnlyStat,m_nominal->m_effOfRecoLevelCutsNominatorOnlyStat,m_rooUnfoldNominal);
  functions::UnfoldBayes(m_histUnfoldedMCBasedOnlyStat,m_recoMCBasedOnlyStat,m_pseudoexperiment->m_signalRecoOnlyStat,m_pseudoexperiment->m_effOfTruthLevelCutsNominatorOnlyStat, m_pseudoexperiment->m_signalTruthOnlyStat,m_pseudoexperiment->m_effOfRecoLevelCutsNominatorOnlyStat,m_rooUnfoldStat);
  functions::UnfoldBayes(m_histUnfoldedMCBasedOnlySys,m_recoMCBasedOnlySys,m_nominal->m_signalRecoOnlyStat,m_nominal->m_effOfTruthLevelCutsNominatorOnlyStat,m_nominal->m_signalTruthOnlyStat,m_nominal->m_effOfRecoLevelCutsNominatorOnlyStat,m_rooUnfoldNominal);
  functions::UnfoldBayes(m_histUnfoldedMCBasedStatAndSys,m_recoMCBasedStatAndSys,m_pseudoexperiment->m_signalRecoOnlyStat,m_pseudoexperiment->m_effOfTruthLevelCutsNominatorOnlyStat, m_pseudoexperiment->m_signalTruthOnlyStat,m_pseudoexperiment->m_effOfRecoLevelCutsNominatorOnlyStat,m_rooUnfoldStat);
  functions::UnfoldBayes(m_histUnfoldedMCBasedStatAndSysAlternative,m_recoMCBasedStatAndSysAlternative,m_pseudoexperiment->m_signalRecoStatAndSys,m_pseudoexperiment->m_effOfTruthLevelCutsNominatorStatAndSys,m_pseudoexperiment->m_signalTruthOnlyStat,m_pseudoexperiment->m_effOfRecoLevelCutsNominatorStatAndSys,m_rooUnfoldStatAndSys);

  // Special unfolding to evaluate just statistical fluctuations of multijet background after unfolding 
  functions::UnfoldBayes(m_histUnfoldedDataBasedOnlyMultijetBkgStat,m_recoDataBasedOnlyMultijetBkgStat,m_nominal->m_signalRecoOnlyStat,m_nominal->m_effOfTruthLevelCutsNominatorOnlyStat, m_nominal->m_signalTruthOnlyStat,m_nominal->m_effOfRecoLevelCutsNominatorOnlyStat,m_rooUnfoldNominal);  
  functions::UnfoldBayes(m_histUnfoldedDataBasedOnlyMCStatWithMultijetFixed,m_recoDataBasedOnlyMCStatWithMultijetFixed,m_pseudoexperiment->m_signalRecoOnlyStat,m_pseudoexperiment->m_effOfTruthLevelCutsNominatorOnlyStat, m_pseudoexperiment->m_signalTruthOnlyStat,m_pseudoexperiment->m_effOfRecoLevelCutsNominatorOnlyStat,m_rooUnfoldStat);
  
}


//----------------------------------------------------------------------

void CovarianceCalculator::applyPostUnfoldingShifts(){
  
  //functions::copyBinContents(m_centralValues,m_histUnfoldedDataBasedOnlySigMod);
  for(int i=0;i<m_nbins_truth;i++) m_histUnfoldedDataBasedOnlySigMod->SetBinContent(i+1,m_centralValues->GetBinContent(i+1)*m_centralValues->GetXaxis()->GetBinWidth(i+1));
  
  functions::copyBinContents(m_histUnfoldedDataBasedStatAndSys,m_histUnfoldedDataBasedStatSysAndSigMod);
  functions::copyBinContents(m_histUnfoldedDataBasedOnlySys,m_histUnfoldedDataBasedSysAndSigMod);
  functions::copyBinContents(m_histUnfoldedDataBasedStatMCStatRecoAndSys,m_histUnfoldedDataBasedStatMCStatRecoSysAndSigMod);
  functions::copyBinContents(m_histUnfoldedDataBasedStatAndSysAlternative,m_histUnfoldedDataBasedStatSysAndSigModAlternative);
  functions::copyBinContents(m_histUnfoldedMCBasedStatAndSys,m_histUnfoldedMCBasedStatSysAndSigMod);
  functions::copyBinContents(m_histUnfoldedMCBasedOnlySys,m_histUnfoldedMCBasedSysAndSigMod);
  functions::copyBinContents(m_histUnfoldedMCBasedStatAndSysAlternative,m_histUnfoldedMCBasedStatSysAndSigModAlternative);
  
  applyPostUnfoldingShifts(m_histUnfoldedDataBasedOnlySigMod,m_pseudoexperiment->m_signalModelingShifts);
  applyPostUnfoldingShifts(m_histUnfoldedDataBasedSysAndSigMod,m_pseudoexperiment->m_signalModelingShifts);
  applyPostUnfoldingShifts(m_histUnfoldedDataBasedStatSysAndSigMod,m_pseudoexperiment->m_signalModelingShifts);
  applyPostUnfoldingShifts(m_histUnfoldedDataBasedStatMCStatRecoSysAndSigMod,m_pseudoexperiment->m_signalModelingShifts);
  applyPostUnfoldingShifts(m_histUnfoldedDataBasedStatSysAndSigModAlternative,m_pseudoexperiment->m_signalModelingShiftsAlternative);
  
  applyPostUnfoldingShifts(m_histUnfoldedMCBasedSysAndSigMod,m_pseudoexperiment->m_signalModelingShifts);
  applyPostUnfoldingShifts(m_histUnfoldedMCBasedStatSysAndSigMod,m_pseudoexperiment->m_signalModelingShifts);
  applyPostUnfoldingShifts(m_histUnfoldedMCBasedStatSysAndSigModAlternative,m_pseudoexperiment->m_signalModelingShiftsAlternative);
  
}

//----------------------------------------------------------------------

void CovarianceCalculator::applyPostUnfoldingShifts(TH1D* h,const vector<double>& shifts) { 
  for(int i=0;i<m_nbins_truth;i++) {
    //std::cout << h->GetBinContent(i+1) << " " << shifts[i] << " " << h->GetBinContent(i+1)*(1.+ shifts[i]) << std::endl;
    h->SetBinContent(i+1,h->GetBinContent(i+1)*(1.+ shifts[i])); 
  }
}

//----------------------------------------------------------------------


void CovarianceCalculator::prepareNormalizedDistributions(){
  
  functions::copyBinContents(m_histUnfoldedDataBasedOnlyDataStat,m_histUnfoldedDataBasedOnlyDataStatNormalized );
  functions::copyBinContents(m_histUnfoldedDataBasedOnlyMCStat,m_histUnfoldedDataBasedOnlyMCStatNormalized);
  functions::copyBinContents(m_histUnfoldedDataBasedOnlyMCStatReco,m_histUnfoldedDataBasedOnlyMCStatRecoNormalized);
  functions::copyBinContents(m_histUnfoldedDataBasedOnlyStat,m_histUnfoldedDataBasedOnlyStatNormalized);
  functions::copyBinContents(m_histUnfoldedDataBasedOnlySys,m_histUnfoldedDataBasedOnlySysNormalized);
  functions::copyBinContents(m_histUnfoldedDataBasedOnlySigMod,m_histUnfoldedDataBasedOnlySigModNormalized);
  functions::copyBinContents(m_histUnfoldedDataBasedStatAndSys,m_histUnfoldedDataBasedStatAndSysNormalized);
  functions::copyBinContents(m_histUnfoldedDataBasedSysAndSigMod,m_histUnfoldedDataBasedSysAndSigModNormalized);
  functions::copyBinContents(m_histUnfoldedDataBasedStatMCStatRecoAndSys,m_histUnfoldedDataBasedStatMCStatRecoAndSysNormalized);
  functions::copyBinContents(m_histUnfoldedDataBasedStatAndSysAlternative,m_histUnfoldedDataBasedStatAndSysAlternativeNormalized);
  functions::copyBinContents(m_histUnfoldedDataBasedStatSysAndSigMod,m_histUnfoldedDataBasedStatSysAndSigModNormalized);
  functions::copyBinContents(m_histUnfoldedDataBasedStatMCStatRecoSysAndSigMod,m_histUnfoldedDataBasedStatMCStatRecoSysAndSigModNormalized);
  functions::copyBinContents(m_histUnfoldedDataBasedStatSysAndSigModAlternative,m_histUnfoldedDataBasedStatSysAndSigModAlternativeNormalized);
  
  functions::copyBinContents(m_histUnfoldedMCBasedOnlyDataStat,m_histUnfoldedMCBasedOnlyDataStatNormalized );
  functions::copyBinContents(m_histUnfoldedMCBasedOnlyMCStat,m_histUnfoldedMCBasedOnlyMCStatNormalized);
  functions::copyBinContents(m_histUnfoldedMCBasedOnlyMCStatReco,m_histUnfoldedMCBasedOnlyMCStatRecoNormalized);
  functions::copyBinContents(m_histUnfoldedMCBasedOnlyStat,m_histUnfoldedMCBasedOnlyStatNormalized);
  functions::copyBinContents(m_histUnfoldedMCBasedOnlySys,m_histUnfoldedMCBasedOnlySysNormalized);
  functions::copyBinContents(m_histUnfoldedMCBasedStatAndSys,m_histUnfoldedMCBasedStatAndSysNormalized);
  functions::copyBinContents(m_histUnfoldedMCBasedSysAndSigMod,m_histUnfoldedMCBasedSysAndSigModNormalized);
  functions::copyBinContents(m_histUnfoldedMCBasedStatAndSysAlternative,m_histUnfoldedMCBasedStatAndSysAlternativeNormalized);
  functions::copyBinContents(m_histUnfoldedMCBasedStatSysAndSigMod,m_histUnfoldedMCBasedStatSysAndSigModNormalized);
  functions::copyBinContents(m_histUnfoldedMCBasedStatSysAndSigModAlternative,m_histUnfoldedMCBasedStatSysAndSigModAlternativeNormalized);
  
  functions::copyBinContents(m_histUnfoldedDataBasedOnlyMultijetBkgStat,m_histUnfoldedDataBasedOnlyMultijetBkgStatNormalized );
  functions::copyBinContents(m_histUnfoldedDataBasedOnlyMCStatWithMultijetFixed,m_histUnfoldedDataBasedOnlyMCStatWithMultijetFixedNormalized );
  
  this->normalizeDistributions(); // Divide by integral and by bin widths
  
  
}

//----------------------------------------------------------------------

void CovarianceCalculator::normalizeDistributions(){

  if(!m_isConcatenated){
    m_histUnfoldedDataBasedOnlyDataStatNormalized->Scale(1./m_histUnfoldedDataBasedOnlyDataStatNormalized->Integral());
    m_histUnfoldedDataBasedOnlyMCStatNormalized->Scale(1./m_histUnfoldedDataBasedOnlyMCStatNormalized->Integral());
    m_histUnfoldedDataBasedOnlyMCStatRecoNormalized->Scale(1./m_histUnfoldedDataBasedOnlyMCStatRecoNormalized->Integral());
    m_histUnfoldedDataBasedOnlyStatNormalized->Scale(1./m_histUnfoldedDataBasedOnlyStatNormalized->Integral());
    m_histUnfoldedDataBasedOnlySysNormalized->Scale(1./m_histUnfoldedDataBasedOnlySysNormalized->Integral());
    m_histUnfoldedDataBasedOnlySigModNormalized->Scale(1./m_histUnfoldedDataBasedOnlySigModNormalized->Integral());
    m_histUnfoldedDataBasedStatAndSysNormalized->Scale(1./m_histUnfoldedDataBasedStatAndSysNormalized->Integral());
    m_histUnfoldedDataBasedSysAndSigModNormalized->Scale(1./m_histUnfoldedDataBasedSysAndSigModNormalized->Integral());
    m_histUnfoldedDataBasedStatMCStatRecoAndSysNormalized->Scale(1./m_histUnfoldedDataBasedStatMCStatRecoAndSysNormalized->Integral());
    m_histUnfoldedDataBasedStatAndSysAlternativeNormalized->Scale(1./m_histUnfoldedDataBasedStatAndSysAlternativeNormalized->Integral());
    m_histUnfoldedDataBasedStatSysAndSigModNormalized->Scale(1./m_histUnfoldedDataBasedStatSysAndSigModNormalized->Integral());
    m_histUnfoldedDataBasedStatMCStatRecoSysAndSigModNormalized->Scale(1./m_histUnfoldedDataBasedStatMCStatRecoSysAndSigModNormalized->Integral());
    m_histUnfoldedDataBasedStatSysAndSigModAlternativeNormalized->Scale(1./m_histUnfoldedDataBasedStatSysAndSigModAlternativeNormalized->Integral());
    
    m_histUnfoldedMCBasedOnlyDataStatNormalized->Scale(1./m_histUnfoldedMCBasedOnlyDataStatNormalized->Integral());
    m_histUnfoldedMCBasedOnlyMCStatRecoNormalized->Scale(1./m_histUnfoldedMCBasedOnlyMCStatRecoNormalized->Integral());
    m_histUnfoldedMCBasedOnlyStatNormalized->Scale(1./m_histUnfoldedMCBasedOnlyStatNormalized->Integral());
    m_histUnfoldedMCBasedOnlySysNormalized->Scale(1./m_histUnfoldedMCBasedOnlySysNormalized->Integral());
    m_histUnfoldedMCBasedStatAndSysNormalized->Scale(1./m_histUnfoldedMCBasedStatAndSysNormalized->Integral());
    m_histUnfoldedMCBasedSysAndSigModNormalized->Scale(1./m_histUnfoldedMCBasedSysAndSigModNormalized->Integral());
    m_histUnfoldedMCBasedStatAndSysAlternativeNormalized->Scale(1./m_histUnfoldedMCBasedStatAndSysAlternativeNormalized->Integral());
    m_histUnfoldedMCBasedStatSysAndSigModNormalized->Scale(1./m_histUnfoldedMCBasedStatSysAndSigModNormalized->Integral());
    m_histUnfoldedMCBasedStatSysAndSigModAlternativeNormalized->Scale(1./m_histUnfoldedMCBasedStatSysAndSigModAlternativeNormalized->Integral());
    
    m_histUnfoldedDataBasedOnlyMultijetBkgStatNormalized->Scale(1./m_histUnfoldedDataBasedOnlyMultijetBkgStatNormalized->Integral());
    m_histUnfoldedDataBasedOnlyMCStatWithMultijetFixedNormalized->Scale(1./m_histUnfoldedDataBasedOnlyMCStatWithMultijetFixedNormalized->Integral());
  }
  else {
    int imin=1,imax=0;
    for(unsigned int i=0;i<m_subHistNbins.size();i++){
      imax+=m_subHistNbins[i];
      functions::ScaleInRange(m_histUnfoldedDataBasedOnlyDataStatNormalized,1./m_histUnfoldedDataBasedOnlyDataStatNormalized->Integral(imin,imax), imin,imax);
      functions::ScaleInRange(m_histUnfoldedDataBasedOnlyMCStatNormalized,1./m_histUnfoldedDataBasedOnlyMCStatNormalized->Integral(imin,imax), imin,imax);
      functions::ScaleInRange(m_histUnfoldedDataBasedOnlyMCStatRecoNormalized,1./m_histUnfoldedDataBasedOnlyMCStatRecoNormalized->Integral(imin,imax), imin,imax);
      functions::ScaleInRange(m_histUnfoldedDataBasedOnlyStatNormalized,1./m_histUnfoldedDataBasedOnlyStatNormalized->Integral(imin,imax), imin,imax);
      functions::ScaleInRange(m_histUnfoldedDataBasedOnlySysNormalized,1./m_histUnfoldedDataBasedOnlySysNormalized->Integral(imin,imax), imin,imax);
      functions::ScaleInRange(m_histUnfoldedDataBasedOnlySigModNormalized,1./m_histUnfoldedDataBasedOnlySigModNormalized->Integral(imin,imax), imin,imax);
      functions::ScaleInRange(m_histUnfoldedDataBasedStatAndSysNormalized,1./m_histUnfoldedDataBasedStatAndSysNormalized->Integral(imin,imax), imin,imax);
      functions::ScaleInRange(m_histUnfoldedDataBasedSysAndSigModNormalized,1./m_histUnfoldedDataBasedSysAndSigModNormalized->Integral(imin,imax), imin,imax);
      functions::ScaleInRange(m_histUnfoldedDataBasedStatMCStatRecoAndSysNormalized,1./m_histUnfoldedDataBasedStatMCStatRecoAndSysNormalized->Integral(imin,imax), imin,imax);
      functions::ScaleInRange(m_histUnfoldedDataBasedStatAndSysAlternativeNormalized,1./m_histUnfoldedDataBasedStatAndSysAlternativeNormalized->Integral(imin,imax), imin,imax);
      functions::ScaleInRange(m_histUnfoldedDataBasedStatSysAndSigModNormalized,1./m_histUnfoldedDataBasedStatSysAndSigModNormalized->Integral(imin,imax), imin,imax);
      functions::ScaleInRange(m_histUnfoldedDataBasedStatMCStatRecoSysAndSigModNormalized,1./m_histUnfoldedDataBasedStatMCStatRecoSysAndSigModNormalized->Integral(imin,imax), imin,imax);
      functions::ScaleInRange(m_histUnfoldedDataBasedStatSysAndSigModAlternativeNormalized,1./m_histUnfoldedDataBasedStatSysAndSigModAlternativeNormalized->Integral(imin,imax), imin,imax);
      
      functions::ScaleInRange(m_histUnfoldedMCBasedOnlyDataStatNormalized,1./m_histUnfoldedMCBasedOnlyDataStatNormalized->Integral(imin,imax), imin,imax);
      functions::ScaleInRange(m_histUnfoldedMCBasedOnlyMCStatRecoNormalized,1./m_histUnfoldedMCBasedOnlyMCStatRecoNormalized->Integral(imin,imax), imin,imax);
      functions::ScaleInRange(m_histUnfoldedMCBasedOnlyStatNormalized,1./m_histUnfoldedMCBasedOnlyStatNormalized->Integral(imin,imax), imin,imax);
      functions::ScaleInRange(m_histUnfoldedMCBasedOnlySysNormalized,1./m_histUnfoldedMCBasedOnlySysNormalized->Integral(imin,imax), imin,imax);
      functions::ScaleInRange(m_histUnfoldedMCBasedStatAndSysNormalized,1./m_histUnfoldedMCBasedStatAndSysNormalized->Integral(imin,imax), imin,imax);
      functions::ScaleInRange(m_histUnfoldedMCBasedSysAndSigModNormalized,1./m_histUnfoldedMCBasedSysAndSigModNormalized->Integral(imin,imax), imin,imax);
      functions::ScaleInRange(m_histUnfoldedMCBasedStatAndSysAlternativeNormalized,1./m_histUnfoldedMCBasedStatAndSysAlternativeNormalized->Integral(imin,imax), imin,imax);
      functions::ScaleInRange(m_histUnfoldedMCBasedStatSysAndSigModNormalized,1./m_histUnfoldedMCBasedStatSysAndSigModNormalized->Integral(imin,imax), imin,imax);
      functions::ScaleInRange(m_histUnfoldedMCBasedStatSysAndSigModAlternativeNormalized,1./m_histUnfoldedMCBasedStatSysAndSigModAlternativeNormalized->Integral(imin,imax), imin,imax);
      
      functions::ScaleInRange(m_histUnfoldedDataBasedOnlyMultijetBkgStatNormalized,1./m_histUnfoldedDataBasedOnlyMultijetBkgStatNormalized->Integral(imin,imax), imin,imax);
      functions::ScaleInRange(m_histUnfoldedDataBasedOnlyMCStatWithMultijetFixedNormalized,1./m_histUnfoldedDataBasedOnlyMCStatWithMultijetFixedNormalized->Integral(imin,imax), imin,imax);
      
      imin+=m_subHistNbins[i];
    }
  }
  functions::DivideByBinWidth(m_histUnfoldedDataBasedOnlyDataStatNormalized);
  functions::DivideByBinWidth(m_histUnfoldedDataBasedOnlyMCStatNormalized);
  functions::DivideByBinWidth(m_histUnfoldedDataBasedOnlyMCStatRecoNormalized);
  functions::DivideByBinWidth(m_histUnfoldedDataBasedOnlyStatNormalized);
  functions::DivideByBinWidth(m_histUnfoldedDataBasedOnlySysNormalized);
  functions::DivideByBinWidth(m_histUnfoldedDataBasedOnlySigModNormalized);
  functions::DivideByBinWidth(m_histUnfoldedDataBasedStatMCStatRecoAndSysNormalized);
  functions::DivideByBinWidth(m_histUnfoldedDataBasedStatAndSysNormalized);
  functions::DivideByBinWidth(m_histUnfoldedDataBasedSysAndSigModNormalized);
  functions::DivideByBinWidth(m_histUnfoldedDataBasedStatAndSysAlternativeNormalized);
  functions::DivideByBinWidth(m_histUnfoldedDataBasedStatMCStatRecoSysAndSigModNormalized);
  functions::DivideByBinWidth(m_histUnfoldedDataBasedStatSysAndSigModNormalized);
  functions::DivideByBinWidth(m_histUnfoldedDataBasedStatSysAndSigModAlternativeNormalized);
  
  functions::DivideByBinWidth(m_histUnfoldedMCBasedOnlyDataStatNormalized);
  functions::DivideByBinWidth(m_histUnfoldedMCBasedOnlyMCStatNormalized);
  functions::DivideByBinWidth(m_histUnfoldedMCBasedOnlyMCStatRecoNormalized);
  functions::DivideByBinWidth(m_histUnfoldedMCBasedOnlyStatNormalized);
  functions::DivideByBinWidth(m_histUnfoldedMCBasedOnlySysNormalized);
  functions::DivideByBinWidth(m_histUnfoldedMCBasedStatAndSysNormalized);
  functions::DivideByBinWidth(m_histUnfoldedMCBasedSysAndSigModNormalized);
  functions::DivideByBinWidth(m_histUnfoldedMCBasedStatAndSysAlternativeNormalized);
  functions::DivideByBinWidth(m_histUnfoldedMCBasedStatSysAndSigModNormalized);
  functions::DivideByBinWidth(m_histUnfoldedMCBasedStatSysAndSigModAlternativeNormalized);
  
  functions::DivideByBinWidth(m_histUnfoldedDataBasedOnlyMultijetBkgStatNormalized);
  functions::DivideByBinWidth(m_histUnfoldedDataBasedOnlyMCStatWithMultijetFixedNormalized);
  
  //for(int i=1;i<=m_nbins_truth;i++) cout << m_isConcatenated << " " << i << " "  << m_histUnfoldedDataBasedStatAndSysNormalized->GetBinContent(i) << " " << m_centralValues->GetBinContent(i) << endl;
  
  
}


void CovarianceCalculator::prepareSpecialPseudoexperiments(){
  functions::copyBinContents(m_pseudoexperiment->m_effOfTruthLevelCutsNominatorOnlyStat,m_effOfTruthLevelOnlyStat);
  functions::copyBinContents(m_pseudoexperiment->m_effOfTruthLevelCutsNominatorOnlySys,m_effOfTruthLevelOnlySys);
  functions::copyBinContents(m_pseudoexperiment->m_effOfTruthLevelCutsNominatorStatAndSys,m_effOfTruthLevelStatAndSys);
  functions::copyBinContents(m_pseudoexperiment->m_effOfRecoLevelCutsNominatorOnlyStat,m_effOfRecoLevelOnlyStat);
  functions::copyBinContents(m_pseudoexperiment->m_effOfRecoLevelCutsNominatorOnlySys,m_effOfRecoLevelOnlySys);
  functions::copyBinContents(m_pseudoexperiment->m_effOfRecoLevelCutsNominatorStatAndSys,m_effOfRecoLevelStatAndSys);
  
  m_effOfTruthLevelOnlyStat->Divide(m_pseudoexperiment->m_signalRecoOnlyStat);
  m_effOfTruthLevelOnlySys->Divide(m_pseudoexperiment->m_signalRecoOnlySys);
  m_effOfTruthLevelStatAndSys->Divide(m_pseudoexperiment->m_signalRecoStatAndSys);
  m_effOfRecoLevelOnlyStat->Divide(m_pseudoexperiment->m_signalTruthOnlyStat);
  m_effOfRecoLevelOnlySys->Divide(m_nominal->m_signalTruthOnlyStat);
  m_effOfRecoLevelStatAndSys->Divide(m_pseudoexperiment->m_signalTruthOnlyStat);
}

void CovarianceCalculator::scaleAbsoluteDistributions(){
  m_histUnfoldedDataBasedOnlyDataStat->Scale(m_SFToTtbarCrossSection/m_lumi);
  functions::DivideByBinWidth(m_histUnfoldedDataBasedOnlyDataStat);
  m_histUnfoldedDataBasedOnlyMCStat->Scale(m_SFToTtbarCrossSection/m_lumi);
  functions::DivideByBinWidth(m_histUnfoldedDataBasedOnlyMCStat);
  m_histUnfoldedDataBasedOnlyMCStatReco->Scale(m_SFToTtbarCrossSection/m_lumi);
  functions::DivideByBinWidth(m_histUnfoldedDataBasedOnlyMCStatReco);
  m_histUnfoldedDataBasedOnlyStat->Scale(m_SFToTtbarCrossSection/m_lumi);
  functions::DivideByBinWidth(m_histUnfoldedDataBasedOnlyStat);
  m_histUnfoldedDataBasedOnlySys->Scale(m_SFToTtbarCrossSection/m_lumi);
  functions::DivideByBinWidth(m_histUnfoldedDataBasedOnlySys);
  //m_histUnfoldedDataBasedOnlySigMod->Scale(m_SFToTtbarCrossSection/m_lumi);
  functions::DivideByBinWidth(m_histUnfoldedDataBasedOnlySigMod);
  m_histUnfoldedDataBasedStatAndSys->Scale(m_SFToTtbarCrossSection/m_lumi);
  functions::DivideByBinWidth(m_histUnfoldedDataBasedStatAndSys);
  m_histUnfoldedDataBasedSysAndSigMod->Scale(m_SFToTtbarCrossSection/m_lumi);
  functions::DivideByBinWidth(m_histUnfoldedDataBasedSysAndSigMod);
  m_histUnfoldedDataBasedStatMCStatRecoAndSys->Scale(m_SFToTtbarCrossSection/m_lumi);
  functions::DivideByBinWidth(m_histUnfoldedDataBasedStatMCStatRecoAndSys);
  m_histUnfoldedDataBasedStatAndSysAlternative->Scale(m_SFToTtbarCrossSection/m_lumi_shifted);
  functions::DivideByBinWidth(m_histUnfoldedDataBasedStatAndSysAlternative);
  m_histUnfoldedDataBasedStatSysAndSigMod->Scale(m_SFToTtbarCrossSection/m_lumi);
  functions::DivideByBinWidth(m_histUnfoldedDataBasedStatSysAndSigMod);
  m_histUnfoldedDataBasedStatMCStatRecoSysAndSigMod->Scale(m_SFToTtbarCrossSection/m_lumi);
  functions::DivideByBinWidth(m_histUnfoldedDataBasedStatMCStatRecoSysAndSigMod);
  m_histUnfoldedDataBasedStatSysAndSigModAlternative->Scale(m_SFToTtbarCrossSection/m_lumi_shifted);
  functions::DivideByBinWidth(m_histUnfoldedDataBasedStatSysAndSigModAlternative);
  
  m_histUnfoldedMCBasedOnlyDataStat->Scale(m_SFToTtbarCrossSection/m_lumi);
  functions::DivideByBinWidth(m_histUnfoldedMCBasedOnlyDataStat);
  m_histUnfoldedMCBasedOnlyMCStat->Scale(m_SFToTtbarCrossSection/m_lumi);
  functions::DivideByBinWidth(m_histUnfoldedMCBasedOnlyMCStat);
  m_histUnfoldedMCBasedOnlyMCStatReco->Scale(m_SFToTtbarCrossSection/m_lumi);
  functions::DivideByBinWidth(m_histUnfoldedMCBasedOnlyMCStatReco);
  m_histUnfoldedMCBasedOnlyStat->Scale(m_SFToTtbarCrossSection/m_lumi);
  functions::DivideByBinWidth(m_histUnfoldedMCBasedOnlyStat);
  m_histUnfoldedMCBasedOnlySys->Scale(m_SFToTtbarCrossSection/m_lumi);
  functions::DivideByBinWidth(m_histUnfoldedMCBasedOnlySys);
  m_histUnfoldedMCBasedStatAndSys->Scale(m_SFToTtbarCrossSection/m_lumi);
  functions::DivideByBinWidth(m_histUnfoldedMCBasedStatAndSys);
  m_histUnfoldedMCBasedSysAndSigMod->Scale(m_SFToTtbarCrossSection/m_lumi);
  functions::DivideByBinWidth(m_histUnfoldedMCBasedSysAndSigMod);
  m_histUnfoldedMCBasedStatAndSysAlternative->Scale(m_SFToTtbarCrossSection/m_lumi_shifted);
  functions::DivideByBinWidth(m_histUnfoldedMCBasedStatAndSysAlternative);
  m_histUnfoldedMCBasedStatSysAndSigMod->Scale(m_SFToTtbarCrossSection/m_lumi);
  functions::DivideByBinWidth(m_histUnfoldedMCBasedStatSysAndSigMod);
  m_histUnfoldedMCBasedStatSysAndSigModAlternative->Scale(m_SFToTtbarCrossSection/m_lumi_shifted);
  functions::DivideByBinWidth(m_histUnfoldedMCBasedStatSysAndSigModAlternative);
  
  m_histUnfoldedDataBasedOnlyMultijetBkgStat->Scale(m_SFToTtbarCrossSection/m_lumi);
  functions::DivideByBinWidth(m_histUnfoldedDataBasedOnlyMultijetBkgStat);
  
  m_histUnfoldedDataBasedOnlyMCStatWithMultijetFixed->Scale(m_SFToTtbarCrossSection/m_lumi);
  functions::DivideByBinWidth(m_histUnfoldedDataBasedOnlyMCStatWithMultijetFixed);
  
}

void CovarianceCalculator::makeMCBasedResultsProportionalToData(){
  double nominalValue,centralValue;
  for(int i=1;i<=m_nbins_truth;i++){
    nominalValue=m_nominal->m_signalTruthOnlyStat->GetBinContent(i)*m_SFToTtbarCrossSection/(m_nominal->m_signalTruthOnlyStat->GetBinWidth(i)*m_lumi);
    centralValue=m_centralValues->GetBinContent(i);
    m_histUnfoldedMCBasedOnlyDataStat->SetBinContent(i,centralValue*(1. + (m_histUnfoldedMCBasedOnlyDataStat->GetBinContent(i) - nominalValue)/nominalValue));
    m_histUnfoldedMCBasedOnlyMCStat->SetBinContent(i,centralValue*(1. + (m_histUnfoldedMCBasedOnlyMCStat->GetBinContent(i) - nominalValue)/nominalValue));
    m_histUnfoldedMCBasedOnlyMCStatReco->SetBinContent(i,centralValue*(1. + (m_histUnfoldedMCBasedOnlyMCStatReco->GetBinContent(i) - nominalValue)/nominalValue));
    m_histUnfoldedMCBasedOnlyStat->SetBinContent(i,centralValue*(1. + (m_histUnfoldedMCBasedOnlyStat->GetBinContent(i) - nominalValue)/nominalValue));
    m_histUnfoldedMCBasedOnlySys->SetBinContent(i,centralValue*(1. + (m_histUnfoldedMCBasedOnlySys->GetBinContent(i) - nominalValue)/nominalValue));
    m_histUnfoldedMCBasedStatAndSys->SetBinContent(i,centralValue*(1. + (m_histUnfoldedMCBasedStatAndSys->GetBinContent(i) - nominalValue)/nominalValue));
    m_histUnfoldedMCBasedSysAndSigMod->SetBinContent(i,centralValue*(1. + (m_histUnfoldedMCBasedSysAndSigMod->GetBinContent(i) - nominalValue)/nominalValue));
    m_histUnfoldedMCBasedStatAndSysAlternative->SetBinContent(i,centralValue*(1. + (m_histUnfoldedMCBasedStatAndSysAlternative->GetBinContent(i) - nominalValue)/nominalValue));
    m_histUnfoldedMCBasedStatSysAndSigMod->SetBinContent(i,centralValue*(1. + (m_histUnfoldedMCBasedStatSysAndSigMod->GetBinContent(i) - nominalValue)/nominalValue));
    m_histUnfoldedMCBasedStatSysAndSigModAlternative->SetBinContent(i,centralValue*(1. + (m_histUnfoldedMCBasedStatSysAndSigModAlternative->GetBinContent(i) - nominalValue)/nominalValue));
    
    nominalValue=m_nominal->m_signalTruthOnlyStat->GetBinContent(i)/(m_nominal->m_signalTruthOnlyStat->GetBinWidth(i)*m_nominal->m_signalTruthOnlyStat->Integral());
    centralValue=m_centralValuesNormalized->GetBinContent(i);
    
    m_histUnfoldedMCBasedOnlyDataStatNormalized->SetBinContent(i,centralValue*(1. + (m_histUnfoldedMCBasedOnlyDataStatNormalized->GetBinContent(i) - nominalValue)/nominalValue));
    m_histUnfoldedMCBasedOnlyMCStatNormalized->SetBinContent(i,centralValue*(1. + (m_histUnfoldedMCBasedOnlyMCStatNormalized->GetBinContent(i) - nominalValue)/nominalValue));
    m_histUnfoldedMCBasedOnlyMCStatRecoNormalized->SetBinContent(i,centralValue*(1. + (m_histUnfoldedMCBasedOnlyMCStatRecoNormalized->GetBinContent(i) - nominalValue)/nominalValue));
    m_histUnfoldedMCBasedOnlyStatNormalized->SetBinContent(i,centralValue*(1. + (m_histUnfoldedMCBasedOnlyStatNormalized->GetBinContent(i) - nominalValue)/nominalValue));
    m_histUnfoldedMCBasedOnlySysNormalized->SetBinContent(i,centralValue*(1. + (m_histUnfoldedMCBasedOnlySysNormalized->GetBinContent(i) - nominalValue)/nominalValue));
    m_histUnfoldedMCBasedStatAndSysNormalized->SetBinContent(i,centralValue*(1. + (m_histUnfoldedMCBasedStatAndSysNormalized->GetBinContent(i) - nominalValue)/nominalValue));
    m_histUnfoldedMCBasedSysAndSigModNormalized->SetBinContent(i,centralValue*(1. + (m_histUnfoldedMCBasedSysAndSigModNormalized->GetBinContent(i) - nominalValue)/nominalValue));
    m_histUnfoldedMCBasedStatAndSysAlternativeNormalized->SetBinContent(i,centralValue*(1. + (m_histUnfoldedMCBasedStatAndSysAlternativeNormalized->GetBinContent(i) - nominalValue)/nominalValue));
    m_histUnfoldedMCBasedStatSysAndSigModNormalized->SetBinContent(i,centralValue*(1. + (m_histUnfoldedMCBasedStatSysAndSigModNormalized->GetBinContent(i) - nominalValue)/nominalValue));
    m_histUnfoldedMCBasedStatSysAndSigModAlternativeNormalized->SetBinContent(i,centralValue*(1. + (m_histUnfoldedMCBasedStatSysAndSigModAlternativeNormalized->GetBinContent(i) - nominalValue)/nominalValue));
  }
}

void CovarianceCalculator::fillPseudoexperimentIntoCovariances(){
  
  m_covFromToys["DataBasedOnlyDataStat"]->addToy(m_histUnfoldedDataBasedOnlyDataStat);
  m_covFromToys["DataBasedOnlyMCStat"]->addToy(m_histUnfoldedDataBasedOnlyMCStat);
  m_covFromToys["DataBasedOnlyMCStatReco"]->addToy(m_histUnfoldedDataBasedOnlyMCStatReco);
  m_covFromToys["DataBasedOnlyStat"]->addToy(m_histUnfoldedDataBasedOnlyStat);
  m_covFromToys["DataBasedOnlySys"]->addToy(m_histUnfoldedDataBasedOnlySys);
  m_covFromToys["DataBasedOnlySigMod"]->addToy(m_histUnfoldedDataBasedOnlySigMod);
  //cout << "Nbins: " << m_histUnfoldedDataBasedStatAndSys->GetNbinsX() << " Integral " << m_histUnfoldedDataBasedStatAndSys->Integral() << endl;
  m_covFromToys["DataBasedStatAndSys"]->addToy(m_histUnfoldedDataBasedStatAndSys);
  m_covFromToys["DataBasedSysAndSigMod"]->addToy(m_histUnfoldedDataBasedSysAndSigMod);
  m_covFromToys["DataBasedStatMCStatRecoAndSys"]->addToy(m_histUnfoldedDataBasedStatMCStatRecoAndSys);
  m_covFromToys["DataBasedStatAndSysAlternative"]->addToy(m_histUnfoldedDataBasedStatAndSysAlternative);
  m_covFromToys["DataBasedStatSysAndSigMod"]->addToy(m_histUnfoldedDataBasedStatSysAndSigMod);
  m_covFromToys["DataBasedStatMCStatRecoSysAndSigMod"]->addToy(m_histUnfoldedDataBasedStatMCStatRecoSysAndSigMod);
  m_covFromToys["DataBasedStatSysAndSigModAlternative"]->addToy(m_histUnfoldedDataBasedStatSysAndSigModAlternative);
  
  m_covFromToys["MCBasedOnlyDataStat"]->addToy(m_histUnfoldedMCBasedOnlyDataStat);
  m_covFromToys["MCBasedOnlyMCStat"]->addToy(m_histUnfoldedMCBasedOnlyMCStat);
  m_covFromToys["MCBasedOnlyMCStatReco"]->addToy(m_histUnfoldedMCBasedOnlyMCStatReco);
  m_covFromToys["MCBasedOnlyStat"]->addToy(m_histUnfoldedMCBasedOnlyStat);
  m_covFromToys["MCBasedOnlySys"]->addToy(m_histUnfoldedMCBasedOnlySys);
  m_covFromToys["MCBasedStatAndSys"]->addToy(m_histUnfoldedMCBasedStatAndSys);
  m_covFromToys["MCBasedSysAndSigMod"]->addToy(m_histUnfoldedMCBasedSysAndSigMod);
  m_covFromToys["MCBasedStatSysAndSigMod"]->addToy(m_histUnfoldedMCBasedStatSysAndSigMod);
  m_covFromToys["MCBasedStatAndSysAlternative"]->addToy(m_histUnfoldedMCBasedStatAndSysAlternative);
  m_covFromToys["MCBasedStatSysAndSigModAlternative"]->addToy(m_histUnfoldedMCBasedStatSysAndSigModAlternative);
  
  m_covFromToys["DataBasedOnlyDataStatNormalized"]->addToy(m_histUnfoldedDataBasedOnlyDataStatNormalized);
  m_covFromToys["DataBasedOnlyMCStatNormalized"]->addToy(m_histUnfoldedDataBasedOnlyMCStatNormalized);
  m_covFromToys["DataBasedOnlyMCStatRecoNormalized"]->addToy(m_histUnfoldedDataBasedOnlyMCStatRecoNormalized);
  m_covFromToys["DataBasedOnlyStatNormalized"]->addToy(m_histUnfoldedDataBasedOnlyStatNormalized);
  m_covFromToys["DataBasedOnlySysNormalized"]->addToy(m_histUnfoldedDataBasedOnlySysNormalized);
  m_covFromToys["DataBasedOnlySigModNormalized"]->addToy(m_histUnfoldedDataBasedOnlySigModNormalized);
  m_covFromToys["DataBasedStatAndSysNormalized"]->addToy(m_histUnfoldedDataBasedStatAndSysNormalized);
  m_covFromToys["DataBasedSysAndSigModNormalized"]->addToy(m_histUnfoldedDataBasedSysAndSigModNormalized);
  m_covFromToys["DataBasedStatMCStatRecoAndSysNormalized"]->addToy(m_histUnfoldedDataBasedStatMCStatRecoAndSysNormalized);
  m_covFromToys["DataBasedStatAndSysAlternativeNormalized"]->addToy(m_histUnfoldedDataBasedStatAndSysAlternativeNormalized);
  m_covFromToys["DataBasedStatSysAndSigModNormalized"]->addToy(m_histUnfoldedDataBasedStatSysAndSigModNormalized);
  m_covFromToys["DataBasedStatMCStatRecoSysAndSigModNormalized"]->addToy(m_histUnfoldedDataBasedStatMCStatRecoSysAndSigModNormalized);
  m_covFromToys["DataBasedStatSysAndSigModAlternativeNormalized"]->addToy(m_histUnfoldedDataBasedStatSysAndSigModAlternativeNormalized);
  
  m_covFromToys["MCBasedOnlyDataStatNormalized"]->addToy(m_histUnfoldedMCBasedOnlyDataStatNormalized);
  m_covFromToys["MCBasedOnlyMCStatNormalized"]->addToy(m_histUnfoldedMCBasedOnlyMCStatNormalized);
  m_covFromToys["MCBasedOnlyMCStatRecoNormalized"]->addToy(m_histUnfoldedMCBasedOnlyMCStatRecoNormalized);
  m_covFromToys["MCBasedOnlyStatNormalized"]->addToy(m_histUnfoldedMCBasedOnlyStatNormalized);
  m_covFromToys["MCBasedOnlySysNormalized"]->addToy(m_histUnfoldedMCBasedOnlySysNormalized);
  m_covFromToys["MCBasedSysAndSigModNormalized"]->addToy(m_histUnfoldedMCBasedSysAndSigModNormalized);
  m_covFromToys["MCBasedStatAndSysAlternativeNormalized"]->addToy(m_histUnfoldedMCBasedStatAndSysAlternativeNormalized);
  m_covFromToys["MCBasedStatSysAndSigModNormalized"]->addToy(m_histUnfoldedMCBasedStatSysAndSigModNormalized);
  m_covFromToys["MCBasedStatSysAndSigModAlternativeNormalized"]->addToy(m_histUnfoldedMCBasedStatSysAndSigModAlternativeNormalized);
  
  m_covFromToys["DataBasedOnlyMultijetBkgStat"]->addToy(m_histUnfoldedDataBasedOnlyMultijetBkgStat);
  m_covFromToys["DataBasedOnlyMultijetBkgStatNormalized"]->addToy(m_histUnfoldedDataBasedOnlyMultijetBkgStatNormalized);
  
  m_covFromToys["DataBasedOnlyMCStatWithMultijetFixed"]->addToy(m_histUnfoldedDataBasedOnlyMCStatWithMultijetFixed);
  m_covFromToys["DataBasedOnlyMCStatWithMultijetFixedNormalized"]->addToy(m_histUnfoldedDataBasedOnlyMCStatWithMultijetFixedNormalized);
  
  m_covFromToys["EffOfTruthLevelOnlyStat"]->addToy(m_effOfTruthLevelOnlyStat);
  m_covFromToys["EffOfTruthLevelOnlySys"]->addToy(m_effOfTruthLevelOnlySys);
  m_covFromToys["EffOfTruthLevelStatAndSys"]->addToy(m_effOfTruthLevelStatAndSys);
  
  m_covFromToys["EffOfRecoLevelOnlyStat"]->addToy(m_effOfRecoLevelOnlyStat);
  m_covFromToys["EffOfRecoLevelOnlySys"]->addToy(m_effOfRecoLevelOnlySys);
  m_covFromToys["EffOfRecoLevelStatAndSys"]->addToy(m_effOfRecoLevelStatAndSys);
  
  m_covFromToys["MultijetBkgOnlyStat"]->addToy(m_pseudoexperiment->m_bkgMultijetOnlyStat);
  m_covFromToys["MultijetBkgOnlySys"]->addToy(m_pseudoexperiment->m_bkgMultijetOnlySys);
  m_covFromToys["MultijetBkgStatAndSys"]->addToy(m_pseudoexperiment->m_bkgMultijetStatAndSys);
  
}

void CovarianceCalculator::loadSignalModelingCovariances(TFile* f){
  if (m_debug>0 ){
    cout << "Loading Signal modeling covariances." << endl;
    cout << (TString)"totalCovariance_"+m_variable_name+"_" + m_level << endl;
  }
  
  m_signalModelingCovariances["Total"] = std::shared_ptr<TMatrixT<double> >((TMatrixT<double>*)f->Get((TString)"SignalModelingPlusPDFCovariance_"+m_variable_name+"_" + m_level));
  m_signalModelingCovariances["ME"] = std::shared_ptr<TMatrixT<double> >((TMatrixT<double>*)f->Get((TString)"MatrixElementCovariance_"+m_variable_name+"_" + m_level));
  m_signalModelingCovariances["PS"] = std::shared_ptr<TMatrixT<double> >((TMatrixT<double>*)f->Get((TString)"PartonShowerCovariance_"+m_variable_name+"_" + m_level));
  m_signalModelingCovariances["FSR"] = std::shared_ptr<TMatrixT<double> >((TMatrixT<double>*)f->Get((TString)"FSRCovariance_"+m_variable_name+"_" + m_level));
  
  m_signalModelingCovariances["TotalNormalized"] = std::shared_ptr<TMatrixT<double> >((TMatrixT<double>*)f->Get((TString)"SignalModelingPlusPDFCovariance_normalized_"+m_variable_name+"_" + m_level));
  m_signalModelingCovariances["MENormalized"] = std::shared_ptr<TMatrixT<double> >((TMatrixT<double>*)f->Get((TString)"MatrixElementCovariance_normalized_"+m_variable_name+"_" + m_level));
  m_signalModelingCovariances["PSNormalized"] = std::shared_ptr<TMatrixT<double> >((TMatrixT<double>*)f->Get((TString)"PartonShowerCovariance_normalized_"+m_variable_name+"_" + m_level));
  m_signalModelingCovariances["FSRNormalized"] = std::shared_ptr<TMatrixT<double> >((TMatrixT<double>*)f->Get((TString)"FSRCovariance_normalized_"+m_variable_name+"_" + m_level));
  
  m_signalModelingCovariances["TotalAlternative"] = std::shared_ptr<TMatrixT<double> >((TMatrixT<double>*)f->Get((TString)"SignalModelingPlusPDFCovariance_Alternative_"+m_variable_name+"_" + m_level));
  m_signalModelingCovariances["ME_Alternative"] = std::shared_ptr<TMatrixT<double> >((TMatrixT<double>*)f->Get((TString)"MatrixElementCovariance_Alternative_"+m_variable_name+"_" + m_level));
  m_signalModelingCovariances["PS_Alternative"] = std::shared_ptr<TMatrixT<double> >((TMatrixT<double>*)f->Get((TString)"PartonShowerCovariance_Alternative_"+m_variable_name+"_" + m_level));
  m_signalModelingCovariances["FSR_Alternative"] = std::shared_ptr<TMatrixT<double> >((TMatrixT<double>*)f->Get((TString)"FSRCovariance_Alternative_"+m_variable_name+"_" + m_level));
  
  m_signalModelingCovariances["TotalAlternativeNormalized"] = std::shared_ptr<TMatrixT<double> >((TMatrixT<double>*)f->Get((TString)"SignalModelingPlusPDFCovariance_Alternative_normalized_"+m_variable_name+"_" + m_level));
  m_signalModelingCovariances["ME_AlternativeNormalized"] = std::shared_ptr<TMatrixT<double> >((TMatrixT<double>*)f->Get((TString)"MatrixElementCovariance_Alternative_normalized_"+m_variable_name+"_" + m_level));
  m_signalModelingCovariances["PS_AlternativeNormalized"] = std::shared_ptr<TMatrixT<double> >((TMatrixT<double>*)f->Get((TString)"PartonShowerCovariance_Alternative_normalized_"+m_variable_name+"_" + m_level));
  m_signalModelingCovariances["FSR_AlternativeNormalized"] = std::shared_ptr<TMatrixT<double> >((TMatrixT<double>*)f->Get((TString)"FSRCovariance_Alternative_normalized_"+m_variable_name+"_" + m_level));    
  
  m_signalModelingLoaded=true;
}

void CovarianceCalculator::finalize(){
  if (m_debug>0 ) cout << "CovarianceCalculator::Finalizing covariances" << endl;
  for(auto it=m_covFromToys.begin();it!=m_covFromToys.end();it++){
    it->second->finalize();
  }
  
  
  //m_signalModelingCovarianceTotal->Print();
  if(m_signalModelingLoaded){
    m_covDataBasedStatAndSysAndSignalModeling = std::make_shared<TMatrixT<double> >(m_nbins_truth,m_nbins_truth);
    *m_covDataBasedStatAndSysAndSignalModeling+=*m_covFromToys["DataBasedStatAndSys"]->getCovariance();
    //*m_covDataBasedStatAndSysAndSignalModeling+=*m_signalModelingCovariances["Total"];
    *m_covDataBasedStatAndSysAndSignalModeling+=*m_signalModelingCovariances["ME"];
    m_covDataBasedStatMCStatRecoAndSysAndSignalModeling = std::make_shared<TMatrixT<double> >(m_nbins_truth,m_nbins_truth);
    *m_covDataBasedStatMCStatRecoAndSysAndSignalModeling+=*m_covFromToys["DataBasedStatMCStatRecoAndSys"]->getCovariance();
    *m_covDataBasedStatMCStatRecoAndSysAndSignalModeling+=*m_signalModelingCovariances["Total"];
    m_covDataBasedStatAndSysAlternativeAndSignalModeling= std::make_shared<TMatrixT<double> >(m_nbins_truth,m_nbins_truth);
    *m_covDataBasedStatAndSysAlternativeAndSignalModeling+=*m_covFromToys["DataBasedStatAndSysAlternative"]->getCovariance();
    *m_covDataBasedStatAndSysAlternativeAndSignalModeling+=*m_signalModelingCovariances["TotalAlternative"];
    
    m_covDataBasedStatAndSysAndSignalModelingNormalized= std::make_shared<TMatrixT<double> >(m_nbins_truth,m_nbins_truth);
    *m_covDataBasedStatAndSysAndSignalModelingNormalized+=*m_covFromToys["DataBasedStatAndSysNormalized"]->getCovariance();
    *m_covDataBasedStatAndSysAndSignalModelingNormalized+=*m_signalModelingCovariances["TotalNormalized"];
    m_covDataBasedStatMCStatRecoAndSysAndSignalModelingNormalized= std::make_shared<TMatrixT<double> >(m_nbins_truth,m_nbins_truth);
    *m_covDataBasedStatMCStatRecoAndSysAndSignalModelingNormalized+=*m_covFromToys["DataBasedStatMCStatRecoAndSysNormalized"]->getCovariance();
    *m_covDataBasedStatMCStatRecoAndSysAndSignalModelingNormalized+=*m_signalModelingCovariances["TotalNormalized"];

    m_covDataBasedStatAndSysAlternativeAndSignalModelingNormalized= std::make_shared<TMatrixT<double> >(m_nbins_truth,m_nbins_truth);
    *m_covDataBasedStatAndSysAlternativeAndSignalModelingNormalized+=*m_covFromToys["DataBasedStatAndSysAlternativeNormalized"]->getCovariance();
    *m_covDataBasedStatAndSysAlternativeAndSignalModelingNormalized+=*m_signalModelingCovariances["TotalAlternativeNormalized"];
    
    m_covMCBasedStatAndSysAndSignalModeling= std::make_shared<TMatrixT<double> >(m_nbins_truth,m_nbins_truth);
    *m_covMCBasedStatAndSysAndSignalModeling+=*m_covFromToys["MCBasedStatAndSys"]->getCovariance();
    //*m_covMCBasedStatAndSysAndSignalModeling+=*m_signalModelingCovariances["Total"];
    *m_covMCBasedStatAndSysAndSignalModeling+=*m_signalModelingCovariances["ME"];
    m_covMCBasedStatAndSysAlternativeAndSignalModeling= std::make_shared<TMatrixT<double> >(m_nbins_truth,m_nbins_truth);
    *m_covMCBasedStatAndSysAlternativeAndSignalModeling+=*m_covFromToys["MCBasedStatAndSysAlternative"]->getCovariance();
    *m_covMCBasedStatAndSysAlternativeAndSignalModeling+=*m_signalModelingCovariances["TotalAlternative"];
    
    m_covMCBasedStatAndSysAndSignalModelingNormalized= std::make_shared<TMatrixT<double> >(m_nbins_truth,m_nbins_truth);
    *m_covMCBasedStatAndSysAndSignalModelingNormalized+=*m_covFromToys["MCBasedStatAndSysNormalized"]->getCovariance();
    *m_covMCBasedStatAndSysAndSignalModelingNormalized+=*m_signalModelingCovariances["TotalNormalized"];
    m_covMCBasedStatAndSysAlternativeAndSignalModelingNormalized= std::make_shared<TMatrixT<double> >(m_nbins_truth,m_nbins_truth);
    *m_covMCBasedStatAndSysAlternativeAndSignalModelingNormalized+=*m_covFromToys["MCBasedStatAndSysAlternativeNormalized"]->getCovariance();
    *m_covMCBasedStatAndSysAlternativeAndSignalModelingNormalized+=*m_signalModelingCovariances["TotalAlternativeNormalized"];
  }
  
  
}
void CovarianceCalculator::printMeanValues(){
  for(auto it=m_covFromToys.begin();it!=m_covFromToys.end();it++){
    cout << "CovarianceCalculator: Printing mean values and relative uncertainties using " << it->first << "." << endl;
    it->second->printMeanAndRelUnc();
  }
}
void CovarianceCalculator::printCovariances(){
  cout << "CovarianceCalculator: Printing Covariance with statistical errors only from pseudoexperiments." << endl;
  m_covFromToys["DataBasedOnlyStat"]->getCovariance()->Print();
  
  cout << "CovarianceCalculator: Printing Covariance with systematic errors only from pseudoexperiments." << endl;
  m_covFromToys["DataBasedOnlySys"]->getCovariance()->Print();
  
  cout << "CovarianceCalculator: Printing Covariance with combined sys and stat errors from pseudoexperiments." << endl;
  m_covFromToys["DataBasedStatAndSys"]->getCovariance()->Print();
  if(m_signalModelingLoaded){  
    cout << "CovarianceCalculator: Printing Covariance with ME signal modeling errors." << endl;
    m_signalModelingCovariances["ME"]->Print();
    cout << "CovarianceCalculator: Printing Covariance with PS signal modeling errors." << endl;
    m_signalModelingCovariances["PS"]->Print();
    cout << "CovarianceCalculator: Printing Covariance with FSR signal modeling errors." << endl;
    m_signalModelingCovariances["FSR"]->Print();
    cout << "CovarianceCalculator: Printing Covariance with total signal modeling errors." << endl;
    m_signalModelingCovariances["Total"]->Print();
    
    
    cout << "CovarianceCalculator: Printing Covariance for the final result." << endl;
    m_covDataBasedStatAndSysAndSignalModeling->Print();
  }
}

void CovarianceCalculator::printLatexTablesWithFinalResults(const TString& path){
//	m_covMCBasedOnlyStat
  
//	m_covDataBasedStatAndSysAndSignalModeling
  gSystem->mkdir(path,true);
  ofstream uncertainties_relative(path+"/" + m_variable_name+"_" + m_level + "_uncertainties_relative.tex" );
  
  vector<std::shared_ptr<TMatrixT<double> > > covariances;
  covariances.push_back(m_covFromToys["DataBasedOnlyDataStatNormalized"]->getCovariance());
  covariances.push_back(m_covFromToys["DataBasedOnlyMCStatNormalized"]->getCovariance());
  covariances.push_back(m_covFromToys["DataBasedOnlySysNormalized"]->getCovariance());
  if(m_signalModelingLoaded){ 
    covariances.push_back(m_signalModelingCovariances["TotalNormalized"]);
    covariances.push_back(m_covDataBasedStatAndSysAndSignalModelingNormalized);
  }
  //for(int i=0;i<(int)covariances.size();i++) cout << covariances[i]->GetNcols() << " " << covariances[i]->GetNrows() << endl;
  
  printTableOfUncertainties(uncertainties_relative,covariances,m_centralValuesNormalized,"relative");
  
  covariances.clear();
  ofstream uncertainties_absolute(path+"/" + m_variable_name+"_" + m_level + "_uncertainties_absolute.tex" );
  covariances.push_back(m_covFromToys["DataBasedOnlyDataStat"]->getCovariance());
  covariances.push_back(m_covFromToys["DataBasedOnlyMCStat"]->getCovariance());
  covariances.push_back(m_covFromToys["DataBasedOnlySys"]->getCovariance());
  if(m_signalModelingLoaded){ 
    covariances.push_back(m_signalModelingCovariances["Total"]);
    covariances.push_back(m_covDataBasedStatAndSysAndSignalModeling);
  }
  printTableOfUncertainties(uncertainties_absolute,covariances,m_centralValues,"absolute");
  if(m_signalModelingLoaded){ 
    ofstream diffxsec_relative(path+"/" + m_variable_name+"_" + m_level + "_diffCrossSection_relative.tex");
    printTableWithDiffCrossSection(diffxsec_relative,m_centralValuesNormalized,*m_covFromToys["DataBasedOnlyDataStatNormalized"]->getCovariance(),*m_covDataBasedStatAndSysAndSignalModelingNormalized,"relative");
    
    ofstream diffxsec_absolute(path+"/" + m_variable_name+"_" + m_level + "_diffCrossSection_absolute.tex");
    printTableWithDiffCrossSection(diffxsec_absolute,m_centralValues,*m_covFromToys["DataBasedOnlyDataStat"]->getCovariance(),*m_covDataBasedStatAndSysAndSignalModeling,"absolute");
  }
}

void CovarianceCalculator::printTableWithDiffCrossSection(ofstream& texfile,TH1D* centralValues,TMatrixT<double>& covStat,TMatrixT<double>& covAll,const TString& diffxsec){
  if (m_debug>0 )cout << "Printing table with diff cross section" << endl;
  texfile << setprecision(3);
  //texfile << "\\begin{table} [htbp]" << endl;
  //texfile << "\\footnotesize" << endl;
  //texfile << "\\centering" << endl;
  texfile << "\\begin{tabular}{|r|r|r|r|}" << endl;
  texfile << "\\hline" << endl;
  
  TString x_unit="GeV";
  double unitSF=1;
  if(m_variable_name.Contains("ttbar_mass")){
    x_unit="TeV";
    unitSF=1000;
  }
  
  TString y_unit;
  if(diffxsec=="relative")y_unit=(TString)"[1/" + x_unit +"]";
  else y_unit=(TString)"[pb/" + x_unit +"]";
  
  texfile << (TString)"Range ["+x_unit+"] & Cross section " + y_unit + " & Stat. unc. [\\%] & Total unc.  [\\%] \\\\ \\hline " << endl;
  
  for(int i=0;i<m_nbins_truth;i++){
    double centralValue=centralValues->GetBinContent(i+1);
    texfile << setprecision(5) << "[" << centralValues->GetBinLowEdge(i+1)/unitSF << "," << centralValues->GetBinLowEdge(i+2)/unitSF << "]";
    texfile << setprecision(3);
    texfile << " & " << centralValue*unitSF;
    texfile << " & " << sqrt(covStat[i][i])/centralValue*100;
    texfile << " & " << sqrt(covAll[i][i])/centralValue*100;
    texfile << "\\\\ \\hline" << endl;
  }
  TString name=m_variable_name;
  name.ReplaceAll("topCandidate", "random top");
  name.ReplaceAll("_"," ");
  texfile << "\\end{tabular}";
  //texfile << (TString)"\\caption{Table with relative uncertaintes for the " + diffxsec +" differential cross section at parton level as a function of " + name + "}" << endl;
  //texfile << "\\end{table}";
}


void CovarianceCalculator::printTableOfUncertainties(ofstream& texfile,const vector<std::shared_ptr<TMatrixT<double> > >& covariances,const TH1D* centralValues,const TString& diffxsec){
  if (m_debug>0 ) cout << "Printing table of uncertainties" << endl;
  texfile << setprecision(3);
  texfile << "\\begin{table} [htbp]" << endl;
  texfile << "\\footnotesize" << endl;
  texfile << "\\centering" << endl;
  texfile << "\\begin{tabular}{";
  const int ncovariances=covariances.size();
  for(int i=0;i<ncovariances;i++){
    texfile << "|r";
  }
  TString x_unit="GeV";
  double unitSF=1;
  if(m_variable_name.Contains("ttbar_mass")){
    x_unit="TeV";
    unitSF=1000;
  }
  
  texfile<< "|r|}" << endl; 
  texfile << "\\hline" << endl;
  texfile << (TString)"Bin range ["+x_unit+"] & Data stat [\\%] & MC stat [\\%] & Detector sys [\\%]& Signal modeling [\\%] & Total [\\%] \\\\ \\hline" << endl;
  
  for(int i=0;i<m_nbins_truth;i++){
    texfile << setprecision(5) << "[" << centralValues->GetBinLowEdge(i+1)/unitSF << "," << centralValues->GetBinLowEdge(i+2)/unitSF << "]";
    texfile << setprecision(3);
    double centralValue=centralValues->GetBinContent(i+1);
    for(int j=0;j<ncovariances;j++){
      texfile << " & " << sqrt((*covariances[j])[i][i])/centralValue*100.;
    }
    texfile << "\\\\ \\hline" << endl;
  }
  
  TString name=m_variable_name;
  name.ReplaceAll("topCandidate", "random top");
  name.ReplaceAll("_"," ");
  
  texfile << "\\end{tabular}";
  texfile << (TString)"\\caption{Table with relative uncertaintes for the " + diffxsec +" differential cross section at parton level as a function of " + name + "}" << endl;
  texfile << "\\end{table}";
	
}



void CovarianceCalculator::writeCovariances(TFile* f){
  f->cd();
  m_covFromToys["DataBasedOnlyDataStat"]->getCovariance()->Write(m_variable_name+"_covDataBasedOnlyDataStat");
  m_covFromToys["DataBasedOnlyMCStat"]->getCovariance()->Write(m_variable_name+"_covDataBasedOnlyMCStat");
  m_covFromToys["DataBasedOnlyMCStatReco"]->getCovariance()->Write(m_variable_name+"_covDataBasedOnlyMCStatReco");
  m_covFromToys["DataBasedOnlyStat"]->getCovariance()->Write(m_variable_name+"_covDataBasedOnlyStat");
  m_covFromToys["DataBasedOnlySys"]->getCovariance()->Write(m_variable_name+"_covDataBasedOnlySys");
  m_covFromToys["DataBasedOnlySigMod"]->getCovariance()->Write(m_variable_name+"_covDataBasedOnlySigMod");
  m_covFromToys["DataBasedStatAndSys"]->getCovariance()->Write(m_variable_name+"_covDataBasedStatAndSys");
  m_covFromToys["DataBasedSysAndSigMod"]->getCovariance()->Write(m_variable_name+"_covDataBasedSysAndSigMod");
  m_covFromToys["DataBasedStatMCStatRecoAndSys"]->getCovariance()->Write(m_variable_name+"_covDataBasedStatMCStatRecoAndSys");
  m_covFromToys["DataBasedStatAndSysAlternative"]->getCovariance()->Write(m_variable_name+"_covDataBasedStatAndSysAlternative");
  m_covFromToys["DataBasedStatSysAndSigMod"]->getCovariance()->Write(m_variable_name+"_covDataBasedStatSysAndSigMod");
  m_covFromToys["DataBasedStatMCStatRecoSysAndSigMod"]->getCovariance()->Write(m_variable_name+"_covDataBasedStatMCStatRecoSysAndSigMod");
  m_covFromToys["DataBasedStatSysAndSigModAlternative"]->getCovariance()->Write(m_variable_name+"_covDataBasedStatSysAndSigModAlternative");
  if(m_signalModelingLoaded){
    m_covDataBasedStatAndSysAndSignalModeling->Write(m_variable_name+"_covDataBasedAllUnc");
    m_covDataBasedStatMCStatRecoAndSysAndSignalModeling->Write(m_variable_name+"_covDataBasedAllUncMCStatReco");
    m_covDataBasedStatAndSysAlternativeAndSignalModeling->Write(m_variable_name+"_covDataBasedAllUncAlternative");
  }
  
  m_covFromToys["DataBasedOnlyDataStatNormalized"]->getCovariance()->Write(m_variable_name+"_covDataBasedOnlyDataStatNormalized");
  m_covFromToys["DataBasedOnlyMCStatNormalized"]->getCovariance()->Write(m_variable_name+"_covDataBasedOnlyMCStatNormalized");
  m_covFromToys["DataBasedOnlyMCStatRecoNormalized"]->getCovariance()->Write(m_variable_name+"_covDataBasedOnlyMCStatRecoNormalized");
  m_covFromToys["DataBasedOnlyStatNormalized"]->getCovariance()->Write(m_variable_name+"_covDataBasedOnlyStatNormalized");
  m_covFromToys["DataBasedOnlySysNormalized"]->getCovariance()->Write(m_variable_name+"_covDataBasedOnlySysNormalized");
  m_covFromToys["DataBasedOnlySigModNormalized"]->getCovariance()->Write(m_variable_name+"_covDataBasedOnlySigModNormalized");
  m_covFromToys["DataBasedStatAndSysNormalized"]->getCovariance()->Write(m_variable_name+"_covDataBasedStatAndSysNormalized");
  m_covFromToys["DataBasedSysAndSigModNormalized"]->getCovariance()->Write(m_variable_name+"_covDataBasedSysAndSigModNormalized");
  m_covFromToys["DataBasedStatMCStatRecoAndSysNormalized"]->getCovariance()->Write(m_variable_name+"_covDataBasedStatMCStatRecoAndSysNormalized");
  m_covFromToys["DataBasedStatAndSysAlternativeNormalized"]->getCovariance()->Write(m_variable_name+"_covDataBasedStatAndSysAlternativeNormalized");
  m_covFromToys["DataBasedStatSysAndSigModNormalized"]->getCovariance()->Write(m_variable_name+"_covDataBasedStatSysAndSigModNormalized");
  m_covFromToys["DataBasedStatMCStatRecoSysAndSigModNormalized"]->getCovariance()->Write(m_variable_name+"_covDataBasedStatMCStatRecoSysAndSigModNormalized");
  m_covFromToys["DataBasedStatSysAndSigModAlternativeNormalized"]->getCovariance()->Write(m_variable_name+"_covDataBasedStatSysAndSigModAlternativeNormalized");
  if(m_signalModelingLoaded){
    m_covDataBasedStatAndSysAndSignalModelingNormalized->Write(m_variable_name+"_covDataBasedAllUncNormalized");
    m_covDataBasedStatMCStatRecoAndSysAndSignalModelingNormalized->Write(m_variable_name+"_covDataBasedAllUncMCStatRecoNormalized");
    m_covDataBasedStatAndSysAlternativeAndSignalModelingNormalized->Write(m_variable_name+"_covDataBasedAllUncAlternativeNormalized");
  }
  
  m_covFromToys["MCBasedOnlyDataStat"]->getCovariance()->Write(m_variable_name+"_covMCBasedOnlyDataStat");
  m_covFromToys["MCBasedOnlyMCStat"]->getCovariance()->Write(m_variable_name+"_covMCBasedOnlyMCStat");
  m_covFromToys["MCBasedOnlyMCStatReco"]->getCovariance()->Write(m_variable_name+"_covMCBasedOnlyMCStatReco");
  m_covFromToys["MCBasedOnlyStat"]->getCovariance()->Write(m_variable_name+"_covMCBasedOnlyStat");
  m_covFromToys["MCBasedOnlySys"]->getCovariance()->Write(m_variable_name+"_covMCBasedOnlySys");
  m_covFromToys["MCBasedStatAndSys"]->getCovariance()->Write(m_variable_name+"_covMCBasedStatAndSys");
  m_covFromToys["MCBasedSysAndSigMod"]->getCovariance()->Write(m_variable_name+"_covMCBasedSysAndSigMod");
  m_covFromToys["MCBasedStatAndSysAlternative"]->getCovariance()->Write(m_variable_name+"_covMCBasedStatAndSysAlternative");
  m_covFromToys["MCBasedStatSysAndSigMod"]->getCovariance()->Write(m_variable_name+"_covMCBasedStatSysAndSigMod");
  m_covFromToys["MCBasedStatSysAndSigModAlternative"]->getCovariance()->Write(m_variable_name+"_covMCBasedStatSysAndSigModAlternative");
  if(m_signalModelingLoaded){
    m_covMCBasedStatAndSysAndSignalModeling->Write(m_variable_name+"_covMCBasedAllUnc");
    m_covMCBasedStatAndSysAlternativeAndSignalModeling->Write(m_variable_name+"_covMCBasedAllUncAlternative");
  }
  
  m_covFromToys["MCBasedOnlyDataStatNormalized"]->getCovariance()->Write(m_variable_name+"_covMCBasedOnlyDataStatNormalized");
  m_covFromToys["MCBasedOnlyMCStatNormalized"]->getCovariance()->Write(m_variable_name+"_covMCBasedOnlyMCStatNormalized");
  m_covFromToys["MCBasedOnlyMCStatRecoNormalized"]->getCovariance()->Write(m_variable_name+"_covMCBasedOnlyMCStatRecoNormalized");
  m_covFromToys["MCBasedOnlyStatNormalized"]->getCovariance()->Write(m_variable_name+"_covMCBasedOnlyStatNormalized");
  m_covFromToys["MCBasedOnlySysNormalized"]->getCovariance()->Write(m_variable_name+"_covMCBasedOnlySysNormalized");
  m_covFromToys["MCBasedStatAndSysNormalized"]->getCovariance()->Write(m_variable_name+"_covMCBasedStatAndSysNormalized");
  m_covFromToys["MCBasedSysAndSigModNormalized"]->getCovariance()->Write(m_variable_name+"_covMCBasedSysAndSigModNormalized");
  m_covFromToys["MCBasedStatAndSysAlternativeNormalized"]->getCovariance()->Write(m_variable_name+"_covMCBasedStatAndSysAlternativeNormalized");
  m_covFromToys["MCBasedStatSysAndSigModNormalized"]->getCovariance()->Write(m_variable_name+"_covMCBasedStatSysAndSigModNormalized");
  m_covFromToys["MCBasedStatSysAndSigModAlternativeNormalized"]->getCovariance()->Write(m_variable_name+"_covMCBasedStatSysAndSigModAlternativeNormalized");
  if(m_signalModelingLoaded){
    m_covMCBasedStatAndSysAndSignalModelingNormalized->Write(m_variable_name+"_covMCBasedAllUncNormalized");
    m_covMCBasedStatAndSysAlternativeAndSignalModelingNormalized->Write(m_variable_name+"_covMCBasedAllUncAlternativeNormalized");
  }
  
  m_covFromToys["DataBasedOnlyMultijetBkgStat"]->getCovariance()->Write(m_variable_name+"_covDataBasedOnlyMultijetBkgStat");
  m_covFromToys["DataBasedOnlyMultijetBkgStatNormalized"]->getCovariance()->Write(m_variable_name+"_covDataBasedOnlyMultijetBkgStatNormalized");
  m_covFromToys["DataBasedOnlyMCStatWithMultijetFixed"]->getCovariance()->Write(m_variable_name+"_covDataBasedOnlyMCStatWithMultijetFixed");
  m_covFromToys["DataBasedOnlyMCStatWithMultijetFixedNormalized"]->getCovariance()->Write(m_variable_name+"_covDataBasedOnlyMCStatWithMultijetFixedNormalized");
  
  m_covFromToys["EffOfRecoLevelOnlyStat"]->getCovariance()->Write(m_variable_name+"_covEffOfRecoLevelOnlyStat");
  m_covFromToys["EffOfRecoLevelOnlySys"]->getCovariance()->Write(m_variable_name+"_covEffOfRecoLevelOnlySys");
  m_covFromToys["EffOfRecoLevelStatAndSys"]->getCovariance()->Write(m_variable_name+"_covEffOfRecoLevelStatAndSys");
  m_covFromToys["EffOfTruthLevelOnlyStat"]->getCovariance()->Write(m_variable_name+"_covEffOfTruthLevelOnlyStat");
  m_covFromToys["EffOfTruthLevelOnlySys"]->getCovariance()->Write(m_variable_name+"_covEffOfTruthLevelOnlySys");
  m_covFromToys["EffOfTruthLevelStatAndSys"]->getCovariance()->Write(m_variable_name+"_covEffOfTruthLevelStatAndSys");
  
  m_covFromToys["MultijetBkgOnlyStat"]->getCovariance()->Write(m_variable_name+"_covMultijetBkgOnlyStat");
  m_covFromToys["MultijetBkgOnlySys"]->getCovariance()->Write(m_variable_name+"_covMultijetBkgOnlySys");
  m_covFromToys["MultijetBkgStatAndSys"]->getCovariance()->Write(m_variable_name+"_covMultijetBkgStatAndSys");
  
  
  m_signalModelingCovariances["Total"]->Write(m_variable_name+"_covDataBasedOnlySigModOriginal");
  m_signalModelingCovariances["ME"]->Write(m_variable_name+"_covDataBasedOnlyMEOriginal");
  m_signalModelingCovariances["PS"]->Write(m_variable_name+"_covDataBasedOnlyPSOriginal");
  m_signalModelingCovariances["FSR"]->Write(m_variable_name+"_covDataBasedOnlyIFSROriginal");
  *m_signalModelingCovariances["ME"]+=*m_signalModelingCovariances["PS"];
  m_signalModelingCovariances["ME"]->Write(m_variable_name+"_covDataBasedOnlyMEAndPSOriginal");
  
  m_signalModelingCovariances["MENormalized"]->Write(m_variable_name+"_covDataBasedOnlyMEOriginalNormalized");
  m_signalModelingCovariances["PSNormalized"]->Write(m_variable_name+"_covDataBasedOnlyPSOriginalNormalized");
  m_signalModelingCovariances["FSRNormalized"]->Write(m_variable_name+"_covDataBasedOnlyIFSROriginalNormalized");
  *m_signalModelingCovariances["MENormalized"]+=*m_signalModelingCovariances["PSNormalized"];
  m_signalModelingCovariances["MENormalized"]->Write(m_variable_name+"_covDataBasedOnlyMEAndPSOriginalNormalized");
  
  m_signalModelingCovariances["TotalNormalized"]->Write(m_variable_name+"_covDataBasedOnlySigModOriginalNormalized");
  m_signalModelingCovariances["TotalAlternative"]->Write(m_variable_name+"_covDataBasedOnlySigModOriginalAlternative");
  m_signalModelingCovariances["TotalAlternativeNormalized"]->Write(m_variable_name+"_covDataBasedOnlySigModOriginalAlternativeNormalized");
  
  //m_covStatAndSysAlternative.Print();
  //m_covStatAndSysAlternativeAndSignalModeling.Print();
  //m_covStatAndSysAlternativeNormalized.Print();
  //m_covStatAndSysAlternativeAndSignalModelingNormalized.Print();
}
void CovarianceCalculator::writeCentralValues(TFile* f){
  f->cd();
  m_centralValues->SetName(m_variable_name+"_centralValues");
  m_centralValues->Write(m_variable_name+"_centralValues");
  m_centralValues->SetName(m_variable_name+"_centralValuesNormalized");
  m_centralValuesNormalized->Write(m_variable_name+"_centralValuesNormalized");
}


