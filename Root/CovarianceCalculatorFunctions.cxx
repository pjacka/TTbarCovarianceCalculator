// Peter Berta, 15.10.2015

#include "TTbarCovarianceCalculator/CovarianceCalculatorFunctions.h"
#include <algorithm>

namespace functions{

   TH1D* UnfoldInvert(TH1D* h_data_reco,TH1D* h_signal_reco,TH1D* h_effOfTruthLevelCutsNominator,TH1D* h_signal_truth,TH1D* h_effOfRecoLevelCutsNominator,RooUnfoldResponse* response, int debug){
      TH1D* h=(TH1D*)h_data_reco->Clone();
      h->Multiply(h_effOfTruthLevelCutsNominator);
      h->Divide(h_signal_reco);

      //RooUnfoldResponse* response = new RooUnfoldResponse(0,0,h_migration);
      RooUnfoldInvert* rooUnfold = new RooUnfoldInvert();
      rooUnfold->SetResponse(response);
      rooUnfold->SetMeasured(h);
      rooUnfold->SetVerbose(debug);

      TH1D* h_unfolded = (TH1D*)rooUnfold->Hreco();
      h_unfolded->Divide(h_effOfRecoLevelCutsNominator);
      h_unfolded->Multiply(h_signal_truth);

      return h_unfolded;
   }

//----------------------------------------------------------------------

  TH1D* UnfoldBayes(TH1D* h_data_reco,TH1D* h_signal_reco,TH1D* h_signal_truth,TH2D* h_migration,int niter,int debug){
    RooUnfoldResponse* response = new RooUnfoldResponse(h_signal_reco,h_signal_truth,h_migration);
    //response->UseOverflow(true);
    RooUnfoldBayes unfold (response,h_data_reco,niter);
    unfold.SetVerbose(debug);
    return (TH1D*)unfold.Hreco();
  }
  
//----------------------------------------------------------------------
  
  TH1D* UnfoldBayes(TH1D* h_data_reco,TH1D* h_signal_reco,TH1D* h_effOfTruthLevelCutsNominator,TH1D* h_signal_truth,TH1D* h_effOfRecoLevelCutsNominator,RooUnfoldResponse* response,int niter,int debug){
    TH1D* h=(TH1D*)h_data_reco->Clone();
    h->Multiply(h_effOfTruthLevelCutsNominator);
    h->Divide(h_signal_reco);
    
    

    //RooUnfoldBayes* unfold = new RooUnfoldBayes(response,h,niter);
    RooUnfoldBayes* unfold = new RooUnfoldBayes();
    unfold->SetRegParm( niter );
    unfold->SetNToys( 1000 );
    
    unfold->SetResponse( response );
    unfold->SetMeasured( h );
    
    
    if( unfold->GetSmoothing() != 0 ) {
      //cout << "Warning in unfolding: Smoothing was " << unfold->GetSmoothing() << endl;
      unfold->SetSmoothing( 0 );
    }
    //unfold->SetNToys( 1000 );
  
    unfold->SetVerbose(debug);
    TH1D* h_unfolded=(TH1D*)unfold->Hreco();
    h_unfolded->Divide(h_effOfRecoLevelCutsNominator);
    h_unfolded->Multiply(h_signal_truth);
    
    //delete response;
    //delete h;
    
    return h_unfolded;
  }

//----------------------------------------------------------------------
  
  void UnfoldBayes(TH1D* h_result, TH1D* h_data_reco,TH1D* h_signal_reco,TH1D* h_effOfTruthLevelCutsNominator,TH1D* h_signal_truth,TH1D* h_effOfRecoLevelCutsNominator,MyUnfold* rooUnfold){
    h_data_reco->Multiply(h_effOfTruthLevelCutsNominator);
    h_data_reco->Divide(h_signal_reco);
    rooUnfold->SetMeasured( h_data_reco );
    if( rooUnfold->GetSmoothing() != 0 ) {
      //cout << "Warning in unfolding: Smoothing was " << unfold->GetSmoothing() << endl;
      rooUnfold->SetSmoothing( 0 );
    }
    rooUnfold->setIsUnfolded(false);
    TVectorD & vec = rooUnfold->Vreco();
    const int nbins=vec.GetNrows();
    for(int i=0;i<nbins;i++){
      h_result->SetBinContent(i+1,vec(i));
      //double denum=h_signal_truth->GetBinContent(i+1);
      //double num=h_effOfRecoLevelCutsNominator->GetBinContent(i+1);
      //if(denum>0 && num > 0) h_result->SetBinContent(i+1,vec(i)*denum/num);
    }
    h_result->Divide(h_effOfRecoLevelCutsNominator);
    h_result->Multiply(h_signal_truth);
  }

//----------------------------------------------------------------------  
  
  TH1D* UnfoldBayes(TH1D* h_data_reco,TH1D* h_effOfTruthLevelCuts,TH1D* h_effOfRecoLevelCuts,RooUnfoldResponse* response,int niter,int debug){
    TH1D* h=(TH1D*)h_data_reco->Clone();
    h->Multiply(h_effOfTruthLevelCuts);
    response->UseOverflow( false );
    RooUnfoldBayes* unfold = new RooUnfoldBayes();
    unfold->SetRegParm( niter );
    unfold->SetNToys( 1000 );
    unfold->SetResponse( response );
    unfold->SetMeasured( h );
    if( unfold->GetSmoothing() != 0 ) {
      //cout << "Warning in unfolding: Smoothing was " << unfold->GetSmoothing() << endl;
      unfold->SetSmoothing( 0 );
    }
    unfold->SetVerbose(debug);
    TH1D* h_unfolded=(TH1D*)unfold->Hreco();
    h_unfolded->Divide(h_effOfRecoLevelCuts);
    return h_unfolded;
  }


//----------------------------------------------------------------------

  TH1D* calculateRelativeShiftsForAbsoluteSpectra(const OneSysHistos& nominal, const OneSysHistos& shifted, const TString& option) {
   
    const bool printMessages=false;
   
    // Calculate shifted signal using formula: signal_shifted = signal_nominal + (pseudodata_shifted - pseudodata_nominal)
    // pseudodata = signal + background
    // psedodata_shifted = signal_shifted + background_shifted
    const int nbins=nominal.signal_truth->GetNbinsX();
    const TH1D* data_shifted = (TH1D*)shifted.data;
    const TH1D* data_nominal = (TH1D*)nominal.data;
    
    TH1D* signal_reco_shifted = (TH1D*)nominal.signal_reco->Clone(); // Starting from nominal signal reco distribution
    
    for(int ibin=1;ibin<=signal_reco_shifted->GetNbinsX();ibin++) {
      double relShiftData = 0.;
      if(data_shifted && data_nominal) {
	relShiftData=(data_shifted->GetBinContent(ibin) - data_nominal->GetBinContent(ibin))/data_nominal->GetBinContent(ibin);
      }
      
      double binContent = signal_reco_shifted->GetBinContent(ibin);
      double pseudodataShifted = shifted.pseudodata->GetBinContent(ibin)*(1.-relShiftData);
      double pseudodataNominal = nominal.pseudodata->GetBinContent(ibin);

      signal_reco_shifted->SetBinContent(ibin,binContent + pseudodataShifted - pseudodataNominal); // Shifting from nominal to sys variation
      
    }
    
    // Perform unfolding using standard setup. Unfolded spectrum is saved in hAbsolute histogram
    RooUnfoldResponse* response = new RooUnfoldResponse(0,0,nominal.migration);
    TH1D* hAbsolute = UnfoldBayes(signal_reco_shifted,nominal.signal_reco,nominal.effOfTruthLevelCutsNominator,nominal.signal_truth,nominal.effOfRecoLevelCutsNominator,response,4,0);
    if(printMessages) std::cout << "unfolded: " << hAbsolute->Integral() << " nominal: " << nominal.signal_truth->Integral() << " relative error:  " << (hAbsolute->Integral() - nominal.signal_truth->Integral())/nominal.signal_truth->Integral() << std::endl;
    //------------------------------------------------------
    
    // Calculating relative shift: The unfolded spectrum is compared with the truth spectrum
    
    double truthValue;
    
    
    
    for(int ibin=1;ibin<=nbins;ibin++){
      
      // Truth value depends on if we calculate detector level systematic or signal modeling shifts
      if(option=="detector") { // Detector level systematics: Uncertainties related with calibration of objects
	truthValue=nominal.signal_truth->GetBinContent(ibin);
      }
      else { // Signal modeling systematics: Parton-showering, matrix element, PDF, IFSR, FSR
	truthValue=shifted.signal_truth->GetBinContent(ibin);
      }
      double shiftedValue=hAbsolute->GetBinContent(ibin);
      
      if (printMessages) std::cout << "           " << "functions::calculateRelativeShiftsForAbsoluteSpectra: " << ibin << " "  << shiftedValue << " " << truthValue << " " << (shiftedValue - truthValue)/truthValue << std::endl;
      
      // Value of hAbsolute is overwritten by relative shift
      if      (truthValue>0) hAbsolute->SetBinContent(ibin,(shiftedValue - truthValue)/truthValue);
      else if (shiftedValue) hAbsolute->SetBinContent(ibin,1.);
      else                   hAbsolute->SetBinContent(ibin,0.);
    }
    // Cleaning memory
    delete signal_reco_shifted;
    delete response;
    
    return hAbsolute;
  }

//----------------------------------------------------------------------
  
  TH1D* calculateRelativeShiftsForRelativeSpectra( const OneSysHistos& nominal, const OneSysHistos& shifted, const TString& option){
    
    const bool printMessages=false;
   
    // Calculate shifted signal using formula: signal_shifted = signal_nominal + (pseudodata_shifted - pseudodata_nominal)
    // pseudodata = signal + background
    // psedodata_shifted = signal_shifted + background_shifted
    const int nbins=nominal.signal_truth->GetNbinsX();
    const TH1D* data_shifted = (TH1D*)shifted.data;
    const TH1D* data_nominal = (TH1D*)nominal.data;
    
    TH1D* signal_reco_shifted = (TH1D*)nominal.signal_reco->Clone(); // Starting from nominal signal reco distribution
    
    for(int ibin=1;ibin<=signal_reco_shifted->GetNbinsX();ibin++) {
      double relShiftData = 0.;
      if(data_shifted && data_nominal) {
	relShiftData=(data_shifted->GetBinContent(ibin) - data_nominal->GetBinContent(ibin))/data_nominal->GetBinContent(ibin);
      }
      
      double binContent = signal_reco_shifted->GetBinContent(ibin);
      double pseudodataShifted = shifted.pseudodata->GetBinContent(ibin)*(1.-relShiftData);
      double pseudodataNominal = nominal.pseudodata->GetBinContent(ibin);

      signal_reco_shifted->SetBinContent(ibin,binContent + pseudodataShifted - pseudodataNominal); // Shifting from nominal to sys variation
      
    }
    
    // Perform unfolding using standard setup. Unfolded spectrum is saved in hRelative histogram
    RooUnfoldResponse* response = new RooUnfoldResponse(0,0,nominal.migration);
    TH1D* hRelative = UnfoldBayes(signal_reco_shifted,nominal.signal_reco,nominal.effOfTruthLevelCutsNominator,nominal.signal_truth,nominal.effOfRecoLevelCutsNominator,response,4,0);
    hRelative->Scale(1./hRelative->Integral()); // Spectrum is normalized to unity
    //------------------------------------------------------
    
    // Calculating relative shift: The unfolded spectrum is compared with the truth spectrum
    const int integral=nominal.signal_truth->Integral();
    const int shifted_integral=shifted.signal_truth->Integral();
    
    double truthValue;
    for(int ibin=1;ibin<=nbins;ibin++){
      // Truth value depends on if we calculate detector level systematic or signal modeling shifts
      if(option=="detector") { // Detector level systematics: Uncertainties related with calibration of objects
	truthValue=nominal.signal_truth->GetBinContent(ibin)/integral;
      }
      else { // Signal modeling systematics: Parton-showering, matrix element, PDF, IFSR, FSR
	truthValue=shifted.signal_truth->GetBinContent(ibin)/shifted_integral;
      }
      double shiftedValue=hRelative->GetBinContent(ibin);
      if (printMessages) cout << "           " << shiftedValue << " " << truthValue << " " << (shiftedValue - truthValue)/truthValue << endl;
      
      // Value of hRelative is overwritten by relative shift
      if      (truthValue>0) hRelative->SetBinContent(ibin,(shiftedValue - truthValue)/truthValue);
      else if (shiftedValue) hRelative->SetBinContent(ibin,1.);
      else                  hRelative->SetBinContent(ibin,0.);
      
    }
    
    // Cleaning memory
    delete signal_reco_shifted;
    delete response;
    
    return hRelative;
  }
 
} // namespace functions




