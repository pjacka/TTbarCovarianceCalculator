Table of Contents
=========================

**[Description](#description)**

**[Installation](#installation)**

**[User guide](#user-guide)**



# Authors : 
- Petr Jacka <petr.jacka@cern.ch>



Description
=======================
This package is a framework for propagation systematic and statistical uncertainties through unfolding procedure. It is used in TTbar differential cross-section measurement.


## Installation

This branch should be used together with TtbarDiffCrossSection package, branch rel21. Please follow instructions in

https://gitlab.cern.ch/pjacka/TtbarDiffCrossSection/tree/Rel21

to install the environment.

## User guide

Under construction

### Loading input histograms

Histograms corresponding to one source of systematic uncertainty are loaded in the [OneSysHistos](https://gitlab.cern.ch/pjacka/TTbarCovarianceCalculator/blob/Rel21/Root/OneSysHistos.cxx) class. 

The [SystematicHistos](https://gitlab.cern.ch/pjacka/TTbarCovarianceCalculator/blob/Rel21/Root/SystematicHistos.cxx) class contains a list of OneSysHistos classes corresponding to 
all sources of systematic uncertainties including signal modeling. It also provides possibility to calculate relative shifts at reco level corresponding to shifts one sigma up and down. 
Relative shifts can be stored also for the signal modeling uncertainties. These shifts are used in pseudo-experiments (toys). 

### Running pseudo-experiments

Pseudo-experiments (toys) are random fluctuations of input histograms according to their statistical and systematic uncertainties. Shift corresponding to one pseudo-experiment are prepared in the
[PseudoexperimentsCalculator](https://gitlab.cern.ch/pjacka/TTbarCovarianceCalculator/blob/Rel21/Root/PseudoexperimentsCalculator.cxx) class and saved in the [Pseudoexperiment](https://gitlab.cern.ch/pjacka/TTbarCovarianceCalculator/blob/Rel21/Root/Pseudoexperiment.cxx). Then, Pseudoexperiment is passed into the [CovarianceCalculator](https://gitlab.cern.ch/pjacka/TTbarCovarianceCalculator/blob/Rel21/Root/CovarianceCalculator.cxx). The CovarianceCalculator handles unfolding of the pseudoexperiment and it assigns post-unfolding shifts to the unfolded spectra. Fluctuating pseudo-experiments are collected in order to calculate the total covariance matrix and covariances corresponding to different kinds of uncertainties.
Example: [ProducePseudoexperiments](https://gitlab.cern.ch/pjacka/TtbarDiffCrossSection/blob/Rel21/util/ProducePseudoexperiments.cxx)

### Plots and tables with uncertainties

Tables and plots with systematic uncertainties are created with the [AsymmetricErrorsCalculator](https://gitlab.cern.ch/pjacka/TTbarCovarianceCalculator/blob/Rel21/Root/AsymmetricErrorsCalculator.cxx). 
It creates tables with a full breakdown of systematic uncertainties and with summarized uncertainties. Plots are made for several categories of systematic uncertainties. 
Example: [CalculateAsymmetricErrors](https://gitlab.cern.ch/pjacka/TtbarDiffCrossSection/blob/Rel21/util/CalculateAsymmetricErrors.cxx)



