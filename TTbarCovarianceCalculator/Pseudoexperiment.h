#ifndef Pseudoexperiment_H
#define Pseudoexperiment_H

#include "TLorentzVector.h"
#include <vector>


#include <vector>
#include <map>
#include <unordered_map>
#include <string>
#include <sstream>
#include <iostream>
#include <fstream>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <iomanip>      // std::setprecision

#include "TString.h"
#include "TH1D.h"
#include "TH2D.h"
#include "TCanvas.h"
#include "TFile.h"
#include "TAxis.h"
#include "TGaxis.h"
#include "TSystem.h"
#include "TLatex.h"
#include "TLegend.h"
#include "TProfile.h"
#include "TStyle.h"
#include "TROOT.h"
#include "THStack.h"
#include "TGraphAsymmErrors.h"
#include "TMatrixT.h"
#include "TLegend.h"
#include "TPad.h"
#include "TEnv.h"
#include "TRandom.h"
#include "TRandom3.h"

#include "TTbarHelperFunctions/TtbarDiffCrossSection_exception.h"
#include "TTbarCovarianceCalculator/PseudoexperimentsCalculator.h"


#include "../RooUnfold/src/RooUnfoldResponse.h"
#include "../RooUnfold/src/RooUnfoldBayes.h"
#include "../RooUnfold/src/RooUnfoldBinByBin.h"
#include "../RooUnfold/src/RooUnfoldSvd.h"
#include "../RooUnfold/src/RooUnfoldInvert.h"

using namespace std;
class PseudoexperimentsCalculator;

class Pseudoexperiment{
	friend class PseudoexperimentsCalculator;
	friend class CovarianceCalculator;
protected:
	TH1D* m_dataOnlyStat;
	TH1D* m_dataOnlyMCStat;
	TH1D* m_dataOnlySys;
	TH1D* m_dataStatAndSys;
	TH1D* m_dataStatMCStatAndSys;
	TH1D* m_signalRecoOnlyStat;
	TH1D* m_signalRecoOnlySys;
	TH1D* m_signalRecoStatAndSys;
	TH1D* m_bkgMCOnlyStat;
	TH1D* m_bkgMCOnlySys;
	TH1D* m_bkgMCStatAndSys;
	vector<TH1D*> m_ABCDInputsMCOnlyStat;
	vector<TH1D*> m_ABCDInputsMCOnlySys;
	vector<TH1D*> m_ABCDInputsMCStatAndSys;
	vector<TH1D*> m_ABCDInputsDataOnlyStat;
	vector<TH1D*> m_ABCDInputsDataOnlySys;
	vector<TH1D*> m_ABCDInputsDataStatAndSys;
	TH1D* m_bkgMultijetOnlyStat;
	TH1D* m_bkgMultijetOnlySys;
	TH1D* m_bkgMultijetStatAndSys;
	TH1D* m_bkgAllOnlyStat;
	TH1D* m_bkgAllOnlySys;
	TH1D* m_bkgAllStatAndSys;
	TH1D* m_pseudodataMCStat;
	TH1D* m_pseudodataDataStat;
	TH1D* m_pseudodataSys;
	TH1D* m_pseudodataMCStatAndSys;
	TH1D* m_pseudodataDataStatAndSys;
	TH1D* m_pseudodataDataStatMCStatAndSys;
	
	TH2D* m_migrationOnlyStat;
	TH2D* m_migrationOnlySys;
	TH2D* m_migrationStatAndSys;
	TH1D* m_signalTruthOnlyStat;
	TH1D* m_signalTruthOnlySys;
	TH1D* m_signalTruthStatAndSys;
	TH1D* m_effOfRecoLevelCutsNominatorOnlyStat;
	TH1D* m_effOfRecoLevelCutsNominatorOnlySys;
	TH1D* m_effOfRecoLevelCutsNominatorStatAndSys;
	TH1D* m_effOfTruthLevelCutsNominatorOnlyStat;
	TH1D* m_effOfTruthLevelCutsNominatorOnlySys;
	TH1D* m_effOfTruthLevelCutsNominatorStatAndSys;
	
	TH1D* m_histLumi;
	
	vector<double> m_signalModelingShifts;
	vector<double> m_signalModelingShiftsAlternative;
	
	int m_nbins_reco;
	int m_nbins_truth;
	const TString m_gap;
	TString m_variable_name;
	double m_lumi;
	
public:
	virtual ~Pseudoexperiment () = default;
	Pseudoexperiment (const Pseudoexperiment&) = default;
	Pseudoexperiment& operator= (const Pseudoexperiment&) = default;
	Pseudoexperiment (Pseudoexperiment&&) = default;
	Pseudoexperiment& operator= (Pseudoexperiment&&) = default;
	Pseudoexperiment();
	//Pseudoexperiment(HistStore* histStore);
	Pseudoexperiment(PseudoexperimentsCalculator* psedudoexperimentCalculator);
	Pseudoexperiment(TFile* f, TString experiment_name, TString variable_name);
	//void createEmptyHistos(HistStore* histStore);
	void createEmptyHistos(PseudoexperimentsCalculator* psedudoexperimentCalculator);
	void prepareSignalMCStat(TH2D* migrationNominal, TH1D* effOfRecoLevelCutsNominatorNominal ,TH1D* signalRecoNominal, TH1D* effOfTruthLevelCutsNominatorNominal,TH1D* signalTruthNominal);
	void prepareBkgMCStat(TH1D* bkgMCNominal);
	void prepareABCDInputsStat(vector<TH1D*>& data16RegionsNominal,vector<TH1D*>& MC16RegionsNominal);
	void prepareDataStat(TH1D* data);
	
	//void prepareSysHistos(HistStore* histStore);
	
	//void preparePseudodata(HistStore* histStore);
	
	//void prepareNominalHistos(HistStore* histStore);
	
	
	void writePseudoexperiment(TFile*f,TString foldername);
	ClassDef(Pseudoexperiment,1)

};





#endif

