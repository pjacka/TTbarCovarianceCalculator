#ifndef GeneratorSystematics_H
#define GeneratorSystematics_H

#include "TLorentzVector.h"
#include <vector>


#include <vector>
#include <map>
#include <unordered_map>
#include <string>
#include <sstream>
#include <iostream>
#include <fstream>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <iomanip>      // std::setprecision

#include "TString.h"
#include "TH1D.h"
#include "TH2D.h"
#include "TCanvas.h"
#include "TFile.h"
#include "TAxis.h"
#include "TGaxis.h"
#include "TSystem.h"
#include "TLatex.h"
#include "TLegend.h"
#include "TProfile.h"
#include "TStyle.h"
#include "TROOT.h"
#include "THStack.h"
#include "TGraphAsymmErrors.h"
#include "TMatrixT.h"
#include "TLegend.h"
#include "TPad.h"
#include "TEnv.h"
#include "TRandom.h"
#include "TRandom3.h"

#include "TTbarHelperFunctions/TtbarDiffCrossSection_exception.h"
#include "TTbarCovarianceCalculator/Signal_histos.h"
#include "TTbarCovarianceCalculator/GeneratorSystematicsBase.h"



#include "../RooUnfold/src/RooUnfoldResponse.h"
#include "../RooUnfold/src/RooUnfoldBayes.h"
#include "../RooUnfold/src/RooUnfoldBinByBin.h"
#include "../RooUnfold/src/RooUnfoldSvd.h"
#include "../RooUnfold/src/RooUnfoldInvert.h"


using namespace std;


class GeneratorSystematics : public GeneratorSystematicsBase {
 public:
	GeneratorSystematics(){}
	virtual ~GeneratorSystematics () = default;
	GeneratorSystematics (const GeneratorSystematics&) = default;
	GeneratorSystematics& operator= (const GeneratorSystematics&) = default;
	GeneratorSystematics (GeneratorSystematics&&) = default;
	GeneratorSystematics& operator= (GeneratorSystematics&&) = default;
	//GeneratorSystematics(TString mainDir,TString path_config_files,TString path_config_lumi,TString signal);
	void drawTruthHistComparison();
	void drawRecoLevelHistComparison();
	void drawUnfoldedHistComparison();
	void drawErrorsComparison();
	void drawRecoCorrectionsComparison();
	void drawTruthCorrectionsComparison();
	void drawMigrationMatrices();
	void drawNormalizedErrorsComparison();
	void drawNormalizedTruthHistComparison();
	void drawNormalizedRecoLevelHistComparison();
	void drawNormalizedUnfoldedHistComparison();
	void drawHistComparison(TH1D* nominal, vector<TH1D*>& hist,TLegend* leg,TString dirname,TString name, TString y2name, double y2min, double y2max, TString lumi="",bool use_logscale=0);
	void drawHistComparison(TH1D* nominal, unordered_map<string,TH1D*>& hist,TLegend* leg,TString dirname,TString name, TString y2name, double y2min, double y2max, TString lumi="",bool use_logscale=0);
	
	
 protected:
 
	
	ClassDef(GeneratorSystematics,1)
};




#endif
