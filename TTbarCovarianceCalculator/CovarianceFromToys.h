#ifndef CovarianceFromToys_H
#define CovarianceFromToys_H

#include "TLorentzVector.h"
#include <vector>


#include <vector>
#include <map>
#include <unordered_map>
#include <string>
#include <sstream>
#include <iostream>
#include <fstream>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <iomanip>      // std::setprecision

#include "TString.h"
#include "TH1D.h"
#include "TH2D.h"
#include "TCanvas.h"
#include "TFile.h"
#include "TAxis.h"
#include "TGaxis.h"
#include "TSystem.h"
#include "TLatex.h"
#include "TLegend.h"
#include "TProfile.h"
#include "TStyle.h"
#include "TROOT.h"
#include "THStack.h"
#include "TGraphAsymmErrors.h"
#include "TMatrixT.h"
#include "TLegend.h"
#include "TPad.h"
#include "TEnv.h"
#include "TRandom.h"
#include "TRandom3.h"
#include "TPrincipal.h"

#include "TTbarHelperFunctions/TtbarDiffCrossSection_exception.h"

class Pseudoexperiment;

class CovarianceFromToys {
private:
  std::shared_ptr<TMatrixT<double> > m_cov;
  std::vector<double> m_mean;
  int m_counter;
  const int m_nrows;
public:
  CovarianceFromToys(const int nrows);
  virtual ~CovarianceFromToys () = default;
  CovarianceFromToys (const CovarianceFromToys&) = default;
  CovarianceFromToys& operator= (const CovarianceFromToys&) = default;
  CovarianceFromToys (CovarianceFromToys&&) = default;
  CovarianceFromToys& operator= (CovarianceFromToys&&) = default;
  
  void addToy(TH1* h);
  void finalize();
  void printMeanAndRelUnc() const;
  inline std::shared_ptr<TMatrixT<double> > getCovariance(){return m_cov;}
  inline const std::vector<double>& getMean() const {return m_mean;}
  ClassDef(CovarianceFromToys,1)
};






#endif
