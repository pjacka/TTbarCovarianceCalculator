#ifndef Chi2TestMaker_H
#define Chi2TestMaker_H

#include <vector>
#include <memory>
#include "TVectorT.h"
#include "TMatrixT.h"

class TH1;

class Chi2TestMaker {
private:
  std::unique_ptr<TMatrixT<double> > m_cov1;
  std::unique_ptr<TMatrixT<double> > m_cov2;
  std::unique_ptr<TMatrixT<double> > m_distr1;
  std::unique_ptr<TMatrixT<double> > m_distr2;
  std::unique_ptr<TMatrixT<double> > m_inverseCov;
  double m_chi2;
  int m_ndf;
  double m_pValue;
public:

  virtual ~Chi2TestMaker () = default;
  Chi2TestMaker (const Chi2TestMaker&) = default;
  Chi2TestMaker& operator= (const Chi2TestMaker&) = default;
  Chi2TestMaker (Chi2TestMaker&&) = default;
  Chi2TestMaker& operator= (Chi2TestMaker&&) = default;
  Chi2TestMaker() = default;
  
  bool checkSetup() const;
  void calculateInverseCov();
  void removeBin(const int index);
  void chi2Test();
  
  void setDistr1(const std::vector<double>& distr);
  void setDistr1(const TVectorD& distr);
  void setDistr1(const TH1* distr);
  void setDistr1(const TMatrixT<double>& distr);
  
  void setDistr2(const std::vector<double>& distr);
  void setDistr2(const TVectorD& distr);
  void setDistr2(const TH1* distr);
  void setDistr2(const TMatrixT<double>& distr);
  
  inline void setCov1(const TMatrixT<double>& cov) {m_cov1=std::make_unique<TMatrixT<double>>(cov);}
  inline void setCov2(const TMatrixT<double>& cov) {m_cov2=std::make_unique<TMatrixT<double>>(cov);}
  
  inline const TMatrixT<double>* getDistr1() const {return m_distr1.get();} 
  inline const TMatrixT<double>* getDistr2() const {return m_distr2.get();} 
  inline const TMatrixT<double>* getCov1() const {return m_cov1.get();} 
  inline const TMatrixT<double>* getCov2() const {return m_cov2.get();} 
  
  inline TMatrixT<double> getInverse() const {return *m_inverseCov;}
  inline double getPValue() const {return m_pValue;}
  inline int getNDF() const {return m_ndf;}
  inline double getChi2() const {return m_chi2;}
  ClassDef(Chi2TestMaker,1)
};

#endif
