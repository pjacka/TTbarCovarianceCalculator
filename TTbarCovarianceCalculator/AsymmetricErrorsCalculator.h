#ifndef AsymmetricErrorsCalculator_H
#define AsymmetricErrorsCalculator_H

#include "TLorentzVector.h"

#include <vector>
#include <map>
#include <unordered_map>
#include <string>
#include <sstream>
#include <iostream>
#include <fstream>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <iomanip>      // std::setprecision

#include "TString.h"
#include "TH1D.h"
#include "TH2D.h"
#include "TCanvas.h"
#include "TFile.h"
#include "TAxis.h"
#include "TGaxis.h"
#include "TSystem.h"
#include "TLatex.h"
#include "TLegend.h"
#include "TProfile.h"
#include "TStyle.h"
#include "TROOT.h"
#include "THStack.h"
#include "TGraphAsymmErrors.h"
#include "TMatrixT.h"
#include "TLegend.h"
#include "TPad.h"
#include "TEnv.h"
#include "TRandom.h"
#include "TRandom3.h"

#include "TTbarHelperFunctions/TTBarConfig.h"
#include "TTbarHelperFunctions/TtbarDiffCrossSection_exception.h"
#include "TTbarHelperFunctions/MultiDimensionalPlotsSettings.h"
#include "TTbarHelperFunctions/HistogramNDto1DConverter.h"

#include "TTbarCovarianceCalculator/OneSysHistos.h"
#include "TTbarCovarianceCalculator/SystematicHistos.h"

#include "../RooUnfold/src/RooUnfoldResponse.h"
#include "../RooUnfold/src/RooUnfoldBayes.h"
#include "../RooUnfold/src/RooUnfoldBinByBin.h"
#include "../RooUnfold/src/RooUnfoldSvd.h"
#include "../RooUnfold/src/RooUnfoldInvert.h"

class SystematicHistos;

class AsymmetricErrorsCalculator {
  int m_debug;
  TString m_variable,m_level;
  TString m_mainDir;
  double m_lumi;
  TString m_lumi_string;
  TH1D* m_hist_template; // histogram with correct binning and empty bins  
  TH1D* m_hist_template_truth; // histogram with correct binning and empty bins  
  TEnv *m_config_sys_names;
  std::unique_ptr<TTBarConfig> m_ttbarConfig;
  SystematicHistos* m_sysHistos;
  int m_nbins_reco,m_nbins_truth;
  int m_nsys_all,m_nsys_pairs,m_nsys_single,m_nsys_pdf,m_nsys_modeling;
  
  bool m_doSignalModeling;
  TString m_mc_samples_production;
  
  std::vector<TString> m_sys_chain_names_all, m_sys_chain_names_single, m_sys_chain_names_pdf, m_sys_chain_names_modeling;
  std::vector<std::pair<TString,TString> > m_sys_chain_names_paired;
  
  std::vector<TString> m_summaryNames;
  
  bool m_useNewISR;
  
  std::unique_ptr<MultiDimensionalPlotsSettings> m_plotSettingsND;
  std::unique_ptr<HistogramNDto1DConverter> m_histogramConverterND;

  std::vector<std::pair<std::vector<double>,std::vector<double> > > m_sys_uncertainties_paired;
  std::vector<std::vector<double> > m_sys_uncertainties_single;
  std::vector<std::vector<double> > m_sys_uncertainties_pdf;
  std::vector<std::pair<std::vector<double>,std::vector<double> > > m_sys_uncertainties_normalized_paired;
  std::vector<std::vector<double> > m_sys_uncertainties_normalized_single;
  std::vector<std::vector<double> > m_sys_uncertainties_normalized_pdf;
  
  std::vector<double> m_total_uncertainties;
  std::vector<double> m_total_uncertainties_normalized;
  std::vector<double> m_sys_uncertainties_total;
  std::vector<double> m_sys_uncertainties_normalized_total;
  std::vector<double> m_sys_uncertainties_stat_combined;
  std::vector<double> m_sys_uncertainties_normalized_stat_combined;
  std::vector<double> m_sys_uncertainties_stat;
  std::vector<double> m_sys_uncertainties_normalized_stat;
  std::vector<double> m_sys_uncertainties_MCstat;
  std::vector<double> m_sys_uncertainties_normalized_MCstat;
  std::vector<double> m_sys_uncertainties_MCstatWithMultijetFixed;
  std::vector<double> m_sys_uncertainties_normalized_MCstatWithMultijetFixed;
  std::vector<double> m_sys_uncertainties_multijetBkg_stat;
  std::vector<double> m_sys_uncertainties_normalized_multijetBkg_stat;
  
  std::vector<double> m_central_values,m_central_values_normalized;
  
  std::map<TString,std::vector<double> > m_sys_uncertainties_signal_modeling;
  std::map<TString,std::vector<double> > m_sys_uncertainties_normalized_signal_modeling;
  
  std::pair<std::vector<double>,std::vector<double> > m_sys_uncertainties_bkg_all;
  std::pair<std::vector<double>,std::vector<double> > m_sys_uncertainties_detector_all;
  std::pair<std::vector<double>,std::vector<double> > m_sys_uncertainties_bkg_all_normalized;
  std::pair<std::vector<double>,std::vector<double> > m_sys_uncertainties_detector_all_normalized;
  
  std::map<TString,std::pair<std::vector<double>,std::vector<double> > > m_uncertainties_for_summary_table_absolute;
  std::map<TString,std::pair<std::vector<double>,std::vector<double> > > m_uncertainties_for_summary_table_relative;

  std::map<TString,TH1D*> m_hist_uncertainties_absolute;
  std::map<TString,TH1D*> m_hist_uncertainties_relative;
  
  std::vector<double> m_binning2D_x,m_binning2D_truth_x;
  std::vector<std::vector<double> > m_binning2D_y, m_binning2D_truth_y;
  bool m_is1D;
  
  int m_irowAbsolute;
  int m_irowRelative;
  double m_printUncertaintyLowThreshold;
  
  
  public:
  
  virtual ~AsymmetricErrorsCalculator () = default;
  AsymmetricErrorsCalculator (const AsymmetricErrorsCalculator&) = default;
  AsymmetricErrorsCalculator& operator= (const AsymmetricErrorsCalculator&) = default;
  AsymmetricErrorsCalculator (AsymmetricErrorsCalculator&&) = default;
  AsymmetricErrorsCalculator& operator= (AsymmetricErrorsCalculator&&) = default;
  
  AsymmetricErrorsCalculator(){};
  AsymmetricErrorsCalculator(SystematicHistos*,TString config_lumi,TString config_sysnames,TString mainDir,TString variable,TString level,int debugLevel, bool is1D=true);
  void init(SystematicHistos*,TString config_lumi,TString config_sysnames,TString mainDir,TString variable,TString level,int debugLevel,bool is1D=true);
  
  void initializeLatexTable(ofstream& textfile);
  void printBinningInfoIntoLatexTable(ofstream& textfile,const TString& unit);
  void printCentralValuesIntoLatexTable(ofstream& textfile,const std::vector<double>& centralValues,const TString& crossSection,const int& precision);
  void printOveralUncertaintiesIntoLatexTable(ofstream& textfile, const std::map<TString,std::pair<std::vector<double>,std::vector<double> > >& uncertainties_for_summary_table, const std::vector<double>& sys_uncertainties_stat,int& irow );
  void printOveralUncertaintiesIntoLatexTable(ofstream& textfile, const std::vector<double>& uncertainties_total, const std::vector<double>& uncertainties_sys, const std::vector<double>& uncertainties_stat,int& irow );
  TString getCategory(const TString& name);
  void printPairedSystematicUncertaintiesIntoLatexTable(ofstream& textfileAbsolute,ofstream& textfileNormalized);
  void printOneSidedSystematicUncertaintiesIntoLatexTable(ofstream& textfileAbsolute,ofstream& textfileNormalized);
  void printSignalModelingUncertaintiesIntoLatexTable(ofstream& textfileAbsolute,ofstream& textfileNormalized);
  void finalizeLatexTable(ofstream& textfile);
  
  void loadStatisticalUncertainties(TFile* f);
  void load2Dbinning(TFile* f);
  void loadNDbinning(const TString& path_config_binning);
  void loadMultiDPlotsSettings(const TString& path);
 
  void initializeSummaryTables();
  void initializeUncertaintiesVectors();
 
  void calculateSystematicsPaired();
  void calculateSystematicsSingle();
  void calculateSystematicsSignalModeling();
  void calculateOneSystematicsPaired(std::vector<double>& errup,std::vector<double>& errdown,TH1D* shift_up, TH1D* shift_down);
  void calculateOneSystematicsSingle(std::vector<double>& err,TH1D* shift);
  void finalizeSummaryUncertainties();
  
  
  
  void printDetailedTablesOfUncertainties(ofstream& textfileAbsolute,ofstream& textfileNormalized);
  void printSummaryTables(ofstream& textfileSummaryAbsolute,ofstream& textfileSummaryNormalized);
  void printOneUncertainty(ofstream& textfile,TString name,TString category,const std::vector<double>& errup,const std::vector<double>& errdown,bool useColor=false);
  void printOneUncertaintySymmetrized(ofstream& textfile,TString name,TString category,const std::vector<double>& err,bool useColor=false);
  void printOneUncertaintyWithAbsoluteValues(ofstream& textfile,TString name,TString category,const std::vector<double>& errup,const std::vector<double>& errdown,bool useColor=false);
  void prepareSysUncertainties();
  TH1D* histFromSummaryUncertainties(const std::pair<std::vector<double>,std::vector<double> >& uncertainties);
  TH1D* histFromSummaryUncertainties(const std::vector<double>& uncertainties);
  
  void plotUncertainties(const TString& foldername,std::map<TString,TH1D*>& histos,TString crossSection);
  void plotUncertainties(const TString& foldername);
  
  void plotNDhistograms(std::vector<TH1D*>& histos, TLegend* leg, const TString& dirname, const TString& figurename, const TString& info1, const TString& info2) const;
  
  
  const TString getSummaryName(const TString& sysName);
  
  ClassDef(AsymmetricErrorsCalculator,1)
};


#endif
