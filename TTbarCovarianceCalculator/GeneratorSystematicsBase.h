#ifndef GeneratorSystematicsBase_H
#define GeneratorSystematicsBase_H

#include "TLorentzVector.h"
#include <vector>


#include <vector>
#include <map>
#include <unordered_map>
#include <string>
#include <sstream>
#include <iostream>
#include <fstream>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <iomanip>      // std::setprecision

#include "TString.h"
#include "TH1D.h"
#include "TH2D.h"
#include "TCanvas.h"
#include "TFile.h"
#include "TAxis.h"
#include "TGaxis.h"
#include "TSystem.h"
#include "TLatex.h"
#include "TLegend.h"
#include "TProfile.h"
#include "TStyle.h"
#include "TROOT.h"
#include "THStack.h"
#include "TGraphAsymmErrors.h"
#include "TMatrixT.h"
#include "TLegend.h"
#include "TPad.h"
#include "TEnv.h"
#include "TRandom.h"
#include "TRandom3.h"

#include "TTbarHelperFunctions/TTBarConfig.h"
#include "TTbarHelperFunctions/TtbarDiffCrossSection_exception.h"
#include "TTbarCovarianceCalculator/Signal_histos.h"


#include "../RooUnfold/src/RooUnfoldResponse.h"
#include "../RooUnfold/src/RooUnfoldBayes.h"
#include "../RooUnfold/src/RooUnfoldBinByBin.h"
#include "../RooUnfold/src/RooUnfoldSvd.h"
#include "../RooUnfold/src/RooUnfoldInvert.h"


using namespace std;



class GeneratorSystematicsBase {
 public:
	GeneratorSystematicsBase(){}
	virtual ~GeneratorSystematicsBase () = default;
	GeneratorSystematicsBase (const GeneratorSystematicsBase&) = default;
	GeneratorSystematicsBase& operator= (const GeneratorSystematicsBase&) = default;
	GeneratorSystematicsBase (GeneratorSystematicsBase&&) = default;
	GeneratorSystematicsBase& operator= (GeneratorSystematicsBase&&) = default;
	
	//GeneratorSystematicsBase(TString mainDir,TString path_config_files,TString path_config_lumi,TString signal);
	void init(const TString& mainDir,const TString& inputdir,const TString& outputdir,const TString& path_config_lumi,const TString& signal,bool doSysPDF,int debugLevel,bool is2D=false);
	void LoadHistos(const TString& variable,const TString& Level);
	void LoadDataHistos(const TString& variable,const TString& Level);
	void RebinHistos(int nbinsx,Double_t* binsx,int nbinsy,Double_t* binsy);
	
	TH1D* CalculateSysErrors(Signal_histos* shifted,Signal_histos* nominal,TH1D* unfolded_data,TMatrixT<double>& cov,const bool useMultijet=false);
	TH1D* CalculateIFSRSysErrors(Signal_histos* histos_up,Signal_histos* histos_down,Signal_histos* nominal,TH1D* unfolded_data,TMatrixT<double>& cov,const bool useMultijet=false,const bool alternative=false);
	TH1D* CalculateNormalizedSysErrors(Signal_histos* shifted,Signal_histos* nominal,TH1D* unfolded_data,TMatrixT<double>& cov,const bool useMultijet=false);
	TH1D* CalculateNormalizedIFSRSysErrors(Signal_histos* histos_up,Signal_histos* histos_down,Signal_histos* nominal,TH1D* unfolded_data,TMatrixT<double>& cov,const bool useMultijet=false,const bool alternative=false);

	void CalculateTotalCovariances();
	void writeCovariances();

	
	TH1D* Unfold(TH1D*,Signal_histos*);
	void CalculateSysErrors();
	void CalculateIFSRSysErrors();
	void CalculatePartonShowerSysErrors();
	void CalculateMatrixElementSysErrors();
	
	void CalculatePdfSysErrors();
	
	
	void DeleteObjects();
	void prepareHistos();
	void prepareReplicas(int nPseudoExperiments,bool shiftNominal=true);
	void runPseudoexperiments(vector<vector<double> >& average,vector<vector<double> >& errors,vector<vector<double> >& average_normalized,vector<vector<double> >& errors_normalized);
	void runPseudoexperiments();
	void deleteReplicas();
	void calculateCorrelationMatrices();
	
	void prepareListOfPdfChains();
	
	void printUncertainties();
	
 protected:
	std::unique_ptr<TTBarConfig> m_ttbarConfig;
	int m_debug;
	TString m_inputdir,m_outputdir;
	TString m_dirname,m_variable_name;
	TString m_mainDir,m_signal;
	TFile* m_file_signal_nominal;
	Signal_histos* m_nominal_histos;
	Signal_histos* m_MECOffSample_histos;

	int m_ngenSystematics_histos;
	unordered_map<string,TFile*> m_genSystematic_files;
	unordered_map<string,Signal_histos*> m_genSystematic_histos;
	unordered_map<string,TH1D*> m_unfolded_genSystematic_histos;
	unordered_map<string,TH1D*> m_genSystematic_errors;
	unordered_map<string,TH1D*> m_genSystematic_errors_normalized;
	unordered_map<string,TString> m_foldernames;
	vector<string> m_accessors,m_rootfilenames;
	vector<int> m_colors;
	vector<int> m_lineStyles;
	bool m_useNewISR;
	
	unordered_map<string,TString> m_legend_entries;
	TString m_signal_legend_entry;
	
	unordered_map<string,TMatrixT<double>> m_covariances,m_correlations;
	TMatrixT<double> m_totalCovariance,m_totalCorrelation,m_cov_FSR,m_cor_FSR,m_cov_ME,m_cor_ME,m_cov_PartonShower,m_cor_PartonShower;
	TMatrixT<double> m_totalCovariance_normalized,m_totalCorrelation_normalized,m_cov_FSR_normalized,m_cor_FSR_normalized,m_cov_ME_normalized,m_cor_ME_normalized,m_cov_PartonShower_normalized,m_cor_PartonShower_normalized;
	
	TMatrixT<double> m_SignalModelingPlusPDFCovariance,m_SignalModelingPlusPDFCovariance_normalized;
	TMatrixT<double> m_SignalModelingPlusPDFCovariance_Alternative,m_SignalModelingPlusPDFCovariance_Alternative_normalized;
	
	TFile* m_file_data; 
	TFile* m_file_systematics;
	TH1D* m_hist_data_reco;
	TH1D* m_hist_data_unfolded;
	TH1D* m_hist_data_unfolded_normalized;
	
	vector<TString> m_pdfSystematics_names;
	int m_nsys_pdf;
	bool m_doSysPDF;
	
	vector<Signal_histos*> m_pdfSystematics_histos;
	vector<TMatrixT<double> > m_pdfCovariances,m_pdfCovariancesNormalized;
	TMatrixT<double> m_pdfTotalCovariance,m_pdfTotalCovarianceNormalized;
	vector<TMatrixT<double> > m_pdfCovariancesAlternative,m_pdfCovariancesNormalizedAlternative;
	TMatrixT<double> m_pdfTotalCovarianceAlternative,m_pdfTotalCovarianceNormalizedAlternative;
	
	int m_nPseudoExperiments;
	
	vector<Signal_histos*> m_nominal_replicas;
	vector<Signal_histos*> m_MECOffSample_replicas;
	vector<vector<Signal_histos*> > m_sys_replicas;
	vector<vector<double> > m_mean_of_errors, m_mean_of_errors_normalized;
	vector<vector<double> > m_errors_of_errors, m_errors_of_errors_normalized;
	

	std::unique_ptr<TFile> m_outputTFile;
	
	double m_lumi;
	double m_cross_sec_unit;
	TString m_mc_samples_production;
	
	bool m_isConcatenated;
	vector<int> m_subHistNbins;
	
	bool m_is2D;
	vector<double> m_binning_x,m_binning_x_truth;
	vector<vector<double> > m_binning_y,m_binning_y_truth;
	
	ClassDef(GeneratorSystematicsBase,1)
};




#endif
