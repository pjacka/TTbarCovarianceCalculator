#ifndef SystematicHistos_H
#define SystematicHistos_H

#include "TLorentzVector.h"
#include <vector>


#include <vector>
#include <map>
#include <unordered_map>
#include <string>
#include <sstream>
#include <iostream>
#include <fstream>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <iomanip>      // std::setprecision

#include "TString.h"
#include "TH1D.h"
#include "TH2D.h"
#include "TCanvas.h"
#include "TFile.h"
#include "TAxis.h"
#include "TGaxis.h"
#include "TSystem.h"
#include "TLatex.h"
#include "TLegend.h"
#include "TProfile.h"
#include "TStyle.h"
#include "TROOT.h"
#include "THStack.h"
#include "TGraphAsymmErrors.h"
#include "TMatrixT.h"
#include "TLegend.h"
#include "TPad.h"
#include "TEnv.h"
#include "TRandom.h"
#include "TRandom3.h"

#include "TTbarHelperFunctions/TtbarDiffCrossSection_exception.h"
#include "TTbarHelperFunctions/TTBarConfig.h"
#include "TTbarCovarianceCalculator/OneSysHistos.h"
#include "TTbarCovarianceCalculator/AsymmetricErrorsCalculator.h"
#include "TTbarCovarianceCalculator/PseudoexperimentsCalculator.h"

#include "../RooUnfold/src/RooUnfoldResponse.h"
#include "../RooUnfold/src/RooUnfoldBayes.h"
#include "../RooUnfold/src/RooUnfoldBinByBin.h"
#include "../RooUnfold/src/RooUnfoldSvd.h"
#include "../RooUnfold/src/RooUnfoldInvert.h"
using namespace std;

class SystematicHistos {
  friend class AsymmetricErrorsCalculator;
  friend class PseudoexperimentsCalculator;
  friend class Pseudoexperiment;
  TString m_variable;
  
  TFile* m_fileSystematics;
  const TTBarConfig* m_ttbarConfig;
  
  vector<TString> m_sys_names_all, m_sys_names_single, m_sys_names_PDF,m_sys_names_signalModeling;
  vector<pair<TString,TString> > m_sys_names_pairs;
  
  OneSysHistos* m_nominalHistos;
  OneSysHistos* m_SystHistos;


  vector<pair<OneSysHistos*,OneSysHistos*> > m_sysHistosPaired;
  vector<OneSysHistos*> m_sysHistosSingle,m_sysHistosPDF;
  std::map<TString,OneSysHistos*> m_sysHistosSignalModeling;
  
  vector<pair<TH1D*,TH1D*> > m_shifts_paired;
  vector<pair<TH1D*,TH1D*> > m_shifts_relative_paired;
  
  vector<TH1D*> m_shifts_single,m_shifts_PDF,m_shifts_PDF_alternative;
  std::map<TString,TH1D*> m_shifts_signalModeling,m_shifts_signalModeling_alternative;
  vector<TH1D*> m_shifts_relative_single,m_shifts_relative_PDF,m_shifts_relative_PDF_alternative;
  std::map<TString,TH1D*> m_shifts_relative_signalModeling,m_shifts_relative_signalModeling_alternative;
  
  int m_nsys_all;
  int m_nsys_pairs;
  int m_nsys_single;
  int m_nsys_PDF;
  int m_nsys_signalModeling;
  
  public:
  
  virtual ~SystematicHistos () = default;
  SystematicHistos (const SystematicHistos&) = default;
  SystematicHistos& operator= (const SystematicHistos&) = default;
  SystematicHistos (SystematicHistos&&) = default;
  SystematicHistos& operator= (SystematicHistos&&) = default;
  SystematicHistos() = default;
  
  SystematicHistos(TFile* file,const TString& variable, const TTBarConfig& ttbarConfig);
  SystematicHistos(const vector<SystematicHistos*>& vec);
  void init(TFile* file,const TString& variable, const TTBarConfig& ttbarConfig);
    
  void prepareListOfSystematics();
  void prepareRelativeShifts();
  void prepareRelativeShiftsSignalModeling();
  void printRelativeShifts();
  void write(TFile* f);
  
  void loadHistos();
  
  OneSysHistos* getNominalHistos(){return m_nominalHistos;};
  vector<TString> getListOfSyst(){return m_sys_names_all;};
  vector<pair<TString,TString> >  getListOfSystPairs(){return m_sys_names_pairs;};
  vector<TString> getListOfSystSingle(){return m_sys_names_single;};
  vector<pair<OneSysHistos*,OneSysHistos*> > getSystPaired(){return m_sysHistosPaired;};
  vector<OneSysHistos*>  getSystSingle(){return m_sysHistosSingle;};
	
  ClassDef(SystematicHistos,1)

};

#endif
