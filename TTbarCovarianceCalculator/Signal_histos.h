#ifndef Signal_histos_H
#define Signal_histos_H

#include "TLorentzVector.h"
#include <vector>


#include <vector>
#include <map>
#include <unordered_map>
#include <string>
#include <sstream>
#include <iostream>
#include <fstream>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <iomanip>      // std::setprecision

#include "TString.h"
#include "TH1D.h"
#include "TH2D.h"
#include "TCanvas.h"
#include "TFile.h"
#include "TAxis.h"
#include "TGaxis.h"
#include "TSystem.h"
#include "TLatex.h"
#include "TLegend.h"
#include "TProfile.h"
#include "TStyle.h"
#include "TROOT.h"
#include "THStack.h"
#include "TGraphAsymmErrors.h"
#include "TMatrixT.h"
#include "TLegend.h"
#include "TPad.h"
#include "TEnv.h"
#include "TRandom.h"
#include "TRandom3.h"

#include "TTbarHelperFunctions/TtbarDiffCrossSection_exception.h"


#include "../RooUnfold/src/RooUnfoldResponse.h"
#include "../RooUnfold/src/RooUnfoldBayes.h"
#include "../RooUnfold/src/RooUnfoldBinByBin.h"
#include "../RooUnfold/src/RooUnfoldSvd.h"
#include "../RooUnfold/src/RooUnfoldInvert.h"
using namespace std;

class Signal_histos {
	
	TString m_path_to_histos;
	public:
	TH1D* reco;
	TH1D* truth_cuts_correction;
	TH2D* migration;
	TH1D* reco_cuts_correction;
	TH1D* truth;
	TH1D* multijet;
	RooUnfoldResponse* response;
	TH1D* truth_normalized;
	
	
	virtual ~Signal_histos () = default;
	Signal_histos (const Signal_histos&) = default;
	Signal_histos& operator= (const Signal_histos&) = default;
	Signal_histos (Signal_histos&&) = default;
	Signal_histos& operator= (Signal_histos&&) = default;
	
	Signal_histos(TString path_to_histos="nominal/Leading_1t_1b_Recoil_1t_1b_tight/unfolding/");
	Signal_histos(TString variable,TString Level,TFile* file, TString path_to_histos="nominal/Leading_1t_1b_Recoil_1t_1b_tight/unfolding/");
	Signal_histos(const TString variable,const TString level,const TString foldername,TFile* file, TString path_to_histos="nominal/Leading_1t_1b_Recoil_1t_1b_tight/unfolding/");
	void loadHistos(TString variable,TString Level, TFile* file);
	void loadHistos(const TString variable,const TString level,const TString foldername,TFile* file);
	
	void rebinHistos(int nbinsx,Double_t* binsx,int nbinsy,Double_t* binsy);
	void scaleHistos(double SF);
	void divideByBinWidth();
	void cloneHistos(Signal_histos* cloned,TString name);
	void smearHistos();
	ClassDef(Signal_histos,1)

};

#endif
