#ifndef PseudoexperimentsCalculator_H
#define PseudoexperimentsCalculator_H

#include "TLorentzVector.h"
#include <vector>


#include <vector>
#include <map>
#include <unordered_map>
#include <string>
#include <sstream>
#include <iostream>
#include <fstream>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <iomanip>      // std::setprecision

#include "TString.h"
#include "TH1D.h"
#include "TH2D.h"
#include "TCanvas.h"
#include "TFile.h"
#include "TAxis.h"
#include "TGaxis.h"
#include "TSystem.h"
#include "TLatex.h"
#include "TLegend.h"
#include "TProfile.h"
#include "TStyle.h"
#include "TROOT.h"
#include "THStack.h"
#include "TGraphAsymmErrors.h"
#include "TMatrixT.h"
#include "TLegend.h"
#include "TPad.h"
#include "TEnv.h"
#include "TRandom.h"
#include "TRandom3.h"

#include "TTbarHelperFunctions/TtbarDiffCrossSection_exception.h"
#include "TTbarCovarianceCalculator/OneSysHistos.h"
#include "TTbarCovarianceCalculator/SystematicHistos.h"
#include "TTbarCovarianceCalculator/Pseudoexperiment.h"

#include "BootstrapGenerator/TH1Bootstrap.h"
#include "BootstrapGenerator/TH1DBootstrap.h"
#include "BootstrapGenerator/TH2Bootstrap.h"
#include "BootstrapGenerator/TH2DBootstrap.h"

#include "../RooUnfold/src/RooUnfoldResponse.h"
#include "../RooUnfold/src/RooUnfoldBayes.h"
#include "../RooUnfold/src/RooUnfoldBinByBin.h"
#include "../RooUnfold/src/RooUnfoldSvd.h"
#include "../RooUnfold/src/RooUnfoldInvert.h"
using namespace std;
class SystematicHistos;
class Pseudoexperiment;
class TTBarConfig;

class PseudoexperimentsCalculator{
  friend class Pseudoexperiment;
  
  std::unique_ptr<TTBarConfig> m_ttbarConfig;
  
  int m_debug;
  const TString m_gap;
  TString m_variable,m_level;
  TString m_mainDir;
  double m_lumi;
  double m_lumi_shifted;
  TString m_lumi_string;
  TH1D* m_hist_template; // histogram with correct binning and empty bins  
  TH1D* m_hist_template_truth; // histogram with correct binning and empty bins  
  TEnv *m_config_sys_names;
  SystematicHistos* m_sysHistos;
  TH1D* m_histLumi;
  int m_nbins_reco;
  int m_nbins_truth;
  int m_nsys_all,m_nsys_pairs,m_nsys_single,m_nsys_pdf;
  bool m_useBootstrapHistos;
  int m_pseudoexperimentCounter;
  
  vector<TString> m_sys_chain_names_all, m_sys_chain_names_single, m_sys_chain_names_pdf;
  vector<pair<TString,TString> > m_sys_chain_names_paired;
  
  
  // vector with random relative shifts for pseudo-experiments
  vector<double> m_data_shifts;
  vector<vector<double> > m_data_shifts_16_regions;
  vector<double> m_signal_shifts_onlyStat;
  vector<double> m_bkg_MC_shifts_onlyStat;
  vector<double> m_bkg_All_shifts_onlyStat;
  vector<double> m_pseudodata_shifts_onlyStat;
  
  vector<double> m_data_shifts_onlySys;
  vector<double> m_signal_shifts_onlySys;
  vector<double> m_bkg_MC_shifts_onlySys;
  vector<double> m_bkg_All_shifts_onlySys;
  vector<double> m_pseudodata_shifts_onlySys;
  vector<vector<double> > m_MC_16_regions_shifts_onlySys;
  vector<vector<double> > m_data_16_regions_shifts_onlySys;
  
  vector<vector<double> > m_migration_shifts_onlySys;
  vector<double> m_EffOfRecoLevelCutsNominator_shifts_onlySys;
  vector<double> m_EffOfTruthLevelCutsNominator_shifts_onlySys;
  vector<double> m_signal_truth_shifts_onlySys;
  
  vector<double> m_signalModelingShifts;
  vector<double> m_signalModelingShiftsAlternative;
  
  
  TH1DBootstrap* m_bootstrapData;
  TH1DBootstrap* m_bootstrapMC;
  TH1DBootstrap* m_bootstrap_signal;
  TH1DBootstrap* m_bootstrap_bkg_MC;
  vector<TH1DBootstrap*> m_bootstrapSidebandRegionsData,m_bootstrapSidebandRegionsMC;
  
  public:
  
  virtual ~PseudoexperimentsCalculator () = default;
  PseudoexperimentsCalculator (const PseudoexperimentsCalculator&) = default;
  PseudoexperimentsCalculator& operator= (const PseudoexperimentsCalculator&) = default;
  PseudoexperimentsCalculator (PseudoexperimentsCalculator&&) = default;
  PseudoexperimentsCalculator& operator= (PseudoexperimentsCalculator&&) = default;
  PseudoexperimentsCalculator() = default;
  PseudoexperimentsCalculator(SystematicHistos*,const TString& config_lumi,const TString& config_sysnames,const TString& mainDir,const TString& variable,const TString& level,int debugLevel);
  void init(SystematicHistos* sysHistos,const TString& config_lumi,const TString& config_sysnames,const TString& mainDir,const TString& variable,const TString& level,int debugLevel);
  void loadBootstrapHistos(TFile*f,const TString& variable);
 
 
  void prepareShiftsForOnePseudoExperiment();
  void setShiftsToZero();
  void prepareDataStatShifts();
  void addShiftsForPairedSystematics();
  void addShiftForSingleSystematicBranch(const OneSysHistos* sysHistos, const OneSysHistos* nominalHistos,bool shiftLumi,const double& y);
  void addShiftsForSingleSystematics();
  void addShiftsForSignalModeling();
  void addShiftForSignalModeling(const TString& name);
  void addShiftForSignalModeling(const TString& name_up,const TString& name_down);
  void addShiftForSignalModeling(const double x,TH1D* hshift,TH1D* hshiftAlternative);
  void normalizeShifts();
  void allocateMemoryForPseudoExperiments();
  
  void preparePseudoexperiment(Pseudoexperiment* pseudoExperiment);
  void prepareStatPartOfPseudoexperiment(Pseudoexperiment* pseudoExperiment);
  void prepareSysPartOfPseudoexperiment(Pseudoexperiment* pseudoExperiment);
  void preparePseudodataForPseudoexperiment(Pseudoexperiment* pseudoExperiment);
  void prepareNominalPseudoexperiment(Pseudoexperiment* pseudoExperiment);
  void prepareSignalModelingPartOfPseudoexperiment(Pseudoexperiment* pseudoExperiment);
  
  inline void useBootstrap(bool useBootstrap=true){ m_useBootstrapHistos=useBootstrap;}
  inline int getNBootstrapPseudoexperiments(){return m_useBootstrapHistos ? m_bootstrapData->GetNReplica() : -1;}

  TH1D* Calculate_ABCD16_estimate(const vector<TH1D*>& data,const vector<TH1D*>& MC,bool calculate_errors=false);
  
};



#endif
