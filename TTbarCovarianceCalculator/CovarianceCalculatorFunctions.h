//  Peter Berta, 16.10.2012

#ifndef CovarianceCalculatorFunctions_h
#define CovarianceCalculatorFunctions_h 

#include <vector>
#include <string>
#include <sstream>
#include <iostream>
#include <fstream>
#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <iomanip>      // std::setprecision

#include "TString.h"
#include "TKey.h"
#include "TTree.h"
#include "TDirectory.h"
#include "TCollection.h"
#include "TClass.h"
#include "TLorentzVector.h"
#include "TH1D.h"
#include "TH2D.h"
#include "TLegend.h"
#include "TPad.h"
#include "TCanvas.h"
#include "TLatex.h"
#include "TFile.h"
#include "TMatrixT.h"
#include "THnSparse.h"

#include "BootstrapGenerator/TH1Bootstrap.h"
#include "BootstrapGenerator/TH1DBootstrap.h"
#include "BootstrapGenerator/TH2Bootstrap.h"
#include "BootstrapGenerator/TH2DBootstrap.h"

#include "../RooUnfold/src/RooUnfoldResponse.h"
#include "../RooUnfold/src/RooUnfoldBayes.h"
#include "../RooUnfold/src/RooUnfoldBinByBin.h"
#include "../RooUnfold/src/RooUnfoldSvd.h"
#include "../RooUnfold/src/RooUnfoldInvert.h"

#include "TTbarCovarianceCalculator/OneSysHistos.h"
#include "TTbarCovarianceCalculator/CovarianceCalculator.h"

#include "TTbarHelperFunctions/TtbarDiffCrossSection_exception.h"

using namespace std;

namespace functions{

  TH1D* calculateRelativeShiftsForAbsoluteSpectra(const OneSysHistos& nominal, const OneSysHistos& shifted, const TString& option="detector");
  TH1D* calculateRelativeShiftsForRelativeSpectra(const OneSysHistos& nominal, const OneSysHistos& shifted, const TString& option="detector");

  TH1D* UnfoldBayes(TH1D* h_data_reco,TH1D* h_signal_reco,TH1D* h_signal_truth,TH2D* h_migration,int niter,int debug=0);
  TH1D* UnfoldBayes(TH1D* h_data_reco,TH1D* h_signal_reco,TH1D* h_effOfTruthLevelCutsNominator,TH1D* h_signal_truth,TH1D* h_effOfRecoLevelCutsNominator,RooUnfoldResponse* h_response,int niter,int debug=0);
  TH1D* UnfoldInvert(TH1D* h_data_reco,TH1D* h_signal_reco,TH1D* h_effOfTruthLevelCutsNominator,TH1D* h_signal_truth,TH1D* h_effOfRecoLevelCutsNominator,RooUnfoldResponse* h_response,int debug=0);
  TH1D* UnfoldBayes(TH1D* h_data_reco,TH1D* h_effOfTruthLevelCuts,TH1D* h_effOfRecoLevelCuts,RooUnfoldResponse* h_response,int niter,int debug=0);
  void UnfoldBayes(TH1D* h_result, TH1D* h_data_reco,TH1D* h_signal_reco,TH1D* h_effOfTruthLevelCutsNominator,TH1D* h_signal_truth,TH1D* h_effOfRecoLevelCutsNominator,MyUnfold* rooUnfold);

}
#endif
