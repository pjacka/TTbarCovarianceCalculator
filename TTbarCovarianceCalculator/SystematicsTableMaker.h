#ifndef SystematicsTableMaker_H
#define SystematicsTableMaker_H

#include "TLorentzVector.h"
#include <vector>


#include <vector>
#include <map>
#include <unordered_map>
#include <string>
#include <sstream>
#include <iostream>
#include <fstream>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <iomanip>      // std::setprecision

#include "TString.h"
#include "TH1D.h"
#include "TH2D.h"
#include "TCanvas.h"
#include "TFile.h"
#include "TAxis.h"
#include "TGaxis.h"
#include "TSystem.h"
#include "TLatex.h"
#include "TLegend.h"
#include "TProfile.h"
#include "TStyle.h"
#include "TROOT.h"
#include "THStack.h"
#include "TGraphAsymmErrors.h"
#include "TMatrixT.h"
#include "TLegend.h"
#include "TPad.h"
#include "TEnv.h"
#include "TRandom.h"
#include "TRandom3.h"

#include "TTbarHelperFunctions/TtbarDiffCrossSection_exception.h"
#include "TTbarCovarianceCalculator/Pseudoexperiment.h"


#include "../RooUnfold/src/RooUnfoldResponse.h"
#include "../RooUnfold/src/RooUnfoldBayes.h"
#include "../RooUnfold/src/RooUnfoldBinByBin.h"
#include "../RooUnfold/src/RooUnfoldSvd.h"
#include "../RooUnfold/src/RooUnfoldInvert.h"

using namespace std;
class Pseudoexperiment;

class SystematicsTableMaker {
	friend class Pseudoexperiment;
protected:
	TH1D* m_data;
	TH1D* m_signalRecoNominal;
	TH1D* m_pseudodataNominal;
	TH2D* m_migrationNominal;
	RooUnfoldResponse* m_responseNominal;
	TH1D* m_effOfRecoLevelCutsNominal;
	TH1D* m_effOfTruthLevelCutsNominal;
	TH1D* m_signalTruthNominal;
	
	TH1D* m_signalRecoUp;
	TH1D* m_pseudodataUp;
	TH2D* m_migrationUp;
	RooUnfoldResponse* m_responseUp;
	TH1D* m_effOfRecoLevelCutsUp;
	TH1D* m_effOfTruthLevelCutsUp;
	TH1D* m_signalTruthUp;
	
	
	double m_lumi;
	double m_SFToTtbarCrossSection;
	TString m_level;
	
	
public:
  virtual ~SystematicsTableMaker () = default;
  SystematicsTableMaker (const SystematicsTableMaker&) = default;
  SystematicsTableMaker& operator= (const SystematicsTableMaker&) = default;
  SystematicsTableMaker (SystematicsTableMaker&&) = default;
  SystematicsTableMaker& operator= (SystematicsTableMaker&&) = default;
  SystematicsTableMaker () = default;
  inline void setLumi(double lumi){m_lumi=lumi;}
  ClassDef(SystematicsTableMaker,1)

};





#endif
