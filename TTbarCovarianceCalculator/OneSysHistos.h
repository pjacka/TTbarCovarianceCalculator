#ifndef OneSysHistos_H
#define OneSysHistos_H

#include "TLorentzVector.h"
#include <vector>


#include <vector>
#include <map>
#include <unordered_map>
#include <string>
#include <sstream>
#include <iostream>
#include <fstream>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <iomanip>      // std::setprecision

#include "TString.h"
#include "TH1D.h"
#include "TH2D.h"
#include "TCanvas.h"
#include "TFile.h"
#include "TAxis.h"
#include "TGaxis.h"
#include "TSystem.h"
#include "TLatex.h"
#include "TLegend.h"
#include "TProfile.h"
#include "TStyle.h"
#include "TROOT.h"
#include "THStack.h"
#include "TGraphAsymmErrors.h"
#include "TMatrixT.h"
#include "TLegend.h"
#include "TPad.h"
#include "TEnv.h"
#include "TRandom.h"
#include "TRandom3.h"

#include "TTbarHelperFunctions/TtbarDiffCrossSection_exception.h"


#include "../RooUnfold/src/RooUnfoldResponse.h"
#include "../RooUnfold/src/RooUnfoldBayes.h"
#include "../RooUnfold/src/RooUnfoldBinByBin.h"
#include "../RooUnfold/src/RooUnfoldSvd.h"
#include "../RooUnfold/src/RooUnfoldInvert.h"
using namespace std;

class OneSysHistos {
  TString m_variable;
  public:
  TH1D* data;
  TH1D* signal_reco;
  TH1D* bkg_MC;
  TH1D* bkg_Multijet;
  TH1D* bkg_all;
  TH1D* pseudodata;
  TH2D* migration;
  TH1D* effOfRecoLevelCutsNominator;
  TH1D* effOfTruthLevelCutsNominator;
  TH1D* signal_truth;
  vector<TH1D*> sidebandRegions_data;
  vector<TH1D*> sidebandRegions_MC;
  
  virtual ~OneSysHistos () = default;
  OneSysHistos (const OneSysHistos&) = default;
  OneSysHistos& operator= (const OneSysHistos&) = default;
  OneSysHistos (OneSysHistos&&) = default;
  OneSysHistos& operator= (OneSysHistos&&) = default;
  OneSysHistos() = default;
  
  OneSysHistos(const TString& chain_name,const TString& variable,TFile* file);
  void loadHistos(const TString& chain_name,const TString& variable, TFile* file);
  OneSysHistos(const vector<OneSysHistos*>& vec);
  void write(TFile* f,const TString& chain_name,const TString& variable_name);
  void rebin1Dhist(TH1D* hist,const int& nbins,const vector<double>& binning);
  ClassDef(OneSysHistos,1)
};

#endif
