#ifndef CovarianceCalculator_H
#define CovarianceCalculator_H

#include "TLorentzVector.h"
#include <vector>


#include <vector>
#include <map>
#include <unordered_map>
#include <string>
#include <sstream>
#include <iostream>
#include <fstream>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <iomanip>      // std::setprecision

#include "TString.h"
#include "TH1D.h"
#include "TH2D.h"
#include "TCanvas.h"
#include "TFile.h"
#include "TAxis.h"
#include "TGaxis.h"
#include "TSystem.h"
#include "TLatex.h"
#include "TLegend.h"
#include "TProfile.h"
#include "TStyle.h"
#include "TROOT.h"
#include "THStack.h"
#include "TGraphAsymmErrors.h"
#include "TMatrixT.h"
#include "TLegend.h"
#include "TPad.h"
#include "TEnv.h"
#include "TRandom.h"
#include "TRandom3.h"
#include "TPrincipal.h"

#include "TTbarHelperFunctions/TtbarDiffCrossSection_exception.h"
#include "TTbarCovarianceCalculator/Pseudoexperiment.h"
#include "TTbarCovarianceCalculator/CovarianceFromToys.h"


#include "../RooUnfold/src/RooUnfoldResponse.h"
#include "../RooUnfold/src/RooUnfoldBayes.h"
#include "../RooUnfold/src/RooUnfoldBinByBin.h"
#include "../RooUnfold/src/RooUnfoldSvd.h"
#include "../RooUnfold/src/RooUnfoldInvert.h"

using namespace std;
class Pseudoexperiment;

class MyUnfold : public RooUnfoldBayes {
  public:
  inline void setIsUnfolded(bool unfolded){_unfolded=unfolded;}
  
};

class CovarianceCalculator {
  friend class Pseudoexperiment;
protected:
  const int m_debug;
  const TString m_gap;

  int m_nbins_reco;
  int m_nbins_truth;
  //int m_nbinsy;
  TString m_variable_name;
  bool m_isConcatenated;
  vector<int> m_subHistNbins;
  bool m_signalModelingLoaded;
  
  TH1D* m_centralValues;
  TH1D* m_centralValuesNormalized;
  
  // For absolute cross-section
  std::shared_ptr<TMatrixT<double> > m_covDataBasedStatAndSysAndSignalModeling;
  std::shared_ptr<TMatrixT<double> > m_covDataBasedStatMCStatRecoAndSysAndSignalModeling;
  std::shared_ptr<TMatrixT<double> > m_covDataBasedStatAndSysAlternativeAndSignalModeling;
  std::shared_ptr<TMatrixT<double> > m_covMCBasedStatAndSysAndSignalModeling;
  std::shared_ptr<TMatrixT<double> > m_covMCBasedStatAndSysAlternativeAndSignalModeling;  
  
  // For normalized cross-section
  std::shared_ptr<TMatrixT<double> > m_covDataBasedStatAndSysAndSignalModelingNormalized;
  std::shared_ptr<TMatrixT<double> > m_covDataBasedStatMCStatRecoAndSysAndSignalModelingNormalized;
  std::shared_ptr<TMatrixT<double> > m_covDataBasedStatAndSysAlternativeAndSignalModelingNormalized;
  std::shared_ptr<TMatrixT<double> > m_covMCBasedStatAndSysAndSignalModelingNormalized;
  std::shared_ptr<TMatrixT<double> > m_covMCBasedStatAndSysAlternativeAndSignalModelingNormalized;
  
  //Signal modeling covariances
  map<string,vector<double> > m_signalModelingRelativeShifts;
  map<string,std::shared_ptr<TMatrixT<double> > > m_signalModelingCovariances;
  
  // Class for calculation of covariances
  map<string,std::shared_ptr<CovarianceFromToys> > m_covFromToys;
  
  RooUnfoldResponse* m_nominalResponse;
  RooUnfoldResponse* m_responseOnlyStat;
  RooUnfoldResponse* m_responseStatAndSys;
  
  TH1D* m_histUnfoldedDataBasedOnlyDataStat; 		
  TH1D* m_histUnfoldedDataBasedOnlyMCStat; 		
  TH1D* m_histUnfoldedDataBasedOnlyMCStatReco; 		
  TH1D* m_histUnfoldedDataBasedOnlyStat; 		
  TH1D* m_histUnfoldedDataBasedOnlySys; 		
  TH1D* m_histUnfoldedDataBasedOnlySigMod; 		
  TH1D* m_histUnfoldedDataBasedStatAndSys; 		
  TH1D* m_histUnfoldedDataBasedSysAndSigMod; 		
  TH1D* m_histUnfoldedDataBasedStatMCStatRecoAndSys; 		
  TH1D* m_histUnfoldedDataBasedStatAndSysAlternative;
  TH1D* m_histUnfoldedDataBasedStatSysAndSigMod; 		
  TH1D* m_histUnfoldedDataBasedStatMCStatRecoSysAndSigMod; 		
  TH1D* m_histUnfoldedDataBasedStatSysAndSigModAlternative;
  
  
  TH1D* m_histUnfoldedMCBasedOnlyDataStat; 		
  TH1D* m_histUnfoldedMCBasedOnlyMCStat; 		
  TH1D* m_histUnfoldedMCBasedOnlyMCStatReco; 		
  TH1D* m_histUnfoldedMCBasedOnlyStat; 		
  TH1D* m_histUnfoldedMCBasedOnlySys; 		
  TH1D* m_histUnfoldedMCBasedStatAndSys; 		
  TH1D* m_histUnfoldedMCBasedSysAndSigMod; 		
  TH1D* m_histUnfoldedMCBasedStatAndSysAlternative;
  TH1D* m_histUnfoldedMCBasedStatSysAndSigMod;
  TH1D* m_histUnfoldedMCBasedStatSysAndSigModAlternative;

  
  TH1D* m_histUnfoldedDataBasedOnlyDataStatNormalized; 		
  TH1D* m_histUnfoldedDataBasedOnlyMCStatNormalized; 		
  TH1D* m_histUnfoldedDataBasedOnlyMCStatRecoNormalized; 		
  TH1D* m_histUnfoldedDataBasedOnlyStatNormalized; 		
  TH1D* m_histUnfoldedDataBasedOnlySysNormalized; 		
  TH1D* m_histUnfoldedDataBasedOnlySigModNormalized; 		
  TH1D* m_histUnfoldedDataBasedStatAndSysNormalized; 		
  TH1D* m_histUnfoldedDataBasedSysAndSigModNormalized; 		
  TH1D* m_histUnfoldedDataBasedStatMCStatRecoAndSysNormalized; 		
  TH1D* m_histUnfoldedDataBasedStatAndSysAlternativeNormalized;
  TH1D* m_histUnfoldedDataBasedStatSysAndSigModNormalized; 		
  TH1D* m_histUnfoldedDataBasedStatMCStatRecoSysAndSigModNormalized; 		
  TH1D* m_histUnfoldedDataBasedStatSysAndSigModAlternativeNormalized;
  
  TH1D* m_histUnfoldedMCBasedOnlyDataStatNormalized; 		
  TH1D* m_histUnfoldedMCBasedOnlyMCStatNormalized; 		
  TH1D* m_histUnfoldedMCBasedOnlyMCStatRecoNormalized; 		
  TH1D* m_histUnfoldedMCBasedOnlyStatNormalized; 		
  TH1D* m_histUnfoldedMCBasedOnlySysNormalized; 		
  TH1D* m_histUnfoldedMCBasedStatAndSysNormalized; 		
  TH1D* m_histUnfoldedMCBasedSysAndSigModNormalized; 		
  TH1D* m_histUnfoldedMCBasedStatAndSysAlternativeNormalized;
  TH1D* m_histUnfoldedMCBasedStatSysAndSigModNormalized; 		
  TH1D* m_histUnfoldedMCBasedStatSysAndSigModAlternativeNormalized;
  
  TH1D* m_histUnfoldedDataBasedOnlyMultijetBkgStat;
  TH1D* m_histUnfoldedDataBasedOnlyMultijetBkgStatNormalized;
  TH1D* m_histUnfoldedDataBasedOnlyMCStatWithMultijetFixed;
  TH1D* m_histUnfoldedDataBasedOnlyMCStatWithMultijetFixedNormalized;
  
  TH1D* m_effOfRecoLevelOnlyStat;
  TH1D* m_effOfRecoLevelOnlySys; 
  TH1D* m_effOfRecoLevelStatAndSys;
  
  TH1D* m_effOfTruthLevelOnlyStat;
  TH1D* m_effOfTruthLevelOnlySys; 
  TH1D* m_effOfTruthLevelStatAndSys;
  
  TH1D* m_recoDataBasedOnlyDataStat;
  TH1D* m_recoDataBasedOnlyMCStat;
  TH1D* m_recoDataBasedOnlyMCStatReco;
  TH1D* m_recoDataBasedOnlyStat;
  TH1D* m_recoDataBasedOnlySys;
  TH1D* m_recoDataBasedStatAndSys;
  TH1D* m_recoDataBasedStatMCStatAndSys;
  TH1D* m_recoDataBasedStatAndSysAlternative;
  
  TH1D* m_recoDataBasedOnlyMultijetBkgStat;
  TH1D* m_recoDataBasedOnlyMCStatWithMultijetFixed;
  
  TH1D* m_recoMCBasedOnlyDataStat;
  TH1D* m_recoMCBasedOnlyMCStat;
  TH1D* m_recoMCBasedOnlyMCStatReco;
  TH1D* m_recoMCBasedOnlyStat; 
  TH1D* m_recoMCBasedOnlySys;
  TH1D* m_recoMCBasedStatAndSys;
  TH1D* m_recoMCBasedStatAndSysAlternative;
  
  
  MyUnfold* m_rooUnfoldNominal;
  MyUnfold* m_rooUnfoldStat;
  MyUnfold* m_rooUnfoldStatAndSys;
  
  Pseudoexperiment* m_pseudoexperiment;
  Pseudoexperiment* m_nominal;
  
  double m_lumi,m_lumi_shifted;
  double m_SFToTtbarCrossSection;
  TString m_level;
	
	
public:
  // Constructors
  CovarianceCalculator(Pseudoexperiment* pseudoexperiment,const TString& level,const int& debugLevel);
  virtual ~CovarianceCalculator () = default;
  CovarianceCalculator (const CovarianceCalculator&) = default;
  CovarianceCalculator& operator= (const CovarianceCalculator&) = default;
  CovarianceCalculator (CovarianceCalculator&&) = default;
  CovarianceCalculator& operator= (CovarianceCalculator&&) = default;
  
  // Functions used in initialization phase
  void setupUnfolder(MyUnfold* rooUnfold);
  void calculateCentralValues(Pseudoexperiment* nominal);
  void setNominalResponse(Pseudoexperiment* nominal);
  inline void setLevel(const TString& level){m_level=level;}
  inline void setVariableName(const TString& name){m_variable_name=name;}
  inline void setConcatenated(bool isConcatenated=true){m_isConcatenated=isConcatenated;}
  void loadSignalModelingCovariances(TFile* f);
  template<typename T> void setSubHistNbins(const vector<T>& nbins){const int size=nbins.size();m_subHistNbins.resize(size);for(int i=0;i<size;i++)m_subHistNbins[i]=nbins[i];}

  // Functions used per pseudoexperiment
  void addPseudoexperiment(Pseudoexperiment* pseudoexperiment,Pseudoexperiment* nominal);
  void scaleAbsoluteDistributions();
  void normalizeDistributions();
  void makeMCBasedResultsProportionalToData();
  void fillPseudoexperimentIntoCovariances();
  void loadRecoLevelPseudoexperiment();
  void prepareUnfolding();
  void unfoldPseudoexperiment();
  void applyPostUnfoldingShifts();
  void applyPostUnfoldingShifts(TH1D* h,const vector<double>& shifts);
  void prepareNormalizedDistributions();
  void prepareSpecialPseudoexperiments();
  inline void setLumi(double lumi){m_lumi=lumi;}
  
  // Functions used in final phase
  void finalize();
  void printMeanValues();
  void printCovariances();
  void printLatexTablesWithFinalResults(const TString& path);
  void printTableOfUncertainties(ofstream& texfile,const vector<std::shared_ptr<TMatrixT<double> > >& covariances,const TH1D* centralValues,const TString& diffxsec);
  void printTableWithDiffCrossSection(ofstream& texfile,TH1D* centralValues,TMatrixT<double>& covStat,TMatrixT<double>& covAll,const TString& diffxsec);
  void writeCovariances(TFile* f);
  void writeCentralValues(TFile* f);
  

  ClassDef(CovarianceCalculator,1)
};



#endif
