#ifndef ClosureTestMaker_H
#define ClosureTestMaker_H

#include <memory>
#include "TTbarCovarianceCalculator/CovarianceCalculatorFunctions.h"
#include "TH1D.h"
#include "TH2D.h"

class MyUnfold;
class RooUnfoldResponse;
class CovarianceFromToys;

class ClosureTestMaker {
  private:
  
  // Sample is splitted into two parts of the same size
  // reco and truth spectra and migration matrices are required in both samples
  // 1st part is used to determine reco and truth level spectra. Migration is used for coherent smearing only.
  // 2nd part is used to determine unfolding corrections used in the test
  std::shared_ptr<TH1D> m_reco1;
  std::shared_ptr<TH2D> m_migration1;
  std::shared_ptr<TH1D> m_truth1;
  std::shared_ptr<TH1D> m_reco2;
  std::shared_ptr<TH1D> m_accNumerator2;
  std::shared_ptr<TH2D> m_migration2;
  std::shared_ptr<TH1D> m_effNumerator2;
  std::shared_ptr<TH1D> m_truth2;
  
  std::unique_ptr<TH1D> m_reco1Shifted;
  std::unique_ptr<TH1D> m_truth1Shifted;
  std::unique_ptr<TH2D> m_migration1Shifted;
  std::unique_ptr<TH1D> m_reco2Shifted;
  std::unique_ptr<TH1D> m_accNumerator2Shifted;
  std::unique_ptr<TH1D> m_truth2Shifted;
  std::unique_ptr<TH1D> m_effNumerator2Shifted;
  std::unique_ptr<TH2D> m_migration2Shifted;
  
  std::unique_ptr<TH1D> m_unfolded;
  std::unique_ptr<TH1D> m_unfoldedShifted;
  
  std::unique_ptr<RooUnfoldResponse> m_response2;
  std::unique_ptr<MyUnfold> m_rooUnfold2;
  
  std::unique_ptr<TH1D> m_difference;
  std::unique_ptr<TH1D> m_differenceShifted;
  
  std::unique_ptr<CovarianceFromToys> m_cov;
  std::shared_ptr<TMatrixT<double>> m_covariance;
  
  int m_regParam;
  int m_nToys;
  
  int m_debug;
  
  double m_chi2;
  double m_pValue;
  
  public:
  
  ClosureTestMaker();
  virtual ~ClosureTestMaker () = default;
  ClosureTestMaker (const ClosureTestMaker&) = default;
  ClosureTestMaker& operator= (const ClosureTestMaker&) = default;
  ClosureTestMaker (ClosureTestMaker&&) = default;
  ClosureTestMaker& operator= (ClosureTestMaker&&) = default;
  
  void initializeShiftedHistos();
  void initRooUnfold(const TH2D* migration);
  void makeCentralValues();
  
  void runPseudoExperiments();
  void preparePseudoExperiment();
  void unfoldPseudoexperiment();
  void addPseudoexperiment();
  void finalizePseudoexperiments();
  void chi2Test();
  
  inline void setReco1(std::shared_ptr<TH1D> reco) {m_reco1=reco;}
  inline void setTruth1(std::shared_ptr<TH1D> truth) {m_truth1=truth;}
  inline void setMigration1(std::shared_ptr<TH2D> migration) {m_migration1=migration;}
  inline void setReco2(std::shared_ptr<TH1D> reco) {m_reco2=reco;}
  inline void setTruth2(std::shared_ptr<TH1D> truth) {m_truth2=truth;}
  inline void setMigration2(std::shared_ptr<TH2D> migration) {
    m_migration2=migration;
    m_effNumerator2 = std::shared_ptr<TH1D>{(TH1D*)m_migration2->ProjectionY("accNumerator2",1,m_migration2->GetNbinsX())};
    m_accNumerator2 = std::shared_ptr<TH1D>{(TH1D*)m_migration2->ProjectionX("effNumerator2",1,m_migration2->GetNbinsY())};
  }
  
  void setNToys(int ntoys) {m_nToys=ntoys;}
  void setDebugLevel(int debug) {m_debug=debug;}
  
  inline double getPValue() const {return m_pValue;} 
  inline double getChi2() const {return m_chi2;}
  inline double getNDF() const {return m_truth1->GetXaxis()->GetNbins();}
  inline const TMatrixT<double>* getCovariance() const {return m_covariance.get();}
  inline const TH1D* getTruth1() const {return m_truth1.get();}
  inline const TH1D* getUnfolded() const {return m_unfolded.get();}
  
  ClassDef(ClosureTestMaker,1)
  
};

#endif
